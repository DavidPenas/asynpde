#include <structure_paralleltestbed.h>

double calctimeMPI(void*,double);

void DE_correction_bounds(double *,int,double*,double*);

#ifdef MPI2
int create_topology(void *, topology_data *, int);
int destroy_topology( topology_data *);
#endif

void chargecooperativeparameters_(void *, int *, int *, int *,double *, long *, int *);

void chargecooperativeparametersfortran_(void *, int *, int *, int *, double *, long * );

    
void charge_island_size(experiment_total *, int *, int *, int *, int );

int createtopology_(void *);


void chargeid_(void *, int *);


double initmpi_();

double calctimempi_(void *, double *);

int returnmigrationsize_(void *);