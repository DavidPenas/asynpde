#include <structure_paralleltestbed.h>

void localsolverscattersearchinterface_(void *, void *(*fitnessfunction)(double*, void*),
        int *, int *, double *, double *, int *) ;

void initlocalsolvervars_(void *);

void call_to_local_solver_N2SOL(experiment_total *, local_solver *, output_struct *,
        double *, double *, void*(*fitnessfunction)(double*, void*),
        int , int , int , int , int *, void *, int *, int *);

void localsolverinterface_(void * , void* , void *, void *(*fitnessfunction)(double*, void*),
        double *, int *, double *, int *, int *, double *, int *, int *, int *) ;


void localsolverinterfacefortran_(void *, void*, void *,void*(*fitnessfunction)(double*, void*),
        double *, int*, double *, int *, int*, double *, int*, int*);


void addlocalscounter_(void* );


void addlocalscounterfortran_(void* );
