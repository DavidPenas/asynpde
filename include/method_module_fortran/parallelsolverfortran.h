#include <bbobStructures.h>
#include <structure_paralleltestbed.h>
#include <AMIGO_problem.h>
#ifdef MPI2
int  __parallelrandomsearchmpi_MOD_prandomsearchmpi(void *, void *, void *, double(*fitnessfunction)(double*,void*),
        void *, double *, double *, int *);

int  __parallelrandomsearchopenmp_MOD_prandomsearchopenmp(void *, void *, void *, double(*fitnessfunction)(double*,void*),
        void *, double *, double *, int *, int *);

#endif

#ifdef OPENMP
int  __parscattersearch_MOD_parsscattersearch(void *, void *, void  *,   void*(*fitnessfunction)(double*,void*), void *, long*, double*);
#endif
