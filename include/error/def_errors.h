#define error1 "\n*\tERROR[1]: Unknown option in XML for selection algorithm."
#define error2 "\n*\tERROR[2]: Unknown option in XML for replace algorithm."
#define error3 "\n*\tERROR[3]: Unknown option in XML for mutation strategy."
#define error4 "\n*\tERROR[4]: Unknown option in XML for parallel topology."
#define error5 "\n*\tERROR[5]: Unknown option in XML for parallel parametters strategy."
#define error6 "\n*\tERROR[6]: Processor population matrix has an inadequate size. Remember: Proc matrix population = (NP / NPROC)."
#define error7 "\n*\tERROR[7]: The program is implemented with more than one processor, but the program is configured for sequential execution."
#define error8 "\n*\tERROR[8]: There was an error in the execution of the solver."
#define error9 "\n*\tERROR[9]: Unknown option in XML for parallelization kind."
#define error10 "\n*\tERROR[10]: Unknown benchmark in XML."