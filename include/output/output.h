#include <structure_paralleltestbed.h>


void updateresultsandprint_(void *, void *, void *, void *, double *, long *, int *D, double *, double *, int *, int * );
        
int savehdf5solutions_(double *, int *, int *);

int  file_results(experiment_total *, result_values *, FILE *);

int  print_chron_results(experiment_total *, result_values **, const char *);

void initprintfile_(void *, void *, double *, int *, int *, double*, long*);

void print_verbose_local_success(experiment_total, int );

void inittimefortran_(void *, void *, double *);

void print_end_iteration_(void *, void *, double , double ,int );


void verboseiterationfortran_(void *, int *, double *, double *, double *, double *, int *, int *);

void verbose_iteration_(void *, int, double, double, double, double, int, int);


void matlab_plot_file(experiment_total ,  char *,  char *, const char *, int ,  const char *, int , int , int , int , int  );

void plot_file(experiment_total, int, int,int);

void update_results_and_print_(void *, void *, void *, void *, double , double ,  double *, int , int, double *, int, int , int, topology_data * );

int updateresultsrandomsearch_(void *, void *, double *, double *, double * );

void printiteration_(void *, void *, void *, int *, double *, long *, double *);

void printenditeration_(void *, void *, double *, double *, long *);

void print_end_file_(experiment_total *, double *, double *, result_solver *,int *, output_struct *, local_solver *, int *);

void init_print_file_(void *, void *, double , int , int );



void verboseiteration_(void *,      int ,   double ,          double ,        double ,      long  , int ) ;

void printverboselocaloutput_(void *, int *, double *, double *, void *,  int *);

void printverboselocaloutput2_(void *, void *, double *, int *);

void improvelocalsolver_(void *, void *, double *, double * );