#include<AMIGO_problem.h>

//static int c__2 = 2;

int dn2fb_(int *, int *, double *, double *, 
           int *, int *, int *, double *, 
           int *, 
           double *, void* , int *, void *, int *);

int dn2gb_(int *, int *, double *, double *,
           int *, int *, int *, double *,
           int *, double *, void* , int *, void *, int *);

int calcr_(int *, int *, double *, int *, double *, int *, double *, void*, int *, void *, int *);

int calcj_(int *, int*, double *, int *, double *, int *, double *, void *, int *, void *, int *);

int divset_(int *, int *, int *, int *, double *);

int NL2SOL_AMIGO_pe(AMIGO_problem* , int , int* , void *, int *, long *);

int sres_AMIGO_pe(AMIGO_problem* amigo_problem);
