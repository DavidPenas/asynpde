# asynPDE #

## MPI asynchronous Parallel Differential Evolution  ##

This is version 0.1 of the implementation of an island-based MPI asynchronous enhanced Differential Evolution algorithm. This code incorporates several key new mechanisms: 

1. Asynchronous cooperation between parallel processes based on the island-model.
2. Three enhanced strategies: the use of logarithmic space, a local solver and a tabu list, to improve the search.

The asynPDE code has been implemented using Fortran 90 and C. MPI has been used in the parallel implementation. It has been tested in Linux clusters running CentOS 6.7 and Debian 8.


### Main reference: ###

D.R. Penas, J.R. Banga, P. Gonzalez and R. Doallo (2015) Enhanced parallel Differential Evolution algorithm for problems incomputational systems biology. Applied Soft Computing 33, pp 86-99

###Contact

This work is the result of a collaboration between the Computer Architecture Group ([GAC](http://gac.udc.es/english/))
at Universidade da Coruña ([UDC](http://www.udc.gal/index.html?language=en)) and the (Bio)Processing Engineering Group 
([GIngProg](http://gingproc.iim.csic.es)) at [IIM-CSIC](http://www.iim.csic.es).

* Contacts at GAC

	Patricia Gonzalez <[patricia.gonzalez@udc.es](mailto:patricia.gonzalez@udc.es)>
   
* Contact at IIM-CSIC

	David R. Penas <[davidrodpenas@iim.csic.es](mailto:davidrodpenas@iim.csic.es)>

	Julio R. Banga <[julio@iim.csic.es](mailto:julio@iim.csic.es)>