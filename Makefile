PWD=/home/patricia/asynPDE

AMIGO_LIBS=$(PWD)/src/libAMIGO/lib_linux64
SRC_FORTRAN_NL2SOL=$(wildcard $(PWD)/src/libAMIGO/src_nl2sol/*.f)
USENL2SOL+=$(wildcard $(PWD)/src/libAMIGO/src_nl2sol/*.o)

SRC+=$(wildcard $(PWD)/src/input_module/*.c)
SRC+=$(wildcard $(PWD)/src/output/*.c)
SRC+=$(wildcard $(PWD)/src/structure_data/*.c)

SRC+=$(wildcard $(PWD)/src/method_module/*.c)
SRC+=$(wildcard $(PWD)/src/method_module/DE/*.c)
SRC+=$(wildcard $(PWD)/src/method_module/eSS/*.c)
SRC+=$(wildcard $(PWD)/src/bbob/*.c)
SRC+=$(wildcard $(PWD)/src/SB/*.c)
SRCCCP+=$(wildcard $(PWD)/src/lsgo/*.cpp)
SRC+=$(wildcard $(PWD)/src/*.c)
SRC_FORTRAN+=$(wildcard $(PWD)/src/method_module_fortran/eSS/scattersearchtypes.f90)
SRC_FORTRAN+=$(wildcard $(PWD)/src/method_module_fortran/eSS/common_functions.f90)
SRC_FORTRAN+=$(wildcard $(PWD)/src/method_module_fortran/eSS/funcevalinterface.f90)
SRC_FORTRAN+=$(wildcard $(PWD)/src/method_module_fortran/eSS/qsort_mod.f90)
SRC_FORTRAN+=$(wildcard $(PWD)/src/method_module_fortran/eSS/outputhdf5.f90)
SRC_FORTRAN+=$(wildcard $(PWD)/src/method_module_fortran/eSS/dhc.f90)
SRC_FORTRAN+=$(wildcard $(PWD)/src/method_module_fortran/eSS/misqp_interface.f90)
SRC_FORTRAN+=$(wildcard $(PWD)/src/method_module_fortran/eSS/localsolver.f90)
SRC_FORTRAN+=$(wildcard $(PWD)/src/method_module_fortran/eSS/scattersearchfunctions.f90)
SRC_FORTRAN+=$(wildcard $(PWD)/src/method_module_fortran/eSS/parallelscattersearchfunctions.f90)
SRC_FORTRAN+=$(wildcard $(PWD)/src/method_module_fortran/eSS/scattersearch.f90)
SRC_FORTRAN+=$(wildcard $(PWD)/src/method_module_fortran/eSS/parallelscattersearch.f90)
SRC_FORTRAN+=$(wildcard $(PWD)/src/method_module_fortran/eSS/cess.f90)
AMIGO+=$(wildcard $(PWD)/src/libAMIGO/src_cvodes/*.c)
AMIGO+=$(wildcard $(PWD)/src/libAMIGO/src_amigo/*.c)
AMIGO+=$(wildcard $(PWD)/src/libAMIGO/src_de/*.c)
AMIGO+=$(wildcard $(PWD)/src/libAMIGO/src_fullC/*.c)
AMIGO+=$(wildcard $(PWD)/src/libAMIGO/src_julia/*.c)
AMIGO+=$(wildcard $(PWD)/src/libAMIGO/src_SRES/*.c)

INC+=-I$(PWD)/lib/gsl-1-14/include/
INC+=-I$(PWD)/include/
INC+=-I$(PWD)/include/input/
INC+=-I$(PWD)/include/libAMIGO/include_cvodes/
#INC+=-I$(PWD)/include/libAMIGO/include_cvodes/cvcodes/
#INC+=-I$(PWD)/include/libAMIGO/include_cvodes/nvector/
#INC+=-I$(PWD)/include/libAMIGO/include_cvodes/sundials/
INC+=-I$(PWD)/include/libAMIGO/include_amigo/
#INC+=-I$(PWD)/include/libAMIGO/include_clapack/
INC+=-I$(PWD)/include/libAMIGO/include_de/
INC+=-I$(PWD)/include/libAMIGO/include_SRES/
INC+=-I$(PWD)/include/method_module/
INC+=-I$(PWD)/include/method_module/eSS/
INC+=-I$(PWD)/include/method_module/DE/
INC+=-I$(PWD)/include/lsgo/
INC+=-I$(PWD)/include/bbob/
INC+=-I$(PWD)/include/SB/
INC+=-I$(PWD)/include/structure_data/
INC+=-I$(PWD)/include/output/
INC+=-I$(PWD)/include/error/
INC+=-I$(PWD)/lib/libxml2-2.9.1/include/
INC+=-I$(PWD)/include/method_module_fortran/
INC+=-I$(MATLAB_PATH)/extern/include
INC_FORTRAN+=-I$(PWD)/include/
INC_FORTRAN+=-I$(PWD)/include/method_module_fortran/

#INC_FORTRAN+=-I/usr/include/mpif.h
#INC_FORTRAN+=-I/usr/include/mpi.h
#INC_FORTRAN+=-I/usr/local/include/mpif.h


#LIBS+= -L$(PWD)/lib/hdf5/lib -I$(PWD)/lib/hdf5/lib  -I$(PWD)/lib/hdf5/include -L$(PWD)/lib/hdf5-1.8.12/fortran/src -L$(PATH_MPI)/lib  
#LIBS+= -L$(PWD)/lib/libxml2/lib -I$(PWD)/lib/libxml2/lib  -I$(PWD)/lib/libxml2/include/libxml2/libxml

LIBS+=-ldl -lz
LIBS+=-L$(PWD)/lib/gsl-1.14/lib -I$(PWD)/lib/gsl-1.14/include
LIBS+=-L$(PWD)/lib/hdf5-1.8.12/lib -I$(PWD)/lib/hdf5-1.8.12/include -L$(PATH_MPI)/lib
LIBS+=-L$(PWD)/lib -I$(PWD)/lib/libxml2/include
STATIC_LIBS=  -lhdf5  -lstdc++  -lgsl  -lgslcblas  -lpthread -lrt  -lgfortran  -lxml2 -cpp
STATIC_LIBS+=-L$(PWD)/lib/hdf5/lib 
CFLAGS += -lm   -MMD $(INC) -O3  $(LIBS) $(STATIC_LIBS)  -lAMIGO -fPIC -DEXPORT -Wl,-rpath,$(MATLAB_LIB)
CXXFLAGS +=  -D__STDC_LIMIT_MACROS -MMD  $(INC) -O3  $(LIBS) $(STATIC_LIBS)

CLIBS =$(LIBS) -lm 
CLIBS+=-L$(AMIGO_LIBS)

PROG := dist/paralleltestbed
	
FLAGS_HDF5= -L$(PWD)/lib/hdf5-1.8.12/lib -I$(PWD)/lib/hdf5-1.8.12/include

# SERIAL :
#CC := gcc   -O3  $(LIBS) -lm  
#FC := gfortran  -O3 $(LIBS) 
#CP := g++ -fpermissive   -O3  $(LIBS) -lm


# PARALLEL MPI :  
CC:= mpicc -O3  -DMPI2 -DOPENMP -fopenmp -lmpi $(LIBS) 
FC :=  mpif90    -O3 -DMPI2 -DOPENMP -fopenmp -lmpi_f90   $(LIBS) 
CXX := mpic++ -fpermissive -O3   -DOPENMP -fopenmp -lmpi $(LIBS)
SRC_FORTRAN += $(SRC_FORTRAN_MPI)
SRC_FORTRAN += $(SRC_FORTRAN_OPENMP)	

# PARALLEL OPENMP	
#CC:=gcc  -O3 -DOPENMP -fopenmp $(LIBS) -lm 
#FC := gfortran  -O3  -DOPENMP -fopenmp $(LIBS) 
#SRC_FORTRAN += $(SRC_FORTRAN_OPENMP)

FFLAGS+=  -I/usr/local/lib -L/usr/local/lib -L/usr/lib64  -cpp $(INC_FORTRAN)
	
OBJFILES := $(SRC:.c=.o)
DEPFILES := $(SRC:.c=.d)

OBJFORTRANFILES := $(SRC_FORTRAN:.f90=.o)
DEPFORTRANFILES := $(SRC_FORTRAN:.f90=.d)

OBJCPPFILES := $(SRCCCP:.cpp=.o)
DEPCPPFILES := $(SRCCCP:.cpp=.d)
	
OBJFORTRANNL2SOL := $(SRC_FORTRAN_NL2SOL:.f=.o)
DEPFORTRANNL2SOL := $(SRC_FORTRAN_NL2SOL:.f=.d)	
	
OBJAMIGO := $(AMIGO:.c=.o)
DEPAMIGO := $(AMIGO:.c=.d)	

all: libAMIGO.a  $(OBJFORTRANFILES) $(OBJCPPFILES) $(PROG) 

%.o: %.c
	$(CC)  $(CFLAGS) $(CLIBS) -c $< -o $@

%AMIGO_pe.o: %AMIGO_pe.c
	$(CC) $(CFLAGS) $(CLIBS) $(USENL2SOL) -c $< -o $@

libAMIGO.a :  $(OBJAMIGO)
	ar r $(AMIGO_LIBS)/libAMIGO.a $^

%.o: %.cpp
	$(CXX)  $(CXXFLAGS) -c $< -o $@ -lm

%.o: %.f90
	$(FC) $(FFLAGS) -c $< -o $@
	mv $(PWD)/*.mod $(PWD)/src/method_module_fortran/eSS

$(PROG) : $(OBJFILES)
	$(LINK.o) -o $(PROG) $(LIBS)  $(USENL2SOL) $(OBJCPPFILES) $(OBJFORTRANFILES) $^ $(CLIBS) $(CFLAGS) $(CXXFLAGS) $(FFLAGS)


clean :
	rm -f $(PROG) $(OBJFILES) $(OBJFORTRANFILES) $(OBJCPPFILES) $(PWD)/src/method_module_fortran/eSS/*.mod $(OBJAMIGO) $(DEPFILES) $(AMIGO_LIBS)/libAMIGO.a
-include $(DEPFILES)
