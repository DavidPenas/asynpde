#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <structure_paralleltestbed.h>
#include <execution_methods.h>
#include <configuration.h>
#include <read_a_file_line_by_line.h>
#include <common_solver_operations.h>
#include <hdf5.h>
#include <benchmark_functions_SystemBiology.h>
#ifdef MPI2
        #include <mpi.h>
#endif


int savehdf5solutions_(double *conxunto, int *D, int *NP) {
    hid_t file_id, dataset_id, dataspace_id;
    herr_t status;
    const char *FILE;
    hsize_t dimension[2];
    
    dimension[0] = *D;
    dimension[1] = *NP;
    
    FILE = "nfkb.hdf5";
    
    file_id = H5Fcreate(FILE, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
    dataspace_id = H5Screate_simple(2,dimension,NULL);
    dataset_id = H5Dcreate2(file_id, "/dset", H5T_NATIVE_DOUBLE, dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    status = H5Dwrite(dataset_id, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, conxunto);
    
    status = H5Dclose(dataset_id); 
    status = H5Sclose(dataspace_id); 
    status = H5Fclose(file_id); 
    
    return 1;
}


/*int file_results(experiment_total *exp, result_values *vector_result, FILE *fp) {
    int i, maxfun, initfun, cont, noise_funcion;

    noise_funcion = is_noise(*exp);
    maxfun = (*exp).test.bench.max_bench;
    initfun = (*exp).test.bench.min_bench;

    check_fun(*exp, &maxfun, &initfun, noise_funcion);

    //if (strcmp((*exp).test.bench.type, "noiseBBOB") == 0 || strcmp((*exp).test.bench.type, "noiselessBBOB") == 0) {
    fprintf(fp, "Execution details :\n");
    fprintf(fp, "\tXML file : %s\n", (*exp).test.namexml);
    fprintf(fp, "\texperiment type : %s\n", (*exp).test.type);
    if (is_parallel(*exp)) {
        fprintf(fp, "\tparallel execution --> number of processors = %d\n", (*exp).par_st->NPROC);
        if (is_asynchronous(*exp)) fprintf(fp, "\tAsynchronous communication\n");
        else fprintf(fp, "\tSynchronous communication\n");
        fprintf(fp, "\t%s\n", (*exp).par_st->type);
    } else fprintf(fp, "\tSequential execution\n");

    fprintf(fp, "\tsolver %s\n", (*exp).methodDE->name);
    fprintf(fp, "\tmutation %s\n", (*exp).methodDE->mutation_strategy);
    fprintf(fp, "\n\n");
    fprintf(fp, "TABLE - MAXIMUM EVALS %.2e  - STOP IN => f(x) <= OPTIMAL+%.2e\n", (*exp).test.max_eval, (*exp).test.tolerance);
    fprintf(fp, "-----------------------------------------------------------------------------------------\n");
    fprintf(fp, "Name\tDIM\tHITS%\t\tbest(fbest-ftarget)\tavg(fbest-ftarget)\t#evals\ttime\t\trestarts\n");

    cont = 0;

    for (i = initfun; i <= maxfun; i++) {

        fprintf(fp, "f-%d\t%d\t%.2f%\t\t%.4e\t\t%.4e\t\t%.0lf\t%.6lf\t%.2lf\n",
                vector_result[cont].id,
                vector_result[cont].dim,
                vector_result[cont].porcenthits,
                vector_result[cont].best_value_all,
                vector_result[cont].value_avg,
                vector_result[cont].eval_avg,
                vector_result[cont].time_avg,
                vector_result[cont].restart);
        cont++;
    }

    //}
    return 1;
}*/


void initprintfile_(void *exp1_, void *output_, double *best, int *par, int *idp, double *currenttime, long *iterations) {
    experiment_total *exp1;
    output_struct *output;
    char *cadea;
            
    output = (output_struct *) output_;
    exp1 = (experiment_total *) exp1_;
    cadea = (char *) malloc(200* sizeof (char));
    
    (*output).st3 = 0.0;


    output->point1 = *currenttime;        

    
    if (exp1[0].test.ouput == 1) {
        output->oldbest = *best;
        FILE *p3;
        if (*par == 0) {
            p3 = fopen( (const char *) exp1[0].test.output_graph, "a");
        } else {
            
            
            if (exp1[0].execution.initpath == 1) {
                exp1[0].execution.initpath = 0;
                sprintf(cadea, "_NPROC_%d_PROC%d", exp1[0].par_st->NPROC, *idp);
                strcat((char *) exp1[0].test.output_graph, (const char *)cadea);
            }
            p3 = fopen(exp1[0].test.output_graph, "a");
            
        }

        if (p3 != NULL) {
            fprintf(p3, "Time  Feval   Number_evals   Total_local_search  local_search_per_evalration  Number_Fail_per_evalration  Number_Success_per_evalration\n");
            fprintf(p3, "%.20lf    %.20lf  %ld\n", *currenttime, *best, *iterations);
        }

        fclose(p3);
        p3 = NULL;
    }
    
    free(cadea);
    cadea = NULL;
}


void printiteration_(void *exp1_, void *output_, void *local_s_,
        int *k, double *best, long *evaluation_local, double *currenttime) {

   
    experiment_total *exp1;
    output_struct *output;
    //local_solver *local_s;
    FILE *p3;
        
        
    exp1 = (experiment_total *) exp1_;
   // local_s = (local_solver *) local_s_;
    output = (output_struct *) output_;
    
    if (exp1[0].test.ouput == 1) {

        p3 = fopen((char *) exp1[0].test.output_graph, "a");
        if (p3 == NULL) exit(0);
   
        if (*best < output->oldbest ) {
            fprintf(p3, "%.20lf   %.20lf  %ld \n", *currenttime, *best, *evaluation_local);
            output->oldbest = *best;
        }
        
        fclose(p3);
        p3 = NULL;
    }


}

void printverboselocaloutput_(void *exp1_, int *D, double *U, double *fval, void *output_,int *idp) {
    experiment_total *exp1;
    output_struct *output;
    
    exp1 = (experiment_total *) exp1_;
    output = ( output_struct *) output_;
    
    if (exp1->test.verbose != 0) {
        printf("\t%d - Chamando a solver local -- solution a entrar %.10lf...\n", *idp, *fval);


#ifdef MPI2
            (*output).st1 = MPI_Wtime();
#else
            (*output).st1 = clock();            
#endif

    }
}


void printverboselocaloutput2_(void *exp1_, void *output_, double *fval, int *idp) {
    double time_local_solver;
    experiment_total *exp1;
    output_struct *output;
    
    exp1 = (experiment_total *) exp1_;
    output = ( output_struct *) output_;
    
    time_local_solver = 0.0;
    
    if (exp1->test.verbose != 0) {
#ifdef MPI2
            (*output).st2 = MPI_Wtime();
            time_local_solver = (double) ((*output).st2 - (*output).st1);
#else
            (*output).st2 = clock();
            time_local_solver = ((double) ((*output).st2 - (*output).st1) / (double) CLOCKS_PER_SEC);        
#endif

        (*output).st3 = (*output).st3 + time_local_solver;

        printf("\t%d - Salindo a solver local -- solution de salida %.20lf. Tempo gasto en solver local = %lf\n",
                *idp, *fval, time_local_solver);
    }
}

void improvelocalsolver_(void *exp1_, void *local_s_, double *fval, double *fvalold ) {
    local_solver *local_s;
//    experiment_total *exp1;
    
    local_s = (local_solver *) local_s_;
 //   exp1 = (experiment_total *) exp1_;   
    
        if ( *fval < *fvalold) {
            (*local_s).state_ls = 1;
            (*local_s).total_local =(*local_s).total_local  +1;
            (*local_s).sucess_local=(*local_s).sucess_local +1;
            (*local_s).num_ls++;
            (*local_s).sucess_interval++;

        } else {
            (*local_s).state_ls = 2;
            (*local_s).num_ls++;
            (*local_s).total_local++;
        }
}

void print_verbose_local_success(experiment_total exp1, int success) {
    if (exp1.test.verbose != 0) {
        if (success == 1) {
            printf(" Exito.\n");
        } else {
            printf(" falla\n");
        }
    }
}


void printenditeration_(void *exp1_, void *output_, double *best, double *currenttime, long *nfuneval) {
    experiment_total *exp1;
    output_struct *output;
    exp1 = (experiment_total *) exp1_;
    output = (output_struct *) output_;
    if (exp1->test.ouput == 1) {
        FILE *p3;

        p3 = fopen(exp1[0].test.output_graph, "a");
        if (p3 == NULL) exit(0);

        
        fprintf(p3, "%.20lf   %.20lf  %ld\n", *currenttime, *best, *nfuneval);;
        output->oldbest = *best;
        
        fclose(p3);
        p3 = NULL;
    }
}


void print_end_file_(experiment_total *exp1, double *U, double *best, result_solver *result,
        int *D, output_struct *output, local_solver *local_s, int *idp) {
    int i;



    if (exp1[0].test.verbose != 0) {
        if (*idp == 0)
            printf("Numero de chamadas solver local %.5lf (Exitos: %.5lf) tempo gastado --> %.20lf\n",
                local_s->total_local, local_s->sucess_local, (*output).st3);
    }

    if (exp1[0].test.verbose == 1) {
        if (*idp == 0) printf("MELLOR VECTOR:\n");

        // FOR DIFERENTIAL EVOLUTION
        if (exp1->methodDE != NULL) {
            if (exp1[0].test._log  == 1) converttonormal_(U,D); 
        }
        for (i = 0; i < *D; i++) {
            if (*idp == 0) printf("\t%.30lf\n", U[i]);
        }
        if (*idp == 0) printf("f(x)=%.30lf\n", *best);
    }
    if (exp1[0].test.verbose == 1) {
        if (*idp == 0) {
            printf("\tEND--> time %.30lf eval %ld\n", result->time_value, result->eval_value);
        }
    }



}
void verboseiteration_(void *exp1_, int k,  double starttime, double ftarget, double best, long evaluation_local, int idp) {
    double mediumTime;
    experiment_total *exp1;
    
    exp1 = (experiment_total *) exp1_;
    
    if (exp1->test.verbose != 0) {
#ifdef MPI2
        mediumTime = (double) MPI_Wtime();
        printf("%d-iteracion %d best-coste %.10lf evals %.1ld -- TIME %.10lf -- ftarget %.20lf\n", idp, k, best, evaluation_local,
                (double) (mediumTime - starttime), ftarget);  
#else 
        mediumTime = clock();
        
        printf("%d-iteracion %d best-coste %.10lf evals %.1ld -- TIME %.10lf -- ftarget %.20lf\n", idp, k, best, evaluation_local,
                ((double) (mediumTime - starttime) / (double) CLOCKS_PER_SEC), ftarget);        
#endif


    }
}

void verboseiterationfortran_(void *exp1_, int *k, double *time,  double *ftarget, double *best, long *evaluation_local, int *par, int *idp) {
    experiment_total *exp1;

    exp1 = (experiment_total *) exp1_;
    
    if (exp1->test.verbose != 0) {
#ifdef MPI2
        printf("%d-iteracion %d best-coste %.20lf evals %ld -- TIME %lf -- ftarget %.20lf\n\n", *idp, *k, *best, *evaluation_local,
                *time, *ftarget);  
#else 
        printf("%d-iteracion %d best-coste %.20lf evals %ld -- TIME %lf -- ftarget %.20lf\n\n", *idp, *k, *best, *evaluation_local,
                *time, *ftarget);        
#endif


    }
}

char * concat_char(int *BUFFER, char *string_eval_total, char * string_eval) {
    char *string_eval_total_aux;
    size_t len1, len2;
    
    
    len1 = strlen(string_eval_total);
    len2 = strlen(string_eval);

    if (*BUFFER <  (len1 + len2 + 1)) {
        *BUFFER = len1*2 + len2 + 1;
        string_eval_total_aux = (char *) calloc((len1*2 + len2 + 1), sizeof(char));
        memmove( string_eval_total_aux , string_eval_total , len1);
        memmove( string_eval_total_aux+len1 , string_eval , len2);                        
                                
        string_eval_total = string_eval_total_aux;
    } else {
        string_eval_total_aux = strcat(string_eval_total,(const char *) string_eval);
        string_eval_total = string_eval_total_aux;
    }
    
    return string_eval_total;
    
}



void matlab_plot_file(experiment_total exp1,  char *string,  char *string2, const char *color, int ancho_linea,  const char *marca, int par, int idp, int NPROC, int func, int end ) {

        char *path;
        int len,salir,init, contador_puntos;
        const char *mfile;
        const char *auxchar;
        double eval, fx;
        int iter;
        char *string_eval_total, *string_fx_total, *string_evalr_total;
        char *string_eval, *string_fx, *string_evalr;
        struct line_reader lr;
        /*int BIG_BUFFER = 10000;*/
        FILE *p3, *p4;
        int BUFFER1;
        int BUFFER2;
        int BUFFER3;
        int SIZE_PATH;
        int i,j;
        char *line;
        
        line = NULL;
        BUFFER1=100;
        BUFFER2=100;
        BUFFER3=100;
        SIZE_PATH = 500;
        
        p3 = fopen(string2, "r");
        if (p3 == NULL) exit(0);
        
        mfile = ".m";
        path = (char *) calloc(SIZE_PATH,sizeof(char));
        strcpy(path,string);
        strcat( path, mfile);
        if (func == 1) {
            p4 = fopen(path, "w");
        } else {
            p4 = fopen(path, "a");  
        }
        
        if (p4 == NULL) exit(0);
        contador_puntos = 1;
        eval = -1.0; 
        fx = -1.0;
        init = 0;
        
        string_fx_total = ( char *) calloc(BUFFER2+1,sizeof(char));
        string_fx   = (char *) calloc(BUFFER2+1,sizeof(char));     
        
        string_eval = (char *) calloc(BUFFER1+1,sizeof(char));
        string_eval_total = ( char *) calloc(BUFFER1+1,sizeof(char));

        string_evalr = (char *) calloc(BUFFER3+1,sizeof(char));
        string_evalr_total = ( char *) calloc(BUFFER3+1,sizeof(char));
        
        sprintf(string_eval_total, " ");
        sprintf(string_fx_total, " ");
        sprintf(string_evalr_total, " ");        
        sprintf(string_eval, " ");
        sprintf(string_fx, " ");  
        sprintf(string_evalr, " "); 
        
        salir = 0;
        lr_init(&lr, p3);

    if (func == 1) {
        fprintf(p4, "function [ vector_TIME%d timeline%d linebest%d ] =%s_%d()\n", NPROC, NPROC, NPROC, exp1.execution.nameMatlab,NPROC);

        fprintf(p4, "function new_p = prev_interpol(x1,p1,xx);\n");
        fprintf(p4, "    new_p = ones(length(xx), 1 );\n");
        fprintf(p4, "    for i=1:length(xx)\n");
        fprintf(p4, "        ind = find(x1==xx(i));\n");
        fprintf(p4, "        if (isempty(ind)) \n");
        fprintf(p4, "            ind = max(find(x1<xx(i)));\n");
        fprintf(p4, "            if (isempty(ind) )\n");
        fprintf(p4, "                new_p(i) = unique(p1(1));\n");
        fprintf(p4, "            else\n");
        fprintf(p4, "                new_p(i) = unique(p1(ind));\n");
        fprintf(p4, "            end\n");
        fprintf(p4, "        else\n");
        fprintf(p4, "            new_p(i) = unique(p1(ind));\n");
        fprintf(p4, "        end\n");
        fprintf(p4, "    end\n");
        fprintf(p4, "    return\n");
        fprintf(p4, "end\n");
    }

       
        while (salir == 0) {
            
            line = next_line(&lr, &len);
            
            if (line != NULL ) {
                    auxchar = "%lf %lf %d";
                    sscanf(line, auxchar, &eval, &fx, &iter);
            } else {
                salir = 1;
            }
                if ((eval == -1 && fx == -1) || (salir == 1)) {
                    if (init == 1) {
                        sprintf(string_eval, " ];");
                        sprintf(string_fx, " ];");
                        sprintf(string_evalr, " ];");
                        
                        string_eval_total = concat_char(&BUFFER1, string_eval_total, string_eval);
                        string_fx_total = concat_char(&BUFFER2, string_fx_total, string_fx);
                        string_evalr_total = concat_char(&BUFFER3, string_evalr_total, string_evalr);
                        
                        fprintf(p4, "%s\n", string_eval_total);
                        fprintf(p4, "%s\n", string_fx_total);
                        fprintf(p4, "%s\n", string_evalr_total);
                        sprintf(string_eval, " ");
                        sprintf(string_fx, " ");
                        sprintf(string_evalr, " ");
                        contador_puntos++;

                    }
                    init = 1;
                    sprintf(string_eval_total, " ");
                    sprintf(string_fx_total, " ");
                    sprintf(string_eval, " ");
                    sprintf(string_fx, " ");   
                    sprintf(string_evalr_total, " ");
                    sprintf(string_evalr, " ");                      
                    if (par == 0) {
                        sprintf(string_fx_total, "p%d = [ ", contador_puntos);
                        sprintf(string_eval_total, "x%d = [ ", contador_puntos);
                        sprintf(string_evalr_total, "i%d = [ ", contador_puntos);
                    } else {
                        sprintf(string_fx_total, "p%d_%d = [ ", contador_puntos, idp);
                        sprintf(string_eval_total, "x%d_%d = [ ", contador_puntos, idp);
                        sprintf(string_evalr_total, "i%d_%d = [ ", contador_puntos,idp);                        
                    }
                } else {
                    sprintf(string_fx, " %.20lf ", fx);
                    sprintf(string_evalr, " %d ", iter);
                    sprintf(string_eval, " %.20lf ", eval);
                    string_eval_total = concat_char(&BUFFER1, string_eval_total, string_eval);
                    string_fx_total = concat_char(&BUFFER2, string_fx_total, string_fx);
                    string_evalr_total = concat_char(&BUFFER3, string_evalr_total, string_evalr);
                    eval = -1; fx = -1; iter=-1;
                }
        }
        
        
        
        if (!feof(p3)) {
		perror("next_line");
		exit(1);
	}
        
	lr_free(&lr);
#ifdef MPI2
        fprintf(p4, "\n\n");   
       
#else
        fprintf(p4, "vector_TIME%d = [",NPROC);
        for (i=0;i<contador_puntos-1;i++) {
            fprintf(p4, " x%d(length(x%d)) \n",i+1,i+1);
        }       
        fprintf(p4, "];\n");  
        fprintf(p4, "[MIN1, ii1] = min (vector_TIME%d);\n",NPROC);
        fprintf(p4, "[MAX1, jj1] = max (vector_TIME%d);\n",NPROC);        
        fprintf(p4, "xx1 = [");
        for (i=0;i<contador_puntos-1;i++) {
            fprintf(p4, " x%d ",i+1);
        } 
        fprintf(p4, "];\n"); 
        fprintf(p4, "xx2 = unique(sort(xx1));\n");
        fprintf(p4, "indice = find(xx2==MIN1);\n");
        fprintf(p4, "xx= xx2(1:indice);\n");        
        fprintf(p4, "pp = ones(%d,length(xx));\n", contador_puntos-1);  
        for (i=0;i<contador_puntos-1;i++) {
            fprintf(p4, " pp(%d,:) = prev_interpol(x%d,p%d,xx);\n",i+1,i+1,i+1);
        }  

        fprintf(p4, "[ linpp, ind ] = min(pp);\n");
        fprintf(p4, "stairs(xx,linpp,'r','LineWidth',4,'Markersize',5); hold on;\n");
        fprintf(p4, "set(gca, 'YScale', 'log','XScale', 'log');\n");

        for (i=0;i<contador_puntos-1;i++) {
                if (par == 0) {
                                fprintf(p4, "stairs(x%d,p%d,\'--%s%s\',\'LineWidth\',%d,'Markersize',5); hold on;\n",
                                        i+1, i+1, color, marca, ancho_linea);
                                fprintf(p4, "set(gca, 'YScale', 'log','XScale', 'log');  \n" );                                
                } else {
                                fprintf(p4, "stairs(x%d_%d,p%d_%d,\'--%s%s\',\'LineWidth\',%d,'Markersize',5); hold on;\n",
                                        i+1, idp, i+1, idp, color, marca, ancho_linea);
                                fprintf(p4, "set(gca, 'YScale', 'log','XScale', 'log');  \n" );
                }
        }        
        fprintf(p4, "hold off; \n xlabel('Time (s)','FontSize',22);  \nylabel('f(x)','FontSize',22);\n");
        fprintf(p4, "hleg1 = legend('best x ', 'runs x ','Location','SouthWest');\n");
        fprintf(p4, "set(hleg1,'FontSize',18);\n");
        fprintf(p4, "title('MULTIPLE RUNS of X - %s PROBLEM');\n", return_benchmark_SystemBiology(exp1.test.bench.current_bench));
        fprintf(p4, "figure;\n");
        
        fprintf(p4, "\nvector_EVAL%d = [",NPROC);
        for (i=0;i<contador_puntos-1;i++) {
            fprintf(p4, " i%d(length(i%d)) \n",i+1,i+1);
        }       
        fprintf(p4, "];\n");  
        fprintf(p4, "[MIN1_eval, ii1_eval] = min (vector_EVAL%d);\n",NPROC);
        fprintf(p4, "[MAX1_eval, jj1_eval] = max (vector_EVAL%d);\n",NPROC);        
        fprintf(p4, "ii1 = [");
        for (i=0;i<contador_puntos-1;i++) {
            fprintf(p4, " i%d ",i+1);
        } 
        fprintf(p4, "];\n"); 
        fprintf(p4, "ii2 = unique(sort(ii1));\n");
        fprintf(p4, "indice_eval = find(ii2==MIN1_eval);\n");
        fprintf(p4, "ii= ii2(1:indice_eval);\n");        
        fprintf(p4, "pp_eval = ones(%d,length(ii));\n", contador_puntos-1);  
        for (i=0;i<contador_puntos-1;i++) {
            fprintf(p4, " pp_eval(%d,:) = prev_interpol(i%d,p%d,ii);\n",i+1,i+1,i+1);
        }  

        fprintf(p4, "[ linpp_eval, ind_eval ] = min(pp_eval);\n");
        fprintf(p4, "stairs(ii,linpp_eval,'r','LineWidth',4,'Markersize',5); hold on;\n");
        fprintf(p4, "set(gca, 'YScale', 'log','XScale', 'log');\n");

        for (i=0;i<contador_puntos-1;i++) {
                if (par == 0) {
                                fprintf(p4, "stairs(i%d,p%d,\'--%s%s\',\'LineWidth\',%d,'Markersize',5); hold on;\n",
                                        i+1, i+1, color, marca, ancho_linea);
                                fprintf(p4, "set(gca, 'YScale', 'log','XScale', 'log');  \n" );                                
                } else {
                                fprintf(p4, "stairs(i%d_%d,p%d_%d,\'--%s%s\',\'LineWidth\',%d,'Markersize',5); hold on;\n",
                                        i+1, idp, i+1, idp, color, marca, ancho_linea);
                                fprintf(p4, "set(gca, 'YScale', 'log','XScale', 'log');  \n" );
                }
        }        
        fprintf(p4, "hold off; \n xlabel('Num. of evaluations','FontSize',22);  \nylabel('f(x)','FontSize',22);\n");
        fprintf(p4, "hleg1 = legend('best x ', 'runs x ','Location','SouthWest');\n");
        fprintf(p4, "set(hleg1,'FontSize',18);\n");
        fprintf(p4, "title('MULTIPLE RUNS of X - %s PROBLEM');\n", return_benchmark_SystemBiology(exp1.test.bench.current_bench));


#endif
        
        if (end == 1) {
#if MPI2        
        for (j = 0; j < contador_puntos - 1; j++) {
            fprintf(p4, "vector_TIME_RUN%d = [", j+1);
            for (i = 0; i < NPROC; i++) {
                fprintf(p4, " x%d_%d(length(x%d_%d)) ", j + 1,i, j + 1,i);
            }
            fprintf(p4, "];\n");
            fprintf(p4, "[MIN_RUN%d, ii_RUN%d] = min (vector_TIME_RUN%d);\n", j+1,j+1,j+1);
            fprintf(p4, "[MAX_RUN%d, jj_RUN%d] = max (vector_TIME_RUN%d);\n", j+1,j+1,j+1);
            fprintf(p4, "xx1_RUN%d = [",j+1);
            for (i = 0; i < NPROC; i++) {
                fprintf(p4, " x%d_%d ", j+1,i);
            }
            fprintf(p4, "];\n");
            fprintf(p4, "xx2_RUN%d = unique(sort(xx1_RUN%d));\n",j+1,j+1);
            fprintf(p4, "indice_RUN%d = find(xx2_RUN%d==MIN_RUN%d);\n",j+1,j+1,j+1);
            fprintf(p4, "xx_RUN%d= xx2_RUN%d(1:indice_RUN%d);\n",j+1,j+1,j+1);
            fprintf(p4, "pp_RUN%d = ones(%d,length(xx_RUN%d));\n", j+1,NPROC,j+1);
            for (i = 0; i < NPROC; i++) {
                fprintf(p4, " pp_RUN%d(%d,:) = prev_interpol(x%d_%d,p%d_%d,xx_RUN%d);\n", j+1,i+1, j+1,i,j+1,i,j+1);
            }

            fprintf(p4, "[ linpp_RUN%d, ind_RUN%d ] = min(pp_RUN%d);\n",j+1,j+1,j+1);
            fprintf(p4, "stairs(xx_RUN%d,linpp_RUN%d,'r','LineWidth',4,'Markersize',5); hold on;\n",j+1,j+1);
            fprintf(p4, "set(gca, 'YScale', 'log','XScale', 'log');\n");

            for (i = 0; i < NPROC; i++) {
                    fprintf(p4, "stairs(x%d_%d,p%d_%d,\'--%c%c\',\'LineWidth\',%d,'Markersize',5); hold on;\n",
                            j + 1, i, j + 1, i, color[0], marca[0], ancho_linea);
                    fprintf(p4, "set(gca, 'YScale', 'log','XScale', 'log');  \n");
            }
            
            fprintf(p4, "hold off;  \nxlabel('Time (s)','FontSize',22);  \n");
            fprintf(p4, "ylabel('f(x)','FontSize',22);\n");            
            fprintf(p4, "hleg1 = legend('best thread ', 'runs other threads ','Location','SouthWest');  \n");
            fprintf(p4, "set(hleg1,'FontSize',18);\n");        
            fprintf(p4, "title('Best convergence curve - %s - RUN%d');\n",return_benchmark_SystemBiology(exp1.test.bench.current_bench),j+1);        
            fprintf(p4, "figure\n\n\n");                    
        }
        
        fprintf(p4, "\n\n\n");   
        
        fprintf(p4, "vector_TIME%d = [",NPROC);
        for (i=0;i<contador_puntos-1;i++) {
            fprintf(p4, " xx_RUN%d(length(xx_RUN%d)) ",i+1,i+1);
        }       
        fprintf(p4, "];\n");  
        fprintf(p4, "[MIN_TOTAL, ii_TOTAL] = min (vector_TIME%d);\n",NPROC);
        fprintf(p4, "[MAX_TOTAL, jj_TOTAL] = max (vector_TIME%d);\n",NPROC);        
        fprintf(p4, "xx1_TOTAL = [");
        for (i=0;i<contador_puntos-1;i++) {
            fprintf(p4, " xx_RUN%d ",i+1);
        } 
        fprintf(p4, "];\n"); 
        fprintf(p4, "xx2_TOTAL = unique(sort(xx1_TOTAL));\n");
        fprintf(p4, "indice_TOTAL = find(xx2_TOTAL==MIN_TOTAL);\n");
        fprintf(p4, "xx_TOTAL= xx2_TOTAL(1:indice_TOTAL);\n");        
        fprintf(p4, "pp_TOTAL = ones(%d,length(xx_TOTAL));\n", contador_puntos-1);  
        for (i=0;i<contador_puntos-1;i++) {
            fprintf(p4, " pp_TOTAL(%d,:) = prev_interpol(xx_RUN%d,linpp_RUN%d,xx_TOTAL);\n",i+1,i+1,i+1);
        }  

        fprintf(p4, "[ linpp_TOTAL, ind2_TOTAL ] = min(pp_TOTAL);\n");
        fprintf(p4, "stairs(xx_TOTAL,linpp_TOTAL,'r','LineWidth',4,'Markersize',5); hold on;\n");
        fprintf(p4, "set(gca, 'YScale', 'log','XScale', 'log');\n");

        for (i = 0; i < contador_puntos - 1; i++) {
            fprintf(p4, "stairs(xx_RUN%d,linpp_RUN%d,\'--%c%c\',\'LineWidth\',%d,'Markersize',5); hold on;\n",
                    i + 1, i + 1, color[0], marca[0], ancho_linea);
            fprintf(p4, "set(gca, 'YScale', 'log','XScale', 'log');  \n");
        }        
        fprintf(p4, "hold off; \n xlabel('Time (s)','FontSize',22);  \nylabel('f(x)','FontSize',22);\n");
        fprintf(p4, "hleg1 = legend('best thread ','Location','SouthWest');\n");
        fprintf(p4, "set(hleg1,'FontSize',18);\n");
        fprintf(p4, "title('Best convergence curve all RUNS - %s PROBLEM');\n", return_benchmark_SystemBiology(exp1.test.bench.current_bench));
                
        fprintf(p4, "timeline%d = xx_TOTAL;\n",NPROC);
        fprintf(p4, "linebest%d = linpp_TOTAL;\n",NPROC);
        fprintf(p4,"\nend");  
#else
        fprintf(p4, "evalline%d = ii;\n",NPROC);
        fprintf(p4, "linebest%d_eval = linpp_eval;\n",NPROC);        
        fprintf(p4, "timeline%d = xx;\n",NPROC);
        fprintf(p4, "linebest%d = linpp;\n",NPROC);
        fprintf(p4,"\n\nend");        
#endif
        } 

        

        
        cfree(string_eval);
        cfree(string_fx);
        cfree(string_fx_total);
        cfree(string_eval_total);
        cfree(string_evalr);
        cfree(string_evalr_total);        
        cfree(path);
        string_eval = NULL;
        string_fx = NULL;
        string_fx_total = NULL;
        string_evalr = NULL;
        string_evalr_total = NULL;        
        string_eval_total = NULL;
        path = NULL;
        fclose(p3);
        fclose(p4);
        p3 = NULL;
        p4 = NULL;

}

double varianze(double *array, double avg, int size) {
    int i;
    double suma;
    suma = 0;
    for (i=0;i<size;i++){
        suma = pow(array[i]-avg,2);
    }
    
    suma = suma / (double) (size-1);
    
    return suma;
}





char select_color(int i) {
    if (i == 0) {
        return 'c';
    } else if (i == 1) {
        return 'm';
    } else if (i == 2) {
        return 'y';
    } else if (i == 3) {
        return 'k';
    } else if (i == 4) {
        return 'r';
    } else if (i == 5) {
        return 'g';
    } else if (i == 6) {
        return 'b';
    } else if (i == 7) {
        return 'w';
    } else {
        return 'c';
    }
    
}

char select_marca(int i) {
    int result;

    result = (int) (i / 8);

    if (result == 0) {
        return 'e';
    } else if (result == 1) {
        return 'x';
    } else if (result == 2) {
        return '*';
    } else if (result == 3) {
        return 'o';
    } else if (result == 4) {
        return '.';
    } else if (result == 5) {
        return '+';
    } else if (result == 6) {
        return 's';
    } else if (result == 7) {
        return 'd';
    } else {
        return 'e';
    }
}


void plot_file(experiment_total exp1, int par, int idp, int NPROC) {

    char *string,*string2;
    char *vectstring;
    int f,i,end;
    
    if (par == 1) {
#ifdef MPI2

        char *color;
        char *marca;

        color = (char *) calloc(1,sizeof (char));
        marca = (char *) calloc(1,sizeof (char));
        color[0] = select_color(idp % 8);
        marca[0] = select_marca(idp);
        if (marca[0] == 'e') marca = "";

        string = (char *) malloc(100*sizeof(char));
        vectstring = (char *) malloc(100*NPROC*sizeof(char));
        sprintf(string,"%s",exp1.test.output_graph);
        string2 = (char *) malloc(100*sizeof(char));
        
        MPI_Gather(string, 100, MPI_CHAR, vectstring, 100, MPI_CHAR, 0, MPI_COMM_WORLD);
        
            
        if (idp == 0 ) {
            for (i=0;i<NPROC;i++) {
                memmove(string2, &vectstring[i*100], 100* sizeof(char));
                if (i == 0 ) f=1; else f=0;
                if (i == NPROC-1) end=1; else end=0;
                matlab_plot_file(exp1,string, string2, color, 2, marca, 1, i, NPROC,f,end);
            }
        }
        
        MPI_Barrier(MPI_COMM_WORLD);
        free(string);
        free(string2);
        free(vectstring);

        
#endif

    } else {
        char *color;
        char *marca;
        color = ( char *) calloc(1,sizeof(char));
        marca = ( char *) calloc(1,sizeof(char));
        
        matlab_plot_file(exp1,exp1.test.output_graph, exp1.test.output_graph,color,2, marca, 0, 0, NPROC,1,1);

    }
    
    
    
}

void initoutputvars_(void *output_){
    
    output_struct *output;
    
    output = (output_struct *) output_;
    output->point_counter = 0;
    output->point1=0;
    output->oldbest=DBL_MAX;
    output->st1=0.0;
    output->st2=0.0;
    output->st3=0.0;
    
    
    
}



void updateresultsandprint_(void *exp1_, void *result_, void *output_, void *local_s_, double *totaltime, long *evaluation, 
        int *D, double *xbest, double *best, int *idp, int *NPROC ) {
    int i, indexvector;
    double auxbest;
    result_solver *result;
    output_struct *output;
    local_solver *local_s;
    experiment_total *exp1;
    double st3;
    double *vectorbestx,*vectorbest,*bestglobalx,bestglobal;
    
    st3 = 0.0;
    exp1 = (experiment_total *) exp1_;
    result = (result_solver *) result_;
    output = (output_struct *) output_;
    local_s = (local_solver *) local_s_;
    double totalL, sucessL;



#ifdef MPI2
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Reduce(&(local_s->total_local), &totalL, 1, MPI_DOUBLE, MPI_SUM, 0, exp1->execution.topology.comunicator);
    MPI_Reduce(&(local_s->sucess_local), &sucessL, 1, MPI_DOUBLE, MPI_SUM, 0, exp1->execution.topology.comunicator);
    MPI_Reduce(&(output->st3), &st3, 1, MPI_DOUBLE, MPI_SUM, 0, exp1->execution.topology.comunicator);
       
    if (*idp == 0) {
            local_s->total_local = totalL / (double) *NPROC;
           local_s->sucess_local = sucessL/ (double) *NPROC;
            output->st3 = st3 / (double) *NPROC;
    }
        
    MPI_Bcast(&(local_s->total_local), 1, MPI_DOUBLE, 0, exp1->execution.topology.comunicator);
    MPI_Bcast(&(local_s->sucess_local), 1, MPI_DOUBLE, 0, exp1->execution.topology.comunicator);
    MPI_Bcast(&(output->st3), 1, MPI_DOUBLE, 0, exp1->execution.topology.comunicator);
            
    vectorbestx = (double *) malloc(*NPROC*(*D)*sizeof(double));
    vectorbest = (double *) malloc(*NPROC*sizeof(double));
    bestglobalx = (double *) malloc((*D)*sizeof(double));
    MPI_Gather(xbest,*D,MPI_DOUBLE,vectorbestx,*D,MPI_DOUBLE,0,exp1->execution.topology.comunicator);
    MPI_Gather(best,1,MPI_DOUBLE,vectorbest,1,MPI_DOUBLE,0,exp1->execution.topology.comunicator);
        
    if (*idp == 0) {
            auxbest = vectorbest[0];
            indexvector = 0;
            for (i=0;i<*NPROC;i++) {
                if ( vectorbest[i] < auxbest ) {
                    auxbest = vectorbest[i];
                    indexvector = i;
                }
            }
          
            bestglobal=auxbest;
            memmove(bestglobalx,&vectorbestx[indexvector*(*D)],*D*sizeof(double));     
    }
        MPI_Barrier(MPI_COMM_WORLD);
        MPI_Bcast(bestglobalx, *D, MPI_DOUBLE, 0, exp1->execution.topology.comunicator);
        MPI_Bcast(&bestglobal, 1, MPI_DOUBLE, 0, exp1->execution.topology.comunicator);
        if ((*result).best_value >= bestglobal) {
            (*result).best_value = bestglobal;
        }
        (*result).time_value = (*result).time_value + *totaltime;
        (*result).eval_value = (*result).eval_value + *evaluation;  
        
        print_end_file_(exp1,bestglobalx,&bestglobal,result,D,output,local_s,idp);
        
        free(vectorbestx);
        vectorbestx=NULL;
        free(vectorbest  );
        vectorbest=NULL;
        free(bestglobalx  );
        bestglobalx=NULL;
        
        MPI_Barrier(MPI_COMM_WORLD);
#else 
    
        (*result).time_value = (*result).time_value + *totaltime;
        (*result).eval_value = (*result).eval_value + *evaluation;

        print_end_file_(exp1,xbest,best,result,D,output,local_s,idp);
        
        
        if ((*result).best_value >= *best) {
            (*result).best_value = *best;
        }
#endif
        
        
    
}


int updateresultsrandomsearch_(void *exp1_, void *result_, double *totaltime, double *evaluation, double *best ) {
    result_solver *result;
//    experiment_total *exp1;
    
//    exp1 = (experiment_total *) exp1_;
    result = (result_solver *) result_;
    // update_results_and_print : exp1 result  output local_s totaltime U D popul index idp NPROC 
    (*result).time_value = (*result).time_value + *totaltime;
    (*result).eval_value = (*result).eval_value + *evaluation;
    (*result).best_value = *best;

   
    return 1;
}


