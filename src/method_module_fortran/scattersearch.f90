!#define DETERMINISTIC 0

MODULE scattersearch
    USE iso_c_binding
    USE scattersearchtypes
    USE scattersearchfunctions
    USE localsolver
#ifdef DETERMINISTIC
    USE HDF5   
#endif       
CONTAINS

    FUNCTION sscattersearch(exp1, locals, output, fitnessfunction, results1, maxfunevals, ftarget) RESULT (outresult)
    
        ! Declaración de variables
        USE common_functions
        USE qsort_module   
        IMPLICIT NONE

        TYPE(C_PTR), INTENT(INOUT) :: exp1, locals, output, results1
        INTEGER (C_LONG), INTENT(INOUT) :: maxfunevals
        REAL (C_DOUBLE), INTENT(INOUT) :: ftarget
        TYPE(C_FUNPTR), INTENT(INOUT) :: fitnessfunction ! fobj

        TYPE(outfuction) :: outfunct
        INTEGER :: stopoptimization, fin, nreset, status, nvar
        INTEGER(C_LONG) :: nfuneval
        INTEGER :: i, j, nconst, DIMEN(2)
        REAL(C_DOUBLE) :: cputime1, cputime2, cputime3, localsolvertime, cputime!, cputime4, cputime5
        INTEGER :: clock_rate, clock_start, clock_stop
        TYPE(opts) :: opts1, default1
        TYPE(problem) :: problem1
        TYPE(resultsf) :: results
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)) ::  F02
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: xbest, fbest, new_fbest, fbest_lastiter
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: xl_log, xu_log, temp, randomV

        
        INTEGER :: n_minimo, n_critico, NPROC
        INTEGER, DIMENSION(:), ALLOCATABLE :: ncomb1, refset_change
        INTEGER, DIMENSION(:,:), ALLOCATABLE :: ncomb2
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)) :: MaxSubSet, MaxSubSet2
        
        TYPE(Refsettype) :: solutionset, refset, candidateset, childset
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE :: solutions
        INTEGER :: counter
      
        INTEGER, DIMENSION(:), ALLOCATABLE :: indvect
        INTEGER :: iter
        INTEGER :: nrand, final_ref, use_bestx
        INTEGER, DIMENSION(:), ALLOCATABLE :: index1, index2, index, diff_index
        INTEGER :: lb_p, auxsize
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)) :: st_p
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: ppp
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE :: hyper_x_L, hyper_x_U, &
                                                                                                factor, v1, v2, v3, new_comb
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE :: parents_index1, parents_index2
        INTEGER :: stage_1, stage_2, parallel
        INTEGER, DIMENSION(:), ALLOCATABLE :: members_update, candidate_update, members_to_update
        INTEGER(C_INT) :: outresult
        REAL(C_DOUBLE) :: resultado, timetotal, timeparallel, random
        
        ! PARAMETROS DO LOCAL SOLVER TAL E COMO N2FB
        INTEGER :: indexLS, idp
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE :: local_solutions, initial_points
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: local_solutions_values, &
                                                                                                            initial_points_values
#ifdef DETERMINISTIC
        INTEGER :: jjj
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)):: valorR
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: matrixR

        INTEGER :: contador
        INTEGER(HID_T) :: dset_id, file_id, dataspace       ! Dataset identifier
        INTEGER(HSIZE_T), DIMENSION(1) :: data_dims
        CHARACTER(LEN=11), PARAMETER :: filename = "random10.h5" ! File name
        CHARACTER(LEN=8), PARAMETER :: dsetname = "dataset1"     ! Dataset name        
#endif                                                                                            
           
        cputime = 0.0
        localsolvertime = 0.0
        timeparallel = 0.0
        
#ifdef DETERMINISTIC
        PRINT *, "DETERMINISTIC"
        contador = 1  
#endif
        
        CALL problem_specifications(exp1, problem1,opts1,nvar,ftarget,maxfunevals) 
        

#ifdef DETERMINISTIC
        ALLOCATE(matrixR(10000))
        CALL h5open_f (error)
        CALL h5fopen_f (filename, H5F_ACC_RDWR_F, file_id, error)
        CALL h5dopen_f(file_id, dsetname, dset_id, error)
        CALL h5dread_f(dset_id, H5T_NATIVE_DOUBLE, matrixR, data_dims, error)
        CALL h5dclose_f(dset_id, error)
        CALL h5fclose_f(file_id, error)        
#endif        
        idp = 0
        parallel = 0
        stopoptimization = 0        
        nfuneval = 0
        fin = 0
        nreset = 0
        NPROC = 1
        
        CALL SYSTEM_CLOCK(count_rate=clock_rate)
        CALL SYSTEM_CLOCK(count=clock_start)
        CALL initrngrandomserial(exp1)
              
        
        ! Check if bounds have the same dimension   CHECKBOUNDS
        CALL checkbounds(problem1,status)

        !INIT BEST VARIABLES
        CALL initbestvars(problem1,xbest,fbest,nvar)
        
        !INIT INT BIN VARIABLES
        CALL initintbinvars(problem1)
        
        CALL calc_dim_refset(opts1%globaloptions%dim_refset, nvar, opts1%useroptions%iterprint,idp,NPROC) 
        CALL calc_ndiverse(opts1%globaloptions%ndiverse, nvar, opts1%useroptions%iterprint,idp)
        CALL initlocalsolvervars(locals)
        CALL initoutputvars(output)
        
        if (opts1%globaloptions%ndiverse < opts1%globaloptions%dim_refset) then
            opts1%globaloptions%ndiverse = opts1%globaloptions%dim_refset
        end if

        ! INIT LOCAL OPTIONS
        CALL initlocaloptions(opts1) 

        CALL init_inequality_constraints(problem1%neq, problem1%CL, problem1%CU)
   
        problem1%ineq = problem1%neq + problem1%ineq
       
        CALL calcnconst(problem1,nconst) 
        
        ! CHECK OUTPUT OBJECTIVE FUNCTION
        CALL check_output_obj_funct(problem1, opts1,status, nconst)

        ! PASAMOS A LOGARITMO
        ALLOCATE(xl_log(size(problem1%XL)))
        ALLOCATE(xu_log(size(problem1%XU)))
        xl_log = problem1%XL
        xu_log = problem1%XU
             
        
        if (ALLOCATED(opts1%useroptions%log_var)) then    
            CALL converttolog2(xl_log, nvar,opts1%useroptions%log_var)
            CALL converttolog2(xu_log, nvar,opts1%useroptions%log_var)
        end if

        
        CALL build_aux_local_search(opts1)

        !Initialize variables
        stage_1=1
        n_minimo = 0;
        if (opts1%localoptions%empty .eq. 0) n_critico = opts1%localoptions%n1
        ! Possible combinations among the opts1%globaloptions%dim_refset elements in Refset, taken in pairs
        ALLOCATE(ncomb1(opts1%globaloptions%dim_refset))
        i=1
        ncomb1 = (/(i, i = 1, opts1%globaloptions%dim_refset)/)
        call nchoosek_fpar(ncomb1, 2, ncomb2)
        

        MaxSubSet = (opts1%globaloptions%dim_refset**2 - opts1%globaloptions%dim_refset)/2
        MaxSubSet2 = 2 * MaxSubSet

        ! Creas inicialmente o conxunto soluciónW
#ifdef DETERMINISTIC
        CALL create_init_solutions_det(exp1,opts1,solutions,nvar,xl_log,xu_log, contador, matrixR);
#else
        CALL create_init_solutions(exp1,opts1,solutions,nvar,xl_log,xu_log);
#endif   
        
        !DIMEN = SHAPE(solutions)
        !CALL savehdf5solutions(solutions, DIMEN(1), DIMEN(2))
        
        if ((problem1%int_var .GT. 0) .OR. (problem1%bin_var .GT. 1)) then
            CALL ssm_round_int(solutions, problem1%int_var + problem1%bin_var, problem1%XL, problem1%XU)
        end if

        ! avaliamos o conxunto solución e creamos a estructura solutionset
        CALL evaluate_solutions_set(exp1,fitnessfunction,solutionset,problem1,opts1, solutions, nvar, nfuneval, nconst,timeparallel)
        
        
        ! Creamos o conxunto Refset
#ifdef DETERMINISTIC
        CALL create_refset_det ( solutionset, refset, nvar, opts1, nconst, opts1%globaloptions%dim_refset )
#else        
        CALL create_refset ( solutionset, refset, nvar, opts1, nconst, opts1%globaloptions%dim_refset )
#endif
    

        !Check best value in refset
        CALL check_best_value(refset, opts1, xbest, fbest )
        
        iter = 0
        if (opts1%useroptions%iterprint .eq. 1) then
            CALL SYSTEM_CLOCK(count=clock_stop)
            cputime1 = REAL(clock_stop-clock_start)/REAL(clock_rate)
            WRITE(*, '(A25, I10, A6, D40.30, A10, E15.4, A8, E15.4)') "Initial Pop: NFunEvals: ", nfuneval, " Bestf: ", fbest(1), &
            " CPUTime: ", cputime1, " fbest: ", variance_vector(fbest)
        end if
        

        ! Creatos la estructura results y la inicializamos con los primeros valores
        CALL create_results(results, opts1, xbest, fbest, nfuneval, cputime1, nvar)

        ! Sort the Refset
        CALL sort_refset(refset,opts1, indvect)

        if (opts1%globaloptions%combination .eq. 1) then         
            nrand = nvar
        else if (opts1%globaloptions%combination .eq. 2) then
            nrand = 1
        end if

        final_ref = 0
        use_bestx = 0

        auxsize = choose(ncomb1)
        ALLOCATE(index1(auxsize))
        ALLOCATE(index2(auxsize))
        ALLOCATE(index(auxsize))
        ALLOCATE(diff_index(auxsize))
        index1 = ncomb2(:, 1)
        index2 = ncomb2(:, 2)

        index = ncomb2(:, 2)
        
        
        CALL FUSION_VECTOR_INT(index1, index)

        diff_index = (index2 - index1)
        lb_p = 1 !Minimum ppp
        st_p = REAL(0.75d0,KIND =SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))
        !Defines maximum ppp. max(ppp)= lb_p+ st_p
        !Example: lb_p=1.25 and st_p=0.5 varies ppp from
        !%1.25 to 1.75

        ALLOCATE (ppp(auxsize))
        ppp = st_p * REAL((diff_index - 1),KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))/ &
        REAL((opts1%globaloptions%dim_refset - 2),KIND =SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)) + &
        REAL(lb_p,KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))
        

        !hyper_x_L=repmat(problem1%XL,MaxSubSet,1);
        !hyper_problem1%XU=repmat(problem1%XU,MaxSubSet,1);
        ALLOCATE(hyper_x_L(nvar,CEILING(MaxSubSet)))
        ALLOCATE(hyper_x_U(nvar,CEILING(MaxSubSet)))
        i=1
        j=1
        hyper_x_L = reshape((/ ((problem1%XL(i), i = 1,nvar), j = 1,  CEILING(MaxSubSet)) /), (/ nvar, CEILING(MaxSubSet) /))
        hyper_x_U = reshape((/ ((problem1%XU(i), i = 1,nvar), j = 1,  CEILING(MaxSubSet)) /), (/ nvar, CEILING(MaxSubSet) /))


        ALLOCATE(refset_change(opts1%globaloptions%dim_refset))
        refset_change = 0

        
        F02 = fbest(1)
        if (opts1%useroptions%iterprint .eq. 1 ) then
            CALL SYSTEM_CLOCK(count=clock_stop)
            cputime3 = REAL(clock_stop-clock_start)/REAL(clock_rate)
            CALL initprintfile(exp1, output, F02, parallel, idp, cputime3, nfuneval)                
        endif           
        
        ! Algorithm main loop
        do while (fin .eq. 0)
            CALL create_childset ( childset, nvar, CEILING(MaxSubSet2), nconst )   
            
            counter = 1
            if (.not.ALLOCATED(members_update)) then
                ALLOCATE(members_update(opts1%globaloptions%dim_refset))
                members_update =  1
            end if
                
            ! Miramos si hai solucións repetidas, e se as hai sustituímolas
#ifdef DETERMINISTIC
            
            CALL check_duplicated_replace_det (exp1, fitnessfunction, problem1,refset,opts1, parents_index1, parents_index2, &
                 xl_log, xu_log, refset_change, members_update, index1, index2, nfuneval, nvar, nconst, contador, matrixR )
#else            
            
            CALL check_duplicated_replace (exp1, fitnessfunction, problem1,refset,opts1, parents_index1, parents_index2, &
                        xl_log, xu_log, refset_change, members_update, index1, index2, nfuneval, nvar, nconst )
#endif                        
            CALL create_candidate(candidateset, refset)

            if (.not.ALLOCATED(candidate_update)) then
                ALLOCATE(candidate_update(opts1%globaloptions%dim_refset))
                candidate_update = 0
            end if

            
            ALLOCATE( factor(nvar, size(ppp)))
            factor = reshape((/ ((ppp(j), i = 1, nvar), j = 1, size(ppp)) /), (/ nvar,size(ppp) /))
            factor = factor * (parents_index2 - parents_index1)/ REAL(1.5d0,KIND =SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)) 

            
            ALLOCATE( v1(nvar, size(ppp)))
            ALLOCATE( v2(nvar, size(ppp)))
            ALLOCATE( v3(nvar, size(ppp)))


            v1 = parents_index1 - factor
            v2 = parents_index2 - factor
            v3 = REAL(2.0d0,KIND =SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))  * parents_index2  - parents_index1 - factor  
           


#ifdef DETERMINISTIC
            
            CALL check_vector_in_hyper_bounds_det(exp1,opts1, v1, hyper_x_L, hyper_x_U, contador, matrixR )
            CALL check_vector_in_hyper_bounds_det(exp1,opts1, v3, hyper_x_L, hyper_x_U, contador, matrixR )
            
            CALL generate_new_comb_matrix_det(exp1, new_comb, ppp, MaxSubSet, nrand, v1,v2,v3, contador, matrixR )
            
#else
            
            CALL check_vector_in_hyper_bounds(exp1,opts1, v1, hyper_x_L, hyper_x_U )
            CALL check_vector_in_hyper_bounds(exp1,opts1, v3, hyper_x_L, hyper_x_U )
            
            CALL generate_new_comb_matrix(exp1, new_comb, ppp, MaxSubSet, nrand, v1,v2,v3)
#endif
            
            !if (idp .EQ. 0) call cpu_time(cputime4) 
            
            CALL update_candidateset_with_new_comb( exp1,opts1, fitnessfunction,problem1,new_comb,candidateset,childset,&
                candidate_update, members_update,MaxSubSet2,nrand,nconst,nfuneval,counter, index, timeparallel )
              
            !if (idp .EQ. 0) call cpu_time(cputime5) 
            !DIMEN = SHAPE(new_comb)
            !if (idp .EQ. 0) print *, "time evals", cputime5-cputime4, DIMEN(2)         
        
            ! Check the stop criterion
            if (opts1%useroptions%maxtime .GT. 0 ) then        
                CALL SYSTEM_CLOCK(count=clock_stop)
                cputime2 = REAL(clock_stop-clock_start)/REAL(clock_rate)
            else 
                cputime = 0.0
                cputime2 = 0.0
            end if
            fin = check_stopping_criteria (problem1, opts1, cputime2, stopOptimization, fbest, nfuneval) 

            
            CALL reajustchild(childset, counter, nvar,  nconst)
            
            
            ALLOCATE(members_to_update(size(candidate_update)))
            members_to_update = members_to_update * 0
            CALL index_of_ones(candidate_update, members_to_update)
            CALL reajust_index(members_to_update)
        
#ifdef DETERMINISTIC
            
            CALL apply_beyond_to_members_to_update_det( exp1, opts1, fitnessfunction, problem1, members_to_update,  nfuneval, &
                    refset, candidateset, nconst, nrand, nvar, contador, matrixR)            
#else            
            
            CALL apply_beyond_to_members_to_update( exp1, opts1, fitnessfunction, problem1, members_to_update,  nfuneval, &
                    refset, candidateset, nconst, nrand, nvar)
#endif     
            
            if ( .not. ALLOCATED(fbest_lastiter) ) then
                ALLOCATE(fbest_lastiter(size(fbest)))
            end if
            fbest_lastiter=fbest
            CALL select_better_solution(results, opts1, refset, xbest, fbest, use_bestx, nfuneval, clock_start, fin) 

            iter=iter+1
            n_minimo=n_minimo+1
            

            if (opts1%useroptions%iterprint .eq. 1) then
                WRITE(*, '(A25, I10, A6, I10, A9, D40.30, A12, f9.2, A12, E15.4,A5, E15.4)') "Iterations: ", iter, " NFunEvals: ",&
                nfuneval, & 
                " Bestf: ", fbest(1), &
                " CPUTime: ", cputime2, &
                " variance: ", variance_vector(refset%fpen), &
                " VTR", problem1%vtr
            end if
            
            
                
            CALL update_refset_change(members_update, refset_change)  
            
#ifdef DETERMINISTIC            
                
            CALL remove_possible_stuck_members_in_refset_det(problem1, opts1, exp1, fitnessfunction, nconst, &
                        refset, refset_change,nfuneval, nvar, xl_log, xu_log, contador, matrixR )        
#else            
            CALL remove_possible_stuck_members_in_refset(problem1, opts1, exp1, fitnessfunction, nconst, &
                        refset, refset_change,nfuneval, nvar, xl_log, xu_log )
#endif            
            
            CALL check_the_improvement_of_fbest(results, opts1, fbest, xbest, fbest_lastiter, nreset, cputime2, fin, nfuneval)

            if (opts1%localoptions%empty .eq. 0 ) then
                if ( ( LEN(opts1%localoptions%solver) .gt. 0 ) .and. (opts1%localoptions%bestx .gt. 0) .and. &
                                    (n_minimo .ge. n_critico) ) then    
                 if ( use_bestx .gt. 0 ) then
                    CALL ssm_local_filters(exp1, problem1, fitnessfunction, opts1,childset,n_minimo,use_bestx,stage_1,stage_2, &
                    xbest, fbest,n_critico,nvar,local_solutions, local_solutions_values, initial_points, &
                    initial_points_values, fin, NPROC, nfuneval, fbest_lastiter, results, refset, refset_change, &
                    output, locals, localsolvertime)
                    n_minimo=0
                 end if
                else 
                 if ( ( LEN(opts1%localoptions%solver) .gt. 0 ) .and. (n_minimo .ge. n_critico) ) then
                    CALL ssm_local_filters(exp1, problem1, fitnessfunction, opts1,childset,n_minimo,use_bestx,stage_1,stage_2, &
                    xbest, fbest,n_critico,nvar,local_solutions, local_solutions_values, initial_points, &
                    initial_points_values, fin, NPROC, nfuneval, fbest_lastiter, results, refset, refset_change, &
                    output, locals, localsolvertime)
                    n_minimo=0
                 end if 
                end if
            end if
            
            fbest_lastiter=fbest
            
            ! check the stopping criteria
            if (opts1%useroptions%maxtime .GT. 0 ) then        
                CALL SYSTEM_CLOCK(count=clock_stop)
                cputime3 = REAL(clock_stop-clock_start)/REAL(clock_rate)
            else 
                cputime = 0.0
                cputime3 = 0.0
            end if   
            
            fin = check_stopping_criteria (problem1, opts1, cputime3, stopoptimization, fbest, nfuneval)
            
            CALL finalize_algorithm(exp1,problem1,results,opts1,fitnessfunction,output,refset,xbest,fbest,fin,nfuneval,NPROC, &
                    cputime3, nconst, nvar)
            
            
            F02 = fbest(1)
            if (opts1%useroptions%iterprint .eq. 1 ) then
                CALL SYSTEM_CLOCK(count=clock_stop)
                cputime3 = REAL(clock_stop-clock_start)/REAL(clock_rate)
                CALL printiteration(exp1, output, locals, iter, F02, nfuneval, cputime3) 
            endif            
           
            CALL addlocalscounter(locals)
            if (fin > 0) then
                F02 = fbest(1)
                if (opts1%useroptions%iterprint .eq. 1 ) then
                    CALL SYSTEM_CLOCK(count=clock_stop)
                    cputime3 = REAL(clock_stop-clock_start)/REAL(clock_rate)
                endif
               CALL  printenditeration(exp1, output, F02, cputime3, nfuneval)
            end if
            

            CALL destroy_refsettype(candidateset)
            CALL destroy_refsettype(childset)
            
            
            if (ALLOCATED(parents_index1)) DEALLOCATE(parents_index1)
            if (ALLOCATED(parents_index2)) DEALLOCATE(parents_index2)

            if (ALLOCATED(factor))  DEALLOCATE(factor)
            if (ALLOCATED(v1))  DEALLOCATE(v1)
            if (ALLOCATED(v2))  DEALLOCATE(v2)
            if (ALLOCATED(v3))  DEALLOCATE(v3)
            if (ALLOCATED(candidate_update)) DEALLOCATE(candidate_update)
            if (ALLOCATED(members_update)) DEALLOCATE(members_update)
            if (ALLOCATED(new_comb))  DEALLOCATE(new_comb)
            if (ALLOCATED(members_to_update))  DEALLOCATE(members_to_update)
            
          
        end do
        
        ! SUBROUTINE DESTROY_PROBLEMS
        outresult = 1
        timetotal = REAL(cputime3,KIND=C_DOUBLE)
        resultado = fbest(1)
        
        F02 = fbest(1)
        
        
        CALL updateresultsandprint(exp1, results1, output, locals, timetotal, nfuneval, nvar, xbest, F02, idp, NPROC )
        
        !error = updateresultsrandomsearch(exp1, results1, timetotal, evaluations, resultado)
        CALL settotaltime(results1, timetotal)
        CALL setparalleltime(results1, timeparallel)
        CALL setlocalsolvertime(results1, localsolvertime)
        ! Free memory
        CALL destroy_opts(opts1)
        CALL destroy_opts(default1)
        CALL destroy_problem(problem1)

        
        if (ALLOCATED(xl_log)) DEALLOCATE(xl_log)
        if (ALLOCATED(xu_log)) DEALLOCATE(xu_log)   
        
        if (ALLOCATED(randomV)) DEALLOCATE(randomV)
        
        if (ALLOCATED(solutions)) DEALLOCATE(solutions)
        if (ALLOCATED(ncomb1)) DEALLOCATE(ncomb1)
        if (ALLOCATED(ncomb2)) DEALLOCATE(ncomb2)
        
        
        if (ALLOCATED(indvect)) DEALLOCATE(indvect)
        if (ALLOCATED(index1)) DEALLOCATE(index1)
        if (ALLOCATED(index2)) DEALLOCATE(index2)
        if (ALLOCATED(index)) DEALLOCATE(index)
        if (ALLOCATED(diff_index)) DEALLOCATE(diff_index)
        if (ALLOCATED(diff_index)) DEALLOCATE(diff_index)
        if (ALLOCATED(factor)) DEALLOCATE(factor)
        if (ALLOCATED(ppp)) DEALLOCATE(ppp)
        if (ALLOCATED(hyper_x_L)) DEALLOCATE(hyper_x_L)
        if (ALLOCATED(hyper_x_U)) DEALLOCATE(hyper_x_U)
        if (ALLOCATED(refset_change)) DEALLOCATE(refset_change)
        if (ALLOCATED(opts1%localoptions%finish)) DEALLOCATE(opts1%localoptions%finish)
        if (ALLOCATED(xbest)) DEALLOCATE(xbest)
        if (ALLOCATED(fbest)) DEALLOCATE(fbest)
        if (ALLOCATED(new_fbest)) DEALLOCATE(new_fbest)
        if (ALLOCATED(fbest_lastiter)) DEALLOCATE(fbest_lastiter)
        IF (ALLOCATED(initial_points)) DEALLOCATE(initial_points)
        IF (ALLOCATED(initial_points_values)) DEALLOCATE(initial_points_values)
        IF (ALLOCATED(local_solutions)) DEALLOCATE(local_solutions)
        IF (ALLOCATED(local_solutions_values)) DEALLOCATE(local_solutions_values)        
        
        CALL destroy_refsettype(solutionset)
        CALL destroy_refsettype(refset)
        CALL destroy_resultsf(results)
        
    END FUNCTION sscattersearch

END MODULE scattersearch
