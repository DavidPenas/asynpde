MODULE misqp_interface
    USE iso_c_binding
    USE scattersearchtypes
    USE common_functions
    USE qsort_module
CONTAINS

SUBROUTINE EVALUATE_GRADIENT ( problem1,exp1,opts1, X_value, f_value,  g_value, ncont, nfunc, m, df, dg, fitnessfunction_mysqp, &
    neq, fitnessfunction )
    TYPE(problem), INTENT(INOUT) :: problem1
    
END SUBROUTINE EVALUATE_GRADIENT

SUBROUTINE RUN_MISQP( problem1,exp1,opts1,fitnessfunction, X_value, f_value, array_stop, stopval,NPROC,neval )
       IMPLICIT NONE
       TYPE(problem), INTENT(INOUT) :: problem1
       TYPE(opts), INTENT(INOUT) :: opts1
       TYPE(C_PTR), INTENT(INOUT) :: exp1, array_stop
       INTEGER, INTENT(INOUT) :: stopval, NPROC, neval
       REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), INTENT(INOUT) ::f_value
       TYPE(C_FUNPTR), INTENT(INOUT) :: fitnessfunction 
       REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE, INTENT(INOUT):: X_value    
       !INTEGER, INTENT(INOUT) :: nfunc
       
       REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: CCUU
       REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: CCLL
       REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: U, DF
       REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE :: B, DG
       INTEGER, DIMENSION(:), ALLOCATABLE :: N_UPPER, N_LOWER, N_UPPER_AUX, N_LOWER_AUX
       INTEGER, DIMENSION(:), ALLOCATABLE :: CLINF, CUINF
       CHARACTER(len = 2) :: typesearch
       INTEGER :: i,j,delete, contador
       INTEGER :: ncont, n, m, mme, iprint, ngrad
       REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)) :: acc
       
! VARIABLES RUN_MISQP
       ! ALLOCATION MEMORY LOCAL SOLVER MISQP
       REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE :: g_value
       
       ! working arrays
       REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: RW
       INTEGER, DIMENSION(:), ALLOCATABLE :: IW, LRW, LIW, LLW
       LOGICAL :: LW,RESOPT
       INTEGER :: IFAIL, MAXIT, MAXPEN, MAXUND, MODE, NONMON
       REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)) :: accqp
       
               
! SSM_AUX_LOCAL CODE
       IF (ALLOCATED(problem1%CU) ) THEN
           ALLOCATE(CCUU(size(problem1%CU)))
           ALLOCATE(CCLL(size(problem1%CL)))
           CCUU = problem1%CU
           CCLL = problem1%CL
           
           ALLOCATE(N_UPPER(size(problem1%CU)))
           ALLOCATE(N_LOWER(size(problem1%CL)))
           N_UPPER =  (/ (i, i = 1, size(problem1%CU)) /) 
           N_LOWER = N_UPPER
           
        typesearch = "EQ"
        CALL find_in_vector(problem1%CU, INFINITE, CUINF, typesearch)  
        if (ALLOCATED(CUINF)) then
            CALL reajust_index(CUINF)
            ALLOCATE(N_UPPER_AUX( SIZE(N_UPPER) - SIZE(CUINF)  ) )
            contador = 1
            DO i=1, SIZE(N_UPPER)
                delete = 0
                DO j=1, SIZE(CUINF)
                    IF (i .EQ. CUINF(j) ) THEN
                        delete = 1
                        EXIT
                    END IF
                END DO
                IF (delete .NE. 1 ) THEN
                    N_UPPER_AUX(contador) = N_UPPER(i)
                    contador = contador + 1
                END IF
            END DO
            CALL MOVE_ALLOC(N_UPPER_AUX, N_UPPER)
            DEALLOCATE(CUINF)
        end if
        
        CALL find_in_vector(problem1%CL, -INFINITE, CLINF, typesearch)  
        if (ALLOCATED(CLINF)) then
            CALL reajust_index(CLINF)
           ALLOCATE(N_LOWER_AUX( SIZE(N_LOWER) - SIZE(CLINF)  ) )
            contador = 1
            DO i=1, SIZE(N_LOWER)
                delete = 0
                DO j=1, SIZE(CLINF)
                    IF (i .EQ. CLINF(j) ) THEN
                        delete = 1
                        EXIT
                    END IF
                END DO
                IF (delete .NE. 1 ) THEN
                    N_LOWER_AUX(contador) = N_LOWER(i)
                    contador = contador + 1
                END IF
            END DO
            CALL MOVE_ALLOC(N_LOWER_AUX, N_LOWER)     
            DEALLOCATE(CLINF)
        end if        
        
       END IF
       
       
! SSM_LOCAL_SOLVER
       ncont = size(X_value)
       n = problem1%int_var + problem1%bin_var + ncont
       m = size(N_UPPER)+size(N_LOWER)+problem1%neq
       
       IF (opts1%localoptions%tol .EQ. 1 ) THEN
           acc=opts1%useroptions%tolc*100d0
       ELSE IF (opts1%localoptions%tol .EQ. 2 ) THEN
           acc=opts1%useroptions%tolc
       ELSE IF (opts1%localoptions%tol .EQ. 3 ) THEN
           acc=opts1%useroptions%tolc/100d0
       END IF
       
       
! RUN_MISQP
       
       ALLOCATE(U (m+n+n) )
       ALLOCATE(B (n+1,n+1)  )
       ALLOCATE(DG (m+problem1%neq+1, n+1) )
       ALLOCATE(DF (n+1) )
       U=0
       B=0
       DG=0
       DF=0
       
       
       ifail = 0
       maxit=500
       maxpen=50
       maxund=50
       mode=0
       accqp = 1d-3
       resopt=.TRUE.
       nonmon=2
       mme=m+problem1%neq
       iprint = 0
       !nfunc = 0
       ngrad = 0
       !nfunc = nfunc + 1
       
       DO WHILE (ifail <= 0) 
           
       END DO
       
! SSM_LOCAL_SOLVER
        !if ~isnan(fval) && ~isinf(fval) && all(g>=-tolc)
        !  exitflag=1;
        !else
        !  exitflag=-1;
        !end 
        ! numeval = n_fun_eval
       
! END
       IF (ALLOCATED(N_UPPER) ) DEALLOCATE(N_UPPER)
       IF (ALLOCATED(N_LOWER) ) DEALLOCATE(N_LOWER)
       IF (ALLOCATED(U) ) DEALLOCATE(U)
       IF (ALLOCATED(B) ) DEALLOCATE(B)
       IF (ALLOCATED(DG) ) DEALLOCATE(DG)
       IF (ALLOCATED(DF) ) DEALLOCATE(DF)
END SUBROUTINE RUN_MISQP
    
    
    
    
    
END MODULE misqp_interface