#define TIMEPAR 1

MODULE scattersearchfunctions
    USE iso_c_binding
    USE scattersearchtypes
    USE common_functions
    USE qsort_module
    USE funcevalinterface
CONTAINS
    
    
    
! ----------------------------------------------------------------------
! SUBROUTINE PROBLEM_SPECIFICATIONS
! ----------------------------------------------------------------------
    SUBROUTINE problem_specifications(exp1,problem1,opts1,nvar,ftarget,maxfunevals) 
        IMPLICIT NONE
        TYPE(problem), INTENT(INOUT) :: problem1
        TYPE(opts), INTENT(INOUT) :: opts1
        INTEGER, INTENT(INOUT) :: nvar
        INTEGER(C_LONG), INTENT(INOUT) :: maxfunevals
        REAL(C_DOUBLE), INTENT(INOUT) ::ftarget
        INTEGER(C_INT) :: empty1, empty2, empty3, chargeboundsnconst, chargeproblemargs
        INTEGER(C_INT) :: error, chargeuseroptions, chargelocaloptions, chargeglobaloptions, chargebounds, chargedimension
        TYPE(C_PTR), INTENT(INOUT) :: exp1
        INTEGER :: i,status, logar
        TYPE(opts) ::  default1
        CHARACTER(kind=C_CHAR, len=20), POINTER  :: del, fin, sol
        CHARACTER(kind=C_CHAR, len=20), TARGET  :: del2, fin2, sol2
        REAL (C_DOUBLE) :: tolf,tolx,prob_bound, tolc
        REAL (C_FLOAT)  :: thfactor, maxdistfactor, balance
        
        CALL create_new_problem(problem1)
        CALL create_new_opts(opts1)
        
        
        del2 = opts1%globaloptions%delete
        if (ALLOCATED(opts1%localoptions%finish)) THEN
            fin2 = opts1%localoptions%finish(1)
        END IF
        fin2=""
        sol2 = opts1%globaloptions%delete
        del => del2
        fin => fin2
        sol => sol2
        
        problem1%empty = 0
        opts1%empty=0
        opts1%useroptions%maxeval = maxfunevals
        if (.not. ALLOCATED(problem1%vtr) ) ALLOCATE(problem1%vtr(1))
        problem1%vtr(1) = ftarget
        
        empty1 = chargeuseroptions(exp1,opts1%useroptions%maxtime, opts1%useroptions%weight, tolc, &
            prob_bound, opts1%useroptions%nstuck_solution, opts1%useroptions%strategy, &
            opts1%useroptions%inter_save, opts1%useroptions%iterprint, opts1%useroptions%plot, logar, &
            opts1%useroptions%init_point )
            
            opts1%useroptions%tolc = tolc 
            opts1%useroptions%prob_bound = prob_bound
        if ( empty1 .eq. 1 ) then
            opts1%useroptions%empty = 0
        else 
            opts1%useroptions%empty = 1
        end if
        
        empty2 = chargeglobaloptions(exp1,opts1%globaloptions%dim_refset,opts1%globaloptions%ndiverse,opts1%globaloptions%initiate,&
            opts1%globaloptions%combination, opts1%globaloptions%regenerate, del, &
            opts1%globaloptions%intens, tolf, opts1%globaloptions%diverse_criteria, &
            tolx,opts1%globaloptions%n_stuck)
            
            opts1%globaloptions%tolf = tolf
            opts1%globaloptions%tolx = tolx
            
        if ( empty2 .eq. 1 ) then 
            opts1%globaloptions%empty = 0
        else 
            opts1%globaloptions%empty = 1  
        end if
        empty3 = chargelocaloptions(exp1,opts1%localoptions%tol,opts1%localoptions%iterprint,opts1%localoptions%n1, &
            opts1%localoptions%n2,balance, fin, opts1%localoptions%bestx, &
            opts1%localoptions%merit_filter, opts1%localoptions%distance_filter, thfactor, &
            maxdistfactor, opts1%localoptions%wait_maxdist_limit, opts1%localoptions%wait_th_limit, &
            sol, opts1%localoptions%threshold_local)
            
            opts1%localoptions%thfactor = thfactor
            opts1%localoptions%maxdistfactor = maxdistfactor
            opts1%localoptions%balance = balance
            
        if ( empty3 .eq. 1 ) then 
            opts1%localoptions%empty = 0
        else
            opts1%localoptions%empty = 1   
        end if
        
        if ( (empty3 .eq. 1) .or. (empty2 .eq. 1) .or. (empty1 .eq. 1) ) then
            opts1%empty = 0
        else
            opts1%empty = 1
        end if
            
        if ( opts1%localoptions%empty .eq. 0 ) then
            opts1%localoptions%solver = sol
            if ( fin .NE.  "" ) THEN
             ALLOCATE(opts1%localoptions%finish(1))
             opts1%localoptions%finish(1) = fin
            END IF            
            opts1%globaloptions%delete = del
            opts1%localoptions%extrap%empty= 1
        end if
        
        error = chargedimension(exp1,nvar)
        
        ALLOCATE(problem1%XU(nvar))
        ALLOCATE(problem1%XL(nvar))
        error = chargeproblemargs(exp1,problem1%ineq,problem1%int_var,problem1%bin_var)

        if ( problem1%ineq .GT. 0 )  then 
            ALLOCATE(problem1%CU(problem1%ineq))
            ALLOCATE(problem1%CL(problem1%ineq))
            error = chargeboundsnconst(exp1, problem1%XU, problem1%XL, nvar, problem1%CU, problem1%CL, problem1%ineq )
        else
            error = chargebounds(exp1, problem1%XU, problem1%XL, nvar )
        end if
        
            
        if (logar .eq. 1) then
            if (.not.ALLOCATED(opts1%useroptions%log_var)) then
                ALLOCATE(opts1%useroptions%log_var(nvar))
            end if
            opts1%useroptions%log_var = (/ (i, i = 1, nvar) /)
        end if
            
        IF ((opts1%useroptions%maxeval < 0) .AND. (opts1%useroptions%maxtime < 0)) THEN
            PRINT *, 'WARNING:Either opts.maxeval or opts.maxtime must be defined as a stop criterion '
            PRINT *, 'Define any of these options and rerun '
            CALL EXIT(status)
        ELSE
            IF (opts1%useroptions%maxeval < 0) THEN
                opts1%useroptions%maxeval = INT(1e12, KIND = C_LONG_LONG)
            END IF

            IF (opts1%useroptions%maxtime < 0) THEN
                opts1%useroptions%maxtime = INT(1e12, KIND = C_LONG_LONG)
            END IF
        END IF

        ! CARGA DE PARAMETROS POR DEFECTO 
        default1 = ssm_default()
        opts1 = ssm_optset(default1, opts1)
        
        !CALL print_problems(problem1)
        !CALL print_opts(opts1)
            
    END SUBROUTINE problem_specifications
        
    
    
! ----------------------------------------------------------------------
! SUBROUTINE DESTROY_PROBLEM
! ----------------------------------------------------------------------
    SUBROUTINE destroy_problem(problem1)
        IMPLICIT NONE
        TYPE(problem), INTENT(INOUT) :: problem1

        if (ALLOCATED(problem1%XL) ) then
            DEALLOCATE(problem1%XL)
        end if
        if (ALLOCATED(problem1%XU) ) then
            DEALLOCATE(problem1%XU)
        end if
        if (ALLOCATED(problem1%CL) ) then
            DEALLOCATE(problem1%CL)
        end if
        if (ALLOCATED(problem1%CU) ) then
            DEALLOCATE(problem1%CU)
        end if
        if (ALLOCATED(problem1%X0) ) then
            DEALLOCATE(problem1%X0)
        end if
        if (ALLOCATED(problem1%F0) ) then
            DEALLOCATE(problem1%F0)
        end if
        
        if (ALLOCATED(problem1%vtr)) DEALLOCATE(problem1%vtr)

    END SUBROUTINE destroy_problem
    
    
    
! ----------------------------------------------------------------------
! SUBROUTINE DESTROY_OPTS
! ----------------------------------------------------------------------
    SUBROUTINE destroy_opts(opts1)
        IMPLICIT NONE
        TYPE(opts), INTENT(INOUT) :: opts1

        if (ALLOCATED(opts1%useroptions%log_var) ) then
            DEALLOCATE(opts1%useroptions%log_var)
        end if
        if (ALLOCATED(opts1%localoptions%finish) ) then
            DEALLOCATE(opts1%localoptions%finish)
        end if

    END SUBROUTINE destroy_opts
    
    
    
! ----------------------------------------------------------------------
! FUNCTION SSM_DEFAULT
! ----------------------------------------------------------------------
    TYPE(opts) FUNCTION ssm_default()
        IMPLICIT NONE
        TYPE(opts) :: default1
        
        default1%empty = 0
        
        default1%useroptions%empty = 0
        default1%useroptions%nstuck_solution = 20
        default1%useroptions%maxeval = 1000
        default1%useroptions%maxtime = 60
        default1%useroptions%iterprint = 1
        default1%useroptions%plot = 0
        default1%useroptions%weight = 1d6
        default1%useroptions%tolc = REAL(1d-5,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))
        default1%useroptions%prob_bound = REAL(0.5d0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))
        default1%useroptions%strategy = 0
        default1%useroptions%inter_save = 0
        default1%useroptions%init_point = 0

        default1%globaloptions%empty = 0
        default1%globaloptions%dim_refset = -1
        default1%globaloptions%ndiverse = -1
        default1%globaloptions%initiate = 1
        default1%globaloptions%combination = 1
        default1%globaloptions%regenerate = 3
        default1%globaloptions%delete = 'standard'
        default1%globaloptions%intens = 10
        default1%globaloptions%tolf = REAL(1e-4,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))
        default1%globaloptions%diverse_criteria = 1
        default1%globaloptions%tolx = REAL(1e-3,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))
        default1%globaloptions%n_stuck = 0

        default1%localoptions%empty = 1
        default1%localoptions%solver = 'fmincon'
        default1%localoptions%tol = 2
        default1%localoptions%iterprint = 0
        default1%localoptions%n1 = -1
        default1%localoptions%n2 = -1
        default1%localoptions%balance = REAL(-1d0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))
        

        
        default1%localoptions%bestx = 0
        default1%localoptions%merit_filter = 1
        default1%localoptions%distance_filter = 1
        default1%localoptions%thfactor = REAL(0.2,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))
        default1%localoptions%maxdistfactor = REAL(0.2,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))
        default1%localoptions%wait_maxdist_limit = 20
        default1%localoptions%wait_th_limit = 20
        default1%localoptions%threshold_local = 1d-1
        default1%localoptions%extrap%empty = 1

        ssm_default = default1
    END FUNCTION ssm_default
        
    
    
! ----------------------------------------------------------------------
! SUBROUTINE CREATE_NEW_OPTS
! ----------------------------------------------------------------------
    SUBROUTINE create_new_opts(opts1) 
        IMPLICIT NONE
        TYPE(opts), INTENT(INOUT) :: opts1
        
        opts1%empty = 1
        
        opts1%useroptions%empty = 1
        opts1%useroptions%nstuck_solution = -1
        opts1%useroptions%maxeval = -1
        opts1%useroptions%maxtime = -1
        opts1%useroptions%iterprint = -1
        opts1%useroptions%plot = -1
        opts1%useroptions%weight = -1
        opts1%useroptions%tolc = -1
        opts1%useroptions%prob_bound = -1
        opts1%useroptions%strategy = -1
        opts1%useroptions%inter_save = -1
        opts1%useroptions%init_point = -1

        opts1%globaloptions%empty = 1
        opts1%globaloptions%dim_refset = -1
        opts1%globaloptions%ndiverse = -1
        opts1%globaloptions%initiate = -1
        opts1%globaloptions%combination = -1
        opts1%globaloptions%regenerate = -1
        opts1%globaloptions%delete = " "
        opts1%globaloptions%intens = -1
        opts1%globaloptions%tolf = -1
        opts1%globaloptions%diverse_criteria = -1
        opts1%globaloptions%tolx = -1
        opts1%globaloptions%n_stuck = -1

        opts1%localoptions%empty = 1
        opts1%localoptions%solver = " "
        opts1%localoptions%tol = -1
        opts1%localoptions%iterprint = -1
        opts1%localoptions%n1 = -1
        opts1%localoptions%n2 = -1
        opts1%localoptions%balance = -1d0
        opts1%localoptions%bestx = -1
        opts1%localoptions%merit_filter = -1
        opts1%localoptions%distance_filter = -1
        opts1%localoptions%thfactor = -1
        opts1%localoptions%maxdistfactor = -1
        opts1%localoptions%wait_maxdist_limit = -1
        opts1%localoptions%wait_th_limit = -1
        opts1%localoptions%extrap%empty = 1
        opts1%localoptions%threshold_local = -1d0
    END SUBROUTINE
        
    
    
! ----------------------------------------------------------------------
! SUBROUTINE CREATE_NEW_PROBLEM
! ----------------------------------------------------------------------
    SUBROUTINE create_new_problem(problem1)
        IMPLICIT NONE
        TYPE(problem), INTENT(INOUT) :: problem1
        
        problem1%empty = 1        
        problem1%f = ''
        problem1%neq = -1
        problem1%int_var = -1
        problem1%bin_var = -1
        
    END SUBROUTINE create_new_problem
        
    
    
! ----------------------------------------------------------------------
! FUNCTION SSM_OPTSET
! ----------------------------------------------------------------------
    TYPE(opts) FUNCTION ssm_optset(default1,opts1)
        IMPLICIT NONE
        TYPE(opts), INTENT(INOUT):: default1, opts1
        INTEGER :: DIMEN(2)
        
        ! Aquí hai un código para recolocar o campo local da estructura de matlab  opts na última posición da estructura.
        ! En principio non vexo sentido en fortran
        IF (opts1%empty .eq. 0) THEN
            default1%empty= opts1%empty 
            if (opts1%useroptions%empty .eq. 0) then

                default1%useroptions%empty = opts1%useroptions%empty 
                
                if  (ALLOCATED(opts1%useroptions%log_var)) then
                    ALLOCATE(default1%useroptions%log_var(size(opts1%useroptions%log_var)))
                    default1%useroptions%log_var = opts1%useroptions%log_var
                end if
                if  (opts1%useroptions%nstuck_solution >= 0) then
                    default1%useroptions%nstuck_solution = opts1%useroptions%nstuck_solution
                end if                
                if  (opts1%useroptions%maxeval >= 0) then
                    default1%useroptions%maxeval = opts1%useroptions%maxeval
                end if
                if  (opts1%useroptions%maxtime >= 0) then
                    default1%useroptions%maxtime = opts1%useroptions%maxtime
                end if
                if  (opts1%useroptions%iterprint >= 0) then
                    default1%useroptions%iterprint = opts1%useroptions%iterprint
                end if 
                if  (opts1%useroptions%plot >= 0) then
                    default1%useroptions%plot = opts1%useroptions%plot
                end if   
                if  (opts1%useroptions%weight >= 0) then
                    default1%useroptions%weight = opts1%useroptions%weight
                end if     
                if  (opts1%useroptions%tolc >= 0) then
                    default1%useroptions%tolc = opts1%useroptions%tolc
                end if           
                if  (opts1%useroptions%prob_bound >= 0) then
                    default1%useroptions%prob_bound = opts1%useroptions%prob_bound
                end if      
                if  (opts1%useroptions%strategy >= 0) then
                    default1%useroptions%strategy = opts1%useroptions%strategy
                end if   
                if  (opts1%useroptions%inter_save >= 0) then
                    default1%useroptions%inter_save = opts1%useroptions%inter_save
                end if    
                if  (opts1%useroptions%init_point >= 0) then
                    default1%useroptions%init_point = opts1%useroptions%init_point
                end if   
            else
                default1%useroptions%empty = opts1%useroptions%empty
            end if    
            
            if (opts1%globaloptions%empty .eq. 0) then
                default1%globaloptions%empty = opts1%globaloptions%empty 
                if  (opts1%globaloptions%dim_refset >= 0) then
                    default1%globaloptions%dim_refset = opts1%globaloptions%dim_refset
                end if
                if  (opts1%globaloptions%ndiverse >= 0) then
                    default1%globaloptions%ndiverse = opts1%globaloptions%ndiverse
                end if     
                if  (opts1%globaloptions%initiate >= 0) then
                    default1%globaloptions%initiate = opts1%globaloptions%initiate
                end if    
                if  (opts1%globaloptions%combination > 0) then
                    default1%globaloptions%combination = opts1%globaloptions%combination
                end if  
                if  (opts1%globaloptions%regenerate >= 0) then
                    default1%globaloptions%regenerate = opts1%globaloptions%regenerate
                end if 
                if  (opts1%globaloptions%delete .NE. '') then
                    default1%globaloptions%delete = opts1%globaloptions%delete
                end if    
                if  (opts1%globaloptions%intens >= 0) then
                    default1%globaloptions%intens = opts1%globaloptions%intens
                end if    
                if  (opts1%globaloptions%tolf >= 0) then
                    default1%globaloptions%tolf = opts1%globaloptions%tolf
                end if     
                if  (opts1%globaloptions%diverse_criteria >= 0) then
                    default1%globaloptions%diverse_criteria = opts1%globaloptions%diverse_criteria
                end if   
                if  (opts1%globaloptions%tolx >= 0) then
                    default1%globaloptions%tolx = opts1%globaloptions%tolx
                end if          
                if  (opts1%globaloptions%n_stuck >= 0) then
                    default1%globaloptions%n_stuck = opts1%globaloptions%n_stuck
                end if   
            else
                default1%globaloptions%empty = opts1%globaloptions%empty
            end if

            if (opts1%localoptions%empty .eq. 0) then
                default1%localoptions%empty = opts1%localoptions%empty 
                if  (opts1%localoptions%solver .NE. '') then
                    default1%localoptions%solver = opts1%localoptions%solver
                end if  
                if  (opts1%localoptions%tol >= 0) then
                    default1%localoptions%tol = opts1%localoptions%tol
                end if    
                if  (opts1%localoptions%iterprint >= 0) then
                    default1%localoptions%iterprint = opts1%localoptions%iterprint
                end if   
                if  (opts1%localoptions%n1 .GE. 0) then
                    default1%localoptions%n1 = opts1%localoptions%n1
                else
                    opts1%localoptions%empty = 1
                end if         
                if  (opts1%localoptions%n2 .GE. -1) then
                    default1%localoptions%n2 = opts1%localoptions%n2
                end if     
                if  (opts1%localoptions%balance  >= 0) then
                    default1%localoptions%balance = opts1%localoptions%balance
                end if     
                if  (opts1%localoptions%bestx  >= 0) then
                    default1%localoptions%bestx = opts1%localoptions%bestx
                end if       
                if  (opts1%localoptions%merit_filter  >= 0) then
                    default1%localoptions%merit_filter = opts1%localoptions%merit_filter
                end if        
                if  (opts1%localoptions%distance_filter  >= 0) then
                    default1%localoptions%distance_filter = opts1%localoptions%distance_filter
                end if    
                if  (opts1%localoptions%thfactor  >= 0) then
                    default1%localoptions%thfactor = opts1%localoptions%thfactor
                end if   
                if  (opts1%localoptions%maxdistfactor  >= 0) then
                    default1%localoptions%maxdistfactor = opts1%localoptions%maxdistfactor
                end if   
                if  (opts1%localoptions%wait_maxdist_limit  >= 0) then
                    default1%localoptions%wait_maxdist_limit = opts1%localoptions%wait_maxdist_limit
                end if   
                if  (opts1%localoptions%wait_th_limit  >= 0) then
                    default1%localoptions%wait_th_limit = opts1%localoptions%wait_th_limit
                end if      
                if  (opts1%localoptions%threshold_local  >= 0) then
                    default1%localoptions%threshold_local = opts1%localoptions%threshold_local
                end if                  
                if (ALLOCATED(opts1%localoptions%finish) ) then
                    ALLOCATE(default1%localoptions%finish(1) )
                    default1%localoptions%finish = opts1%localoptions%finish
                end if
                if (opts1%localoptions%extrap%empty .eq. 0) then
                    if (default1%localoptions%solver .EQ. 'n2fb') then
                        if (ALLOCATED(opts1%localoptions%extrap%texp)) then
                            ALLOCATE(default1%localoptions%extrap%texp(size(default1%localoptions%extrap%texp )))
                            default1%localoptions%extrap%texp = opts1%localoptions%extrap%texp
                        end if
                        DIMEN = shape(opts1%localoptions%extrap%yexp)
                        if (ALLOCATED(opts1%localoptions%extrap%yexp)) then
                            ALLOCATE(default1%localoptions%extrap%yexp(DIMEN(1),DIMEN(2)))
                            default1%localoptions%extrap%yexp = opts1%localoptions%extrap%yexp
                        end if
                    end if
                    if (default1%localoptions%solver .EQ. 'solnp') then
                        default1%localoptions%extrap%k1 = opts1%localoptions%extrap%k1
                        default1%localoptions%extrap%k2 = opts1%localoptions%extrap%k2
                        default1%localoptions%extrap%k3 = opts1%localoptions%extrap%k3
                        default1%localoptions%extrap%k4 = opts1%localoptions%extrap%k4
                    end if
                end if
            else
                default1%localoptions%empty = opts1%localoptions%empty
            end if
        else
            default1%empty = opts1%empty 
        END IF   
        
        ssm_optset = default1
    END FUNCTION ssm_optset
        
    
    
! ----------------------------------------------------------------------
! SUBROUTINE PRINT_OPTS
! ----------------------------------------------------------------------
    SUBROUTINE print_opts(opts1) 
        IMPLICIT NONE
        TYPE(opts), INTENT(IN) :: opts1
        
        PRINT *, "OPTS OPTIONS ::"
        PRINT *, " opts1%useroptions%maxeval =", opts1%useroptions%maxeval
        PRINT *, " opts1%useroptions%maxtime =", opts1%useroptions%maxtime
        PRINT *, " opts1%useroptions%iterprint =", opts1%useroptions%iterprint
        PRINT *, " opts1%useroptions%plot =", opts1%useroptions%plot
        PRINT *, " opts1%useroptions%weight =", opts1%useroptions%weight 
        PRINT *, " opts1%useroptions%tolc =", opts1%useroptions%tolc 
        PRINT *, " opts1%useroptions%prob_bound =", opts1%useroptions%prob_bound 
        PRINT *, " opts1%useroptions%strategy =", opts1%useroptions%strategy 
        PRINT *, " opts1%useroptions%inter_save =", opts1%useroptions%inter_save

        PRINT *, " opts1%globaloptions%empty =", opts1%globaloptions%empty
        PRINT *, " opts1%globaloptions%dim_refset =", opts1%globaloptions%dim_refset
        PRINT *, " opts1%globaloptions%ndiverse =", opts1%globaloptions%ndiverse
        PRINT *, " opts1%globaloptions%initiate =", opts1%globaloptions%initiate 
        PRINT *, " opts1%globaloptions%combination =", opts1%globaloptions%combination 
        PRINT *, " opts1%globaloptions%regenerate =", opts1%globaloptions%regenerate
        PRINT *, " opts1%globaloptions%delete =", opts1%globaloptions%delete
        PRINT *, " opts1%globaloptions%intens =", opts1%globaloptions%intens
        PRINT *, " opts1%globaloptions%tolf =", opts1%globaloptions%tolf 
        PRINT *, " opts1%globaloptions%diverse_criteria =", opts1%globaloptions%diverse_criteria 
        PRINT *, " opts1%globaloptions%tolx =", opts1%globaloptions%tolx 
        PRINT *, " opts1%globaloptions%n_stuck =", opts1%globaloptions%n_stuck 

        PRINT *, " opts1%localoptions%empty =", opts1%localoptions%empty 
        PRINT *, " opts1%localoptions%solver =", opts1%localoptions%solver 
        PRINT *, " opts1%localoptions%tol =", opts1%localoptions%tol 
        PRINT *, " opts1%localoptions%iterprint =", opts1%localoptions%iterprint 
        PRINT *, " opts1%localoptions%n1 =", opts1%localoptions%n1 
        PRINT *, " opts1%localoptions%n2 =", opts1%localoptions%n2 
        PRINT *, " opts1%localoptions%balance =", opts1%localoptions%balance 
        PRINT *, " opts1%localoptions%bestx =", opts1%localoptions%bestx 
        PRINT *, " opts1%localoptions%merit_filter =", opts1%localoptions%merit_filter
        PRINT *, " opts1%localoptions%distance_filter =", opts1%localoptions%distance_filter 
        PRINT *, " opts1%localoptions%thfactor =", opts1%localoptions%thfactor 
        PRINT *, " opts1%localoptions%maxdistfactor =", opts1%localoptions%maxdistfactor 
        PRINT *, " opts1%localoptions%wait_maxdist_limit =", opts1%localoptions%wait_maxdist_limit 
        PRINT *, " opts1%localoptions%wait_th_limit =", opts1%localoptions%wait_th_limit
        if (ALLOCATED(opts1%localoptions%finish) ) then
            PRINT *, " opts1%localoptions%finish =", opts1%localoptions%finish
        end if
        if (opts1%localoptions%extrap%empty .eq. 0) then
            if (opts1%localoptions%solver .EQ. 'n2fb') then
                PRINT *, " opts1%localoptions%extrap%texp =", opts1%localoptions%extrap%texp
                PRINT *, " opts1%localoptions%extrap%yexp =", opts1%localoptions%extrap%yexp
            end if
            if (opts1%localoptions%solver .EQ. 'solnp') then
                PRINT *, " opts1%localoptions%extrap%k1 =", opts1%localoptions%extrap%k1
                PRINT *, " opts1%localoptions%extrap%k2 =", opts1%localoptions%extrap%k2
                PRINT *, " opts1%localoptions%extrap%k3 =", opts1%localoptions%extrap%k3
                PRINT *, " opts1%localoptions%extrap%k4 =", opts1%localoptions%extrap%k4
            end if
        end if
        PRINT *, "END PRINT"
    END SUBROUTINE  
        
    
    
! ----------------------------------------------------------------------
! SUBROUTINE PRINT_PROBLEMS
! ----------------------------------------------------------------------
    SUBROUTINE print_problems(problem1)
        IMPLICIT NONE
        TYPE(problem), INTENT(IN) :: problem1
        
        PRINT *, "PROBLEM OPTIONS ::"
        PRINT *, " problem1%empty =",problem1%empty     
        PRINT *, " problem1%f =",problem1%f
        PRINT *, " problem1%neq =",problem1%neq
        PRINT *, " problem1%int_var =",problem1%int_var
        PRINT *, " problem1%bin_var =",problem1%bin_var 
        
    END SUBROUTINE print_problems
    
    
    
    
    
    
    
! ----------------------------------------------------------------------
! SUBROUTINES NORMALIZE_VECTOR
! ----------------------------------------------------------------------
!    SUBROUTINE normalize_vector (vector)
!        IMPLICIT NONE
!!        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), INTENT(INOUT) :: vector
!        INTEGER :: i
        
!        do i=1,size(vector)
!            vector(i) = exp(vector(i))
!        end do
!        
!    END SUBROUTINE normalize_vector
        
    
    
! ----------------------------------------------------------------------
! SUBROUTINES log_vector
! ----------------------------------------------------------------------
    SUBROUTINE log_vector (vector)
        IMPLICIT NONE
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), INTENT(INOUT) :: vector
        INTEGER :: i
        
        do i=1,size(vector)
            if (vector(i) .eq. 0) then
                vector(i) = REAL(0.001d0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))
            end if
            vector(i) = log(vector(i))
        end do
        
    END SUBROUTINE log_vector  
        
    
    
! ----------------------------------------------------------------------
! SUBROUTINES ssm_beyond
! ----------------------------------------------------------------------
! z1 --> father in Refset
! z2 --> solution generate -- Child
    SUBROUTINE ssm_beyond ( exp1, z1, z2, z2_val, nfuneval, &
        fitnessfunction,nrand,nconst, outfunction, opts1, problem1, idopenmp)
        
        IMPLICIT NONE
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE, INTENT(INOUT) :: z1, z2
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), INTENT(INOUT) :: z2_val
        INTEGER, INTENT(IN) :: idopenmp
        TYPE(opts), INTENT(IN) :: opts1
        TYPE(problem), INTENT(IN) :: problem1
        TYPE(outfuction), INTENT(INOUT) :: outfunction
        INTEGER(C_LONG), INTENT(INOUT) :: nfuneval
        TYPE(C_PTR), INTENT(INOUT) :: exp1
        TYPE(C_FUNPTR), INTENT(INOUT) :: fitnessfunction ! fobj
        INTEGER, INTENT(IN) :: nconst, nrand
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)) :: denom
        INTEGER ::  continuar, n_improve, sizer
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: delta
        INTEGER, DIMENSION(:), ALLOCATABLE :: index_result, index_out_bound
        CHARACTER(len = 2) :: typesearch
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: aux_zv, xnew, xnew2
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE :: zv, randommatrix
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)) :: random
        TYPE(outfuction):: outf
        INTEGER :: nvar
        
        
        nvar = size(problem1%XL)
        continuar = 1
        denom = REAL(1,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))
        n_improve = 1
        
        ALLOCATE(delta(size(z1)))
        ALLOCATE(zv(nvar,2))
        ALLOCATE(xnew(nvar))
        ALLOCATE(xnew2(nvar))
        ALLOCATE(randommatrix(nrand,1))
            

        do while (continuar .eq. 1)

            ! CREAMOS O RECTÁNGULO
            delta= ( z2-z1) / denom
            zv(:,1)=z2
            zv(:,2)=z2+delta
            
            
            ALLOCATE(aux_zv(size(z2)))
            
            aux_zv=zv(:,2)
            ! Comprobamos os bounds
            typesearch = "LT"
            ALLOCATE(index_result(nvar))
            index_result = compare_multiple_vector(aux_zv,problem1%XL, sizer,typesearch)   
            ALLOCATE(index_out_bound(size(index_result)))
            CALL index_of_ones(index_result,index_out_bound)
            CALL reajust_index(index_out_bound)
            DEALLOCATE(index_result)
            if (ALLOCATED(index_out_bound)) then
                random = RETURN_RANDOM_NUMBER(exp1)
                if (random>opts1%useroptions%prob_bound) then
                    zv(index_out_bound,2)=problem1%XL(index_out_bound)
                end if
                DEALLOCATE(index_out_bound)
            end if
            
            typesearch = "GT"
            ALLOCATE(index_result(size(problem1%XU)))
            index_result = compare_multiple_vector(aux_zv,problem1%XU, sizer,typesearch)   
            ALLOCATE(index_out_bound(size(index_result)))
            CALL index_of_ones(index_result,index_out_bound) 
            CALL reajust_index(index_out_bound)
            DEALLOCATE(index_result)
            if (ALLOCATED(index_out_bound)) then
                random = RETURN_RANDOM_NUMBER(exp1)
                if (random>opts1%useroptions%prob_bound) then
                    zv(index_out_bound,2)=problem1%XU(index_out_bound)
                end if
                DEALLOCATE(index_out_bound)
            end if      
            
            DEALLOCATE(aux_zv)
            
            CALL random_Matrix_SEED_10(exp1,randommatrix,nrand,1)
            
            xnew = zv(:,1)+(zv(:,2)-zv(:,1))*randommatrix(:,1)
            xnew2 = xnew
            outf = ssm_evalfc(exp1,fitnessfunction, xnew ,problem1, opts1, nconst,idopenmp)
            nfuneval = nfuneval + 1
            
            xnew = xnew2
               ! print *, "              ",xnew, " neval ", nfuneval
            if (outf%include .eq. 1) then
                !new_child%x(counter,:) = outfunct % x ....
                
                if (outf%value_penalty < z2_val) then
                    z1=z2
                    z2=xnew
                    z2_val=outf%value_penalty
                    n_improve = n_improve+1
                    if ( n_improve .eq. 2 ) then
                        ! vas haciendo el rectángulo más grande mientras no mejore
                        denom=denom/REAL(2.0d0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))
                        n_improve=0
                    end if
                    if (.not. ALLOCATED(outfunction%x) ) ALLOCATE(outfunction%x(size(outf%x)))
                    outfunction%x = outf%x
                    outfunction%value_penalty = outf%value_penalty
                    outfunction%value = outf%value
                    outfunction%pena = outf%pena
                    
                    if (ALLOCATED(outf%nlc) ) then 
                        if (.not. ALLOCATED(outfunction%nlc) )   ALLOCATE(outfunction%nlc(size(outf%nlc)))
                        outfunction%nlc = outf%nlc
                    end if
                    outfunction%include = outf%include
                else
                    continuar = 0
                end if
            else
                continuar = 0
            end if
            
            
            
        end do
        
        DEALLOCATE(delta)
        if (ALLOCATED (zv) ) DEALLOCATE(zv)
        DEALLOCATE(xnew)
        DEALLOCATE(randommatrix)    
            
        
        
    END SUBROUTINE ssm_beyond
    
    
    
    ! ----------------------------------------------------------------------
! SUBROUTINES ssm_beyond_det
! ----------------------------------------------------------------------
! z1 --> father in Refset
! z2 --> solution generate -- Child
    SUBROUTINE ssm_beyond_det ( exp1, z1, z2, z2_val, nfuneval, &
        fitnessfunction,nrand,nconst, outfunction, opts1, problem1, idopenmp, contador, matrixR)
        
        IMPLICIT NONE
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE, INTENT(INOUT) :: matrixR
        INTEGER, INTENT(INOUT) :: contador
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE, INTENT(INOUT) :: z1, z2
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), INTENT(INOUT) :: z2_val
        INTEGER, INTENT(IN) :: idopenmp
        TYPE(opts), INTENT(IN) :: opts1
        TYPE(problem), INTENT(IN) :: problem1
        TYPE(outfuction), INTENT(INOUT) :: outfunction
        INTEGER(C_LONG), INTENT(INOUT) :: nfuneval
        TYPE(C_PTR), INTENT(INOUT) :: exp1
        TYPE(C_FUNPTR), INTENT(INOUT) :: fitnessfunction ! fobj
        INTEGER, INTENT(IN) :: nconst, nrand
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)) :: denom
        INTEGER ::  continuar, n_improve, sizer
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: delta
        INTEGER, DIMENSION(:), ALLOCATABLE :: index_result, index_out_bound
        CHARACTER(len = 2) :: typesearch
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: aux_zv, xnew, xnew2
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE :: zv, randommatrix
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)) :: random
        TYPE(outfuction):: outf
        INTEGER :: nvar
        
        
        nvar = size(problem1%XL)
        continuar = 1
        denom = REAL(1,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))
        n_improve = 1
        
        ALLOCATE(delta(size(z1)))
        ALLOCATE(zv(nvar,2))
        ALLOCATE(xnew(nvar))
        ALLOCATE(xnew2(nvar))
        ALLOCATE(randommatrix(nrand,1))
            

        do while (continuar .eq. 1)

            ! CREAMOS O RECTÁNGULO
            delta= ( z2-z1) / denom
            zv(:,1)=z2
            zv(:,2)=z2+delta
            
            
            ALLOCATE(aux_zv(size(z2)))
            
            aux_zv=zv(:,2)
            ! Comprobamos os bounds
            typesearch = "LT"
            ALLOCATE(index_result(nvar))
            index_result = compare_multiple_vector(aux_zv,problem1%XL, sizer,typesearch)   
            ALLOCATE(index_out_bound(size(index_result)))
            CALL index_of_ones(index_result,index_out_bound)
            CALL reajust_index(index_out_bound)
            DEALLOCATE(index_result)
            if (ALLOCATED(index_out_bound)) then
                CALL random_det_NUMBER(random, contador, matrixR)
                if (random>opts1%useroptions%prob_bound) then
                    zv(index_out_bound,2)=problem1%XL(index_out_bound)
                end if
                DEALLOCATE(index_out_bound)
            end if
            
            typesearch = "GT"
            ALLOCATE(index_result(size(problem1%XU)))
            index_result = compare_multiple_vector(aux_zv,problem1%XU, sizer,typesearch)   
            ALLOCATE(index_out_bound(size(index_result)))
            CALL index_of_ones(index_result,index_out_bound) 
            CALL reajust_index(index_out_bound)
            DEALLOCATE(index_result)
            if (ALLOCATED(index_out_bound)) then
                CALL random_det_NUMBER(random, contador, matrixR)
                if (random>opts1%useroptions%prob_bound) then
                    zv(index_out_bound,2)=problem1%XU(index_out_bound)
                end if
                DEALLOCATE(index_out_bound)
            end if      
            
            DEALLOCATE(aux_zv)
            
            CALL random_det_Matrix_SEED_10(randommatrix,nrand,1,contador, matrixR)
            
            xnew = zv(:,1)+(zv(:,2)-zv(:,1))*randommatrix(:,1)
            xnew2 = xnew
            outf = ssm_evalfc(exp1,fitnessfunction, xnew ,problem1, opts1, nconst,idopenmp)
            
            nfuneval = nfuneval + 1
            
            xnew = xnew2
            
            if (outf%include .eq. 1) then
                !new_child%x(counter,:) = outfunct % x ....
                
                if (outf%value_penalty < z2_val) then
                    z1=z2
                    z2=xnew
                    z2_val=outf%value_penalty
                    n_improve = n_improve+1
                    if ( n_improve .eq. 2 ) then
                        ! vas haciendo el rectángulo más grande mientras no mejore
                        denom=denom/REAL(2.0d0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))
                        n_improve=0
                    end if
                    if (.not. ALLOCATED(outfunction%x) ) ALLOCATE(outfunction%x(size(outf%x)))
                    outfunction%x = outf%x
                    outfunction%value_penalty = outf%value_penalty
                    outfunction%value = outf%value
                    outfunction%pena = outf%pena
                    
                    if (ALLOCATED(outf%nlc) ) then 
                        if (.not. ALLOCATED(outfunction%nlc) )   ALLOCATE(outfunction%nlc(size(outf%nlc)))
                        outfunction%nlc = outf%nlc
                    end if
                    outfunction%include = outf%include
                else
                    continuar = 0
                end if
            else
                continuar = 0
            end if
            
            
            
        end do
        
        DEALLOCATE(delta)
        if (ALLOCATED (zv) ) DEALLOCATE(zv)
        DEALLOCATE(xnew)
        DEALLOCATE(randommatrix)    
            
        
        
    END SUBROUTINE ssm_beyond_det
    
    
! ----------------------------------------------------------------------
! SUBROUTINES create_init_solutions
! ----------------------------------------------------------------------    
    SUBROUTINE create_init_solutions(exp1,opts1, solutions,nvar,xl_log,xu_log)
        IMPLICIT NONE
        TYPE(C_PTR), INTENT(INOUT) :: exp1
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE, INTENT(INOUT) :: solutions
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:),   ALLOCATABLE, INTENT(IN) :: xl_log, xu_log
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: temp
        TYPE(opts), INTENT(IN) :: opts1
        INTEGER, INTENT(IN) :: nvar
        INTEGER :: j,i, sizet, error, bench;
        INTEGER(C_INT) :: openhdf5solutions, getbench
        !INTEGER(C_INT) :: savehdf5solutions
        !We generate 5 solutions
        sizet = opts1%globaloptions%ndiverse + 5
        ALLOCATE( solutions(nvar, sizet))
        i=1
        j=1
        solutions = reshape((/ ((REAL(0.0d0 , KIND = SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), i = 1, nvar) &
                , j = 1, sizet) /), (/ nvar, sizet /))
        
        ALLOCATE(temp(nvar))
        
        if (opts1 % useroptions % init_point .eq. 0) then
            do i = 1, 5
                CALL random_Vector_5(exp1,temp, nvar, xl_log, xu_log, i)
                
                if (ALLOCATED(opts1 % useroptions % log_var) .and. size(opts1%useroptions%log_var) .GT. 0) then
                        CALL converttonormal(temp, nvar)
                end if
                !CALL all_positive(temp)
                solutions(:,i) = temp
            end do
            
            
            do i = 6, sizet
                CALL random_Vector_SEED(exp1,temp, nvar, xl_log, xu_log)
                
                if (ALLOCATED(opts1 % useroptions % log_var) .and. size(opts1%useroptions%log_var) .GT. 0 ) then
                        CALL converttonormal(temp, nvar)
                end if
                !CALL all_positive(temp)
                
                solutions(:,i) = temp
            end do
            
            !error =  savehdf5solutions(solutions, nvar, sizet)
        else 
  
            error = openhdf5solutions(exp1, solutions,nvar,sizet)
              
            
        end if
        
            
        DEALLOCATE(temp)
        
    END SUBROUTINE create_init_solutions
    
    
! ----------------------------------------------------------------------
! SUBROUTINES create_init_solutions
! ----------------------------------------------------------------------    
    SUBROUTINE create_init_solutions_det(exp1,opts1, solutions,nvar,xl_log,xu_log, contador, matrixR)
        IMPLICIT NONE
        TYPE(C_PTR), INTENT(INOUT) :: exp1
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:),   ALLOCATABLE, INTENT(IN) :: matrixR
        INTEGER, INTENT(INOUT) :: contador       
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE, INTENT(INOUT) :: solutions
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:),   ALLOCATABLE, INTENT(IN) :: xl_log, xu_log
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: temp
        TYPE(opts), INTENT(IN) :: opts1
        INTEGER, INTENT(IN) :: nvar
        INTEGER :: j,i, sizet
        !We generate 5 solutions
        sizet = opts1%globaloptions%ndiverse + 5
        ALLOCATE( solutions(nvar, sizet))
        i=1
        j=1
        solutions = reshape((/ ((REAL(0.0d0 , KIND = SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), i = 1, nvar) &
                , j = 1, sizet) /), (/ nvar, sizet /))
        
        ALLOCATE(temp(nvar))
        
        !if (opts1 % useroptions % init_point .eq. 0) then
            do i = 1, 5
                CALL random_det_Vector_5(temp, nvar, xl_log, xu_log, i, contador, matrixR)
                if (ALLOCATED(opts1 % useroptions % log_var) .and. size(opts1%useroptions%log_var) .GT. 0) then
                        CALL converttonormal(temp, nvar)
                end if
                !CALL all_positive(temp)
                solutions(:,i) = temp
            end do
            
            
            do i = 6, sizet
                CALL random_det_Vector_SEED(temp, nvar, xl_log, xu_log, contador, matrixR)
                
                if (ALLOCATED(opts1 % useroptions % log_var) .and. size(opts1%useroptions%log_var) .GT. 0 ) then
                        CALL converttonormal(temp, nvar)
                end if
                !CALL all_positive(temp)
                
                solutions(:,i) = temp
            end do
            

        !end if

            
        
        DEALLOCATE(temp)
        
    END SUBROUTINE create_init_solutions_det
    
    
    
! ----------------------------------------------------------------------
! SUBROUTINES init_inequality_constraints
! ----------------------------------------------------------------------      
    SUBROUTINE init_inequality_constraints(neq, c_L,c_U)
        IMPLICIT NONE
        INTEGER, INTENT(INOUT) :: neq
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE, INTENT(INOUT) ::c_U, c_L
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: temp
        INTEGER :: i
        
        if (neq .GT. 0) then
            if (.not.allocated(c_L)) then
                ALLOCATE(c_L(neq))
                i=1
                c_L = (/ (REAL(0.0d0, KIND = SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), i = 1, neq) /)
            else
                ALLOCATE(temp(size(c_L) + neq)) 
                i=1
                temp = (/ (/ (REAL(0.0d0, KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), &
                                    i = 1, neq) /), (/ (c_L(i - neq), i = neq + 1, size(c_L) + neq) /) /)
                CALL MOVE_ALLOC(temp, c_L)
            end if
            if (.not.allocated(c_U)) then
                ALLOCATE(c_U(neq))
                i=1
                c_U = (/ (REAL(0.0d0, KIND = SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), i = 1, neq) /)
            else
                ALLOCATE(temp(size(c_U) + neq))
                i=1
                temp = (/ (/ (REAL(0.0d0, KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), &
                                    i = 1, neq) /), (/ (c_U(i - neq), i = neq + 1, size(c_U) + neq) /) /)
                CALL MOVE_ALLOC(temp, c_U)
            end if
        else
            neq = 0
        end if
        
    END SUBROUTINE init_inequality_constraints
         
    
       
! ----------------------------------------------------------------------
! SUBROUTINES calc_dim_refset
! ----------------------------------------------------------------------     
    SUBROUTINE calc_dim_refset(dim_refset, nvar, iterprint,idp, NPROC) 
        IMPLICIT NONE
        INTEGER, INTENT(INOUT) :: dim_refset
        INTEGER, INTENT(IN) :: nvar, iterprint, idp, NPROC
        REAL(KIND = SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)) :: nnn(2), nn(1),polim(3)
        
        if (dim_refset .EQ. - 1) then

            polim = (/  REAL(1.0d0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), &
                        REAL(-1.0d0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), &
                        -10.0d0 * REAL( nvar/1, KIND = SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)) /)
                        
            CALL SQUARE_ROOTS_POLINOM(polim, nnn)
            nn = maxval(nnn, MASK = nnn .GT. 0)
            dim_refset = CEILING(nn(1))
            if (mod(dim_refset, 2) .EQ. 1) then
                dim_refset = dim_refset + 1
            end if
#ifdef MPI2
            if ((iterprint .EQ. 1) .AND. (idp .EQ. 0)) then
                print *, "Refset size automatically calculated:", dim_refset, nvar
            end if            
#else
            if (iterprint .EQ. 1) then
                print *, "Refset size automatically calculated:", dim_refset, nvar
            end if
#endif
        else 
            if (iterprint .EQ. 1) then
                print *, "Refset size :", dim_refset
            end if            
        end if
    END SUBROUTINE calc_dim_refset
    
    
    
! ----------------------------------------------------------------------
! SUBROUTINES calc_ndiverse
! ----------------------------------------------------------------------      
    SUBROUTINE calc_ndiverse(ndiverse, nvar, iterprint,idp)
        IMPLICIT NONE
        INTEGER, INTENT(INOUT) :: ndiverse
        INTEGER, INTENT(IN) :: nvar, iterprint,idp
        
        if (ndiverse .EQ. - 1) then
            ndiverse = 10 * nvar
#ifdef MPI2
            if ((iterprint .EQ. 1) .AND. (idp .EQ. 0)) then
                print *, "Number of diverse solutions automatically calculated::", ndiverse
            end if
#else            
            if (iterprint .EQ. 1) then
                print *, "Number of diverse solutions automatically calculated::", ndiverse
            end if
#endif
        end if
    END SUBROUTINE calc_ndiverse
        
    
    
! ----------------------------------------------------------------------
! SUBROUTINES destroy_refsettype
! ----------------------------------------------------------------------      
    SUBROUTINE destroy_refsettype(refset) 
        IMPLICIT NONE
        TYPE(Refsettype), INTENT(INOUT) :: refset
        
        if (ALLOCATED(refset%x ))       DEALLOCATE(refset%x )
        if (ALLOCATED(refset%f ))       DEALLOCATE(refset%f )      
        if (ALLOCATED(refset%fpen ))    DEALLOCATE(refset%fpen )
        if (ALLOCATED(refset%penalty )) DEALLOCATE(refset%penalty )
        if (ALLOCATED(refset%nlc ))     DEALLOCATE(refset%nlc )
        if (ALLOCATED(refset%parent_index) ) DEALLOCATE(refset%parent_index )
        
    END SUBROUTINE destroy_refsettype
        
    
    
! ----------------------------------------------------------------------
! SUBROUTINES destroy_refsettype
! ----------------------------------------------------------------------      
    SUBROUTINE destroy_resultsf(results) 
        IMPLICIT NONE
        TYPE(resultsf), INTENT(INOUT) :: results
        
        if (ALLOCATED(results%f)) DEALLOCATE(results%f)
        if (ALLOCATED(results%x)) DEALLOCATE(results%x)
        if (ALLOCATED(results%time)) DEALLOCATE(results%time)
        if (ALLOCATED(results%neval )) DEALLOCATE(results%neval )
        if (ALLOCATED(results%fbest)) DEALLOCATE(results%fbest)
        if (ALLOCATED(results%xbest )) DEALLOCATE(results%xbest )
        CALL destroy_refsettype(results%refsett)
        
    END SUBROUTINE destroy_resultsf    
      
   
    
    
    
! ----------------------------------------------------------------------
! SUBROUTINES evaluate_solutions_set
! ----------------------------------------------------------------------   
   SUBROUTINE  evaluate_solutions_set(exp1,fitnessfunction,solutionset,problem1,opts1, solutions, nvar, nfuneval,nconst,&
        timeparallel)
        TYPE(C_PTR), INTENT(INOUT) :: exp1
        REAL(C_DOUBLE), INTENT(INOUT) :: timeparallel
        TYPE(C_FUNPTR), INTENT(INOUT) :: fitnessfunction ! fobj
        TYPE(Refsettype), INTENT(INOUT) :: solutionset
        TYPE(problem), INTENT(IN) :: problem1
        TYPE(opts), INTENT(IN) :: opts1
        TYPE(outfuction) :: outfunct
        INTEGER, INTENT(IN) :: nconst
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE, INTENT(INOUT) :: solutions
        INTEGER, INTENT(IN)  :: nvar
        INTEGER(C_LONG), INTENT(INOUT) :: nfuneval
        INTEGER :: l_f_0, l_x_0, DIMEN(2), tempnum, counter, i, sizet
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: newsolution, entervalues
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE :: temp2
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)) :: NAN
        INTEGER(C_INT) :: error, setNAN
        INTEGER :: clock_rate, clock_start, clock_stop  
                    
#ifdef TIMEPAR      
            CALL SYSTEM_CLOCK(count_rate=clock_rate)
            CALL SYSTEM_CLOCK(count=clock_start)
#endif         

#ifdef TIMEPAR            
            CALL SYSTEM_CLOCK(count=clock_stop)
            timeparallel = timeparallel + REAL(clock_stop-clock_start)/REAL(clock_rate)
#endif        
        error = setNAN(NAN)
        
        if (ALLOCATED(problem1%F0)) then
            l_f_0 = size(problem1%F0)
        else
            l_f_0 = 0
        end if

        if (ALLOCATED(problem1%X0)) then
            DIMEN = shape(problem1%X0)
            l_x_0 = DIMEN(1)
        else
            l_x_0 = 0
        end if

        ! Put x_0 without their corresponding problem1%F0 values at the beginning of solutions
        if (l_x_0 > 0) then
            DIMEN = shape(problem1%X0)
            
            ALLOCATE(temp2(DIMEN(1), DIMEN(2)- l_f_0))
            temp2 = problem1%X0(:,(l_f_0 + 1):DIMEN(2))
            CALL FUSION_MATRIX(temp2, solutions)
            tempnum = l_x_0 - l_f_0
            !DEALLOCATE(temp2)
        else
            tempnum = 0
        end if
         

        sizet = opts1%globaloptions%ndiverse + 5 + tempnum

        ALLOCATE(solutionset%x(nvar, sizet))
        solutionset%x = solutionset%x * REAL(0.0d0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))
        
        if (problem1%ineq .GT. 0 ) then 
            ALLOCATE(solutionset%nlc(problem1%ineq , sizet))
            solutionset%nlc = solutionset%nlc * REAL(0.0d0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)) 
        end if
        
        ALLOCATE(solutionset%f(sizet))
        solutionset%f = solutionset%f * REAL(0.0d0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))
        
        ALLOCATE(solutionset%fpen(sizet))
        solutionset%fpen = solutionset%fpen * REAL(0.0d0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))
        
        ALLOCATE(solutionset%penalty(sizet))
        solutionset%penalty = solutionset%penalty * REAL(0.0d0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))
        


        counter = 1
        !Evaluate the set of solutions
   
        
        do i = 1, sizet
            ALLOCATE(newsolution(nvar))
            newsolution(:) = solutions(:,i)
            outfunct = ssm_evalfc(exp1, fitnessfunction, newsolution, problem1, opts1, nconst, 0)
            solutions(:,i) = outfunct%x
            nfuneval = nfuneval + 1
            
            if (outfunct%include .eq. 1) then
                solutionset%x(:,counter) = outfunct%x
                solutionset%f(counter) = outfunct%value
                solutionset%fpen(counter) = outfunct%value_penalty
                solutionset%penalty(counter) = outfunct%pena
                if (ALLOCATED(solutionset%nlc)) then 
                    solutionset%nlc(:,counter) = outfunct%nlc
                end if
                counter = counter + 1
            end if
            call destroy_outfuction(outfunct)
            if (ALLOCATED(newsolution)) DEALLOCATE(newsolution)
        end do

#ifdef TIMEPAR            
            CALL SYSTEM_CLOCK(count=clock_stop)
            timeparallel = timeparallel + REAL(clock_stop-clock_start)/REAL(clock_rate)
#endif 

        !Add points with f_0 values. We assume feasiblity
        if (l_f_0 > 0) then
            DIMEN = shape(problem1%X0)
            ALLOCATE(temp2(DIMEN(1),l_f_0))
            temp2 = problem1%X0(:,1:l_f_0)
            call FUSION_MATRIX(temp2, solutionset%x)
            call FUSION_VECTOR(problem1%F0, solutionset%f)
            call FUSION_VECTOR(problem1%F0, solutionset%fpen)
            ALLOCATE(entervalues(l_f_0))
            entervalues = entervalues * 0
            call FUSION_VECTOR(entervalues, solutionset%penalty)
            DEALLOCATE(entervalues)
            
            if (ALLOCATED(solutionset%nlc)) then
                ALLOCATE(temp2(l_f_0,nconst))
                temp2 = 1.0 * NAN
                CALL FUSION_MATRIX(temp2,solutionset%nlc)
            end if
        end if    
        
   END SUBROUTINE evaluate_solutions_set
    
   
   
  
! ----------------------------------------------------------------------
! SUBROUTINES create_refset
! ----------------------------------------------------------------------    
   SUBROUTINE create_refset ( solutionset, refset, nvar, opts1, nconst, dim_refset )
        IMPLICIT NONE
        TYPE(Refsettype), INTENT(INOUT) :: solutionset, refset
        INTEGER, INTENT(IN) :: dim_refset
        INTEGER, INTENT(IN) :: nvar, nconst
        TYPE(opts), INTENT(IN) :: opts1        
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: temp
        INTEGER :: first_members, last_members, DIMEN(2)
        INTEGER, DIMENSION(:), ALLOCATABLE :: indvect, ooo
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE :: temp2
        
        
        ALLOCATE(indvect(size(solutionset%fpen)))
        ALLOCATE(temp(size(solutionset%fpen)))
        temp = solutionset%fpen
        call QsortC_ind(temp, indvect)
        DEALLOCATE(temp)

        first_members = CEILING(REAL(dim_refset/2, KIND = SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)))
        last_members = dim_refset  - first_members

        ! Initialize Refset
        ALLOCATE(refset%x(nvar,first_members))
        refset%x = refset%x * REAL(0.0d0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))
        ALLOCATE(refset%f(first_members))
        refset%f = refset%f * REAL(0.0d0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))
        ALLOCATE(refset%fpen(first_members))
        refset%fpen = refset%fpen *REAL(0.0d0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))
        ALLOCATE(refset%penalty(first_members))
        refset%penalty = refset%penalty * REAL(0.0d0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))
        if (nconst .GT. 0) then
            ALLOCATE(refset%nlc(nconst,first_members))
            refset%nlc = refset%nlc * REAL(0.0d0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))              
        end if
        
        ! Create the initial sorted Refset
        refset%x = solutionset%x(:,indvect(1:first_members))
        refset%f = solutionset%f(indvect(1:first_members))
        refset%fpen = solutionset%fpen(indvect(1:first_members))
        refset%penalty = solutionset%penalty(indvect(1:first_members))
        if (nconst .GT. 0) then
            refset%nlc = solutionset%nlc(:,indvect(1:first_members))
        end if

        ! The rest of Refset members are chosen randomly
        CALL delete_values_vector_INT(indvect, first_members)

        ALLOCATE (ooo(size(indvect)))
        
        CALL randperm_int(ooo)
        
        
        DIMEN = shape(solutionset%x)
        ALLOCATE(temp2(DIMEN(1), last_members))
        temp2 = solutionset%x(:,indvect(ooo(1:last_members)))
        CALL FUSION_MATRIX(refset%x, temp2)
        CALL MOVE_ALLOC(temp2, refset%x)


        ALLOCATE(temp(last_members))
        temp = solutionset%f(indvect(ooo(1:last_members)))
        CALL FUSION_VECTOR(refset%f, temp)
        CALL MOVE_ALLOC(temp, refset%f)

        ALLOCATE(temp(last_members))
        temp = solutionset%fpen(indvect(ooo(1:last_members)))
        CALL FUSION_VECTOR(refset%fpen, temp)
        CALL MOVE_ALLOC(temp, refset%fpen)
        

        ALLOCATE(temp(last_members))
        temp = solutionset%penalty(indvect(ooo(1:last_members)))
        CALL FUSION_VECTOR(refset%penalty, temp)
        CALL MOVE_ALLOC(temp, refset%penalty)
        
        if (nconst .GT. 0) then
            ALLOCATE(temp2(DIMEN(1), last_members))
            temp2 = solutionset%nlc(:,indvect(ooo(1:last_members)))
           CALL FUSION_MATRIX(refset%nlc, temp2)
           CALL MOVE_ALLOC(temp2, refset%nlc)
        end if
        
        if (ALLOCATED(ooo)) DEALLOCATE(ooo)
        
        
       
        
   END SUBROUTINE create_refset
        
   
   ! ----------------------------------------------------------------------
! SUBROUTINES create_refset_empty
! ----------------------------------------------------------------------    
   SUBROUTINE create_refset_empty ( refset, nvar, opts1, nconst, size )
        IMPLICIT NONE
        TYPE(Refsettype), INTENT(INOUT) :: refset
        INTEGER, INTENT(IN) :: nvar, nconst, size
        TYPE(opts), INTENT(IN) :: opts1        

        ! Initialize Refset
        ALLOCATE(refset%x(nvar,size))
        refset%x = refset%x * REAL(0.0d0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))
        ALLOCATE(refset%f(size))
        refset%f = refset%f * REAL(0.0d0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))
        ALLOCATE(refset%fpen(size))
        refset%fpen = refset%fpen *REAL(0.0d0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))
        ALLOCATE(refset%penalty(size))
        refset%penalty = refset%penalty * REAL(0.0d0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))
        if (nconst .GT. 0) then
            ALLOCATE(refset%nlc(nconst,size))
            refset%nlc = refset%nlc * REAL(0.0d0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))              
        end if
        
   END SUBROUTINE create_refset_empty
        
    
! ----------------------------------------------------------------------
! SUBROUTINES create_refset_det
! ----------------------------------------------------------------------    
   SUBROUTINE create_refset_det ( solutionset, refset, nvar, opts1, nconst, dim_refset )
        IMPLICIT NONE
        TYPE(Refsettype), INTENT(INOUT) :: solutionset, refset
        INTEGER, INTENT(IN) :: nvar, nconst
        TYPE(opts), INTENT(IN) :: opts1        
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: temp
        INTEGER :: first_members, last_members
        INTEGER, DIMENSION(:), ALLOCATABLE :: indvect, ooo
        INTEGER, INTENT(IN) :: dim_refset
        
        
        ALLOCATE(indvect(size(solutionset%fpen)))
        ALLOCATE(temp(size(solutionset%fpen)))
        temp = solutionset%fpen
        call QsortC_ind(temp, indvect)
        DEALLOCATE(temp)

        first_members = CEILING(REAL(dim_refset/2, KIND = SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)))
        last_members = dim_refset 

        ! Initialize Refset
        ALLOCATE(refset%x(nvar,last_members))
        refset%x = refset%x * REAL(0.0d0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))
        ALLOCATE(refset%f(last_members))
        refset%f = refset%f * REAL(0.0d0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))
        ALLOCATE(refset%fpen(last_members))
        refset%fpen = refset%fpen *REAL(0.0d0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))
        ALLOCATE(refset%penalty(last_members))
        refset%penalty = refset%penalty * REAL(0.0d0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))
        if (nconst .GT. 0) then
            ALLOCATE(refset%nlc(nconst,last_members))
            refset%nlc = refset%nlc * REAL(0.0d0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))              
        end if
        ! Create the initial sorted Refset

        refset%x = solutionset%x(:,indvect(1:last_members))
        refset%f = solutionset%f(indvect(1:last_members))
        refset%fpen = solutionset%fpen(indvect(1:last_members))
        refset%penalty = solutionset%penalty(indvect(1:last_members))
        if (nconst .GT. 0) then
            refset%nlc = solutionset%nlc(:,indvect(1:last_members))
        end if

        ! The rest of Refset members are chosen randomly
        CALL delete_values_vector_INT(indvect, first_members)

        ALLOCATE (ooo(size(indvect)))
        
        !CALL randperm_int(ooo)
        
        
        !DIMEN = shape(solutionset%x)
        !ALLOCATE(temp2(DIMEN(1), last_members))
        !temp2 = solutionset%x(:,indvect(ooo(1:last_members)))
        !CALL FUSION_MATRIX(refset%x, temp2)
        !CALL MOVE_ALLOC(temp2, refset%x)


        !ALLOCATE(temp(last_members))
        !temp = solutionset%f(indvect(ooo(1:last_members)))
        !CALL FUSION_VECTOR(refset%f, temp)
        !CALL MOVE_ALLOC(temp, refset%f)

        !ALLOCATE(temp(last_members))
        !temp = solutionset%fpen(indvect(ooo(1:last_members)))
        !CALL FUSION_VECTOR(refset%fpen, temp)
        !CALL MOVE_ALLOC(temp, refset%fpen)
        

        !ALLOCATE(temp(last_members))
        !temp = solutionset%penalty(indvect(ooo(1:last_members)))
        !CALL FUSION_VECTOR(refset%penalty, temp)
        !CALL MOVE_ALLOC(temp, refset%penalty)
        
        !if (nconst .GT. 0) then
        !    ALLOCATE(temp2(DIMEN(1), last_members))
        !    temp2 = solutionset%nlc(:,indvect(ooo(1:last_members)))
        !   CALL FUSION_MATRIX(refset%nlc, temp2)
        !   CALL MOVE_ALLOC(temp2, refset%nlc)
        !end if
        
        if (ALLOCATED(ooo)) DEALLOCATE(ooo)
        
        
       
        
   END SUBROUTINE create_refset_det
   
   
   
   
! ----------------------------------------------------------------------
! SUBROUTINES create_child
! ----------------------------------------------------------------------    
   SUBROUTINE create_childset ( childset, nvar, MaxSubSet2, nconst )   
       IMPLICIT NONE
       TYPE(Refsettype), INTENT(INOUT) :: childset
       INTEGER, INTENT(IN) :: nvar, nconst, MaxSubSet2
       
            ALLOCATE(childset%x(nvar,MaxSubSet2))
            ALLOCATE(childset%f(MaxSubSet2) )
            ALLOCATE(childset%fpen(MaxSubSet2) )
            ALLOCATE(childset%penalty(MaxSubSet2) )
            if (nconst .gt. 0) ALLOCATE(childset%nlc(nvar,MaxSubSet2))
            ALLOCATE(childset%parent_index(MaxSubSet2) )
            
            childset%x = 0d0
            childset%f = 0d0
            childset%fpen = 0d0
            childset%penalty = 0d0
            if (nconst .gt. 0) childset%nlc = 0d0
            childset%parent_index = 0
            
       
   END SUBROUTINE
! ----------------------------------------------------------------------
! SUBROUTINES check_best_value
! ---------------------------------------------------------------------- 
   SUBROUTINE check_best_value(refset, opts1, xbest, fbest )
        IMPLICIT NONE
        TYPE(Refsettype), INTENT(IN) :: refset
        TYPE(opts), INTENT(IN) :: opts1 
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE, INTENT(INOUT) :: xbest, fbest
        CHARACTER(len = 2) :: typesearch
        INTEGER, DIMENSION(:), ALLOCATABLE :: indexfind
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: temp
        INTEGER, DIMENSION(:), ALLOCATABLE :: iiim
        INTEGER :: iii
        
        typesearch = "LT"
        CALL find_in_vector(refset%penalty, opts1%useroptions%tolc, indexfind, typesearch)
        if (ALLOCATED(indexfind)) then
            CALL reajust_index(indexfind)
            ALLOCATE(temp(size(indexfind)))
            temp = refset%fpen(indexfind)
            if (.not. allocated(fbest)) allocate(fbest(1))
            fbest = minval(temp)
            ALLOCATE(iiim(1))
            iiim = minloc(temp)
            iii = iiim(1)
            xbest = refset%x(:,(indexfind(iii)))
            DEALLOCATE(iiim)
            DEALLOCATE(temp)
        end if
        
        IF (ALLOCATED(iiim)) DEALLOCATE(iiim)
        IF (ALLOCATED(indexfind)) DEALLOCATE(indexfind)
    END SUBROUTINE check_best_value
    
    
    
! ----------------------------------------------------------------------
! SUBROUTINES create_results
! ----------------------------------------------------------------------     
    SUBROUTINE create_results(results, opts1, xbest, fbest, nfuneval, cputime1, nvar)
        IMPLICIT NONE
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE, INTENT(IN) :: xbest, fbest
        TYPE(opts), INTENT(IN) :: opts1 
        INTEGER(C_LONG), INTENT(IN) :: nfuneval
        INTEGER, INTENT(IN):: nvar
        REAL(C_DOUBLE), INTENT(IN) :: cputime1
        TYPE(resultsf), INTENT(INOUT) :: results
        
        ! Initialize Refset for restuls struct 
        ALLOCATE(results%refsett%x(nvar,opts1%globaloptions%dim_refset))
        results%refsett%x =  REAL ( 0.0d0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))
        
        ALLOCATE(results%refsett%f(opts1%globaloptions%dim_refset))
        results%refsett%f =  REAL ( 0.0d0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))
        
        ALLOCATE(results%refsett%fpen(opts1%globaloptions%dim_refset))
        results%refsett%fpen =  REAL ( 0.0d0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))
        
        ALLOCATE(results%refsett%penalty(opts1%globaloptions%dim_refset))
        results%refsett%penalty =  REAL ( 0.0d0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))  
        
        ALLOCATE(results%f(size(fbest)))
        results%f = fbest
        
        ALLOCATE(results%x(size(xbest),1))
        results%x(:,1) = xbest
        
        ALLOCATE(results%time(1))
        results%time(1) = cputime1
        
        ALLOCATE(results%neval(1))
        results%neval(1) = nfuneval
        
        
    END SUBROUTINE create_results
    
    
! ----------------------------------------------------------------------
! SUBROUTINES sort_refset
! ----------------------------------------------------------------------     
    SUBROUTINE sort_refset(refset, opts1, indvect)   
        IMPLICIT NONE
        TYPE(Refsettype), INTENT(INOUT) :: refset
        TYPE(opts), INTENT(IN) :: opts1 
        INTEGER, DIMENSION(:), ALLOCATABLE, INTENT(INOUT) :: indvect
        
        if (ALLOCATED(indvect)) then
            DEALLOCATE(indvect)
        end if
        ALLOCATE(indvect(opts1%globaloptions%dim_refset))
        
        call QsortC_ind(refset%fpen, indvect)
        refset%f = refset%f(indvect)
        refset%penalty = refset%penalty(indvect)
        refset%x = refset%x(:,indvect)
        if (ALLOCATED(refset%nlc)) refset%nlc = refset%nlc(:,indvect)
    END SUBROUTINE sort_refset
    
    
    
! ----------------------------------------------------------------------
! SUBROUTINES create_candidate
! ----------------------------------------------------------------------     
    SUBROUTINE create_candidate(candidateset, refset)
        TYPE(Refsettype), INTENT(IN) :: refset
        TYPE(Refsettype), INTENT(INOUT) :: candidateset
        INTEGER :: DIMEN(2)
        
        
        DIMEN = shape(refset%x)
        ALLOCATE(candidateset%x(DIMEN(1), DIMEN(2)))
        ALLOCATE(candidateset%f(size(refset%f)))
        ALLOCATE(candidateset%fpen(size(refset%fpen)))
        ALLOCATE(candidateset%penalty(size(refset%penalty)))
        if (ALLOCATED(refset%nlc)) ALLOCATE(candidateset%nlc(DIMEN(1), DIMEN(2)))

        candidateset%x = refset%x
        candidateset%f = refset%f
        candidateset%fpen = refset%fpen
        if (ALLOCATED(refset%nlc)) candidateset%nlc = refset%nlc
        candidateset%penalty = refset%penalty
        
    END SUBROUTINE create_candidate
        
    
    
    
! ----------------------------------------------------------------------
! SUBROUTINES check_duplicated_replace
! ----------------------------------------------------------------------     
    SUBROUTINE check_duplicated_replace (exp1, fitnessfunction, problem1, refset, opts1, parents_index1, parents_index2, &
                        xl_log, xu_log, refset_change, members_update, index1, index2, nfuneval, nvar, nconst )
            TYPE(problem), INTENT(IN) :: problem1
            TYPE(C_PTR), INTENT(INOUT) :: exp1
            TYPE(Refsettype), INTENT(INOUT) :: refset
            TYPE(opts), INTENT(IN) :: opts1 
            TYPE(C_FUNPTR), INTENT(INOUT) :: fitnessfunction ! fobj
            REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE, INTENT(INOUT) :: parents_index1,&
                                                                                                                 parents_index2
            INTEGER, DIMENSION(:), ALLOCATABLE, INTENT(INOUT) :: refset_change            
            INTEGER, DIMENSION(:), ALLOCATABLE, INTENT(IN) :: index1, index2
            INTEGER, DIMENSION(:), ALLOCATABLE, INTENT(INOUT) :: members_update
            INTEGER(C_LONG), INTENT(INOUT) :: nfuneval
            INTEGER, INTENT(IN) :: nvar, nconst
            REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE, INTENT(IN) :: xl_log, xu_log
            INTEGER :: continuar, auxsize, DIMEN(2)
            INTEGER, DIMENSION(:), ALLOCATABLE :: indvect
            REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: parents_index1_values_penalty, &
                                                                                                       parents_index2_values_penalty
            REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE :: denom22, AAA, temp2
            INTEGER, DIMENSION(:), ALLOCATABLE :: BBB
            CHARACTER(len = 2) :: typesearch
            REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: new_refset_member
            INTEGER, DIMENSION(:), ALLOCATABLE :: index_result, index_refset_out
            INTEGER, DIMENSION(:,:), ALLOCATABLE :: index_result2
            TYPE(outfuction) :: outfunct
            
           
            continuar = 0
            do while (continuar .eq. 0)
                !Sort Refset
                !Sort Refset
                !This is mandatory because the variable index is also sorted
                CALL sort_refset(refset,opts1, indvect)
                
                refset_change = refset_change(indvect)
                DIMEN = shape(refset%x)
                
                if (.not.ALLOCATED(parents_index1)) ALLOCATE(parents_index1(DIMEN(1), size(index1)))
                parents_index1 = refset%x(:,index1)

                if (.not.ALLOCATED(parents_index2)) ALLOCATE(parents_index2(DIMEN(1), size(index2)))
                parents_index2 = refset%x(:,index2)
                
                if (.not.ALLOCATED(parents_index1_values_penalty)) &
                ALLOCATE(parents_index1_values_penalty(size(index1)))
                parents_index1_values_penalty = refset%fpen(index1)
                
                if (.not.ALLOCATED(parents_index2_values_penalty)) &
                ALLOCATE(parents_index2_values_penalty(size(index2)))
                parents_index2_values_penalty = refset%fpen(index2)

                ALLOCATE(denom22(DIMEN(1),size(index1)))
                denom22 = max_matrix(abs_matrix(parents_index1), abs_matrix(parents_index2))

                call replace_matrix(denom22, REAL(0.0, KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)))
                ALLOCATE(temp2(DIMEN(1),size(index1)))
                temp2 = (parents_index1 - parents_index2)  / denom22
                
                ALLOCATE(AAA(DIMEN(1),size(index1)))
                AAA = abs_matrix(temp2)
                
                DEALLOCATE(temp2)
                typesearch = "LT"
                DIMEN = shape(AAA)
                auxsize = 0
                ALLOCATE(index_result2(DIMEN(1),DIMEN(2)))
                index_result2 = compare_matrix(AAA, REAL(1.0d-3, KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), typesearch)
                ALLOCATE(index_result(size(index1)))
                CALL row_one(index_result2, index_result, auxsize)
                ALLOCATE(BBB(size(index_result)))
                CALL index_of_ones(index_result, BBB)
                CALL reajust_index(BBB)
                DEALLOCATE(index_result2)
                DEALLOCATE(index_result)
                
                if (ALLOCATED(BBB)) then
                    !print *, " - check_duplicated_replace - duplicados = ", size(BBB), " solutions"
                    ALLOCATE( index_result(size(BBB)))
                    index_result = index2(BBB)
                    ALLOCATE(index_refset_out(1))
                    index_refset_out = MAXVAL(index_result)
                    DEALLOCATE(index_result)
                    outfunct%include = 0
                    do while (outfunct%include .eq. 0)
                        ALLOCATE(new_refset_member(nvar))
                        CALL random_Vector_SEED(exp1,new_refset_member, nvar, xl_log, xu_log)
                        
                        if (ALLOCATED(opts1%useroptions%log_var) .and. size(opts1%useroptions%log_var) .GT. 0 )  &
                                CALL converttonormal(new_refset_member, nvar)
                        
                        outfunct = ssm_evalfc(   exp1, fitnessfunction, new_refset_member, problem1, opts1, nconst, 0)
                        nfuneval = nfuneval + 1
                        if (outfunct%include .eq. 1) then
                            refset%x(:,index_refset_out(1)) = outfunct%x
                            refset%f(index_refset_out(1)) = outfunct%value
                            refset%fpen(index_refset_out(1)) = outfunct%value_penalty
                            if (ALLOCATED(refset%nlc)) refset%nlc(:,index_refset_out(1)) = outfunct%nlc
                            refset%penalty(index_refset_out(1)) = outfunct%pena
                            members_update(index_refset_out(1)) = 0
                        end if
                        DEALLOCATE(new_refset_member)
                    end do
                    DEALLOCATE(index_refset_out)
                    call destroy_outfuction(outfunct)
                    
                else
                    continuar = 1
                end if

                DEALLOCATE(denom22)
                if ( ALLOCATED(AAA)) DEALLOCATE(AAA)
                if ( ALLOCATED(BBB)) DEALLOCATE(BBB)

            if (ALLOCATED(indvect)) DEALLOCATE(indvect)
            if (ALLOCATED(parents_index1_values_penalty)) DEALLOCATE(parents_index1_values_penalty)
            if (ALLOCATED(parents_index2_values_penalty)) DEALLOCATE(parents_index2_values_penalty)                
            end do ! Fin bucle interior
            
    END SUBROUTINE check_duplicated_replace        
          
    
    
! ----------------------------------------------------------------------
! SUBROUTINES check_duplicated_replace
! ----------------------------------------------------------------------     
    SUBROUTINE check_duplicated_replace_det (exp1, fitnessfunction, problem1, refset, opts1, parents_index1, parents_index2, &
                        xl_log, xu_log, refset_change, members_update, index1, index2, nfuneval, nvar, nconst, contador, matrixR )
            TYPE(problem), INTENT(IN) :: problem1
            TYPE(C_PTR), INTENT(INOUT) :: exp1
            TYPE(Refsettype), INTENT(INOUT) :: refset
            TYPE(opts), INTENT(IN) :: opts1 
            TYPE(C_FUNPTR), INTENT(INOUT) :: fitnessfunction ! fobj
            REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE, INTENT(INOUT) :: parents_index1,&
                                                                                                                 parents_index2
            INTEGER, DIMENSION(:), ALLOCATABLE, INTENT(INOUT) :: refset_change    
            INTEGER, INTENT(INOUT) :: contador
            INTEGER, DIMENSION(:), ALLOCATABLE, INTENT(IN) :: index1, index2
            REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE, INTENT(INOUT) :: matrixR 
            INTEGER, DIMENSION(:), ALLOCATABLE, INTENT(INOUT) :: members_update
            INTEGER(C_LONG), INTENT(INOUT) :: nfuneval
            INTEGER, INTENT(IN) :: nvar, nconst
            REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE, INTENT(IN) :: xl_log, xu_log
            INTEGER :: continuar, auxsize, DIMEN(2)
            INTEGER, DIMENSION(:), ALLOCATABLE :: indvect
            REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: parents_index1_values_penalty, &
                                                                                                       parents_index2_values_penalty
            REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE :: denom22, AAA, temp2
            INTEGER, DIMENSION(:), ALLOCATABLE :: BBB
            CHARACTER(len = 2) :: typesearch
            REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: new_refset_member
            INTEGER, DIMENSION(:), ALLOCATABLE :: index_result, index_refset_out
            INTEGER, DIMENSION(:,:), ALLOCATABLE :: index_result2
            TYPE(outfuction) :: outfunct
            
           
            continuar = 0
            do while (continuar .eq. 0)
                !Sort Refset
                !Sort Refset
                !This is mandatory because the variable index is also sorted
                CALL sort_refset(refset,opts1, indvect)
                
                refset_change = refset_change(indvect)
                DIMEN = shape(refset%x)
                
                if (.not.ALLOCATED(parents_index1)) ALLOCATE(parents_index1(DIMEN(1), size(index1)))
                parents_index1 = refset%x(:,index1)

                if (.not.ALLOCATED(parents_index2)) ALLOCATE(parents_index2(DIMEN(1), size(index2)))
                parents_index2 = refset%x(:,index2)
                
                if (.not.ALLOCATED(parents_index1_values_penalty)) &
                ALLOCATE(parents_index1_values_penalty(size(index1)))
                parents_index1_values_penalty = refset%fpen(index1)
                
                if (.not.ALLOCATED(parents_index2_values_penalty)) &
                ALLOCATE(parents_index2_values_penalty(size(index2)))
                parents_index2_values_penalty = refset%fpen(index2)

                ALLOCATE(denom22(DIMEN(1),size(index1)))
                denom22 = max_matrix(abs_matrix(parents_index1), abs_matrix(parents_index2))

                call replace_matrix(denom22, REAL(0.0, KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)))
                ALLOCATE(temp2(DIMEN(1),size(index1)))
                temp2 = (parents_index1 - parents_index2)  / denom22
                
                ALLOCATE(AAA(DIMEN(1),size(index1)))
                AAA = abs_matrix(temp2)
                
                DEALLOCATE(temp2)
                typesearch = "LT"
                DIMEN = shape(AAA)
                auxsize = 0
                ALLOCATE(index_result2(DIMEN(1),DIMEN(2)))
                index_result2 = compare_matrix(AAA, REAL(1.0d-3, KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), typesearch)
                ALLOCATE(index_result(size(index1)))
                CALL row_one(index_result2, index_result, auxsize)
                ALLOCATE(BBB(size(index_result)))
                CALL index_of_ones(index_result, BBB)
                CALL reajust_index(BBB)
                DEALLOCATE(index_result2)
                DEALLOCATE(index_result)
                
                if (ALLOCATED(BBB)) then
                    !print *, " - check_duplicated_replace - duplicados = ", size(BBB), " solutions"
                    ALLOCATE( index_result(size(BBB)))
                    index_result = index2(BBB)
                    ALLOCATE(index_refset_out(1))
                    index_refset_out = MAXVAL(index_result)
                    DEALLOCATE(index_result)
                    outfunct%include = 0
                    do while (outfunct%include .eq. 0)
                        ALLOCATE(new_refset_member(nvar))
                        CALL random_det_Vector_SEED(new_refset_member, nvar, xl_log, xu_log, contador, matrixR)
                        
                        if (ALLOCATED(opts1%useroptions%log_var) .and. size(opts1%useroptions%log_var) .GT. 0 )  &
                                CALL converttonormal(new_refset_member, nvar)
                        
                        outfunct = ssm_evalfc(   exp1, fitnessfunction, new_refset_member, problem1, opts1, nconst, 0)
                        nfuneval = nfuneval + 1
                        if (outfunct%include .eq. 1) then
                            refset%x(:,index_refset_out(1)) = outfunct%x
                            refset%f(index_refset_out(1)) = outfunct%value
                            refset%fpen(index_refset_out(1)) = outfunct%value_penalty
                            if (ALLOCATED(refset%nlc)) refset%nlc(:,index_refset_out(1)) = outfunct%nlc
                            refset%penalty(index_refset_out(1)) = outfunct%pena
                            members_update(index_refset_out(1)) = 0
                        end if
                        DEALLOCATE(new_refset_member)
                    end do
                    DEALLOCATE(index_refset_out)
                    call destroy_outfuction(outfunct)
                    
                else
                    continuar = 1
                end if

                DEALLOCATE(denom22)
                if ( ALLOCATED(AAA)) DEALLOCATE(AAA)
                if ( ALLOCATED(BBB)) DEALLOCATE(BBB)

            if (ALLOCATED(indvect)) DEALLOCATE(indvect)
            if (ALLOCATED(parents_index1_values_penalty)) DEALLOCATE(parents_index1_values_penalty)
            if (ALLOCATED(parents_index2_values_penalty)) DEALLOCATE(parents_index2_values_penalty)                
            end do ! Fin bucle interior
            
    END SUBROUTINE check_duplicated_replace_det    
  
    
    
    
    
! ----------------------------------------------------------------------
! SUBROUTINES check_vector_in_hyper_bounds
! ----------------------------------------------------------------------        
    SUBROUTINE check_vector_in_hyper_bounds(exp1,opts1, v, hyper_x_L, hyper_x_U )
            REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE, INTENT(IN) :: hyper_x_L, hyper_x_U
            REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE, INTENT(INOUT) :: v
            TYPE(C_PTR), INTENT(INOUT) :: exp1
            TYPE(opts), INTENT(IN) :: opts1 
            CHARACTER(len = 2) :: typesearch
            INTEGER, DIMENSION(:), ALLOCATABLE :: aa, bb, aux
            INTEGER :: DIMEN(2)
            INTEGER, DIMENSION(:), ALLOCATABLE :: index_result, index_result3
            INTEGER, DIMENSION(:,:), ALLOCATABLE :: index_result2
            INTEGER :: auxsize 
            REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: temp
                        
            DIMEN = shape(hyper_x_L)
            ALLOCATE(index_result2(DIMEN(1), DIMEN(2)))
            typesearch = "LT"
            index_result2 = compare_multiple_matrix(v, hyper_x_L, auxsize, typesearch)
            
            ALLOCATE(aa(auxsize))
            CALL index_of_ones_matrix(index_result2, aa)
            CALL reajust_index(aa)
            DEALLOCATE(index_result2)
            if (ALLOCATED (aa) ) then
                ALLOCATE(temp(size(aa)))
                call  random_Vector_SEED_10(exp1,temp, size(aa))         
                
                ALLOCATE(index_result(size(aa)))
                typesearch = "GT"
                index_result = compare_vector(temp, opts1%useroptions%prob_bound, typesearch, auxsize)
                DEALLOCATE(temp)
                ALLOCATE(index_result3(auxsize))
                call index_of_ones(index_result, index_result3)
                CALL reajust_index(index_result3)
                DEALLOCATE(index_result)
                if ( ALLOCATED ( index_result3 ) ) then
                    ALLOCATE(aux(size(index_result3)))
                    aux = aa(index_result3)
                    call return_values_matrix_index(v, hyper_x_L, aux)
                    DEALLOCATE(index_result3)
                    DEALLOCATE(aux)
                end if
            end if

            
            
            
            DIMEN = shape(hyper_x_U)
            ALLOCATE(index_result2(DIMEN(1), DIMEN(2)))
            typesearch = "GT"
            index_result2 = compare_multiple_matrix(v, hyper_x_U, auxsize, typesearch)   
            
            ALLOCATE(bb(auxsize))
            CALL index_of_ones_matrix(index_result2, bb)
            
            CALL reajust_index(bb)
            DEALLOCATE(index_result2)
            if (ALLOCATED (bb) ) then
                ALLOCATE(temp(size(bb)))
                call random_Vector_SEED_10(exp1,temp, size(bb))
                ALLOCATE(index_result(size(bb)))
                typesearch = "GT"
                index_result = compare_vector(temp, opts1%useroptions%prob_bound, typesearch, auxsize)
                DEALLOCATE(temp)
                ALLOCATE(index_result3(auxsize))
                call index_of_ones(index_result, index_result3)
                CALL reajust_index(index_result3)
                DEALLOCATE(index_result)
                if ( ALLOCATED ( index_result3 ) ) then
                    ALLOCATE(aux(size(index_result3)))
                    aux = bb(index_result3)
                    call return_values_matrix_index(v, hyper_x_U, aux)
                    DEALLOCATE(index_result3)
                    DEALLOCATE(aux)
                end if
            end if
            
            IF (ALLOCATED(aa)) DEALLOCATE(aa)
            IF (ALLOCATED(bb)) DEALLOCATE(bb)
        
    END SUBROUTINE check_vector_in_hyper_bounds
    
    
    
    
    
! ----------------------------------------------------------------------
! SUBROUTINES check_vector_in_hyper_bounds
! ----------------------------------------------------------------------        
    SUBROUTINE check_vector_in_hyper_bounds_det(exp1,opts1, v, hyper_x_L, hyper_x_U, contador, matrixR )
        
            REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE, INTENT(IN) :: hyper_x_L, hyper_x_U
            REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE, INTENT(INOUT) :: v
            REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE, INTENT(INOUT) :: matrixR
            INTEGER, INTENT(INOUT) :: contador
            TYPE(C_PTR), INTENT(INOUT) :: exp1
            TYPE(opts), INTENT(IN) :: opts1 
            CHARACTER(len = 2) :: typesearch
            INTEGER, DIMENSION(:), ALLOCATABLE :: aa, bb, aux
            INTEGER :: DIMEN(2)
            INTEGER, DIMENSION(:), ALLOCATABLE :: index_result, index_result3
            INTEGER, DIMENSION(:,:), ALLOCATABLE :: index_result2
            INTEGER :: auxsize 
            REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: temp
                        
            DIMEN = shape(hyper_x_L)
            ALLOCATE(index_result2(DIMEN(1), DIMEN(2)))
            typesearch = "LT"
            index_result2 = compare_multiple_matrix(v, hyper_x_L, auxsize, typesearch)
            
            ALLOCATE(aa(auxsize))
            CALL index_of_ones_matrix(index_result2, aa)
            CALL reajust_index(aa)
            DEALLOCATE(index_result2)
            if (ALLOCATED (aa) ) then
                ALLOCATE(temp(size(aa)))
                call  random_det_Vector_SEED_10(temp, size(aa), contador, matrixR)        
                
                ALLOCATE(index_result(size(aa)))
                typesearch = "GT"
                index_result = compare_vector(temp, opts1%useroptions%prob_bound, typesearch, auxsize)
                DEALLOCATE(temp)
                ALLOCATE(index_result3(auxsize))
                call index_of_ones(index_result, index_result3)
                CALL reajust_index(index_result3)
                DEALLOCATE(index_result)
                if ( ALLOCATED ( index_result3 ) ) then
                    ALLOCATE(aux(size(index_result3)))
                    aux = aa(index_result3)
                    call return_values_matrix_index(v, hyper_x_L, aux)
                    DEALLOCATE(index_result3)
                    DEALLOCATE(aux)
                end if
            end if

            
            
            
            DIMEN = shape(hyper_x_U)
            ALLOCATE(index_result2(DIMEN(1), DIMEN(2)))
            typesearch = "GT"
            index_result2 = compare_multiple_matrix(v, hyper_x_U, auxsize, typesearch)   
            
            ALLOCATE(bb(auxsize))
            CALL index_of_ones_matrix(index_result2, bb)
            
            CALL reajust_index(bb)
            DEALLOCATE(index_result2)
            if (ALLOCATED (bb) ) then
                ALLOCATE(temp(size(bb)))
                call random_det_Vector_SEED_10(temp, size(bb), contador, matrixR)
                ALLOCATE(index_result(size(bb)))
                typesearch = "GT"
                index_result = compare_vector(temp, opts1%useroptions%prob_bound, typesearch, auxsize)
                DEALLOCATE(temp)
                ALLOCATE(index_result3(auxsize))
                call index_of_ones(index_result, index_result3)
                CALL reajust_index(index_result3)
                DEALLOCATE(index_result)
                if ( ALLOCATED ( index_result3 ) ) then
                    ALLOCATE(aux(size(index_result3)))
                    aux = bb(index_result3)
                    call return_values_matrix_index(v, hyper_x_U, aux)
                    DEALLOCATE(index_result3)
                    DEALLOCATE(aux)
                end if
            end if
            
            IF (ALLOCATED(aa)) DEALLOCATE(aa)
            IF (ALLOCATED(bb)) DEALLOCATE(bb)
        
    END SUBROUTINE check_vector_in_hyper_bounds_det
    
! ----------------------------------------------------------------------
! SUBROUTINES generate_new_comb_matrix
! ----------------------------------------------------------------------     
    SUBROUTINE generate_new_comb_matrix(exp1, new_comb, ppp, MaxSubSet, nrand, v1,v2,v3)
        TYPE(C_PTR), INTENT(INOUT) :: exp1
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), INTENT(IN) :: MaxSubSet
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE, INTENT(IN) :: ppp
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE, INTENT(INOUT) ::  new_comb
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE, INTENT(IN) :: v1, v2, v3
        INTEGER, INTENT(IN) :: nrand
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE :: array1, array2
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE :: new_comb1, new_comb2
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)) :: random
        

        
            if (nrand > 1) then
                !print *, " - generate_new_comb_matrix -- new_comb = ", MaxSubSet * 2
                ALLOCATE(array1(nrand, CEILING(MaxSubSet)))
                call random_Matrix_SEED_10(exp1,array1, nrand, CEILING(MaxSubSet))
                ALLOCATE( new_comb1(nrand,size(ppp)))
                new_comb1 = (v1 + (v2 - v1) * array1)
                DEALLOCATE(array1)
                
                ALLOCATE(array2(nrand, CEILING(MaxSubSet)))
                call random_Matrix_SEED_10(exp1,array2, nrand, CEILING(MaxSubSet))
                ALLOCATE( new_comb2(nrand,size(ppp)))

                new_comb2 = ( v2 + (v3 - v2) * array2)              
                DEALLOCATE(array2)
            else
                random = RETURN_RANDOM_NUMBER(exp1)
                new_comb1 = (v1 + (v2 - v1) * random) 
                random = RETURN_RANDOM_NUMBER(exp1)
                new_comb2 = (v2 + (v3 - v2) * random) 
            end if

            ALLOCATE(new_comb(nrand,CEILING(MaxSubSet) * 2))
            new_comb(:,1:CEILING(MaxSubSet)) = new_comb1(:,1:CEILING(MaxSubSet))
            new_comb(:,(CEILING(MaxSubSet) + 1):CEILING(MaxSubSet) * 2) = new_comb2(:,1:CEILING(MaxSubSet))
                                                    
            if (ALLOCATED(new_comb1))  DEALLOCATE(new_comb1)
            if (ALLOCATED(new_comb2))  DEALLOCATE(new_comb2)
            
    END SUBROUTINE  generate_new_comb_matrix
    
            
 ! ----------------------------------------------------------------------
! SUBROUTINES generate_new_comb_matrix
! ----------------------------------------------------------------------     
    SUBROUTINE generate_new_comb_matrix_det(exp1, new_comb, ppp, MaxSubSet, nrand, v1,v2,v3, contador, matrixR)
        TYPE(C_PTR), INTENT(INOUT) :: exp1
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), INTENT(IN) :: MaxSubSet
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE, INTENT(IN) :: ppp
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE, INTENT(INOUT) ::  new_comb
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE, INTENT(IN) :: v1, v2, v3
        INTEGER, INTENT(IN) :: nrand
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE :: array1, array2
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE :: new_comb1, new_comb2
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)) :: random
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE, INTENT(INOUT) :: matrixR
        INTEGER, INTENT(INOUT) :: contador        

            if (nrand > 1) then
                !print *, " - generate_new_comb_matrix -- new_comb = ", MaxSubSet * 2
                ALLOCATE(array1(nrand, CEILING(MaxSubSet)))
                call random_det_Matrix_SEED_10(array1, nrand, CEILING(MaxSubSet), contador, matrixR)
                ALLOCATE( new_comb1(nrand,size(ppp)))
                new_comb1 = (v1 + (v2 - v1) * array1)
                DEALLOCATE(array1)
                
                ALLOCATE(array2(nrand, CEILING(MaxSubSet)))
                call random_det_Matrix_SEED_10(array2, nrand, CEILING(MaxSubSet), contador, matrixR)
                ALLOCATE( new_comb2(nrand,size(ppp)))

                new_comb2 = ( v2 + (v3 - v2) * array2)              
                DEALLOCATE(array2)
            else
                CALL random_det_NUMBER(random, contador, matrixR)
                new_comb1 = (v1 + (v2 - v1) * random) 
                CALL random_det_NUMBER(random, contador, matrixR)
                new_comb2 = (v2 + (v3 - v2) * random) 
            end if

            ALLOCATE(new_comb(nrand,CEILING(MaxSubSet) * 2))
            new_comb(:,1:CEILING(MaxSubSet)) = new_comb1(:,1:CEILING(MaxSubSet))
            new_comb(:,(CEILING(MaxSubSet) + 1):CEILING(MaxSubSet) * 2) = new_comb2(:,1:CEILING(MaxSubSet))
                                                    
            if (ALLOCATED(new_comb1))  DEALLOCATE(new_comb1)
            if (ALLOCATED(new_comb2))  DEALLOCATE(new_comb2)
            
    END SUBROUTINE  generate_new_comb_matrix_det   
    
! ----------------------------------------------------------------------
! SUBROUTINES update_candidateset_with_new_comb
! ----------------------------------------------------------------------      
    SUBROUTINE update_candidateset_with_new_comb( exp1,opts1, fitnessfunction,problem1,new_comb,candidateset,childset, &
            candidate_update, members_update,MaxSubSet2,nrand,nconst,nfuneval,counter, index, timeparallel )
            TYPE(problem), INTENT(IN) :: problem1
            TYPE(C_PTR), INTENT(INOUT) :: exp1
            TYPE(Refsettype), INTENT(INOUT) :: candidateset, childset
            REAL(C_DOUBLE), INTENT(INOUT) :: timeparallel
            TYPE(opts), INTENT(IN) :: opts1 
            TYPE(C_FUNPTR), INTENT(INOUT) :: fitnessfunction ! fobj        
            INTEGER, DIMENSION(:), ALLOCATABLE, INTENT(INOUT) :: members_update, candidate_update
            INTEGER(C_LONG), INTENT(INOUT) :: nfuneval
            INTEGER, INTENT(INOUT) :: counter
            INTEGER, INTENT(IN) :: nconst, nrand
            REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), INTENT(IN) :: MaxSubSet2
            REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE, INTENT(IN) ::  new_comb        
            INTEGER, DIMENSION(:), ALLOCATABLE, INTENT(IN) :: index
            REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: temp
            INTEGER :: i
            TYPE(outfuction) :: outfunct
            INTEGER :: clock_rate, clock_start, clock_stop  
            INTEGER :: DIMEN(2)
                    
#ifdef TIMEPAR      
            CALL SYSTEM_CLOCK(count_rate=clock_rate)
            CALL SYSTEM_CLOCK(count=clock_start)
#endif          
        
            DIMEN = SHAPE(new_comb)
            
            do i = 1, DIMEN(2)
                ! Evaluate
                ALLOCATE(temp(nrand))
                temp = new_comb(:,i)

                outfunct = ssm_evalfc(exp1, fitnessfunction, temp, problem1, opts1, nconst, 0)
                nfuneval = nfuneval + 1
                DEALLOCATE(temp)
                if (outfunct%include .eq. 1) then
                    childset%x(:,counter)=outfunct%x
                    childset%f(counter)= outfunct%value
                    childset%fpen(counter) = outfunct%value_penalty
                    childset%penalty(counter) = outfunct%pena
                    if (ALLOCATED(outfunct%nlc) ) childset%nlc(:,counter) = outfunct%nlc
                    childset%parent_index(counter) = index(i)
                    !print *, i, "childset%f -->", childset%fpen(counter), "index -->", index(i)
                    counter = counter + 1
                   ! print *, outfunct%value_penalty, "<", candidateset%fpen(index(i)), " - ", index(i)
                    if (outfunct%value_penalty < candidateset%fpen(index(i))) then
                        !if ( (candidateset%fpen(index(i)) - outfunct%value_penalty) .GE. TOL_PEN) then
                            !Update candidate
                            !This can be accelerated saving the index and replacing at
                            !the end with the best one
                           ! print *, "EXITO en ", index(i)
                            candidateset%x(:,index(i)) = outfunct%x
                            candidateset%fpen(index(i)) = outfunct%value_penalty
                            candidateset%f(index(i)) = outfunct%value
                            candidateset%penalty(index(i)) = outfunct%pena
                            if (ALLOCATED(outfunct%nlc)) candidateset%nlc(:,index(i)) = outfunct%nlc
                            candidate_update(index(i)) = 1
                            members_update(index(i)) = 0
                        !end if
                    end if
                end if
                call destroy_outfuction(outfunct)
            end do
            
#ifdef TIMEPAR            
            CALL SYSTEM_CLOCK(count=clock_stop)
            timeparallel = timeparallel + REAL(clock_stop-clock_start)/REAL(clock_rate)
#endif              
            
    END SUBROUTINE update_candidateset_with_new_comb
            
    
    
    
! ----------------------------------------------------------------------
! SUBROUTINES check_stopping_criteria
! ----------------------------------------------------------------------        
    FUNCTION check_stopping_criteria (problem1, opts1, cputime, stopOptimization, fbest, nfuneval)  RESULT(fin)
        IMPLICIT NONE
        REAL(C_DOUBLE), INTENT(IN) :: cputime
        INTEGER, INTENT(IN) :: stopoptimization
        INTEGER(C_LONG), INTENT(INOUT) :: nfuneval
        TYPE(opts), INTENT(IN) :: opts1 
        TYPE(problem), INTENT(IN) :: problem1
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE, INTENT(IN) :: fbest
        INTEGER :: fin, i, j
        fin = 0
            ! Check the stop criterion
            if (nfuneval >= opts1%useroptions%maxeval) then
                fin = 1
            else if (cputime >= opts1%useroptions%maxtime) then
                fin = 2
            else if (stopOptimization .eq. 1) then
                fin = 4
            elseif (ALLOCATED(problem1%vtr)) then
                do i = 1, size(problem1%vtr)
                    do j = 1, size(fbest)
                        if (fbest(j) .LE. problem1%vtr(i)) then
                            fin = 3
                            EXIT
                        end if
                    end do
                end do
            end if
            
    END FUNCTION check_stopping_criteria
    
    
    
    
! ----------------------------------------------------------------------
! SUBROUTINES select_better_solution
! ----------------------------------------------------------------------      
    SUBROUTINE select_better_solution(results, opts1, refset, xbest, fbest, use_bestx, nfuneval, clock_start, fin) 
        TYPE(opts), INTENT(IN) :: opts1 
        TYPE(resultsf), INTENT(INOUT) :: results
        TYPE(Refsettype), INTENT(INOUT) :: refset
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE, INTENT(INOUT) :: xbest, fbest
        INTEGER, INTENT(IN) :: clock_start
        INTEGER :: clock_stop
        INTEGER, INTENT(IN) :: fin
        INTEGER, INTENT(INOUT) :: use_bestx
        INTEGER, DIMENSION(:), ALLOCATABLE :: index_result
        INTEGER, DIMENSION(:), ALLOCATABLE :: gg, hh
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE ::new_fbest,temp 
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)) :: cputime2
        INTEGER(C_LONG), INTENT(IN) :: nfuneval
        CHARACTER(len = 2) :: typesearch
        INTEGER :: auxsize
        
            ! Extraemos los indices de 
            ALLOCATE(index_result(size(refset%penalty)))
            typesearch = "LT"
            index_result = compare_vector(refset%penalty, opts1%useroptions%tolc, typesearch, auxsize)
            ALLOCATE(gg(auxsize))
            call index_of_ones(index_result, gg)
            CALL reajust_index(gg)
            DEALLOCATE(index_result)
            ! indice de los valores de penalty que son menores que la tolerancia de las restricciones
            if (ALLOCATED(gg)) then
                ALLOCATE(temp(size(gg)))
                temp(1: size(gg)) = refset%fpen(gg)
                if (.not. ALLOCATED(new_fbest)) ALLOCATE(new_fbest(1))
                if (.not. ALLOCATED(hh)) ALLOCATE(hh(1))                
                new_fbest = minval(temp)
                hh = minloc(temp)
                DEALLOCATE(temp)
                if (new_fbest(1) < fbest(1)) then
                    xbest=refset%x(:,gg(hh(1)))
                    fbest=new_fbest
                    use_bestx=1
                    if (opts1%useroptions%inter_save .eq. 1) then 
                        CALL SYSTEM_CLOCK(count=clock_stop)
                        cputime2 = REAL(clock_stop-clock_start)/REAL(clock_rate)      
                        
                        if (.not. ALLOCATED(results%fbest) ) ALLOCATE(results%fbest(size(fbest)))
                        results%fbest(1)=fbest(1);
                        if (.not. ALLOCATED(results%xbest) ) ALLOCATE(results%xbest(size(xbest)))
                        results%xbest=xbest
                        results%numeval=nfuneval
                        results%end_crit=fin
                        results%cputime=cputime2
                        results%refsett%x=refset%x                       
                        results%refsett%f=refset%f                  
                        results%refsett%fpen=refset%fpen  
                        if (ALLOCATED(refset%nlc)) results%refsett%nlc=refset%nlc             
                        results%refsett%penalty=refset%penalty  
                    end if
                end if
                if (ALLOCATED(new_fbest)) DEALLOCATE(new_fbest)
            end if
            IF (ALLOCATED(gg)) DEALLOCATE(gg)
            IF (ALLOCATED(hh)) DEALLOCATE(hh)
            
     END SUBROUTINE select_better_solution
 
     
     
     
! ----------------------------------------------------------------------
! SUBROUTINES update_refset_change
! ----------------------------------------------------------------------      
     SUBROUTINE update_refset_change(members_update, refset_change) 
         INTEGER, DIMENSION(:), ALLOCATABLE, INTENT(INOUT) :: refset_change
         INTEGER, DIMENSION(:), ALLOCATABLE, INTENT(INOUT) :: members_update
         INTEGER, DIMENSION(:), ALLOCATABLE :: members_update_non_zero_index, members_update_zero_index
         CHARACTER(len = 2) :: typesearch
         
         typesearch = "GT"
         ALLOCATE(members_update_non_zero_index(size(members_update)))
         members_update_non_zero_index = 0
         CALL find_in_vector_int(members_update, 1 , members_update_non_zero_index, typesearch)
         if (ALLOCATED(members_update_non_zero_index)) then
              CALL reajust_index(members_update_non_zero_index)
              if (ALLOCATED(members_update_non_zero_index)) &
                refset_change(members_update_non_zero_index) = refset_change(members_update_non_zero_index) + 1
         end if
            
         typesearch = "EQ"
         
         ALLOCATE(members_update_zero_index(size(members_update)))
         
         members_update_zero_index = members_update_zero_index * 0
         CALL find_in_vector_int(members_update, 0 , members_update_zero_index, typesearch)
         
         if (ALLOCATED(members_update_zero_index)) then
              CALL reajust_index(members_update_zero_index)  
              if (ALLOCATED(members_update_zero_index)) &
                refset_change(members_update_zero_index) = refset_change(members_update_zero_index) * 0
         end if
         
         if (ALLOCATED(members_update_non_zero_index))  DEALLOCATE(members_update_non_zero_index)
         if (ALLOCATED(members_update_zero_index))  DEALLOCATE(members_update_zero_index)
            
    END SUBROUTINE update_refset_change
    
    
    
! ----------------------------------------------------------------------
! SUBROUTINES remove_possible_stuck_members_in_refset
! ----------------------------------------------------------------------      
    SUBROUTINE remove_possible_stuck_members_in_refset(problem1, opts1, exp1, fitnessfunction, nconst, &
                        refset, refset_change,nfuneval, nvar, xl_log, xu_log )
        INTEGER, DIMENSION(:), ALLOCATABLE, INTENT(INOUT) :: refset_change
        TYPE(opts), INTENT(INOUT) :: opts1 
        TYPE(Refsettype), INTENT(INOUT) :: refset
        TYPE(problem), INTENT(IN) :: problem1
        TYPE(C_PTR), INTENT(INOUT) :: exp1
        TYPE(C_FUNPTR), INTENT(INOUT) :: fitnessfunction ! fobj 
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE, INTENT(IN) :: xl_log, xu_log
        INTEGER, INTENT(IN) :: nconst, nvar 
        INTEGER(C_LONG), INTENT(INOUT) :: nfuneval
        CHARACTER(len = 2) :: typesearch
        INTEGER, DIMENSION(:), ALLOCATABLE ::  index_members_to_change
        INTEGER :: to_replace, replaced
        TYPE(outfuction) :: outfunct
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: new_refset_member
        
        typesearch = "GT"
        ALLOCATE(index_members_to_change(size(refset_change)))
        index_members_to_change = 0

        
        CALL find_in_vector_int(refset_change, opts1 % useroptions % nstuck_solution, &
        index_members_to_change, typesearch)
        if (ALLOCATED(index_members_to_change)) then
            CALL reajust_index(index_members_to_change)
            if (ALLOCATED(index_members_to_change)) &
            refset_change(index_members_to_change) = 0
        end if
            
            if (ALLOCATED(index_members_to_change) ) then
                to_replace = size(index_members_to_change)
                replaced = 0
                !print *, " - remove_possible_stuck_members_in_refset -- to_remplace= ", &
                 !   to_replace, "solutions"
                do while (replaced < to_replace )
                    !print *, "stuck"
                        ALLOCATE(new_refset_member(nvar))
                        CALL random_Vector_SEED(exp1,new_refset_member, nvar, xl_log, xu_log)
                        if (ALLOCATED(opts1%useroptions%log_var) .and. size(opts1%useroptions%log_var) .GT. 0)  &
                                CALL converttonormal(new_refset_member, nvar)
                        outfunct = ssm_evalfc(exp1, fitnessfunction, new_refset_member, problem1, opts1, nconst, 0)
                        nfuneval = nfuneval + 1
                        if (outfunct%include .eq. 1) then
                            refset%x(:, index_members_to_change(replaced+1)) = outfunct%x
                            refset%f(index_members_to_change(replaced+1)) = outfunct%value
                            refset%fpen(index_members_to_change(replaced+1)) = outfunct%value_penalty
                            if (ALLOCATED(refset%nlc))  refset%nlc(:, index_members_to_change(replaced+1))=outfunct%nlc
                            refset%penalty(index_members_to_change(replaced+1)) = outfunct%pena
                            refset_change(index_members_to_change(replaced+1)) = 0;
                            replaced=replaced +1
                        end if
                        call destroy_outfuction(outfunct)
                        DEALLOCATE(new_refset_member)
                end do
                
            end if
            
            if (ALLOCATED(index_members_to_change))  DEALLOCATE(index_members_to_change)
     END SUBROUTINE remove_possible_stuck_members_in_refset       
     
     
     
! ----------------------------------------------------------------------
! SUBROUTINES remove_possible_stuck_members_in_refset
! ----------------------------------------------------------------------      
    SUBROUTINE remove_possible_stuck_members_in_refset_det(problem1, opts1, exp1, fitnessfunction, nconst, &
                        refset, refset_change,nfuneval, nvar, xl_log, xu_log, contador, matrixR )
        INTEGER, DIMENSION(:), ALLOCATABLE, INTENT(INOUT) :: refset_change
        TYPE(opts), INTENT(INOUT) :: opts1 
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE, INTENT(INOUT) :: matrixR
        INTEGER, INTENT(INOUT) :: contador
        TYPE(Refsettype), INTENT(INOUT) :: refset
        TYPE(problem), INTENT(IN) :: problem1
        TYPE(C_PTR), INTENT(INOUT) :: exp1
        TYPE(C_FUNPTR), INTENT(INOUT) :: fitnessfunction ! fobj 
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE, INTENT(IN) :: xl_log, xu_log
        INTEGER, INTENT(IN) :: nconst, nvar 
        INTEGER(C_LONG), INTENT(INOUT) :: nfuneval
        CHARACTER(len = 2) :: typesearch
        INTEGER, DIMENSION(:), ALLOCATABLE ::  index_members_to_change
        INTEGER :: to_replace, replaced
        TYPE(outfuction) :: outfunct
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: new_refset_member
        
        typesearch = "GT"
        ALLOCATE(index_members_to_change(size(refset_change)))
        index_members_to_change = 0

        
        CALL find_in_vector_int(refset_change, opts1 % useroptions % nstuck_solution, &
        index_members_to_change, typesearch)
        if (ALLOCATED(index_members_to_change)) then
            CALL reajust_index(index_members_to_change)
            if (ALLOCATED(index_members_to_change)) &
            refset_change(index_members_to_change) = 0
        end if
            
            if (ALLOCATED(index_members_to_change) ) then
                to_replace = size(index_members_to_change)
                replaced = 0
                !print *, " - remove_possible_stuck_members_in_refset -- to_remplace= ", &
                 !   to_replace, "solutions"
                do while (replaced < to_replace )
                    !print *, "stuck"
                        ALLOCATE(new_refset_member(nvar))
                        
                        CALL random_det_Vector_SEED(new_refset_member, nvar, xl_log, xu_log, contador, matrixR)
                        if (ALLOCATED(opts1%useroptions%log_var) .and. size(opts1%useroptions%log_var) .GT. 0)  &
                                CALL converttonormal(new_refset_member, nvar)
                        outfunct = ssm_evalfc(exp1, fitnessfunction, new_refset_member, problem1, opts1, nconst, 0)
                        nfuneval = nfuneval + 1
                        if (outfunct%include .eq. 1) then
                            refset%x(:, index_members_to_change(replaced+1)) = outfunct%x
                            refset%f(index_members_to_change(replaced+1)) = outfunct%value
                            refset%fpen(index_members_to_change(replaced+1)) = outfunct%value_penalty
                            if (ALLOCATED(refset%nlc))  refset%nlc(:, index_members_to_change(replaced+1))=outfunct%nlc
                            refset%penalty(index_members_to_change(replaced+1)) = outfunct%pena
                            refset_change(index_members_to_change(replaced+1)) = 0;
                            replaced=replaced +1
                        end if
                        call destroy_outfuction(outfunct)
                        DEALLOCATE(new_refset_member)
                end do
                
            end if
            
            if (ALLOCATED(index_members_to_change))  DEALLOCATE(index_members_to_change)
    END SUBROUTINE remove_possible_stuck_members_in_refset_det       
     
     
     
! ----------------------------------------------------------------------
! SUBROUTINES check_the_improvement_of_fbest
! ----------------------------------------------------------------------                               
     SUBROUTINE check_the_improvement_of_fbest(results, opts1, fbest, xbest, fbest_lastiter, nreset, cputime2, fin, nfuneval)
        TYPE(opts), INTENT(IN) :: opts1 
        TYPE(resultsf), INTENT(INOUT) :: results
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE, INTENT(INOUT) :: fbest, fbest_lastiter,&
                                                                                                           xbest
        INTEGER, INTENT(INOUT) :: nreset, fin
        INTEGER(C_LONG), INTENT(INOUT) :: nfuneval
        REAL(C_DOUBLE), INTENT(IN) :: cputime2
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE :: temp2
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE ::  temp
        REAL(C_DOUBLE), DIMENSION(:), ALLOCATABLE :: timeR
        INTEGER(C_LONG), DIMENSION(:), ALLOCATABLE :: index_result
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)) :: auxiliarvalue
        INTEGER :: DIMEN(2)
            
        auxiliarvalue = REAL(0.0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))
                
        if (fbest(1)<fbest_lastiter(1)) auxiliarvalue=1
                
        auxiliarvalue = auxiliarvalue/fbest_lastiter(1)
            
        if (auxiliarvalue > opts1%useroptions%tolc ) then
               ALLOCATE(temp(size(results%f) + 1))
               temp(1:size(results%f)) = results%f
               temp(   size(results%f)+1) = fbest(1)
               CALL MOVE_ALLOC(temp, results%f )
                
               DIMEN = shape(results%x)
               ALLOCATE(temp2(DIMEN(1),DIMEN(2)+1))
               temp2(:,1:DIMEN(2)) = results%x
               temp2(:,DIMEN(2)+1) = xbest
               CALL MOVE_ALLOC(temp2,results%x)
                
               ALLOCATE(timeR(size(results%time) + 1))
               timeR(1:size(results%time)) = results%time
               timeR( size(results%time) +1) = cputime2
               CALL MOVE_ALLOC(timeR, results%time )
               
               ALLOCATE(index_result(size(results%neval) + 1))
               index_result(1:size(results%neval)) = results%neval
               index_result( size(results%neval)+1) = nfuneval
               CALL MOVE_ALLOC(index_result, results%neval )
               
               nreset = 0
               
               !if opts1%useroptions%plot
         else 
                if (opts1%globaloptions%n_stuck > 0) then
                    nreset=nreset+1
                    if (nreset .eq. opts1%globaloptions%n_stuck) then
                        fin = 5
                    end if
                end if
         end if
     
    END SUBROUTINE check_the_improvement_of_fbest
          
    

! ----------------------------------------------------------------------
! SUBROUTINES apply_beyond_to_members_to_update
! ----------------------------------------------------------------------   
    SUBROUTINE apply_beyond_to_members_to_update( exp1, opts1, fitnessfunction, problem1, members_to_update,  nfuneval, &
        refset, candidateset, nconst, nrand, nvar)
        TYPE(opts), INTENT(IN) :: opts1 
        TYPE(Refsettype), INTENT(INOUT) :: refset, candidateset
        TYPE(problem), INTENT(IN) :: problem1
        TYPE(C_PTR), INTENT(INOUT) :: exp1
        TYPE(C_FUNPTR), INTENT(INOUT) :: fitnessfunction ! fobj 
        INTEGER(C_LONG), INTENT(INOUT) :: nfuneval
        INTEGER, DIMENSION(:), ALLOCATABLE, INTENT(INOUT)  ::  members_to_update
        INTEGER, INTENT(IN) :: nconst, nvar , nrand
        
        TYPE(outfuction) :: outfunct
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: temp1, temp
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)) :: auxiliarvalue
        
        if (ALLOCATED(members_to_update)) then
                ! exploramos los miembros a actualizar (los que prometen más resultados)
                ! y aplicamos ls técnica go-beyond en ellos
                
                !print *, " - apply_beyond_to_members_to_update to ", size(members_to_update), "solutions"
                
                do i = 1, size(members_to_update)
                    ALLOCATE(temp(nvar))
                    temp = refset % x(:,members_to_update(i))
                    ALLOCATE(temp1(nvar))

                    
                    temp1 = candidateset % x(:,members_to_update(i))
                    auxiliarvalue = candidateset % fpen(members_to_update(i))

                    ! ssm_beyond
                    CALL ssm_beyond(exp1, temp, temp1, auxiliarvalue, nfuneval, &
                    fitnessfunction, nrand, nconst, outfunct, opts1, problem1, 0)
                    DEALLOCATE(temp)
                    DEALLOCATE(temp1)

                    !print *, "ENTRA x ->", candidateset%x(:,members_to_update(i)), &
                    !    "eval OLD ->", candidateset%f(members_to_update(i)), &
                    !  "eval OLD", outfunct % value
                    
                    !print *, "refset",  refset%f(members_to_update(i)), &
                    !    "candidateset", candidateset%f(members_to_update(i)), &
                    !    "vector", outfunct%value
                          
                             
                    if (ALLOCATED(outfunct%x) ) then  
                        !if ((refset % fpen(members_to_update(i)) - outfunct % value_penalty) .GE. TOL_PEN) then 
                     !   print *, "         MELLORA CON :", outfunct % x
                        refset % x(:,members_to_update(i)) = outfunct % x
                        refset % f(members_to_update(i)) = outfunct % value
                        refset % fpen(members_to_update(i)) = outfunct % value_penalty
                        refset % penalty(members_to_update(i)) = outfunct % pena
                        if (ALLOCATED(refset%nlc)) refset % nlc(:,members_to_update(i)) = outfunct % nlc
                        !end if
                    else    
                       ! if ((refset % fpen(members_to_update(i)) - candidateset % fpen(members_to_update(i))) .GE. TOL_PEN) then
                      !  print *, "non include"
                        refset % x(:,members_to_update(i)) = candidateset % x(:,members_to_update(i))
                        refset % f(members_to_update(i)) = candidateset % f(members_to_update(i))
                        refset % fpen(members_to_update(i)) = candidateset % fpen(members_to_update(i))
                        refset % penalty(members_to_update(i)) = candidateset % penalty(members_to_update(i))
                        if (ALLOCATED(refset%nlc)) refset % nlc(:,members_to_update(i)) = candidateset % nlc(:,members_to_update(i))
                      !  end if
                    end if
                    
                    call destroy_outfuction(outfunct)
                end do
            end if
            
    END SUBROUTINE apply_beyond_to_members_to_update
    
    
! ----------------------------------------------------------------------
! SUBROUTINES apply_beyond_to_members_to_update_det
! ----------------------------------------------------------------------   
    SUBROUTINE apply_beyond_to_members_to_update_det( exp1, opts1, fitnessfunction, problem1, members_to_update,  nfuneval, &
        refset, candidateset, nconst, nrand, nvar, contador, matrixR)
        TYPE(opts), INTENT(IN) :: opts1 
        TYPE(Refsettype), INTENT(INOUT) :: refset, candidateset
        TYPE(problem), INTENT(IN) :: problem1
        TYPE(C_PTR), INTENT(INOUT) :: exp1
        TYPE(C_FUNPTR), INTENT(INOUT) :: fitnessfunction ! fobj 
        INTEGER(C_LONG), INTENT(INOUT) :: nfuneval
        INTEGER, DIMENSION(:), ALLOCATABLE, INTENT(INOUT)  ::  members_to_update
        INTEGER, INTENT(IN) :: nconst, nvar , nrand
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE, INTENT(INOUT) :: matrixR
        INTEGER, INTENT(INOUT) :: contador
        INTEGER :: cont1
        
        TYPE(outfuction) :: outfunct
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: temp1, temp
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)) :: auxiliarvalue
        
        if (ALLOCATED(members_to_update)) then
                ! exploramos los miembros a actualizar (los que prometen más resultados)
                ! y aplicamos ls técnica go-beyond en ellos
                
                !print *, " - apply_beyond_to_members_to_update to ", size(members_to_update), "solutions"
                
            cont1 = 0
                do i = 1, size(members_to_update)
                    ALLOCATE(temp(nvar))
                    temp = refset % x(:,members_to_update(i))
                    ALLOCATE(temp1(nvar))

                    
                    temp1 = candidateset % x(:,members_to_update(i))
                    auxiliarvalue = candidateset % fpen(members_to_update(i))

                        
                    ! ssm_beyond
                    CALL ssm_beyond_det(exp1, temp, temp1, auxiliarvalue, nfuneval, &
                    fitnessfunction, nrand, nconst, outfunct, opts1, problem1, 0, contador, matrixR)                  

                    DEALLOCATE(temp)
                    DEALLOCATE(temp1)
                          
                             
                    if (ALLOCATED(outfunct%x) ) then  
                        refset % x(:,members_to_update(i)) = outfunct % x
                        refset % f(members_to_update(i)) = outfunct % value
                        refset % fpen(members_to_update(i)) = outfunct % value_penalty
                        refset % penalty(members_to_update(i)) = outfunct % pena
                        if (ALLOCATED(refset%nlc)) refset % nlc(:,members_to_update(i)) = outfunct % nlc
                    else    
                        refset % x(:,members_to_update(i)) = candidateset % x(:,members_to_update(i))
                        refset % f(members_to_update(i)) = candidateset % f(members_to_update(i))
                        refset % fpen(members_to_update(i)) = candidateset % fpen(members_to_update(i))
                        refset % penalty(members_to_update(i)) = candidateset % penalty(members_to_update(i))
                        if (ALLOCATED(refset%nlc)) refset % nlc(:,members_to_update(i)) = candidateset % nlc(:,members_to_update(i))
                    end if
                    
                    call destroy_outfuction(outfunct)
                end do
                
            end if
            
    END SUBROUTINE apply_beyond_to_members_to_update_det
    
! ----------------------------------------------------------------------
! SUBROUTINES reajustchild
! ----------------------------------------------------------------------      
    SUBROUTINE reajustchild(childset, cont, nvar, nconst)
       IMPLICIT NONE
       TYPE(Refsettype), INTENT(INOUT) :: childset
       TYPE(Refsettype) :: childset_aux
       INTEGER, INTENT(IN) :: nvar, nconst, cont
       INTEGER :: counter 
       
       counter = cont-1
       
       if (counter .lt. size(childset%fpen)) then
        CALL create_childset(childset_aux, nvar, counter, nconst)
        childset_aux % x = childset % x(:, 1:counter)
        childset_aux % f = childset % f(1:counter)
        childset_aux % fpen = childset % fpen(1:counter)
        childset_aux % penalty = childset % penalty(1:counter)
        if (nconst .gt. 0) childset_aux % nlc = childset % nlc(:, 1:counter)
        childset_aux % parent_index = childset % parent_index(1:counter)
        CALL MOVE_ALLOC(childset_aux % x, childset % x)
        CALL MOVE_ALLOC(childset_aux % f, childset % f)
        CALL MOVE_ALLOC(childset_aux % fpen, childset % fpen)
        CALL MOVE_ALLOC(childset_aux % penalty, childset % penalty)
        CALL MOVE_ALLOC(childset_aux % parent_index, childset % parent_index)
        if (nconst .gt. 0) CALL MOVE_ALLOC(childset_aux % nlc, childset % nlc)

        end if
    END SUBROUTINE reajustchild

! ----------------------------------------------------------------------
! SUBROUTINES serializerefset
! ----------------------------------------------------------------------      
    SUBROUTINE serializerefset(refset, nvar, tam, servector, rest)
        IMPLICIT NONE
        TYPE(Refsettype), INTENT(INOUT) :: refset
        INTEGER, INTENT(IN) :: nvar, tam      
        INTEGER, INTENT(INOUT) :: rest
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE, INTENT(INOUT) :: servector
        INTEGER :: i
        
        DO i=1, tam
            servector(1:nvar,i) =  refset%x(:,i)
            if (ALLOCATED(refset % nlc)) then
                servector((nvar + 1):(nvar + nvar), i) = refset % nlc(:, i)
                servector(nvar + nvar + 1, i) = refset % f(i)
                servector(nvar + nvar + 2, i) = refset % penalty(i)
                servector(nvar + nvar + 3, i) = refset % fpen(i)
                rest = nvar + 3
            else
                servector(nvar  + 1, i) = refset % f(i)
                servector(nvar  + 2, i) = refset % penalty(i)
                servector(nvar  + 3, i) = refset % fpen(i)    
                rest = 3
            end if

        END DO
            
            
    END SUBROUTINE    serializerefset
    
    
! ----------------------------------------------------------------------
! SUBROUTINES serializerefset
! ----------------------------------------------------------------------      
    SUBROUTINE deserializerefset(refset, nvar, tam, servector)
        IMPLICIT NONE
        TYPE(Refsettype), INTENT(INOUT) :: refset
        INTEGER, INTENT(IN) :: nvar, tam      
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE, INTENT(INOUT) :: servector
        INTEGER :: i
        
        DO i=1, tam
            refset%x(:,i) = servector(1:nvar,i) 
            if (ALLOCATED(refset % nlc)) then
                refset % nlc(:, i) = servector((nvar + 1):(nvar + nvar), i) 
                refset % f(i) = servector(nvar + nvar + 1, i)
                refset % penalty(i) = servector(nvar + nvar + 2, i) 
                refset % fpen(i) = servector(nvar + nvar + 3, i) 
            else
                refset % f(i) = servector(nvar  + 1, i) 
                refset % penalty(i) = servector(nvar  + 2, i) 
                refset % fpen(i)   = servector(nvar  + 3, i)               
            end if

        END DO
            
            
    END SUBROUTINE    deserializerefset    
    
! ----------------------------------------------------------------------
! SUBROUTINES sizeseriallize
! ----------------------------------------------------------------------     
    SUBROUTINE sizeseriallize(nvar, nconst, size)
        IMPLICIT NONE
        INTEGER, INTENT(INOUT) :: nvar, nconst, size
        
        if (nconst .GT. 0) then
            size = nvar+nvar+1+1+1
        else
            size = nvar+1+1+1
        endif
        
    END SUBROUTINE sizeseriallize
    
    
    
! ----------------------------------------------------------------------
! SUBROUTINES checkbounds
! ----------------------------------------------------------------------  
    SUBROUTINE checkbounds(problem1,st)
        IMPLICIT NONE
        TYPE(problem), INTENT(IN) :: problem1
        INTEGER, INTENT(INOUT) :: st
        ! Check if bounds have the same dimension   CHECKBOUNDS
        if (size(problem1%XU) .NE. size(problem1%XL)) then
            PRINT *, "Upper and lower bounds have different dimension!!!!"
            PRINT *, "EXITING"
            CALL EXIT(st)
        end if
        ! ..   
    END SUBROUTINE checkbounds
    
! ----------------------------------------------------------------------
! SUBROUTINES initbestvars
! ----------------------------------------------------------------------  
    SUBROUTINE initbestvars(problem1,xbest,fbest,nvar) 
        IMPLICIT NONE
        TYPE(problem), INTENT(IN) :: problem1
        INTEGER, INTENT(IN) :: nvar
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE, INTENT(INOUT) :: xbest, fbest
        INTEGER, DIMENSION(:), ALLOCATABLE :: iiim
        INTEGER :: iii
        
        !INIT BEST VARIABLES
        if (ALLOCATED(problem1%F0)) then
            if (ALLOCATED(fbest)) DEALLOCATE(fbest) 
            ALLOCATE(fbest(1))
            fbest = minval(problem1%F0)
            ALLOCATE(iiim(1))
            iiim = minloc(problem1%F0)
            iii = iiim(1)
            if (ALLOCATED(xbest)) DEALLOCATE(xbest) 
            ALLOCATE(xbest(nvar))
            xbest = problem1%X0(:,iii)
        else
            if (ALLOCATED(fbest)) DEALLOCATE(fbest) 
            ALLOCATE(fbest(1))
            fbest(1) = INFINITE
            if (ALLOCATED(xbest)) DEALLOCATE(xbest) 
            ALLOCATE(xbest(nvar))
            xbest = INFINITE
        end if
        
        IF (ALLOCATED(iiim)) DEALLOCATE(iiim)
    
    END SUBROUTINE initbestvars  
    
    
! ----------------------------------------------------------------------
! SUBROUTINES initintbinvars
! ----------------------------------------------------------------------  
    SUBROUTINE initintbinvars(problem1)
        IMPLICIT NONE
        TYPE(problem), INTENT(INOUT) :: problem1        
        
        if (problem1%int_var .EQ. - 1) then
            problem1%int_var = 0
        end if

        if (problem1%bin_var .EQ. - 1) then
            problem1%bin_var = 0
        end if
        ! ---
    END SUBROUTINE initintbinvars 
    
    
! ----------------------------------------------------------------------
! SUBROUTINES initlocaloptions
! ----------------------------------------------------------------------  
    SUBROUTINE initlocaloptions(opts1)    
        IMPLICIT NONE
        TYPE(opts), INTENT(INOUT) :: opts1         
            ! INIT LOCAL OPTIONS
        
        if (opts1 % localoptions % empty .eq. 0) then
           ! if (opts1 % localoptions % n1 .EQ. - 1) then
           !     opts1 % localoptions % n1 = 1
           ! end if

           ! if (opts1 % localoptions % n2 .EQ. - 1) then
           !     opts1 % localoptions % n2 = 10
           ! end if

            if (ALLOCATED(opts1 % localoptions % finish)) then
                opts1 % localoptions % finish(1) = opts1 % localoptions % solver
            end if
        end if
        
    END SUBROUTINE initlocaloptions 
        
    
! ----------------------------------------------------------------------
! SUBROUTINES initlocaloptions
! ----------------------------------------------------------------------  
    SUBROUTINE calcnconst(problem1,nconst) 
        IMPLICIT NONE
        TYPE(problem), INTENT(IN) :: problem1 
        INTEGER, INTENT(INOUT) :: nconst
        
        if (ALLOCATED(problem1%CU)) then
            nconst = size(problem1%CU)
        else
            nconst = 0
        end if
        
    END SUBROUTINE calcnconst 
    
    
! ----------------------------------------------------------------------
! SUBROUTINES check_output_obj_funct
! ----------------------------------------------------------------------     
    SUBROUTINE check_output_obj_funct(problem1, opts1,status, nconst)
        IMPLICIT NONE    
        TYPE(opts), INTENT(INOUT) :: opts1 
        TYPE(problem), INTENT(IN) :: problem1 
        INTEGER, INTENT(INOUT) :: status, nconst
                
        ! CHECK OUTPUT OBJECTIVE FUNCTION
        !Transform the objective function into a handle
        !fobj=str2func(problem.f);

        !Check if the objecttive function has 2 output arguments in constrained problems
        if (opts1 % localoptions % empty .eq. 0) then
            if (nconst .EQ. 1) then
                if (opts1 % localoptions % extrap % empty .eq. 0) then
                    PRINT *, "For constrained problems the objective function must have at least 2 output arguments \n"
                    PRINT *, "EXITING"
                    CALL EXIT(status)
                end if
            end if

            if (((problem1 % int_var + problem1 % bin_var) > 0) .AND. (opts1 % localoptions % solver .NE. '') &
                .AND. (opts1 % localoptions % solver .EQ. 'misqp')) then
                PRINT *, "For problems with integer and/or binary variables you must use MISQP as a local solver"
                PRINT *, "EXITING"
                CALL EXIT(status)
            end if

            ! --- COMPROBAMOS O ESTADO DOS PARÁMETROS DO LOCAL SOLVER N2FB, QUEDA PENDENTE
            if ((opts1 % localoptions % solver .EQ. 'nf2b') .OR. (opts1 % localoptions % solver .EQ. 'dnf2b')) then
                if (opts1 % localoptions % extrap % empty .eq. 0) then
                    PRINT *, "nf2d or dnf2b require 3 output arguments\n"
                    PRINT *, "EXITING"
                    CALL EXIT(status)
                else
                    !CALL random_Vector_SEED(randomV, nvar, problem1%XL, problem1%XU)
                end if
            end if
        end if
    
    END SUBROUTINE check_output_obj_funct
    
    
! ----------------------------------------------------------------------
! SUBROUTINES build_aux_local_search
! ----------------------------------------------------------------------     
    SUBROUTINE build_aux_local_search(opts1)
        IMPLICIT NONE    
        TYPE(opts), INTENT(INOUT) :: opts1     
            
        ! Build auxiliary files in case local search is activated
        if (opts1%localoptions%empty .eq. 0) then
            ! ssm_aux_local(fitnessfunction, problem1%XL, problem1%XU, problem1%CL, problem1%CU, problem1%neq, opts1%localoptions%solver, nvar, opts1%localoptions%pextra)
            ! ssm_aux_local <-- ESTA FUNCION E PARA CARGAR OS LOCAL SOLVER DA FUNCION DE MATLAB
        end if
        
    END SUBROUTINE build_aux_local_search        
    
    
    
    
    
    SUBROUTINE finalize_algorithm(exp1,problem1,results,opts1,fitnessfunction,output,refset,xbest,fbest,fin,nfuneval,NPROC, &
        cputime3, nconst, nvar)
        USE localsolver
        IMPLICIT NONE
        TYPE(C_PTR), INTENT(INOUT) :: exp1, output
        TYPE(C_FUNPTR), INTENT(INOUT) :: fitnessfunction       
        TYPE(resultsf), INTENT(INOUT) :: results
        TYPE(opts), INTENT(INOUT) :: opts1    
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)) :: cputime3
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE, INTENT(INOUT) :: xbest, fbest
        INTEGER(C_LONG), INTENT(INOUT) :: nfuneval
        INTEGER(C_INT), INTENT(INOUT) :: fin, NPROC, nconst, nvar
        TYPE(Refsettype), INTENT(INOUT) :: refset
        TYPE(problem), INTENT(INOUT) :: problem1 
        
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: X0
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)) :: F0
        INTEGER(C_LONG) :: nevals
        INTEGER :: DIMEN(2)
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: temp
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE :: temp2
        REAL(C_DOUBLE), DIMENSION(:), ALLOCATABLE :: timeR
        INTEGER(C_LONG), DIMENSION(:), ALLOCATABLE :: temp_int
        TYPE(outfuction) :: outfunct
        
        if (fin > 0) then
            if (opts1%localoptions%empty .eq. 0 ) then
               if (ALLOCATED(opts1%localoptions%finish) .and. (fin < 3)) then
                    if ( xbest(1) .eq. INFINITE ) then
                       ALLOCATE(X0(size(xbest)))
                       X0 = xbest
                       F0 = fbest(1)
                    else
                       ALLOCATE(X0(nvar))
                       X0 = refset%x(:,1)
                       F0 = refset%fpen(1)                   
                    end if
                    
                    if  (opts1%useroptions%iterprint .eq. 1 ) then
                        print *, "Final local refinement with: ",  opts1%localoptions%solver
                        print *, "Initial point function value: ", F0
                    end if
                    CALL call_local_solver(problem1, exp1,opts1,fitnessfunction, X0, F0, fin,NPROC,nevals, output)
                    outfunct = ssm_evalfc(exp1,fitnessfunction, X0 ,problem1, opts1, nconst,0)
                    F0 = outfunct%value_penalty
                    nfuneval=nfuneval+nevals
                    if ( (outfunct%include .eq. 1 ) .and. (outfunct%pena .LE. opts1%useroptions%tolc ) ) then
                        if (  outfunct%value_penalty .LT. fbest(1) ) then
                            fbest(1) = F0
                            xbest=outfunct%value
                            if ( fbest(1) .LT. problem1%vtr(1)  ) then
                                fin = 3
                            end if
                        end if
                    end if
                
                    if ( opts1%useroptions%iterprint .eq. 1 ) then
                        print *, "Local solution function value:",outfunct%pena
                        print *, "Number of function evaluations in the local search: ", nfuneval
                        !print *, "CPU Time of the local search: %f  seconds "
                    end if
            
                end if
            end if
               
            ALLOCATE(temp(size(results%f) + 1))
            temp(1:size(results%f)) = results%f
            temp(   size(results%f)+1) = fbest(1)
            CALL MOVE_ALLOC(temp, results%f )
                
            DIMEN = shape(results%x)
            ALLOCATE(temp2(DIMEN(1),DIMEN(2)+1))
            temp2(:,1:DIMEN(2)) = results%x
            temp2(:,DIMEN(2)+1) = xbest
            CALL MOVE_ALLOC(temp2,results%x)
                
            ALLOCATE(timeR(size(results%time) + 1))
            timeR(1:size(results%time)) = results%time
            timeR( size(results%time) +1) = cputime3
            CALL MOVE_ALLOC(timeR, results%time )
               
            ALLOCATE(temp_int(size(results%neval) + 1))
            temp_int(1:size(results%neval)) = results%neval
            temp_int( size(results%neval)+1) = nfuneval
            CALL MOVE_ALLOC(temp_int, results%neval )
               
            if (.not. ALLOCATED(results%fbest) ) ALLOCATE(results%fbest(size(fbest)))
               
            results%fbest(1)=fbest(1)
            if (.not. ALLOCATED(results%xbest) ) ALLOCATE(results%xbest(size(xbest)))
            results%xbest = xbest
            results%numeval = nfuneval
            results%end_crit = fin
            results%cputime = cputime3
            results%refsett%x = refset%x
            results%refsett%f = refset%f
            results%refsett%fpen = refset%fpen
            if (ALLOCATED(results%refsett%nlc))    results%refsett%nlc =refset%nlc;                
            results%refsett%penalty = refset%penalty
            if ( (opts1%useroptions%plot .eq. 1) .or. (opts1%useroptions%plot .eq. 2 ) ) then
                ! .....
            end if
        end if  
            
    END SUBROUTINE finalize_algorithm
    
END MODULE scattersearchfunctions





