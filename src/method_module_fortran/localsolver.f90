#define TIMELOCAL 1

MODULE localsolver
    USE iso_c_binding
    USE scattersearchtypes
    USE common_functions
    USE misqp_interface
    USE qsort_module
    USE funcevalinterface
CONTAINS




! ----------------------------------------------------------------------
! SUBROUTINE call_local_solver
! ----------------------------------------------------------------------
   SUBROUTINE call_local_solver(problem1, exp1,opts1,fitnessfunction, x0, fval, stopval,NPROC,neval, output)
       IMPLICIT NONE
       TYPE(problem), INTENT(INOUT) :: problem1
       TYPE(opts), INTENT(INOUT) :: opts1
       TYPE(C_PTR), INTENT(INOUT) :: exp1, output
       INTEGER, INTENT(INOUT) :: stopval, NPROC
       INTEGER(C_LONG), INTENT(INOUT) :: neval
       REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), INTENT(INOUT) ::fval
       TYPE(C_FUNPTR), INTENT(INOUT) :: fitnessfunction 
       REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE, INTENT(INOUT):: x0

       
      
  
       if ( opts1%localoptions%solver(1:5) .EQ. 'n2sol' ) then           
           print *, opts1%localoptions%solver
           CALL localsolverscattersearchinterface(exp1,fitnessfunction,NPROC,stopval,x0,fval,neval)      
           
           
       else if ( opts1%localoptions%solver(1:5) .EQ. 'misqp' ) then
           !CALL RUN_MISQP( problem1,exp1,opts1,fitnessfunction, x0, fval, stopval,NPROC,neval )
       end if
       
   END SUBROUTINE call_local_solver

! ----------------------------------------------------------------------
! SUBROUTINE transform_norm
! ----------------------------------------------------------------------
   SUBROUTINE transform_norm ( oldmt, newmt, UB, LB)
       IMPLICIT NONE
       REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE, INTENT(IN) :: oldmt
       REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE, INTENT(INOUT) ::  newmt
       REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE, INTENT(IN) :: UB, LB
       REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: new, old
       INTEGER :: z, j, DIMEN(2) 
       
       DIMEN = shape(oldmt)
       ALLOCATE(new(DIMEN(1)))
       ALLOCATE(old(DIMEN(1)))
       
          do j=1,DIMEN(2)
            old = oldmt(:,j)
            do z=1,DIMEN(1)
                if ( old(z) .eq. LB(z) ) then 
                    new(z) = 0d0
                else if  ( (abs(LB(z)) + abs(UB(z))) .eq. 0 ) then
                    new(z) = 0d0
                else if  ( LB(z) .eq. UB(z) ) then
                    new(z) = 0d0
                else
                      new(z) = old(z) / ( UB(z) - LB(z) )
                end if
            end do
            newmt(:,j) = new
          end do
       DEALLOCATE(new)
       DEALLOCATE(old)
       
   END SUBROUTINE transform_norm





! ----------------------------------------------------------------------
! SUBROUTINE matrix_eucl_dist
! ----------------------------------------------------------------------
   SUBROUTINE matrix_eucl_dist ( a, b , res)
       IMPLICIT NONE
       REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE, INTENT(IN) :: a, b
       REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE, INTENT(INOUT) :: res
       REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)) :: sum
       INTEGER :: i, j, z, DIMEN(2), DIMEN2(2)
       
       DIMEN = shape(res)
       DIMEN2 = shape(a)
       do i=1,  DIMEN(1)
           do j=1, DIMEN(2)
               sum = 0d0
               do z=1,DIMEN2(1)
                    sum =  sum + (a(z,j) - b(z,i))**2d0
               end do
               res(i,j) =  REAL(sqrt(dabs(sum)), KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))             
           end do
       end do
   END SUBROUTINE matrix_eucl_dist   
   
! ----------------------------------------------------------------------
! SUBROUTINE ssm_isdif2
! ----------------------------------------------------------------------
   SUBROUTINE ssm_isdif2(x,group,tol,flag,f,ind,ind2)
       IMPLICIT NONE
       REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE, INTENT(IN) :: x
       REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE, INTENT(INOUT) :: group
       REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), INTENT(IN) :: tol 
       INTEGER, INTENT(IN) :: flag
       INTEGER :: i, DIMEN(2)
       REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), INTENT(INOUT) :: f
       REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)) :: eps
       INTEGER, DIMENSION(:), ALLOCATABLE, INTENT(INOUT) :: ind, ind2
       INTEGER, DIMENSION(:), ALLOCATABLE :: ind_aux, aaa, indexfind
       REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), ALLOCATABLE, DIMENSION(:) :: num, denom, difference
       CHARACTER(len = 2) :: typesearch
       
       typesearch = "LT"
       eps = 2.2204d-016
       DIMEN = shape(group)
       f=0d0
       
       do i=1,DIMEN(2)
           ALLOCATE(num(size(x)))
           ALLOCATE(denom(size(x)))
           ALLOCATE(difference(size(x)))
           num = abs(x-group(:,i) )
           denom = min(abs(x),abs(group(:,i)))
           CALL find_in_vector(denom, eps, indexfind, typesearch)
           if (ALLOCATED(indexfind)) then
               denom(indexfind) = 1
               DEALLOCATE(indexfind)
           end if
           difference = num / denom
           typesearch = "GE"
           CALL find_in_vector(difference, tol, aaa, typesearch)
           
           if (flag .eq. 1 ) then
               if (.not. ALLOCATED(aaa)) then
                   f = f + 1d0
                   if ( .not. ALLOCATED( ind ) ) then
                       ALLOCATE(ind (1) )
                       ind(1) = i
                   else
                       ALLOCATE( ind_aux ( size(ind) + 1 ) )
                       ind_aux(size(ind) + 1) = i
                       CALL MOVE_ALLOC(ind_aux,ind)
                   end if
               end if
           else if ( flag .eq. 2) then
               if  ( ALLOCATED(aaa) .and. (size(aaa) .ne. size(x) ) ) then 
                       if ( .not. ALLOCATED( ind ) ) then
                            ALLOCATE(ind (1) )
                            ind(1) = i
                       else
                            ALLOCATE( ind_aux ( size(ind) + 1 ) )
                            ind_aux(size(ind) + 1) = i
                            CALL MOVE_ALLOC(ind_aux,ind)
                       end if
               else
                       if ( .not. ALLOCATED( ind2 ) ) then
                            ALLOCATE(ind2 (1) )
                            ind2(1) = i
                       else
                            ALLOCATE( ind_aux ( size(ind2) + 1 ) )
                            ind_aux(size(ind2) + 1) = i
                            CALL MOVE_ALLOC(ind_aux,ind2)
                       end if
               end if
           end if
           
           if (ALLOCATED( denom) )DEALLOCATE(denom)
           if (ALLOCATED( difference) )DEALLOCATE(difference)
           if (ALLOCATED( num) )DEALLOCATE(num)
           if (ALLOCATED( aaa) ) DEALLOCATE(aaa)
           
       end do
       
   END SUBROUTINE ssm_isdif2   

! ----------------------------------------------------------------------
! SUBROUTINE ssm_local_filters
! ----------------------------------------------------------------------
    SUBROUTINE ssm_local_filters(exp1,problem1, fitnessfunction, opts1,childset,n_minimo,use_bestx,stage_1,stage_2,xbest, &
        fbest,n_critico,nvar, local_solutions, local_solutions_values, initial_points, initial_points_values, & 
         stopval, NPROC, numeval, fbest_lastiter, results, refset, refset_change, output, locals, localtime)
        IMPLICIT NONE
        TYPE(C_PTR), INTENT(INOUT) :: exp1, output, locals
        TYPE(resultsf), INTENT(INOUT) :: results
        TYPE(C_FUNPTR), INTENT(INOUT) :: fitnessfunction 
        TYPE(opts), INTENT(INOUT) :: opts1
        TYPE(Refsettype), INTENT(INOUT) :: childset, refset
        REAL(C_DOUBLE), INTENT(INOUT) :: localtime
        INTEGER :: clock_rate, clock_start, clock_stop 
        INTEGER, INTENT(INOUT) :: stopval, NPROC
        INTEGER(C_LONG), INTENT(INOUT) :: numeval
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)),  DIMENSION(:), ALLOCATABLE, INTENT(INOUT) :: fbest_lastiter
        TYPE(problem), INTENT(INOUT) :: problem1
        INTEGER, DIMENSION(:), ALLOCATABLE, INTENT(INOUT) :: refset_change 
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE, INTENT(INOUT) :: &
                                                                                            local_solutions, initial_points
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)),DIMENSION(:,:),ALLOCATABLE :: new_initial_points, &
                                                                                        new_local_solutions
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)),DIMENSION(:),ALLOCATABLE :: new_local_solutions_values, &
                                                                                                   new_initial_points_values
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE,INTENT(INOUT) ::local_solutions_values,&
                                                                                                 initial_points_values
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE, INTENT(INOUT) :: xbest,fbest
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: temp_fpen
        INTEGER, INTENT(INOUT) :: n_minimo, stage_1, stage_2, n_critico, use_bestx
        INTEGER, INTENT(IN) :: nvar
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: minimo, X0, X, dismin, child_points, &
                                                                             dismin_save
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE :: local_init, child_norm, &
                                                                                local_init_norm, repmat, dist
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)) :: F0, indexj, fval, f11
        INTEGER, DIMENSION(:), ALLOCATABLE :: I, indrefset, indmin, ind11,ind12
        INTEGER :: ind,j,nconst, DIMEN(2), DIMEN2(2), adiccionar_local
        TYPE(outfuction) :: outfunct
        INTEGER :: MODE
       INTEGER :: idp, par, getidp
       
       par = 0
       idp = 0
       
       idp = getidp(exp1)
       
        ALLOCATE(X0(nvar))
        ALLOCATE(X(nvar))   
        
    if (opts1%localoptions%empty .eq. 0) then
          if (stage_1 .eq. 1) then           
            ALLOCATE(minimo(1) )
            minimo = minval(childset%fpen)
            ALLOCATE(I(1))
            I = minloc(childset%fpen)
            ind = I(1)
            if (minimo(1) .LT. fbest(1) ) then
                X0 =  childset%x(:,ind)
                F0 =  minimo(1)
            else
                X0 = xbest
                F0 = fbest(1)
            end if
            DEALLOCATE(minimo)
            n_critico = opts1%localoptions%n2
            
            if (opts1%localoptions%bestx .eq. 1) then
                use_bestx=0
            end if
            
            
          else if (stage_2 .eq. 1 ) then 
            if (opts1%localoptions%bestx .eq. 1) then
                X0 = xbest
                F0 = fbest(1)
                use_bestx = 0
            else
                !"if ( ALLOCATED(local_solutions) .or. ALLOCATED (initial_points) ) then
                  !  if (.not. ALLOCATED(local_solutions)) then 
                   !     DIMEN = shape(local_solutions)
                    !    ALLOCATE( local_init(DIMEN(1) ,DIMEN(2) ))
                     !   local_init(:, 1:DIMEN(2) ) = initial_points                        
                   ! else if (.not. ALLOCATED(initial_points)) then
                    !    DIMEN = shape(initial_points)
                     !   ALLOCATE( local_init(DIMEN(1) ,DIMEN(2) ))
                      !  local_init(:, 1:DIMEN(2) ) = local_solutions
                    !else
                        DIMEN  =  shape(local_solutions)
                        DIMEN2 =  shape(initial_points)
                        ALLOCATE( local_init(nvar,DIMEN2(2) + DIMEN(2) ))
                        local_init(:, 1:DIMEN(2) ) = local_solutions
                        local_init(:, (DIMEN(2)+1): (DIMEN2(2) + DIMEN(2)) ) = initial_points
                   ! end if
                    DIMEN =  shape(local_init)
                    ALLOCATE( local_init_norm(nvar, DIMEN(2)) )
                    DIMEN =  shape(childset%x)
                    ALLOCATE( child_norm(nvar, DIMEN(2)) )
                    
                    CALL transform_norm(local_init, local_init_norm, problem1%XU, problem1%XL)
                    CALL transform_norm(childset%x, child_norm, problem1%XU, problem1%XL)
                    
                    DIMEN = shape(local_init_norm)
                    DIMEN2 = shape(child_norm)
                    
                    ALLOCATE(dist(DIMEN(2), DIMEN2(2)))
                    CALL matrix_eucl_dist(child_norm,local_init_norm, dist )
                           
                    DIMEN = shape(dist)
                    ALLOCATE(dismin( DIMEN(2) ))
                    ALLOCATE(dismin_save( DIMEN(2)) )
 

                    IF ( opts1%localoptions%balance .GE. 0 ) THEN
                        PRINT *, "*****BALANCE"
                        dismin = minval(dist,DIM=1)
                        dismin_save = dismin
                        dismin = (-1.0d0) * dismin
                        !print *, "childset  :", childset%fpen
                        ALLOCATE(indmin(  size(dismin) ))
                        call QsortC_ind(dismin, indmin)
                    
                        ALLOCATE(temp_fpen( size(childset%fpen) ))
                        temp_fpen=childset%fpen
                        ALLOCATE(indrefset( size(childset%fpen) ))
                        call QsortC_ind(temp_fpen, indrefset)   
                  
                        !print *, "childset indmin  :", childset%fpen(indmin)
                        !print *, "childset indrefset :", childset%fpen(indrefset)
 
                        ALLOCATE(child_points(size(childset%fpen)))
                        child_points=0d0                        
                        do j=1,size(childset%fpen)
                            indexj = REAL(j,KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))
                            child_points(indmin(j)) = &
                                REAL(child_points(indmin(j)),KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))  &
                                + opts1%localoptions%balance* indexj
                            child_points(indrefset(j)) = &
                                   REAL(child_points(indrefset(j)),KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)) &
                                 + ( 1d0 - opts1%localoptions%balance)* indexj                       
                        end do
                        
                        DEALLOCATE(indrefset)             
                    
                        ALLOCATE(minimo(1) )
                        minimo = minval(child_points)
                        ALLOCATE(I(1))
                        I = minloc(child_points)
                    
                        X0 = childset%x(:,I(1) )
                        F0 = childset%fpen( I(1) )
                                                
                        !print *, "minimo :", minimo
                        !print *, "raning :", child_points   
                        !print *, "refset :", refset%fpen 
                        !print *, "F0", F0   
                    ELSE 
                        PRINT *, "*****NO BALANCE"
                        ALLOCATE(temp_fpen( size(childset%fpen) ))
                        temp_fpen=childset%fpen
                        ALLOCATE(indrefset( size(childset%fpen) ))
                        call QsortC_ind(temp_fpen, indrefset)   
                        dismin = minval(dist,DIM=1)
                        ALLOCATE(I(1))
                        
                        do j=1,size(childset%fpen)
                            IF ( dismin(indrefset(j)) .GT. opts1%localoptions%threshold_local ) THEN
                                I(1) = indrefset(j)
                                X0 = childset%x(:, I(1) )
                                F0 = childset%fpen( I(1) ) 
                                EXIT
                            END IF      
                        end do  
                        
                        DEALLOCATE(indrefset)  

                    END IF
                    
                        DEALLOCATE(dismin_save)
                        DEALLOCATE(dismin)                    
                !else
                !    X0 = xbest
                !    F0 = fbest(1)
                !end if
            end if
        end if
        
        
        if ( opts1%useroptions%iterprint .eq. 1 ) then
            if ( opts1%localoptions%solver == 'n2sol' ) then
                write (*,*)  idp, "Call local solver: N2sol - INPUT: Initial point function value: [", f0, "]"
            else if ( opts1%localoptions%solver == 'misqp' ) then
                write (*,*)  idp, "Call local solver: misqp - INPUT: Initial point function value: [", f0, "]"
            end if
        end if
        
        X=X0
        
        CALL setentervalue( locals, fbest);
        
        CALL printverboselocaloutput(exp1, size(x0), X, f0, output, idp)
       
#ifdef TIMELOCAL      
            CALL SYSTEM_CLOCK(count_rate=clock_rate)
            CALL SYSTEM_CLOCK(count=clock_start)
#endif 
            
        CALL call_local_solver(problem1,exp1,opts1,fitnessfunction, x, fval, stopval,NPROC,numeval,output)
        
#ifdef TIMELOCAL            
            CALL SYSTEM_CLOCK(count=clock_stop)
            localtime = localtime + REAL(clock_stop-clock_start)/REAL(clock_rate)
#endif    
        CALL printverboselocaloutput2(exp1,output, fval,idp)
         
        CALL improvelocalsolver(exp1, locals,fval,f0 )
        
        if ( opts1%useroptions%iterprint .eq. 1 ) then
            write (*,*) idp, "**OUTPUT: Local solution function value: [", fval, "]"
        end if
        
        
        !if (fval .LT. 1d-5 ) then
        !        print *, ""
        !        print *, ""
        !        print *, ""
        !        print *, "idp", idp, "ALEEEEERTAAA"
        !        print *, ""
        !        print *, ""
        !        print *, ""
        !        print *, ""
        !        print *, ""
        !        print *, ""
        !endif

        if (.not. ALLOCATED(initial_points) ) then 
            ALLOCATE(initial_points(nvar,1) )
            initial_points(:,1) = X0
            ALLOCATE(initial_points_values(1))
            initial_points_values(1) = f0
        else 
            if (size(initial_points_values) .LT.  opts1%globaloptions%dim_refset) then
                DIMEN = shape(initial_points)
                ALLOCATE(new_initial_points(DIMEN(1), DIMEN(2) +1 ) )
                new_initial_points(:, 1:DIMEN(2)) = initial_points
                new_initial_points(:, DIMEN(2) +1) = X0
                CALL MOVE_ALLOC(new_initial_points, initial_points)
                
                ALLOCATE(new_initial_points_values(size(initial_points_values)+1 ) )
                new_initial_points_values(1:size(initial_points_values)) = initial_points_values
                new_initial_points_values(size(initial_points_values)+1) = F0
                CALL MOVE_ALLOC(new_initial_points_values, initial_points_values)
                
                ! ORDENAMOS LAS LISTAS
                ALLOCATE(indrefset( size(initial_points_values) ))
                call QsortC_ind(initial_points_values, indrefset) 
                initial_points = initial_points(:,indrefset)
                initial_points_values = initial_points_values(indrefset)
                DEALLOCATE(indrefset)                
            else
                DIMEN = shape(initial_points)
                initial_points(:,DIMEN(2)) = X0
                initial_points_values(size(initial_points_values)) = F0
                
                ! ORDENAMOS LAS LISTAS
                ALLOCATE(indrefset( size(initial_points_values) ))
                call QsortC_ind(initial_points_values, indrefset) 
                initial_points = initial_points(:,indrefset)
                initial_points_values = initial_points_values(indrefset)
                DEALLOCATE(indrefset)  
            end if
        end if
        
        
        if (stage_1 .eq. 1) then
            stage_1 = 0
            stage_2 = 1
        end if
        
        if (ALLOCATED(problem1%CU) ) then 
            nconst = size(problem1%CU)
        else 
            nconst = 0
        end if
        
        outfunct = ssm_evalfc(exp1,fitnessfunction, X ,problem1, opts1, nconst, 0)
        numeval = numeval + 1
        if ( (outfunct%include .eq. 1) .and. ( outfunct%pena .le. opts1%useroptions%tolc )) then
            if ( outfunct%value_penalty .lt. fbest(1) ) then 
                fbest(1) = outfunct%value_penalty 
                xbest = X
                if ( fbest(1) .lt. fbest_lastiter(1) ) then
                        if (.not. ALLOCATED(results%fbest) ) ALLOCATE(results%fbest(size(fbest)))
                        results%fbest(1)=fbest(1);
                        if (.not. ALLOCATED(results%xbest) ) ALLOCATE(results%xbest(size(xbest)))
                        results%xbest=xbest
                        results%numeval=numeval
                end if
            end if 
        end if
        
        
        if (outfunct%include .eq. 1) then
            if (ALLOCATED(local_solutions)) then
                adiccionar_local=1
                CALL ssm_isdif2(x,local_solutions,1d-2,1,f11,ind11,ind12)
                if (f11 .gt. 1d0 ) then 
                    adiccionar_local = 0
                end if
            else 
                adiccionar_local=1
            end if
          
            if ( adiccionar_local .eq. 1 ) then 
         
        ! print *, idp,"out%valpen ", outfunct%value_penalty
        ! print *, idp,"refpn ", refset%fpen(childset%parent_index(I(1)))                
        ! print *, idp,"I(1)", I(1) 
                if ( outfunct%value_penalty .LT. refset%fpen(childset%parent_index(I(1))) ) then
                    refset%x(:,childset%parent_index(I(1) )) = outfunct%x
                    refset%f(childset%parent_index(I(1) )) = outfunct%value
                    refset%fpen(childset%parent_index(I(1) )) = outfunct%value_penalty
                    refset%penalty(childset%parent_index(I(1) )) = outfunct%pena
                    if (ALLOCATED (refset%nlc) ) refset%nlc(:,childset%parent_index(I(1) )) = outfunct%nlc
                    refset_change (childset%parent_index(I(1) )) = 0
                end if
                
                if (.not. ALLOCATED(local_solutions)) then
                    ALLOCATE(local_solutions(nvar,1))
                    local_solutions(:,1) = x 
                    ALLOCATE(local_solutions_values(1) )
                    local_solutions_values(1) = outfunct%value_penalty
                else
                    if (size(local_solutions_values) .LT. opts1%globaloptions%dim_refset ) then                        
                        DIMEN = shape( local_solutions )
                        ALLOCATE( new_local_solutions(DIMEN(1),DIMEN(2)+1))
                        new_local_solutions(:,1:DIMEN(2)) = local_solutions
                        new_local_solutions(:,DIMEN(2)+1) = x
                        CALL MOVE_ALLOC(new_local_solutions, local_solutions)
                    
                        ALLOCATE(new_local_solutions_values(size(local_solutions_values)+1))
                        new_local_solutions_values(1:size(local_solutions_values)) = local_solutions_values
                        new_local_solutions_values(size(local_solutions_values)+1) = outfunct%value_penalty
                        CALL MOVE_ALLOC(new_local_solutions_values, local_solutions_values)
                        
                        ! ORDENAMOS LAS LISTAS
                        ALLOCATE(indrefset( size(local_solutions_values) ))
                        call QsortC_ind(local_solutions_values, indrefset) 
                        local_solutions = local_solutions(:,indrefset)
                        local_solutions_values = local_solutions_values(indrefset)
                        DEALLOCATE(indrefset)
                    else
                        DIMEN = shape( local_solutions)
                        local_solutions(:,DIMEN(2)) = x
                        local_solutions_values(size(local_solutions_values)) = outfunct%value_penalty
                        
                        ! ORDENAMOS LAS LISTAS
                        ALLOCATE(indrefset( size(local_solutions_values) ))
                        call QsortC_ind(local_solutions_values, indrefset) 
                        local_solutions = local_solutions(:,indrefset)
                        local_solutions_values = local_solutions_values(indrefset)
                        DEALLOCATE(indrefset)
                    end if
                end if
            end if
        end if
       
        !print *, "local_solutions_values", local_solutions_values
        !print *, "initial_points_values", initial_points_values  
        
        if (ALLOCATED(minimo) ) DEALLOCATE(minimo)
        if (ALLOCATED(repmat) ) DEALLOCATE(repmat)
        if (ALLOCATED(local_init) ) DEALLOCATE(local_init)
        if (ALLOCATED(child_norm) ) DEALLOCATE(child_norm)
        if (ALLOCATED(local_init_norm) ) DEALLOCATE( local_init_norm)
        if (ALLOCATED(I) ) DEALLOCATE(I)
        if (ALLOCATED(X0) ) DEALLOCATE(X0)
        if (ALLOCATED(X) ) DEALLOCATE(X)
        if (ALLOCATED(new_initial_points) ) DEALLOCATE(new_initial_points)
        if (ALLOCATED(new_local_solutions) ) DEALLOCATE(new_local_solutions)
        if (ALLOCATED(dismin) ) DEALLOCATE(dismin)
        if (ALLOCATED(indmin) ) DEALLOCATE(indmin)
        if (ALLOCATED(dist) ) DEALLOCATE(dist)
        if (ALLOCATED(child_points) ) DEALLOCATE(child_points)
        if (ALLOCATED(ind11) ) DEALLOCATE(ind11)
        if (ALLOCATED(ind12) ) DEALLOCATE(ind12)
        if (ALLOCATED(temp_fpen) ) DEALLOCATE(temp_fpen)
        CALL destroy_outfuction(outfunct)
    
        if (opts1%localoptions%n2 .LE. 0)  then 
            opts1%localoptions%empty=1
        end if
    
    end if
    
    END SUBROUTINE ssm_local_filters
    
    
    
! ----------------------------------------------------------------------
! SUBROUTINE local_solver
! ----------------------------------------------------------------------    
    SUBROUTINE local_solver
        
    END SUBROUTINE local_solver
    
    
    
END MODULE localsolver
