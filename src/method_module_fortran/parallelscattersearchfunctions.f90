#define TIMEPAR 1

MODULE parallelscattersearchfunctions
    USE iso_c_binding
    USE scattersearchtypes
    USE common_functions
    USE qsort_module
    USE scattersearchfunctions
    USE funcevalinterface
#ifdef OPENMP
    USE omp_lib
#endif
    
#ifdef MPI2
    
CONTAINS

! ----------------------------------------------------------------------
! SUBROUTINES hete_param_eSS
! ----------------------------------------------------------------------
SUBROUTINE hete_param_eSS (exp1, problem1, opts1, NPROC, dim_refset, idp, nvar )
    TYPE(problem), INTENT(INOUT) :: problem1
    TYPE(opts), INTENT(INOUT) :: opts1 
    TYPE(C_PTR),  INTENT(INOUT) :: exp1
    INTEGER, INTENT(IN) :: NPROC, idp, nvar
    INTEGER, INTENT(INOUT) :: dim_refset
    INTEGER :: ishete, coopera, hete, iscoop, SMALL_DIM_FREC_MIG, MEDIUM_DIM_FREC_MIG, BIG_DIM_FREC_MIG
    REAL(KIND = SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)) :: nnn(2), nn(1),polim(3), dim
    
    hete = ishete(exp1)
    coopera = iscoop(exp1)
    
    if ((coopera .EQ. 1) .AND.(hete .EQ. 1)) then
        SMALL_FREC_MIG = 10
        MEDIUM_DIM_FREC_MIG = 5
        BIG_DIM_FREC_MIG = 2
            if ( MOD(idp+1,30) .EQ. 1 )  then
                dim = 0.5
                opts1%localoptions%n1 = 30
                opts1%localoptions%n2 = 30
                opts1%localoptions%balance =  0.0
                !opts1%localoptions%empty = 1   
                CALL setnumit(exp1, SMALL_FREC_MIG)
            else if ( MOD(idp+1,30) .EQ. 2 )  then
                dim = 5
                opts1%localoptions%n1 = 5
                opts1%localoptions%n2 = 7
                opts1%localoptions%balance = 0.25    
                CALL setnumit(exp1, MEDIUM_DIM_FREC_MIG)
            else if ( MOD(idp+1,30) .EQ. 3 )  then
                dim = 7.5
                opts1%localoptions%n1 = 5
                opts1%localoptions%n2 = 10
                opts1%localoptions%balance = 0.25    
                CALL setnumit(exp1, MEDIUM_DIM_FREC_MIG)
            else if ( MOD(idp+1,30) .EQ. 4 )  then
                dim = 10
                opts1%localoptions%n1 = 5
                opts1%localoptions%n2 = 15
                opts1%localoptions%balance = 0.5        
                CALL setnumit(exp1, MEDIUM_DIM_FREC_MIG)
            else if ( MOD(idp+1,30) .EQ. 5 )  then
                dim = 2
                opts1%localoptions%n1 = 15
                opts1%localoptions%n2 = 50
                opts1%localoptions%balance = 0.0   
                CALL setnumit(exp1, SMALL_FREC_MIG)              
            else if ( MOD(idp+1,30) .EQ. 6 )  then
                dim = 12
                opts1%localoptions%n1 =  5
                opts1%localoptions%n2 =  25
                opts1%localoptions%balance = 0.25     
                CALL setnumit(exp1, BIG_DIM_FREC_MIG)
            else if ( MOD(idp+1,30) .EQ. 7 )  then
                dim = 1
                opts1%localoptions%n1 = 15
                opts1%localoptions%n2 = 50
                opts1%localoptions%balance = 0.0  
                CALL setnumit(exp1, SMALL_FREC_MIG)
            else if ( MOD(idp+1,30) .EQ. 8 )  then
                dim = 15
                opts1%localoptions%n1 = 5
                opts1%localoptions%n2 = 25
                opts1%localoptions%balance = 0.25     
                CALL setnumit(exp1, BIG_DIM_FREC_MIG)
            else if ( MOD(idp+1,30) .EQ. 9 )  then
                dim = 5
                opts1%localoptions%n1 = 10
                opts1%localoptions%n2 = 10
                opts1%localoptions%balance = 0.25
                CALL setnumit(exp1, MEDIUM_DIM_FREC_MIG)
            else if ( MOD(idp+1,30) .EQ. 10 ) then
                dim = 3
                opts1%localoptions%n1 = 10
                opts1%localoptions%n2 = 5
                opts1%localoptions%balance = 0.0
                CALL setnumit(exp1, SMALL_FREC_MIG)
            else if ( MOD(idp+1,30) .EQ. 11 )  then
                dim = 6.5
                opts1%localoptions%n1 = 5
                opts1%localoptions%n2 = 10
                opts1%localoptions%balance = 0.15  
                CALL setnumit(exp1, MEDIUM_DIM_FREC_MIG)
            else if ( MOD(idp+1,30) .EQ. 12 )  then
                dim = 4.5
                opts1%localoptions%n1 = 10
                opts1%localoptions%n2 = 7
                opts1%localoptions%balance = 0.15 
                CALL setnumit(exp1, SMALL_FREC_MIG)
            else if ( MOD(idp+1,30) .EQ. 13 )  then
                dim = 0.5
                opts1%localoptions%n1 = 10
                opts1%localoptions%n2 = 10
                opts1%localoptions%balance = 0.0   
                CALL setnumit(exp1, SMALL_FREC_MIG)
            else if ( MOD(idp+1,30) .EQ. 14 )  then
                dim = 9.5
                opts1%localoptions%n1 = 5
                opts1%localoptions%n2 = 5
                opts1%localoptions%balance = 0.25   
                CALL setnumit(exp1, MEDIUM_DIM_FREC_MIG)
            else if ( MOD(idp+1,30) .EQ. 15 )  then
                dim = 2
                opts1%localoptions%n1 = 10
                opts1%localoptions%n2 = 5
                opts1%localoptions%balance = 0.0    
                CALL setnumit(exp1, SMALL_FREC_MIG)
            else if ( MOD(idp+1,30) .EQ. 16 )  then
                dim = 12.5
                opts1%localoptions%n1 = 5
                opts1%localoptions%n2 = 20
                opts1%localoptions%balance = 0.35       
                CALL setnumit(exp1, BIG_DIM_FREC_MIG)
            else if ( MOD(idp+1,30) .EQ. 17 )  then
                dim = 1.5
                opts1%localoptions%n1 = 10
                opts1%localoptions%n2 = 10
                opts1%localoptions%balance = 0.25   
                CALL setnumit(exp1, SMALL_FREC_MIG)
            else if ( MOD(idp+1,30) .EQ. 18 )  then
                dim = 16.5
                opts1%localoptions%n1 = 5
                opts1%localoptions%n2 = 25
                opts1%localoptions%balance = 0.5      
                CALL setnumit(exp1, BIG_DIM_FREC_MIG)
            else if ( MOD(idp+1,30) .EQ. 19 )  then
                dim = 5.5
                opts1%localoptions%n1 = 10
                opts1%localoptions%n2 = 10
                opts1%localoptions%balance = 0.5     
                CALL setnumit(exp1, MEDIUM_DIM_FREC_MIG)
            else if ( MOD(idp+1,30) .EQ. 20 ) then
                dim = 3.5
                opts1%localoptions%n1 = 10
                opts1%localoptions%n2 = 5
                opts1%localoptions%balance = 0.25
                CALL setnumit(exp1, SMALL_FREC_MIG)
            else if ( MOD(idp+1,30) .EQ. 21 )  then
                dim = 8.5
                opts1%localoptions%n1 = 5
                opts1%localoptions%n2 = 10
                opts1%localoptions%balance = 0.35   
                CALL setnumit(exp1, MEDIUM_DIM_FREC_MIG)
            else if ( MOD(idp+1,30) .EQ. 22 )  then
                dim = 5.5
                opts1%localoptions%n1 = 10
                opts1%localoptions%n2 = 10
                opts1%localoptions%balance = 0.35    
                CALL setnumit(exp1, MEDIUM_DIM_FREC_MIG)
            else if ( MOD(idp+1,30) .EQ. 23 )  then
                dim = 1.5
                opts1%localoptions%n1 = 10
                opts1%localoptions%n2 = 15
                opts1%localoptions%balance = 0.25  
                CALL setnumit(exp1, SMALL_FREC_MIG)
            else if ( MOD(idp+1,30) .EQ. 24 )  then
                dim = 10.5
                opts1%localoptions%n1 = 5
                opts1%localoptions%n2 = 10
                opts1%localoptions%balance = 0.5     
                CALL setnumit(exp1, BIG_DIM_FREC_MIG)
            else if ( MOD(idp+1,30) .EQ. 25 )  then
                dim = 2.5
                opts1%localoptions%n1 = 10
                opts1%localoptions%n2 = 8
                opts1%localoptions%balance = 0.35   
                CALL setnumit(exp1, SMALL_FREC_MIG)
            else if ( MOD(idp+1,30) .EQ. 26 )  then
                dim = 11.5
                opts1%localoptions%n1 = 5
                opts1%localoptions%n2 = 20
                opts1%localoptions%balance = 0.25    
                CALL setnumit(exp1, BIG_DIM_FREC_MIG)
            else if ( MOD(idp+1,30) .EQ. 27 )  then
                dim = 0.75
                opts1%localoptions%n1 = 10
                opts1%localoptions%n2 = 15
                opts1%localoptions%balance = 0.5    
                CALL setnumit(exp1, SMALL_FREC_MIG)
            else if ( MOD(idp+1,30) .EQ. 28 )  then
                dim = 14.5
                opts1%localoptions%n1 = 5
                opts1%localoptions%n2 = 25
                opts1%localoptions%balance =   0.25    
                CALL setnumit(exp1, BIG_DIM_FREC_MIG)
            else if ( MOD(idp+1,30) .EQ. 29 )  then
                dim = 4.5
                opts1%localoptions%n1 = 10
                opts1%localoptions%n2 = 10
                opts1%localoptions%balance = 0.25     
                CALL setnumit(exp1, SMALL_FREC_MIG)
            else if ( MOD(idp+1,30) .EQ. 0 ) then
                dim = 2.5
                opts1%localoptions%n1 = 10
                opts1%localoptions%n2 = 8
                opts1%localoptions%balance = 0.5
                CALL setnumit(exp1, SMALL_FREC_MIG)
            else               
                dim = 10d0
                opts1%localoptions%n2 = 10
                opts1%localoptions%n2 = 10
                opts1%localoptions%balance = 0.5d0 
                CALL setnumit(exp1, MEDIUM_DIM_FREC_MIG)
            end if
        
            polim = (/  REAL(1.0d0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), &
                    REAL(-1.0d0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), &
            (-1d0) *dim * REAL( nvar/1, KIND = SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)) /)                
            CALL SQUARE_ROOTS_POLINOM(polim, nnn)
            nn = maxval(nnn, MASK = nnn .GT. 0)
            dim_refset = CEILING(nn(1))          
            
            opts1%globaloptions%dim_refset = dim_refset
    end if
    
    
END SUBROUTINE
#ifdef OPENMP
! ----------------------------------------------------------------------
! SUBROUTINES update_candidateset_with_new_comb
! ----------------------------------------------------------------------      
    SUBROUTINE update_candidateset_with_new_comb_parallel( exp1,opts1, fitnessfunction,problem1,new_comb,candidateset,childset, &
            candidate_update, members_update,MaxSubSet2,nrand,nconst,nfuneval,counter, index, timeparallel )
            TYPE(problem), INTENT(IN) :: problem1
            TYPE(C_PTR),  INTENT(INOUT) :: exp1
            TYPE(Refsettype), INTENT(INOUT) :: candidateset, childset
            TYPE(Refsettype) :: childset_aux
            TYPE(opts), INTENT(IN) :: opts1 
            TYPE(C_FUNPTR), INTENT(INOUT) :: fitnessfunction ! fobj        
            INTEGER, DIMENSION(:), ALLOCATABLE, INTENT(INOUT) :: members_update, candidate_update
            INTEGER(C_LONG), INTENT(INOUT) ::  nfuneval
            INTEGER, INTENT(INOUT) :: counter
            INTEGER :: neval, cont
            INTEGER :: clock_rate, clock_start, clock_stop            
            INTEGER, INTENT(IN) :: nconst, nrand
            REAL(C_DOUBLE), INTENT(INOUT) :: timeparallel
            REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), INTENT(IN) :: MaxSubSet2
            REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE, INTENT(INOUT) ::  new_comb        
            INTEGER, DIMENSION(:), ALLOCATABLE, INTENT(IN) :: index
            REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: temp
            INTEGER :: i, OMP_NUM_THREADS, IDPROC, ii
            TYPE(outfuction), DIMENSION(:), ALLOCATABLE :: outfunct1
                
            
            IDPROC = 0
            CALL create_childset ( childset_aux, nrand, CEILING(MaxSubSet2), nconst )  
        
#ifdef TIMEPAR      
            CALL SYSTEM_CLOCK(count_rate=clock_rate)
            CALL SYSTEM_CLOCK(count=clock_start)
#endif          
            
 !$OMP PARALLEL SHARED(OMP_NUM_THREADS)           
            OMP_NUM_THREADS = OMP_GET_NUM_THREADS()
 !$OMP END PARALLEL          
            
            ALLOCATE(temp(nrand))
            ALLOCATE(outfunct1(OMP_NUM_THREADS)) 
            
 !$OMP  PARALLEL PRIVATE(IDPROC)   &
 !$OMP& DEFAULT(SHARED)
            IDPROC = OMP_GET_THREAD_NUM()
            cont = 0
            neval = 0
            
 !$OMP DO SCHEDULE(DYNAMIC,1) PRIVATE(temp,i)  REDUCTION(+:cont, neval)
            do i = 1, CEILING(MaxSubSet2)
                ! Evaluate
                temp = new_comb(:,i)
                outfunct1(IDPROC+1) = ssm_evalfc(exp1, fitnessfunction, temp, problem1, opts1, nconst,IDPROC)
                neval = neval + 1

                
                if (outfunct1(IDPROC+1)%include .eq. 1) then
                    childset_aux%x(:,i)=outfunct1(IDPROC+1)%x
                    childset_aux%f(i)= outfunct1(IDPROC+1)%value
                    childset_aux%fpen(i) = outfunct1(IDPROC+1)%value_penalty
                    childset_aux%penalty(i) = outfunct1(IDPROC+1)%pena
                    if (ALLOCATED(outfunct1(IDPROC+1)%nlc) ) childset%nlc(:,i) = outfunct1(IDPROC+1)%nlc
                    childset_aux%parent_index(i) = index(i)
                    cont = cont + 1
                    
                    if (outfunct1(IDPROC+1)%value_penalty < candidateset%fpen(index(i))) then
                            candidateset%x(:,index(i)) = outfunct1(IDPROC+1)%x
                            candidateset%fpen(index(i)) = outfunct1(IDPROC+1)%value_penalty
                            candidateset%f(index(i)) = outfunct1(IDPROC+1)%value
                            candidateset%penalty(index(i)) = outfunct1(IDPROC+1)%pena
                            if (ALLOCATED(outfunct1(IDPROC+1)%nlc)) candidateset%nlc(:,index(i)) = outfunct1(IDPROC+1)%nlc
                            candidate_update(index(i)) = 1
                            members_update(index(i)) = 0
                    end if
                    
                else
                    childset_aux%x(:,i)=INFINITE
                    childset_aux%f(i)=INFINITE
                    childset_aux%fpen(i) =INFINITE
                    childset_aux%penalty(i) =INFINITE
                    if (ALLOCATED(outfunct1(IDPROC+1)%nlc) ) childset%nlc(:,i) = INFINITE
                    childset_aux%parent_index(i) = -1
                end if
                
            end do
 !$OMP END DO
            
 !$OMP END PARALLEL
#ifdef TIMEPAR            
            CALL SYSTEM_CLOCK(count=clock_stop)
            timeparallel = timeparallel + REAL(clock_stop-clock_start)/REAL(clock_rate)
#endif                  
            
            
            counter = counter + cont
            nfuneval = nfuneval + neval
            
            ii=1
            do i=1, CEILING(MaxSubSet2)
                IF ( ( childset_aux%x(1,i) .NE. INFINITE ) .AND. ( childset_aux%f(i) .NE. INFINITE ) .AND. &
                    ( childset_aux%fpen(i) .NE. INFINITE ) .AND. ( childset_aux%penalty(i) .NE. INFINITE ) .AND. &
                    ( childset_aux%parent_index(i) .NE. -1 ) ) THEN
                    
                    childset%x(:,ii) = childset_aux%x(:,i)
                    childset%f(ii) = childset_aux%f(i)
                    childset%fpen(ii) = childset_aux%fpen(i)
                    childset%penalty(ii) = childset_aux%penalty(i)
                    if (ALLOCATED(childset_aux%nlc) ) childset%nlc(:,ii) = childset_aux%nlc(:,i)
                    childset%parent_index(ii) = childset_aux%parent_index(i)
                    ii = ii + 1
                END IF
            end do
            
            CALL destroy_refsettype(childset_aux)
            call destroy_outfuction(outfunct1(IDPROC+1))
            if (ALLOCATED(outfunct1)) DEALLOCATE(outfunct1)            
            DEALLOCATE(temp)
                
    END SUBROUTINE update_candidateset_with_new_comb_parallel

    
    
! ----------------------------------------------------------------------
! SUBROUTINES apply_beyond_to_members_to_update_parallel
! ----------------------------------------------------------------------   
    SUBROUTINE apply_beyond_to_members_to_update_parallel( exp1, opts1, fitnessfunction, problem1, members_to_update,  nfuneval, &
        refset, candidateset, nconst, nrand, nvar, timeparallel )
        TYPE(opts), INTENT(IN) :: opts1 
        TYPE(Refsettype), INTENT(INOUT) :: refset, candidateset
        TYPE(problem), INTENT(IN) :: problem1
        TYPE(C_PTR), INTENT(INOUT) :: exp1
        TYPE(C_FUNPTR), INTENT(INOUT) :: fitnessfunction ! fobj 
        INTEGER(C_LONG), INTENT(INOUT) :: nfuneval
        INTEGER :: clock_rate, clock_start, clock_stop        
        INTEGER(C_LONG) :: neval
        INTEGER, DIMENSION(:), ALLOCATABLE, INTENT(INOUT)  ::  members_to_update
        INTEGER, INTENT(IN) :: nconst, nvar , nrand
        REAL(C_DOUBLE), INTENT(INOUT) :: timeparallel
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: temp1, temp
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)) :: auxiliarvalue
        INTEGER :: OMP_NUM_THREADS, IDPROC
        TYPE(outfuction), DIMENSION(:), ALLOCATABLE :: outfunct1
        
#ifdef TIMEPAR      
            CALL SYSTEM_CLOCK(count_rate=clock_rate)
            CALL SYSTEM_CLOCK(count=clock_start)
#endif              
        IDPROC = 0
 !$OMP PARALLEL SHARED(OMP_NUM_THREADS)           
            OMP_NUM_THREADS = OMP_GET_NUM_THREADS()
 !$OMP END PARALLEL    
       ALLOCATE(outfunct1(OMP_NUM_THREADS)) 
       ALLOCATE(temp(nvar))
       ALLOCATE(temp1(nvar))     

            
       if (ALLOCATED(members_to_update)) then
                ! exploramos los miembros a actualizar (los que prometen más resultados)
                ! y aplicamos ls técnica go-beyond en ellos
            
 !$OMP PARALLEL PRIVATE(OMP_NUM_THREADS,IDPROC) DEFAULT(SHARED)
                IDPROC = OMP_GET_THREAD_NUM()
                neval = 0
 !$OMP DO SCHEDULE(DYNAMIC,1) PRIVATE(temp,temp1,i,auxiliarvalue) REDUCTION(+:neval)
                do i = 1, size(members_to_update)

                    temp = refset%x(:,members_to_update(i))
                    temp1 = candidateset%x(:,members_to_update(i))
                    auxiliarvalue = candidateset%fpen(members_to_update(i))

                    ! ssm_beyond
                    CALL ssm_beyond(exp1, temp, temp1, auxiliarvalue, neval, &
                    fitnessfunction, nrand, nconst, outfunct1(IDPROC+1), opts1, problem1, IDPROC)
                          
                             
                    if (ALLOCATED(outfunct1(IDPROC+1)%x) ) then  
                        refset%x(:,members_to_update(i)) = outfunct1(IDPROC+1)%x
                        refset%f(members_to_update(i)) = outfunct1(IDPROC+1)%value
                        refset%fpen(members_to_update(i)) = outfunct1(IDPROC+1)%value_penalty
                        refset%penalty(members_to_update(i)) = outfunct1(IDPROC+1)%pena
                        if (ALLOCATED(refset%nlc)) refset%nlc(:,members_to_update(i)) = outfunct1(IDPROC+1) % nlc
                        !end if
                    else    
                        refset%x(:,members_to_update(i)) = candidateset%x(:,members_to_update(i))
                        refset%f(members_to_update(i)) = candidateset%f(members_to_update(i))
                        refset%fpen(members_to_update(i)) = candidateset%fpen(members_to_update(i))
                        refset%penalty(members_to_update(i)) = candidateset%penalty(members_to_update(i))
                        if (ALLOCATED(refset%nlc)) refset%nlc(:,members_to_update(i)) = candidateset%nlc(:,members_to_update(i))
                    end if
                    
                end do
 !$OMP END DO

 !$OMP END PARALLEL   
        
#ifdef TIMEPAR            
            CALL SYSTEM_CLOCK(count=clock_stop)
            timeparallel = timeparallel + REAL(clock_stop-clock_start)/REAL(clock_rate)
#endif                
       nfuneval = nfuneval + neval
       end if
            
            DEALLOCATE(temp)
            DEALLOCATE(temp1)
            call destroy_outfuction(outfunct1(IDPROC+1))
            if (ALLOCATED(outfunct1)) DEALLOCATE(outfunct1)    
            
    END SUBROUTINE apply_beyond_to_members_to_update_parallel
    
    
     
! ----------------------------------------------------------------------
! SUBROUTINES evaluate_solutions_set_parallel
! ----------------------------------------------------------------------   
   SUBROUTINE  evaluate_solutions_set_parallel(exp1,fitnessfunction,solutionset,problem1,opts1, solutions, nvar, nfuneval,nconst, &
                    timeparallel)
        TYPE(C_PTR), INTENT(INOUT) :: exp1
        TYPE(C_FUNPTR), INTENT(INOUT) :: fitnessfunction ! fobj
        TYPE(Refsettype), INTENT(INOUT) :: solutionset
        TYPE(Refsettype) ::solutionset_aux
        TYPE(problem), INTENT(IN) :: problem1
        TYPE(opts), INTENT(IN) :: opts1
        INTEGER, INTENT(IN) :: nconst
        REAL(C_DOUBLE), INTENT(INOUT) :: timeparallel
        INTEGER :: clock_rate, clock_start, clock_stop
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE, INTENT(INOUT) :: solutions
        INTEGER, INTENT(IN)  :: nvar
        INTEGER(C_LONG), INTENT(INOUT) :: nfuneval
        INTEGER :: l_f_0, l_x_0, DIMEN(2), tempnum, counter, i, sizet, neval
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: newsolution, entervalues
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE :: temp2
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)) :: NAN
        INTEGER(C_INT) :: error, setNAN
        INTEGER :: OMP_NUM_THREADS, IDPROC, ii
        TYPE(outfuction), DIMENSION(:), ALLOCATABLE :: outfunct1
        
        IDPROC = 0
#ifdef TIMEPAR      
            CALL SYSTEM_CLOCK(count_rate=clock_rate)
            CALL SYSTEM_CLOCK(count=clock_start)
#endif         
 !$OMP PARALLEL SHARED(OMP_NUM_THREADS)           
            OMP_NUM_THREADS = OMP_GET_NUM_THREADS()
 !$OMP END PARALLEL  
#ifdef TIMEPAR            
            CALL SYSTEM_CLOCK(count=clock_stop)
            timeparallel = timeparallel + REAL(clock_stop-clock_start)/REAL(clock_rate)
#endif             
            
       ALLOCATE(outfunct1(OMP_NUM_THREADS)) 
       
       
        error = setNAN(NAN)
        
        if (ALLOCATED(problem1%F0)) then
            l_f_0 = size(problem1%F0)
        else
            l_f_0 = 0
        end if

        if (ALLOCATED(problem1%X0)) then
            DIMEN = shape(problem1%X0)
            l_x_0 = DIMEN(1)
        else
            l_x_0 = 0
        end if

        ! Put x_0 without their corresponding problem1%F0 values at the beginning of solutions
        if (l_x_0 > 0) then
            DIMEN = shape(problem1%X0)
            
            ALLOCATE(temp2(DIMEN(1), DIMEN(2)- l_f_0))
            temp2 = problem1%X0(:,(l_f_0 + 1):DIMEN(2))
            CALL FUSION_MATRIX(temp2, solutions)
            tempnum = l_x_0 - l_f_0
            !DEALLOCATE(temp2)
        else
            tempnum = 0
        end if
         

        sizet = opts1%globaloptions%ndiverse + 5 + tempnum

        ALLOCATE(solutionset%x(nvar, sizet))
        solutionset%x = solutionset%x * REAL(0.0d0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))
        if (problem1%ineq .GT. 0 ) then 
            ALLOCATE(solutionset%nlc(problem1%ineq , sizet))
            solutionset%nlc = solutionset%nlc * REAL(0.0d0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)) 
        end if
        ALLOCATE(solutionset%f(sizet))
        solutionset%f = solutionset%f * REAL(0.0d0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))
        ALLOCATE(solutionset%fpen(sizet))
        solutionset%fpen = solutionset%fpen * REAL(0.0d0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))
        ALLOCATE(solutionset%penalty(sizet))
        solutionset%penalty = solutionset%penalty * REAL(0.0d0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))
        
        ALLOCATE(solutionset_aux%x(nvar, sizet))
        solutionset_aux%x = solutionset_aux%x * REAL(0.0d0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))
        if (problem1%ineq .GT. 0 ) then 
            ALLOCATE(solutionset_aux%nlc(problem1%ineq , sizet))
            solutionset_aux%nlc = solutionset_aux%nlc * REAL(0.0d0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)) 
        end if
        ALLOCATE(solutionset_aux%f(sizet))
        solutionset_aux%f = solutionset_aux%f * REAL(0.0d0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))
        ALLOCATE(solutionset_aux%fpen(sizet))
        solutionset_aux%fpen = solutionset_aux%fpen * REAL(0.0d0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))
        ALLOCATE(solutionset_aux%penalty(sizet))
        solutionset_aux%penalty = solutionset_aux%penalty * REAL(0.0d0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))

        counter = 1
        !Evaluate the set of solutions
   
        
        ALLOCATE(newsolution(nvar))
            
#ifdef TIMEPAR      
            CALL SYSTEM_CLOCK(count_rate=clock_rate)
            CALL SYSTEM_CLOCK(count=clock_start)
#endif            
 !$OMP PARALLEL PRIVATE(OMP_NUM_THREADS,IDPROC) DEFAULT(SHARED)
        IDPROC = OMP_GET_THREAD_NUM()
        neval = 0         
 !$OMP DO SCHEDULE(DYNAMIC,1) PRIVATE(newsolution,temp1,i,auxiliarvalue) REDUCTION(+:neval,counter)        
        do i = 1, sizet
            newsolution = solutions(:,i)
            outfunct1(IDPROC+1) = ssm_evalfc(exp1, fitnessfunction, newsolution, problem1, opts1, nconst, IDPROC)
            solutions(:,i) = outfunct1(IDPROC+1)%x
            neval = neval + 1
            
            if (outfunct1(IDPROC+1)%include .eq. 1) then
                solutionset_aux%x(:,i) = outfunct1(IDPROC+1)%x
                solutionset_aux%f(i) = outfunct1(IDPROC+1)%value
                solutionset_aux%fpen(i) = outfunct1(IDPROC+1)%value_penalty
                solutionset_aux%penalty(i) = outfunct1(IDPROC+1)%pena
                if (ALLOCATED(solutionset_aux%nlc)) then 
                    solutionset_aux%nlc(:,i) = outfunct1(IDPROC+1)%nlc
                end if
                counter = counter + 1
            else  
                solutionset_aux%x(:,i) = INFINITE
                solutionset_aux%f(i) = INFINITE
                solutionset_aux%fpen(i) = INFINITE
                solutionset_aux%penalty(i) = INFINITE
                if (ALLOCATED(solutionset_aux%nlc)) then 
                    solutionset_aux%nlc(:,i) = INFINITE
                end if
            end if
            

        end do
 !$OMP END DO

 !$OMP END PARALLEL  
        
        
#ifdef TIMEPAR            
            CALL SYSTEM_CLOCK(count=clock_stop)
            timeparallel = timeparallel + REAL(clock_stop-clock_start)/REAL(clock_rate)
#endif         
        
        if (ALLOCATED(newsolution)) DEALLOCATE(newsolution)
        nfuneval = nfuneval + neval
        
        ii=1
        do i=1, sizet
                IF ( ( solutionset_aux%x(1,i) .NE. INFINITE ) .AND. ( solutionset_aux%f(i) .NE. INFINITE ) .AND. &
                    ( solutionset_aux%fpen(i) .NE. INFINITE ) .AND. ( solutionset_aux%penalty(i) .NE. INFINITE )) THEN
                    solutionset%x(:,ii) = solutionset_aux%x(:,i)
                    solutionset%f(ii) = solutionset_aux%f(i)
                    solutionset%fpen(ii) = solutionset_aux%fpen(i)
                    solutionset%penalty(ii) = solutionset_aux%penalty(i)
                    if (ALLOCATED(solutionset%nlc) ) solutionset%nlc(:,ii) = solutionset_aux%nlc(:,i)
                    ii = ii + 1
                END IF
        end do
         
        CALL destroy_refsettype(solutionset_aux)
            

        !Add points with f_0 values. We assume feasiblity
        if (l_f_0 > 0) then
            DIMEN = shape(problem1%X0)
            ALLOCATE(temp2(DIMEN(1),l_f_0))
            temp2 = problem1%X0(:,1:l_f_0)
            call FUSION_MATRIX(temp2, solutionset%x)
            call FUSION_VECTOR(problem1%F0, solutionset%f)
            call FUSION_VECTOR(problem1%F0, solutionset%fpen)
            ALLOCATE(entervalues(l_f_0))
            entervalues = entervalues * 0
            call FUSION_VECTOR(entervalues, solutionset%penalty)
            DEALLOCATE(entervalues)
            
            if (ALLOCATED(solutionset%nlc)) then
                ALLOCATE(temp2(l_f_0,nconst))
                temp2 = 1.0 * NAN
                CALL FUSION_MATRIX(temp2,solutionset%nlc)
            end if
        end if    
        
        call destroy_outfuction(outfunct1(IDPROC+1))
        if (ALLOCATED(outfunct1)) DEALLOCATE(outfunct1) 
        
   END SUBROUTINE evaluate_solutions_set_parallel   
#endif   
   
   
! ----------------------------------------------------------------------
! SUBROUTINES create_refset_local
! ----------------------------------------------------------------------    
   SUBROUTINE create_refset_local ( exp1, problem1, opts1, refsetglobal, refset, nvar, nconst, tam )
        IMPLICIT NONE
        TYPE(C_PTR), INTENT(INOUT) :: exp1
        TYPE(opts), INTENT(INOUT) :: opts1
        TYPE(Refsettype), INTENT(INOUT) :: refsetglobal, refset
        TYPE(Refsettype) :: refsetlocal
        INTEGER, INTENT(IN) :: nvar, nconst, tam
        INTEGER :: cond, iscoop, cond2, ishete
        TYPE(problem), INTENT(IN) :: problem1

        
        ! Initialize Refset
        ALLOCATE(refset%x(nvar,tam))
        refset%x = refset%x * REAL(0.0d0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))
        ALLOCATE(refset%f(tam))
        refset%f = refset%f * REAL(0.0d0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))
        ALLOCATE(refset%fpen(tam))
        refset%fpen = refset%fpen *REAL(0.0d0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))
        ALLOCATE(refset%penalty(tam))
        refset%penalty = refset%penalty * REAL(0.0d0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))
        if (nconst .GT. 0) then
            ALLOCATE(refset%nlc(nconst,tam))
            refset%nlc = refset%nlc * REAL(0.0d0,SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))              
        end if
             
        
        
        cond = iscoop(exp1)
        cond2 = ishete(exp1)
    
        ! ISLAND CASE
        if (cond .EQ. 0) then
            if (cond2 .EQ. 0) then
                ! Create the initial sorted Refset
                CALL cooperativedistelement(exp1, refsetglobal%x, nvar*tam, refset%x);
                CALL cooperativedistelement(exp1, refsetglobal%f, tam, refset%f);
                CALL cooperativedistelement(exp1, refsetglobal%fpen, tam, refset%fpen);
                CALL cooperativedistelement(exp1, refsetglobal%penalty, tam, refset%penalty);
                if (nconst .GT. 0) then
                    CALL cooperativedistelement(exp1, refsetglobal%nlc, tam*nconst, refset%nlc); 
                end if
            else 
                PRINT *, "HETEROGENEUS STRATEGY AND ISLAND DIVISION NOT ALLOWED"
                CALL EXIT(0)                    
            end if
            
              
        else
        ! COOPERATIVE CASE
            if (cond2 .EQ. 0) then
                refset = refsetglobal
                CALL cooperativebcastelement(exp1, refsetglobal%x, nvar*tam)
                CALL cooperativebcastelement(exp1, refsetglobal%f, tam);
                CALL cooperativebcastelement(exp1, refsetglobal%fpen, tam);
                CALL cooperativebcastelement(exp1, refsetglobal%penalty, tam);
                if (nconst .GT. 0) then
                    CALL cooperativebcastelement(exp1, refsetglobal%nlc, tam*nconst); 
                end if
                refset = refsetglobal
            else 
                CALL cooperativebcastelement(exp1, refsetglobal%x, nvar*opts1%globaloptions%dim_refset)
                CALL cooperativebcastelement(exp1, refsetglobal%f, opts1%globaloptions%dim_refset);
                CALL cooperativebcastelement(exp1, refsetglobal%fpen, opts1%globaloptions%dim_refset);
                CALL cooperativebcastelement(exp1, refsetglobal%penalty, opts1%globaloptions%dim_refset);     
                if (nconst .GT. 0) then
                    CALL cooperativebcastelement(exp1, refsetglobal%nlc, opts1%globaloptions%dim_refset*nconst); 
                end if  
                
                refset%x =  refsetglobal%x(:,1:opts1%globaloptions%dim_refset)
                refset%f =  refsetglobal%f(1:opts1%globaloptions%dim_refset)
                refset%fpen =  refsetglobal%fpen(1:opts1%globaloptions%dim_refset)
                refset%penalty =  refsetglobal%penalty(1:opts1%globaloptions%dim_refset)
                if (nconst .GT. 0) then
                    refset%nlc =  refsetglobal%nlc(:,1:opts1%globaloptions%dim_refset)
                end if              
            end if            
        end if
        
        opts1%globaloptions%dim_refset = tam
        
        CALL destroy_refsettype(refsetlocal)
   END SUBROUTINE create_refset_local   
   
   
   
   
   
   
#endif
   
   
END MODULE parallelscattersearchfunctions    




