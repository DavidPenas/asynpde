MODULE scattersearch
    USE iso_c_binding
    
    
    TYPE :: problem 
        CHARACTER(len=20) :: f
        INTEGER :: neq
        INTEGER :: int_var
        INTEGER :: bin_var
        DOUBLE PRECISION :: vtr
        DOUBLE PRECISION, DIMENSION(:), ALLOCATABLE:: XL 
        DOUBLE PRECISION, DIMENSION(:), ALLOCATABLE:: XU 
        DOUBLE PRECISION, DIMENSION(:), ALLOCATABLE:: CL 
        DOUBLE PRECISION, DIMENSION(:), ALLOCATABLE:: CU
        DOUBLE PRECISION, DIMENSION(:,:), ALLOCATABLE:: X0 
        DOUBLE PRECISION, DIMENSION(:,:), ALLOCATABLE:: F0
    END TYPE problem
    
    TYPE :: uoptions
        DOUBLE PRECISION, DIMENSION(:), ALLOCATABLE:: log_var 
        INTEGER :: maxeval
        REAL :: maxtime
        INTEGER :: iterprint
        INTEGER :: plot
        INTEGER :: weight
        DOUBLE PRECISION :: tolc
        REAL :: prob_bound
        INTEGER :: strategy
        INTEGER :: inter_save
    END TYPE uoptions
    
    TYPE :: goptions
        INTEGER :: dim_refset
        INTEGER :: ndiverse ! -1 si es auto
        INTEGER :: initiate
        INTEGER :: combination
        INTEGER :: regenerate
        CHARACTER(len=20) :: delete
        INTEGER :: intens
        DOUBLE PRECISION :: tolf
        INTEGER :: diverse_criteria
        DOUBLE PRECISION :: tolx
        INTEGER :: n_stuck
    END TYPE goptions
    
    
    TYPE :: loptions
        CHARACTER(len=20) :: solver
        INTEGER :: tol
        INTEGER :: iterprint
        CHARACTER(len=20) :: n1
        CHARACTER(len=20) :: n2
        REAL :: balance
        CHARACTER(len=20), DIMENSION(:), ALLOCATABLE:: finish 
        INTEGER :: bestx
        INTEGER :: merit_filter
        INTEGER :: distance_filter
        REAL :: thfactor
        REAL :: maxdistfactor
        INTEGER :: wait_maxdist_limit
        INTEGER :: wait_th_limit
    END TYPE loptions
        
    TYPE :: opts 
        TYPE(uoptions)  :: useroptions
        TYPE(goptions)  :: globaloptions
        TYPE(loptions)  :: localoptions
    END TYPE opts
    
    
    CONTAINS
    
       INTEGER(C_INT) FUNCTION sscattersearch(exp1,locals,output,fitnessfunction,results,maxfunevals,ftarget)
	! Declaración de variables
            USE common_functions
                IMPLICIT NONE
                
                TYPE(C_PTR), INTENT(INOUT):: exp1, locals, output, results
                REAL (C_DOUBLE), INTENT(IN) ::  maxfunevals, ftarget
                TYPE(C_FUNPTR), INTENT(INOUT) :: fitnessfunction
			
                INTEGER :: stopoptimization, nfuneval, fin, nreset, maxeval
                REAL :: cputime, maxtime
                TYPE(opts) :: opts1, default1
                TYPE(problem) :: problem1        

                ! INICIALIZACION DE VARIABLES PROBLEM
                ALLOCATE(problem1%XL(2))
                ALLOCATE(problem1%XU(2))
                problem1%XL = (/ -1.0, -1.0 /)
                problem1%XU = (/ 1.0, 1.0 /)
                problem1%f = 'ex1'
                
                ! INICIALIZACION DE VARIABLES MAIN_EX1
                opts1%useroptions%maxeval=500
                opts1%globaloptions%ndiverse=40
                opts1%localoptions%solver = 'n2fb'
                ALLOCATE(opts1%localoptions%finish(1))
                opts1%localoptions%finish(1) = 'fmincon' 
                opts1%localoptions%iterprint = 1
                
                ! INICIALIZACION DE VARIABLES EN ESS_KERNEL
                stopoptimization=0
                CALL CPU_TIME(cputime)
                nfuneval=0
                fin=0
                nreset=0
                
                opts1%useroptions%maxtime = 1e12 ! DEFAULT
                maxeval = opts1%useroptions%maxeval
                maxtime = opts1%useroptions%maxtime
                
                
                ! CARGA DE PARAMETROS POR DEFECTO 
                !default1%useroptions%
                default1%useroptions%maxeval = 1000
                default1%useroptions%maxtime = 60
                default1%useroptions%iterprint = 1
                default1%useroptions%plot = 0
                default1%useroptions%weight = 1e6
                default1%useroptions%tolc = 1e-5
                default1%useroptions%prob_bound = 0.5
                default1%useroptions%strategy = 0
                default1%useroptions%inter_save = 0

                DEALLOCATE(problem1%XL)
                DEALLOCATE(problem1%XU)
                DEALLOCATE(opts1%localoptions%finish)
                sscattersearch=1
                
                
                PRINT *, "FIN"
                CALL SLEEP(10)
                END FUNCTION sscattersearch 
END MODULE scattersearch      
