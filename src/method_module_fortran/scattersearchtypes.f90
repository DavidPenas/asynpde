MODULE scattersearchtypes
    USE iso_c_binding
#ifdef MPI
        USE MPI
#endif

    INTEGER, PARAMETER :: PRECISION_D = 15
    INTEGER, PARAMETER :: RANGE_D = 307
   
    INTEGER, PARAMETER :: PRECISION_F = 7
    INTEGER, PARAMETER :: RANGE_F = 150  
    
    REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), PARAMETER :: TOL_PREC = &
            REAL(1d-18,KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))  
    
    REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), PARAMETER :: eps_m = &
            REAL(1d-18,KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))  
            
    REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), PARAMETER :: AUXPRECISIN = &
            REAL(10000d0,KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D))  
            
           
    
    TYPE :: problem
        INTEGER :: empty 
        CHARACTER(len = 20) :: f
        INTEGER :: neq
        INTEGER :: int_var
        INTEGER :: bin_var
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE  :: vtr
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: XL
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: XU
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: CL
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: CU
        INTEGER :: ineq
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE :: X0
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: F0
    END TYPE problem

    TYPE :: uoptions
        INTEGER :: empty 
        INTEGER :: iterprint
        INTEGER :: plot
        INTEGER :: weight
        INTEGER :: nstuck_solution
        INTEGER :: strategy
        INTEGER :: inter_save        
        INTEGER(C_LONG_LONG) :: maxeval
        REAL(C_DOUBLE) :: maxtime
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)) :: tolc
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)) :: prob_bound
        INTEGER, DIMENSION(:), ALLOCATABLE :: log_var    
        INTEGER :: init_point
    END TYPE uoptions

    TYPE :: goptions
        INTEGER :: empty 
        INTEGER :: dim_refset ! -1 si es auto
        INTEGER :: ndiverse ! -1 si es auto
        INTEGER :: initiate
        INTEGER :: combination
        INTEGER :: regenerate
        INTEGER :: intens
        INTEGER :: diverse_criteria
        INTEGER :: n_stuck
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)) :: tolf
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)) :: tolx
        CHARACTER(len = 20,kind=C_CHAR) :: delete
    END TYPE goptions

    TYPE :: extraparametters
        INTEGER :: empty
        INTEGER(KIND=8), DIMENSION(:), ALLOCATABLE :: texp ! nf2kb parametter
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE :: yexp ! nf2kb parametter
        REAL(C_FLOAT) :: k1 ! solnp parametter
        REAL(C_FLOAT) :: k2 ! solnp parametter
        REAL(C_FLOAT) :: k3 ! solnp parametter
        REAL(C_FLOAT) :: k4 ! solnp parametter
    END TYPE extraparametters

    TYPE :: loptions
        INTEGER :: empty 
        INTEGER :: tol
        INTEGER :: iterprint
        INTEGER :: n1
        INTEGER :: n2
        INTEGER :: bestx
        INTEGER :: merit_filter
        INTEGER :: distance_filter
        INTEGER :: wait_maxdist_limit
        INTEGER :: wait_th_limit
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)) :: threshold_local
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)) :: thfactor
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)) :: maxdistfactor
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)) :: balance
        TYPE(extraparametters) :: extrap        
        CHARACTER(len = 20,kind=C_CHAR) :: solver
        CHARACTER(len = 20,kind=C_CHAR), DIMENSION(:), ALLOCATABLE :: finish
    END TYPE loptions

    TYPE :: opts
        INTEGER :: empty
        TYPE(uoptions) :: useroptions
        TYPE(goptions) :: globaloptions
        TYPE(loptions) :: localoptions
    END TYPE opts

    TYPE :: Refsettype
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE :: x
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: f
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: fpen
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE :: nlc
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: penalty
        INTEGER, DIMENSION(:), ALLOCATABLE :: parent_index
    END TYPE Refsettype
    
    TYPE :: resultsf
        TYPE(Refsettype) :: refsett
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: fbest
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: xbest
        REAL(C_DOUBLE)  :: cputime
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: f
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:,:), ALLOCATABLE :: x
        REAL(C_DOUBLE), DIMENSION(:), ALLOCATABLE :: time
        INTEGER(C_LONG), DIMENSION(:), ALLOCATABLE :: neval
        INTEGER(C_LONG) :: numeval
        INTEGER(C_LONG_LONG) :: local_solutions
        INTEGER(C_LONG_LONG), DIMENSION(:), ALLOCATABLE :: local_solutions_values
        INTEGER(C_LONG_LONG) :: end_crit
    END TYPE resultsf
    
        

    TYPE :: outfuction
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)) :: value
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)) :: value_penalty
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)) :: pena
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: nlc
        INTEGER :: include
        REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE :: x
    END TYPE outfuction
    

    REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), PARAMETER :: INFINITE = 1.7976931348623158D308
    
    
    TYPE :: topology_data_fortran
        INTEGER :: num_r
        INTEGER :: num_left
        INTEGER, DIMENSION(:), ALLOCATABLE :: rigth
        INTEGER, DIMENSION(:), ALLOCATABLE :: left
#ifdef MPI2
!        MPI_COMM :: comunicator
#endif
    END TYPE
    

END MODULE scattersearchtypes
