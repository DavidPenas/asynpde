MODULE serialrandomsearch
    USE iso_c_binding
    
    USE scattersearchtypes
    CONTAINS
! PROGRAMA PRINCIPAL
! void serRandomSearch(void *, void *, void  *,  double(*fitnessfunction)(double*,void*), void *, double, double);
! (void *exp1_, void *local_s_, void  *output_, double(*fitnessfunction)(double*, void *), void *result_, double maxfunevals, double ftarget) 
       INTEGER(C_INT) FUNCTION srandomsearch(exp1,locals,output,fitnessfunction,results,maxfunevals,ftarget)
        ! Declaración de variables
            USE common_functions
                IMPLICIT NONE
                TYPE(C_PTR), INTENT(INOUT):: exp1, locals, output, results
                REAL (C_DOUBLE), INTENT(IN) ::  maxfunevals, ftarget
                TYPE(C_FUNPTR), INTENT(INOUT) :: fitnessfunction
                INTEGER(C_INT) :: randomsearch, chargedimension, initializebenchmarks, error, updateresultsrandomsearch
                INTEGER(C_INT) :: verboseiterationfortran, par, idp
                REAL(C_DOUBLE) :: callfitnessfunction, evaluations
                INTEGER(C_INT):: D, maxEval, i
                REAL(C_DOUBLE):: time1, time2, time3, timetotal, timeparcial
                REAL time4, time5, time6
                REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)) resultado, resultadoOld, init
                REAL(KIND=SELECTED_REAL_KIND(P=PRECISION_D,R=RANGE_D)), DIMENSION(:), ALLOCATABLE:: Mx, Xl, Xu
                        
                ! sentencias FORTRAN
                error = chargedimension(exp1, D)
                maxEval=INT(maxfunevals,C_INT)
                
                CALL initrngrandomserial(exp1)
                
                par = 0
                idp = 1
                CALL inicializa_Vector(Mx,D+1)
                CALL inicializa_Vector(Xl,D)
                CALL inicializa_Vector(Xu,D)

                error = initializebenchmarks(exp1, Xl, Xu, D)

                time1 = 0
                CALL random_Vector_SEED(exp1,Mx, D, Xl, Xu)
                resultadoOld = callfitnessfunction(fitnessfunction, exp1, Mx, D, Xl, Xu)
   

                PRINT *,"RESULTADO INICIAL",resultadoOld
                PRINT *
                PRINT *,"COMEZO RANDOM SEARCH - MAX EVAL 1e4"
                
                
                CALL CPU_TIME(time1)
                DO i=1,maxEval-1
                        CALL random_Vector_SEED(exp1,Mx, D, Xl, Xu)
                        !CALL CPU_TIME(time4)
                        ! callfitnessfunction_(double(*fitnessfunction)(double*, void *), void *exp1_, double *U, int D, double *Xl, double *Xm)
                        resultado = callfitnessfunction(fitnessfunction, exp1, Mx, D, Xl, Xu)
                        !CALL CPU_TIME(time5)
                        !time6 = time5 -time4
                        !PRINT *,time6 
                       
                        !IF (resultado < resultadoOld) resultadoOld = resultado
                        !evaluations = REAL(i, 10)
                        !CALL CPU_TIME(time2)
                        !timeparcial = time2 - time1
                        !error = verboseiterationfortran(exp1, i, timeparcial, ftarget, resultadoOld, evaluations, par, idp)
                        !IF (resultadoOld < ftarget) EXIT
                END DO
                CALL CPU_TIME(time3)                
                timetotal = time3 - time1
                PRINT *, "** RESULTADO FINAL = ", resultadoOld
                PRINT *, "** NUM EVAL =", i
                PRINT *, "** TEMPO =", timetotal
                
                ! int updateresultsrandomsearch_(void *exp1_, void *result_, double *totaltime, double *evaluation, double *best ) 
                error = updateresultsrandomsearch(exp1,results,timetotal,evaluations,resultadoOld)

                
                CALL  destroy_Vector(Mx)
                CALL  destroy_Vector(Xl)
                CALL  destroy_Vector(Xu)

                srandomsearch=1
                END FUNCTION srandomsearch 
END MODULE serialrandomsearch      
