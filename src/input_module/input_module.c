#include <structure_paralleltestbed.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <def_errors.h>
#include <hdf5.h>
#if  defined(OPENMP) 
    #include <omp.h>
#endif

#define CIRCADIAN "config/init_points/normal/circadian.hdf5"
#define MENDES "config/init_points/normal/mendes.hdf5"
#define NFKB "config/init_points/normal/nfkb.hdf5"


int openhdf5solutions_(void *exp1_, double *conxunto, int *D, int *NP) {
    hid_t file_id, dataset_id;
    herr_t status;
    double *conxunto2;
    int i,j, size;
    int contador, contfila, NN;
    experiment_total *exp1;
    int bench;
    
    exp1 = (experiment_total *) exp1_;
    
    //if (exp1->par_st != NULL) {
    //    if (strcmp(exp1->par_st->type, coop) == 0) cc = 1;
    //    else cc = 0;
    //}

    bench = (*exp1).test.bench.current_bench;
    H5Eset_auto( H5E_DEFAULT, NULL, NULL);
    if (    bench == 0   ) {
            file_id = H5Fopen(CIRCADIAN, H5F_ACC_RDONLY, H5P_DEFAULT);
    } else if (bench == 1) {
            file_id = H5Fopen(MENDES, H5F_ACC_RDONLY, H5P_DEFAULT);
    } else if (bench == 2) {
            file_id = H5Fopen(NFKB, H5F_ACC_RDONLY, H5P_DEFAULT);
    } else {
        perror("NOT LOAD HDF5\n");
        exit(0);
    }
    
    dataset_id  = H5Dopen2(file_id, "/dset", H5P_DEFAULT);
    
    conxunto2=NULL;
    size = (*D)*(1004+1);
    conxunto2 = (double *) malloc(size*sizeof(double));
    status = H5Dread(dataset_id, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, conxunto2);
    NN = 1004;
    contador=0;
    contfila=0;
    while (contador < (*NP)*(*D)) {
        for (i=0;i<NN;i++) {
            for (j=0;j<*D;j++) {
                conxunto[ contfila * *D + j ] = conxunto2[j*(NN+1) + i ];
                contador++;
            }
            if ( contador >= (*NP)*(*D) ) break;
            contfila++;
        }
    }
    
    status = H5Dclose(dataset_id); 
    status = H5Fclose(file_id); 
            
    free(conxunto2);
    conxunto2=NULL;
    
    
    return 1;
}

char* removeSpace(char *str) {
  char *p1 = str, *p2 = str;
  do 
    while (*p2 == ' ')
      p2++;
  while (*p1++ = *p2++);
  
  return str;
}

void removePoint(char *str) {
  char *p1 = str, *p2 = str;
  do 
    while (*p2 == '.')
      p2++;
  while (*p1++ = *p2++);
}


xmlNodePtr extract_init_node(xmlNodePtr cur, const char *name) {
    int exit;
    exit = 0;
    xmlNodePtr end_cur;
    
    end_cur = cur;
     while (end_cur != NULL && exit == 0)  {
        if (!xmlStrcmp(end_cur->name, (const xmlChar *) name )) {
            exit = 1;   
        } else {
            end_cur = end_cur->next;
        }
    }
    
    return end_cur;
} 

char*  extract_element_uniq(xmlDocPtr doc, xmlNodePtr cur, const char *name ) {
    xmlChar *key;
    char *output;
    
    cur = cur->xmlChildrenNode;

    output = NULL;
    while (cur != NULL) {
        if ((!xmlStrcmp(cur->name, (const xmlChar *)name)))  {
            key = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
            output = (char*) malloc((strlen((char *) key) +1) * sizeof (char));
            strcpy(output, (const char *) key);
            xmlFree(key);
        }
        cur = cur->next;
    }
    return output;
}

int  count_element_multi(xmlDocPtr doc, xmlNodePtr cur, const char *name, int *size, int *hit) {
    xmlChar *key;
    xmlNodePtr cur_pre;
    cur_pre = cur->xmlChildrenNode;
    cur = cur->xmlChildrenNode;
    
    
    *size=0;
    *hit=0;
    while (cur_pre != NULL) {
        if ((!xmlStrcmp(cur_pre->name, (const xmlChar *)name)))  {
            key = xmlNodeListGetString(doc, cur_pre->xmlChildrenNode, 1);
            *size= *size + strlen((char *) key);
            *hit= *hit + 1;
        }
        cur_pre = cur_pre->next;
    } 
    
    return 1;
}

void  extract_element_multi(xmlDocPtr doc, xmlNodePtr cur, const char *name,  char *vectorG, int *vect_size, int hit) {
    xmlChar *key;
    xmlNodePtr cur_pre;
    cur_pre = cur->xmlChildrenNode;
    cur = cur->xmlChildrenNode;
    int size;
    
    hit = 0;
    size = 0;
    while (cur != NULL) {
        if ((!xmlStrcmp(cur->name, (const xmlChar *)name)))  {
            key = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
            memmove( vectorG + size, ( char *) key, strlen((char *)key) );
            //sprintf(vectorG + size,"%s", ( char *) key);
            size = size + (strlen((char *)key));
            vect_size[hit] = strlen((char *)key);
            hit++;
            xmlFree(key);
            key = NULL;                
        }
        cur = cur->next;
    }
    
}

int extract_element_test(xmlDocPtr doc, xmlNodePtr *root, experiment_testbed *test) {
    xmlNodePtr cur, init_cur, bench_cur; 
    const char *name_exp;
    const char *name_exptype;
    const char *name_bench;    
    const char *name_bench_type;
    const char *name_bench_dim;
    const char *name_bench_min;
    const char *name_bench_max;
    const char *name_eval;
    const char *name_tol;
    const char *name_rep;
    const char  *name_minDom, *name_maxDom;
    const char *name_scale_log;
    char *vectorDim, *outputDim, *min, *max; 
    int *vectorsize;
    int i, size, hit;
    char const *noiseBBOB;
    char const *noiselessBBOB;
    char const *system;
    char const *name_ouput;    
    char const *name_verbose;
    char const *name_output_temp;
    char const *name_output_graph;  
    char const *name_init_point;
    char const *local_search;
    char const *local_gradient;
    char const * testname;
    char const * maxtimec;
 
    testname = "test";
    noiseBBOB ="noiseBBOB";
    noiselessBBOB="noiselessBBOB";
    system="systemBiology";    
    name_exp= "experiment";
    name_exptype= "exptype";
    name_bench= "benchmark";
    name_bench_type= "typebench";    
    name_bench_dim= "dim";    
    name_bench_min= "min_bench";
    name_bench_max= "max_bench";
    name_eval= "evaluation";
    name_tol= "tolerance";
    name_rep= "repetitions";
    name_minDom = "minDomain";
    name_maxDom = "maxDomain";
    name_scale_log = "log_scale";
    name_verbose =    "verbose";
    name_ouput = "output";
    name_output_temp = "output_temp";
    name_output_graph = "output_graph";
    name_init_point = "init_point";
    local_search= "local_search";
    local_gradient= "gradient";
    maxtimec= "maxtime";
   
    
    init_cur = (*root)->xmlChildrenNode;
    
    test->output_graph = (char *) calloc(500, sizeof(char));
    cur = extract_init_node(init_cur, name_exp);
    test->type = removeSpace(extract_element_uniq(doc,cur,name_exptype));
    test->max_eval = atof(extract_element_uniq(doc,cur,name_eval));
    test->tolerance = (double) atof(extract_element_uniq(doc,cur,name_tol));
    test->repetitions =  atof(extract_element_uniq(doc,cur,name_rep));
    test->_log = atoi(extract_element_uniq(doc, cur, name_scale_log));  
    

    test->output_graph = removeSpace(extract_element_uniq(doc,cur,name_output_graph));
    test->verbose = atoi(extract_element_uniq(doc, cur, name_verbose)); 
    test->ouput = atoi(extract_element_uniq(doc, cur, name_ouput));  
    test->output_temp =  atof(extract_element_uniq(doc,cur,name_output_temp)); 
   
    if (extract_element_uniq(doc,cur,maxtimec) == NULL) test->maxtime=-1; 
    else    test->maxtime = atof(extract_element_uniq(doc,cur,maxtimec));
    
    test->init_point = atoi(extract_element_uniq(doc, cur, name_init_point)); 
    test->local_search = atoi(extract_element_uniq(doc, cur, local_search)); 
    bench_cur = cur->xmlChildrenNode;
    cur = extract_init_node(bench_cur, name_bench);
    test->bench.type = removeSpace(extract_element_uniq(doc,cur,name_bench_type));

    min = extract_element_uniq(doc, cur, name_bench_min);
    max = extract_element_uniq(doc, cur, name_bench_max);

    test->bench.min_dom = (double *) malloc( sizeof (double));
        test->bench.max_dom = (double *) malloc( sizeof (double));
        test->bench.min_dom[0] = atof(extract_element_uniq(doc, cur, name_minDom));
        test->bench.max_dom[0] = atof(extract_element_uniq(doc, cur, name_maxDom));


        test->bench.current_bench = atoi(extract_element_uniq(doc, cur, name_bench_min));
        test->bench.dim = atoi(extract_element_uniq(doc, cur, name_bench_dim));
    printf("test->bench.dim %d\n", test->bench.dim ); 
    
    return 1;
}

int extract_element_method_DE(xmlDocPtr doc, xmlNodePtr *root, experiment_method_DE *method) {
    xmlNodePtr cur, init_cur; 

    const char *name_exp;
    const char *name_name;
    const char *name_F;
    const char *name_CR;
    const char *name_MutationStrategy;   
    const char *name_NP; 
    const char *name_cache;
    const char *ls_counter;
    const char *ls_threshold;
    name_exp= "method";
    name_name = "name";
    name_F= "paramF";
    name_CR= "paramCR";
    name_MutationStrategy = "MutationStrategy";
    name_NP= "paramNP";
    name_cache = "cache";
    ls_counter = "local_search_counter";
    ls_threshold = "local_search_threshold";

    
    init_cur = (*root)->xmlChildrenNode;
    cur = extract_init_node(init_cur, name_exp);
    
    method->name = removeSpace(extract_element_uniq(doc,cur,name_name));
    method->_F = atof(extract_element_uniq(doc,cur,name_F));
    method->_CR = atof(extract_element_uniq(doc,cur,name_CR));
    method->mutation_strategy = removeSpace(extract_element_uniq(doc,cur,name_MutationStrategy));
    method->_NP =  atoi(extract_element_uniq(doc,cur,name_NP));
    method->cache =  atoi(extract_element_uniq(doc,cur,name_cache));
    method->ls_counter = atoi(extract_element_uniq(doc,cur,ls_counter));
    method->ls_threshold = atof(extract_element_uniq(doc,cur,ls_threshold));
    //printf("%lf - %lf - %s - %d\n\n", method->_F, method->_CR, method->mutation_strategy, method->_NP);

    return 1;
}


int extract_element_method_RandomSearch(xmlDocPtr doc, xmlNodePtr *root, experiment_method_RandomSearch *method) {
    xmlNodePtr cur, init_cur; 

    const char *name_exp;
    const char *name_name;
    const char *name_NP; 
    const char *name_cache;
    const char *ls_counter;
    const char *ls_threshold;
    name_exp= "method";
    name_name = "name";
    name_NP= "paramNP";
    name_cache = "cache";
    ls_counter = "local_search_counter";
    ls_threshold = "local_search_threshold";
  
    
    init_cur = (*root)->xmlChildrenNode;
    cur = extract_init_node(init_cur, name_exp);
    
    method->name = removeSpace(extract_element_uniq(doc,cur,name_name));
    method->_NP =  atoi(extract_element_uniq(doc,cur,name_NP));
    method->cache =  atoi(extract_element_uniq(doc,cur,name_cache));
    method->ls_counter = atoi(extract_element_uniq(doc,cur,ls_counter));
    method->ls_threshold = atof(extract_element_uniq(doc,cur,ls_threshold));
    
    return 1;
}

int extract_element_method_ScatterSearch(xmlDocPtr doc, xmlNodePtr *root, experiment_method_ScatterSearch *method) {
    xmlNodePtr cur, init_cur,init_cur2, cur2; 

    const char *name_exp;
    const char *name_name;
    const char *name_NP; 
    const char *ls_threshold;
    const char *int_var;
    const char *bin_var;
    const char *ineq;
    
    const char *user_optionsc;
    const char *weightc;
    const char *tolcc;
    const char *prob_boundc;
    const char *nstuck_solutionc;
    const char *strategyc;
    const char *inter_save;
    
    const char *global_optionsc;
    const char *dim_refc;
    const char *ndiversec;
    const char *initiatec;
    const char *combinationc;
    const char *regeneratec;
    const char *deletec;
    const char *intensc;
    const char *tolfc;
    const char *diverse_criteriac;
    const char *tolxc;
    const char *n_stuckc;

    const char * local_opstionsc;
    const char * tolc;
    const char * iterprintc;
    const char * n1c;
    const char * n2c;
    const char * balancec;
    const char * solverc;
    const char * finishc;
    const char * bestxc;
    const char * merit_filterc;
    const char * distance_filterc;
    const char * thfactorc;
    const char * maxdistfactorc;
    const char * wait_maxdist_limitc;
    const char * wait_th_limitc;
    const char *name_cache;
    
    name_exp= "method";
    name_name = "name";
    name_NP= "paramNP";
    name_cache = "cache";
    ls_threshold = "local_search_threshold";
    int_var = "int_var";
    bin_var = "bin_var";
    ineq = "ineq";

    
    user_optionsc = "user_options";
    weightc = "weight";
    tolcc = "tolc";
    prob_boundc = "prob_bound";
    nstuck_solutionc = "nstuck_solution";
    strategyc = "strategy";
    inter_save = "inter_save";
    
    global_optionsc="global_options";
    dim_refc="dim_ref";
    ndiversec="ndiverse"; 
    initiatec="initiate"; 
    combinationc="combination"; 
    regeneratec="regenerate"; 
    deletec="delete"; 
    intensc="intens"; 
    tolfc="tolf"; 
    diverse_criteriac="diverse_criteria"; 
    tolxc="tolx"; 
    n_stuckc="n_stuck"; 
    
    local_opstionsc = "local_options";
    tolc = "tol";
    iterprintc="iterprint";
    n1c="n1";
    n2c="n2";
    balancec="balance";
    finishc="finish";
    bestxc="bestx";
    merit_filterc="merit_filter";
    distance_filterc="distance_filter";
    thfactorc="thfactor";
    maxdistfactorc="maxdistfactor";
    wait_maxdist_limitc="wait_maxdist_limit";
    wait_th_limitc="wait_th_limit";
    solverc="solver";
    
    init_cur = (*root)->xmlChildrenNode;
    cur = extract_init_node(init_cur, name_exp);
    
    method->name = removeSpace(extract_element_uniq(doc,cur,name_name));
    method->_NP =  atoi(extract_element_uniq(doc,cur,name_NP));
    method->ls_threshold = atof(extract_element_uniq(doc,cur,ls_threshold));
    
    method->int_var =  atoi(extract_element_uniq(doc,cur,int_var));
    method->bin_var = atoi(extract_element_uniq(doc,cur,bin_var));
    method->ineq = atoi(extract_element_uniq(doc,cur,ineq));
    
    init_cur2 = cur->xmlChildrenNode;
    cur2 = extract_init_node(init_cur2, user_optionsc);
    
    if (cur2 != NULL ) {
        method->uoptions = (user_options *) malloc(sizeof(user_options));
        if ( extract_element_uniq(doc,cur2,weightc) != NULL ) {                
                method->uoptions->weight = atoi(extract_element_uniq(doc,cur2,weightc));
        } else method->uoptions->weight = -1;
        if ( extract_element_uniq(doc,cur2,tolcc) != NULL ) {
            method->uoptions->tolc = atof(extract_element_uniq(doc,cur2,tolcc));
        } else method->uoptions->tolc = -1.0;
        if ( extract_element_uniq(doc,cur2,prob_boundc) != NULL ) {
            method->uoptions->prob_bound = atof(extract_element_uniq(doc,cur2,prob_boundc));
        } else  method->uoptions->prob_bound = -1.0;
        if (  extract_element_uniq(doc,cur2,nstuck_solutionc) != NULL ) {
            method->uoptions->nstuck_solution = atoi(extract_element_uniq(doc,cur2,nstuck_solutionc));
        } else method->uoptions->nstuck_solution = -1;
        if ( extract_element_uniq(doc,cur2,strategyc) != NULL  ) {
            method->uoptions->strategy = atoi(extract_element_uniq(doc,cur2,strategyc));
        } else method->uoptions->strategy = -1;
        if ( extract_element_uniq(doc,cur2,inter_save) != NULL  ) {
            method->uoptions->inter_save = atoi(extract_element_uniq(doc,cur2,inter_save));
        } else method->uoptions->inter_save = -1;
    }
    cur2 = extract_init_node(init_cur2, global_optionsc);
    if (cur2 != NULL ) {
        method->goptions = (global_options *) malloc(sizeof(global_options));
        if (extract_element_uniq(doc,cur2,ndiversec) != NULL ) {
            method->goptions->dim_ref = atoi(extract_element_uniq(doc,cur2,dim_refc));
        }  else method->goptions->dim_ref = -1;
        if (extract_element_uniq(doc,cur2,ndiversec) != NULL ) {
            method->goptions->ndiverse = atoi(extract_element_uniq(doc,cur2,ndiversec));
        } else method->goptions->ndiverse = -1;
        if ( extract_element_uniq(doc,cur2,initiatec) != NULL  ) {
            method->goptions->initiate = atoi(extract_element_uniq(doc,cur2,initiatec));
        }    else method->goptions->initiate = -1;
        if ( extract_element_uniq(doc,cur2,combinationc) != NULL  ) {
            method->goptions->combination = atoi(extract_element_uniq(doc,cur2,combinationc));
        } else method->goptions->combination = -1;
        if (  extract_element_uniq(doc,cur2,regeneratec) != NULL )  {
            method->goptions->regenerate  = atoi(extract_element_uniq(doc,cur2,regeneratec));  
        }
        if ( extract_element_uniq(doc,cur2,deletec) != NULL  )  {
            method->goptions->delete = removeSpace(extract_element_uniq(doc,cur2,deletec));
        } else method->goptions->delete = "";
        if ( extract_element_uniq(doc,cur2,intensc) != NULL ) {
            method->goptions->intens  = atoi(extract_element_uniq(doc,cur2,intensc)); 
        } else method->goptions->intens = -1;
        if ( extract_element_uniq(doc,cur2,tolfc) != NULL ) {
            method->goptions->tolf = atof(extract_element_uniq(doc,cur2,tolfc));
        } else  method->goptions->tolf  = -1.0;
        if ( extract_element_uniq(doc,cur2,diverse_criteriac) != NULL   ) {
            method->goptions->diverse_criteria = atoi(extract_element_uniq(doc,cur2,diverse_criteriac));
        } else method->goptions->diverse_criteria = -1;
        if ( extract_element_uniq(doc,cur2,tolxc) != NULL  ) {
            method->goptions->tolx = atof(extract_element_uniq(doc,cur2,tolxc));
        } else method->goptions->tolx = -1.0;
        if ( extract_element_uniq(doc,cur2,n_stuckc) != NULL ) {
            method->goptions->n_stuck = atoi(extract_element_uniq(doc,cur2,n_stuckc));
        } else method->goptions->n_stuck = -1;
    }
    cur2 = extract_init_node(init_cur2, local_opstionsc);
    if (cur2 != NULL ) {
        method->loptions = (local_options *) malloc(sizeof(local_options));
        if (  extract_element_uniq(doc,cur2,tolc) != NULL ) {
            method->loptions->tol = atoi(extract_element_uniq(doc,cur2,tolc)); 
        } else method->loptions->tol = -1;
        if ( extract_element_uniq(doc,cur2,iterprintc) != NULL ) {
            method->loptions->iterprint = atoi(extract_element_uniq(doc,cur2,iterprintc)); 
        } else method->loptions->iterprint = -1;
        if  (  extract_element_uniq(doc,cur2,n1c) != NULL )  {
            method->loptions->n1 = atoi(extract_element_uniq(doc,cur2,n1c)); 
        } else method->loptions->n1 = -1;
        if  (  extract_element_uniq(doc,cur2,n2c) != NULL )  {
                method->loptions->n2 = atoi(extract_element_uniq(doc,cur2,n2c)); 
        } else  method->loptions->n2 = -1;
        if ( extract_element_uniq(doc,cur2,balancec) != NULL ) {
            method->loptions->balance = atof(extract_element_uniq(doc,cur2,balancec));
        } else method->loptions->balance = -1;
        
        if ( extract_element_uniq(doc,cur2,solverc) != NULL ) {
            method->loptions->solver = removeSpace(extract_element_uniq(doc,cur2,solverc));
        } else  method->loptions->solver = "";     
        
        if ( extract_element_uniq(doc,cur2,finishc) != NULL ) {
            method->loptions->finish = removeSpace(extract_element_uniq(doc,cur2,finishc));
        } else  method->loptions->finish = NULL;
        if ( extract_element_uniq(doc,cur2,bestxc) != NULL )  {
            method->loptions->bestx = atoi(extract_element_uniq(doc,cur2,bestxc)); 
        } else method->loptions->bestx = -1;
        if (  extract_element_uniq(doc,cur2,merit_filterc) != NULL  ) {
            method->loptions->merit_filter = atoi(extract_element_uniq(doc,cur2,merit_filterc)); 
        } else method->loptions->merit_filter = -1;
        if ( extract_element_uniq(doc,cur2,distance_filterc) != NULL  ) {
            method->loptions->distance_filter = atoi(extract_element_uniq(doc,cur2,distance_filterc)); 
        } else method->loptions->distance_filter = -1;
        if (   extract_element_uniq(doc,cur2,thfactorc) != NULL  ) {
            method->loptions->thfactor = atof(extract_element_uniq(doc,cur2,thfactorc));
        } else method->loptions->thfactor = -1.0;
        if (  extract_element_uniq(doc,cur2,maxdistfactorc) != NULL ) {
            method->loptions->maxdistfactor = atof(extract_element_uniq(doc,cur2,maxdistfactorc));
        } else method->loptions->maxdistfactor = -1.0;
        if ( extract_element_uniq(doc,cur2,wait_maxdist_limitc) != NULL  ) {
            method->loptions->wait_maxdist_limit = atoi(extract_element_uniq(doc,cur2,wait_maxdist_limitc)); 
        } else method->loptions->wait_maxdist_limit = -1;
        if ( extract_element_uniq(doc,cur2,wait_th_limitc) != NULL  ) {
            method->loptions->wait_th_limit = atoi(extract_element_uniq(doc,cur2,wait_th_limitc)); 
        } else method->loptions->wait_th_limit = -1;
    }
    
    //printf("%lf - %lf - %s - %d\n\n", method->_F, method->_CR, method->mutation_strategy, method->_NP);

    return 1;
}

int extract_element_parallelization_islands(xmlDocPtr doc, xmlNodePtr *root, parallelization_strategy *parallel) {
    xmlNodePtr cur, init_cur; 
 
    const char *name_exp;
    const char *name_communication;
    const char *name_islandstrategy;
    const char *name_migration_size;   
    const char *name_migration_freq_ite;   
    const char *name_topology;
    const char *name_SelectionPolicy;   
    const char *name_ReplacePolicy;   
    name_exp= "parallelization";
    name_communication= "communication";
    name_islandstrategy= "islandstrategy";
    name_migration_size = "migration_size";
    name_migration_freq_ite= "migration_freq_ite";
    name_topology= "topology";
    name_SelectionPolicy= "SelectionPolicy";
    name_ReplacePolicy = "ReplacePolicy";
    
    init_cur = (*root)->xmlChildrenNode;
    cur = extract_init_node(init_cur, name_exp);
        
    
    parallel->commu = removeSpace(extract_element_uniq(doc,cur,name_communication));
    parallel->islandstrategy = removeSpace(extract_element_uniq(doc,cur,name_islandstrategy));
    parallel->migration_size = atoi(extract_element_uniq(doc,cur,name_migration_size));
    parallel->migration_freq_ite =  atoi(extract_element_uniq(doc,cur,name_migration_freq_ite));
    parallel->topology = removeSpace(extract_element_uniq(doc,cur,name_topology));
    parallel->SelectionPolicy = removeSpace(extract_element_uniq(doc,cur,name_SelectionPolicy));
    parallel->ReplacePolicy = removeSpace(extract_element_uniq(doc,cur,name_ReplacePolicy));    
     
    return 1;
}



int load_configuration_XML(char *docname, experiment_total *exptotal, int NPROC, int id){
    xmlDocPtr doc; 
    xmlNodePtr root,init_cur, cur;
    xmlChar *value;
    const char *name_experiment, *island, *cooperative;
    const char *method, *name, *parallelization;
    int exit1;

    method="method";
    name="name";
    parallelization="parallelization";
    island="island";
    cooperative="cooperative";
    
    doc = xmlParseFile(docname);
    if (doc == NULL ) printf("Document parsing failed. \n");
    root = xmlDocGetRootElement(doc);     
    if (root == NULL) {
        xmlFreeDoc(doc);
        printf("Document is Empty!!!\n");
        return 0;
    }    
    init_cur = root->xmlChildrenNode;
    // EXTRACT TEST ELEMENTS
    
    extract_element_test(doc, &root, &exptotal->test);
    exptotal->execution.nameMatlab = (char *) malloc(100*sizeof(char));
    memmove( exptotal->execution.nameMatlab, strchr(exptotal->test.output_graph, '/') +1, strlen(strchr(exptotal->test.output_graph, '/'))  );
    exptotal->test.namexml = (const char*) docname;
    exptotal->test.init_repetition = 0;


    
    // EXTRACT METHOD ELEMENTS
    cur = extract_init_node(init_cur, method);
    if (cur != NULL) {
        exit1 = 0;
        value = NULL;
        while (cur != NULL || exit1 == 0) {
            if ((!xmlStrcmp(cur->name, (const xmlChar *) method))) {
                value = xmlGetProp(cur, (const xmlChar *) name);
                exit1 = 1;
            }
            cur = cur->next;
        }
        if (strcmp((const char *) value, "DE") == 0) {
            exptotal->methodDE = (experiment_method_DE *) malloc(sizeof (experiment_method_DE));
            extract_element_method_DE(doc, &root, exptotal->methodDE);
            exptotal->methodRandomSearch=NULL;
            exptotal->methodScatterSearch=NULL;
        }
        else if (strcmp((const char *) value, "RandomSearch") == 0) {
            exptotal->methodRandomSearch = (experiment_method_RandomSearch *) malloc(sizeof (experiment_method_RandomSearch));
            extract_element_method_RandomSearch(doc, &root, exptotal->methodRandomSearch);
            exptotal->methodDE=NULL;
            exptotal->methodScatterSearch=NULL;
        }
        else if (strcmp((const char *) value, "ScatterSearch") == 0) {
            
            exptotal->methodScatterSearch = (experiment_method_ScatterSearch *) malloc(sizeof (experiment_method_ScatterSearch));
            extract_element_method_ScatterSearch(doc, &root, exptotal->methodScatterSearch);
            exptotal->methodRandomSearch=NULL;
            exptotal->methodDE=NULL;
        }        
        
    } else return 0;
    
    
    // EXTRACT PARALLELIZATION ELEMENTS          printf("++NPROC = %d\n",(*exptotal).par_st->NPROC);
    cur = extract_init_node(init_cur, parallelization);

#if  defined(OPENMP) || defined(MPI2)
    if (cur != NULL) {
        exit1 = 0;
        
        while (cur != NULL || exit1 == 0) {
            if ((!xmlStrcmp(cur->name, (const xmlChar *) parallelization))) {
                value = xmlGetProp(cur, (const xmlChar *) name);
                exit1 = 1;
            }
            cur = cur->next;
        }
        if (strcmp((const char *) value, island) == 0 
                ||
                strcmp((const char *) value, cooperative) == 0) {
            exptotal->par_st = (parallelization_strategy *) malloc(sizeof(parallelization_strategy));
            
            if (strcmp((const char *) value, island) == 0) exptotal->par_st->type=(char *) island;
            else if (strcmp((const char *) value, cooperative) == 0) exptotal->par_st->type=(char *) cooperative;
            
            extract_element_parallelization_islands(doc, &root, exptotal->par_st);
            exptotal->par_st->NPROC = NPROC;
                #if OPENMP
            #pragma omp parallel shared(NPROC) 
            {       
                exptotal->par_st->NPROC_OPENMP =  omp_get_max_threads();
            }
    #endif
        }
        else {
            perror(error9);
            exit(9);
        }
    } else {
        exptotal->par_st = NULL;
    }
    #else
        exptotal->par_st = NULL;
    #endif


    xmlFree(value);       
    
    return 1;    
    
}











