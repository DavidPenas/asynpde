/* 
 * File:   main.c
 * Author: david
 *
 * Created on 30 de mayo de 2013, 12:00
 */

#include <stdio.h>
#include <configuration.h>
#include <string.h>
#include <stdlib.h>
#include <libxml/xmlmemory.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <structure_paralleltestbed.h>
#include <input_module.h>
#include <execution_methods.h>
#include <output.h>


#ifdef MPI2
        #include <mpi.h> 
#endif

#ifdef OPENMP
        #include <omp.h>
#endif

int main(int argc, char** argv) {
    int id, NPROC;
    char *file;
    experiment_total *exptotal;
    

        exptotal = (experiment_total *) malloc(sizeof(experiment_total));
        id = 0;
        NPROC = 1;

    #ifdef MPI2
        MPI_Init(&argc, &argv);
        MPI_Comm_size(MPI_COMM_WORLD, &NPROC);
        MPI_Comm_rank(MPI_COMM_WORLD, &id);   
        exptotal->execution.idp = id;
    #endif
        
        
    // INPUT
    if (argc != 2) {
        if (id == 0)
            printf(" ARG1 file_config PATH\n\n");
        return 0;


    }
    file = argv[1];
    
    if (load_configuration_XML(file, exptotal, NPROC, id) == 0 ) {
        if (id == 0)
            printf(" Error in load configuration\n\n");
        return 0;
    }
    
    // EXECUTION
    execute_Solver(exptotal, id);
    
    if (exptotal->test.ouput == 1) {
        
#ifdef MPI2
    MPI_Comm_rank(MPI_COMM_WORLD, &id); 
    plot_file(*exptotal, 1, id, NPROC);

#else 
    plot_file(*exptotal, 0, 0,NPROC);
#endif
    }
    
    #ifdef MPI2 
        MPI_Finalize();
    #endif
        
    destroyexp(exptotal);
    if (exptotal != NULL) {
        free(exptotal);
        exptotal=NULL;
    }
    return (EXIT_SUCCESS);
}





int main2(int argc, char** argv) {
    int id, NPROC;
    char *file;
    experiment_total *exptotal;
    

    exptotal = (experiment_total *) malloc(sizeof(experiment_total));
    id = 0;
    NPROC = 1;

    #ifdef MPI2
        MPI_Init(&argc, &argv);
        MPI_Comm_size(MPI_COMM_WORLD, &NPROC);
        MPI_Comm_rank(MPI_COMM_WORLD, &id);   
        exptotal->execution.idp = id;
    #endif
        
        
    // INPUT
    if (argc != 2) {
        if (id == 0)
            printf(" ARG1 file_config PATH\n\n");
        return 0;


    }
        
    file = argv[1];
    
    if (load_configuration_XML(file, exptotal, NPROC, id) == 0 ) {
        if (id == 0)
            printf(" Error in load configuration\n\n");
        return 0;
    }
    
    // EXECUTION
    execute_methodSB(exptotal, id);
    
    if (exptotal->test.ouput == 1) {
        
#ifdef MPI2
    MPI_Comm_rank(MPI_COMM_WORLD, &id); 
    plot_file(*exptotal, 1, id, NPROC);

#else 
    plot_file(*exptotal, 0, 0,NPROC);
#endif
    }
    #ifdef MPI2 
        MPI_Finalize();
    #endif
        
    destroyexp(exptotal);
    if (exptotal != NULL) {
        free(exptotal);
        exptotal=NULL;
    }
    return (EXIT_SUCCESS);
}







