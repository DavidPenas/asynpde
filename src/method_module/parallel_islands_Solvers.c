#ifdef MPI2


#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <float.h>
#include <limits.h>
#include <bbobStructures.h>
#include <structure_paralleltestbed.h>
#include <common_solver_operations.h>
#include <parallel_functions.h>
#include <configuration.h>
#include <mpi.h>
#include <dynamic_list.h>
#include <def_errors.h>
#include <parallel_functions_cooperative.h>
#include <output.h>
#include <local_solvers.h>
#include <benchmark_functions_SystemBiology.h>
#include <string.h>
#include <output.h>
#include <input_module.h>
#ifdef OPENMP
    #include <omp.h>
#endif

int parallel_asynchronous_DE(void *exp1_, void *local_s_, void  *output_,
        void*(*fitnessfunction)(double*, void *), void *result_, long maxfunevals,
        double ftarget, int idp) {

    int i, j, z, k, l, NPROC, tam, stop,valor,rest;
    int optioncoop;
    int D, NP, index, cont;
    long evaluation_local, eval_total;
    int flag, par;
    int sizeTTT;
    double starttime, time_total, currenttime;
    double _CR, _F, *U, best, bestOLD, *Xl, *Xm, *nextLocal, *populLocal, *ptrLocal, *popul,
            *iptr, *populVector, *Ptos;
    double    *populonly;
    int global_k;
    int idomp;
    int ccop;

    experiment_total *exp1;
    local_solver *local_s;
    output_struct  *output;
    result_solver *result;
    exp1 = (experiment_total *) exp1_;
    local_s = (local_solver *) local_s_;
    output = (output_struct *) output_;
    result = (result_solver *) result_;
    par = 1;
    rest=0;
    chargedimension_((void *) exp1, &D);
    charge_DE_parameters((void *) exp1, D, &NP, &_CR, &_F); 
    initlocalsolvervars_(local_s);
    initoutputvars_(output);
    Xl = (double *) malloc(D* sizeof (double));
    Xm = (double *) malloc(D* sizeof (double));
    
    initializebenchmarks_((void *) exp1, Xl, Xm, &D);
    setnproc_((void *) exp1, &NPROC);
    ccop = iscoop_(exp1);
    chargecooperativeparameters_( (void *) exp1, &NP, &tam, &idp, &ftarget, &maxfunevals, &ccop);  
    createcooperativetopology_( (void *) exp1);
  

    bestOLD = DBL_MAX;
    best = DBL_MAX;

    index = 0;
    stop = 0;
    
    INITRAND_PAR_BBOB(idp);
    
    
    starttime = MPI_Wtime();
    
    if (NP == tam) popul = (double *) malloc(NP * NPROC * (D + 1)* sizeof (double));
    else  popul = (double *) malloc(NP * (D + 1)* sizeof (double));
    if (popul == NULL) perror("malloc");
    
    
    nextLocal = (double *) malloc((tam)*(D + 1)* sizeof (double));
    if (nextLocal == NULL) perror("malloc");

    populLocal = (double *) malloc((tam)*(D + 1)* sizeof (double));
    if (populLocal == NULL) perror("malloc");

    Ptos = (double *) malloc(tam * (D + 1)* sizeof (double));
    if (Ptos == NULL) perror("malloc");

    for (i = 0; i < tam; i++) {
        for (j = 0; j < (D + 1); j++) Ptos[i * (D + 1) + j] = DBL_MAX;
    }

    populVector = (double *) malloc((D + 1)* sizeof (double));
    if (populVector == NULL) perror("malloc");

    createcooperativestructs_(exp1,&D);
    
    if ((*exp1).test.init_point == 1) {
      
        populonly = (double *) malloc(NP * D * sizeof (double));
        if (populonly == NULL) perror("malloc");
        if (idp == 0) {
            U = (double *) malloc((D + 1)* sizeof (double));
            openhdf5solutions_(exp1, populonly, &D, &NP);
            cont=0;
            for (i = 0; i < NP; i++) {
                    memmove(U, &(populonly[cont * D]), D * sizeof (double));
                    if (exp1[0].test._log == 1) converttolog_(U, &D);
                    callfitnessfunction_(fitnessfunction, (void *) exp1, U, &D, Xl, Xm);
                    evaluation_local++;
                    if (NP == tam) memmove(&(populLocal[i * (D + 1)]), U, (D + 1) * sizeof (double));
                    if (NP > tam) memmove(&(popul[i * (D + 1)]), U, (D + 1) * sizeof (double));
                    cont++;
            }
            free(U);
            U=NULL;
        }
        
        if (NP == tam) {
            sizeTTT = NP * (D+1);
            optioncoop = 0;
            cooperativebcastelement_( (void *) exp1, populLocal, &sizeTTT, &optioncoop); 
        }
        else if (NP > tam) cooperativedist_( (void *) exp1, popul,&D, populLocal); 

        free(populonly);
        populonly = NULL; 
    
    }
    else {
        if (NP == tam) {
            if (idp == 0) generatepopulation_((void *) exp1, populLocal, tam, D, fitnessfunction,Xl, Xm, &evaluation_local);
            sizeTTT = NP * (D+1);
            optioncoop = 0;
            cooperativebcastelement_((void *) exp1, populLocal, &sizeTTT, &optioncoop); 
        }
        else if (NP > tam) {
            if (idp == 0) generatepopulation_((void *) exp1, popul, NP, D, fitnessfunction,Xl, Xm, &evaluation_local);
            cooperativedist_(exp1, popul,&D, populLocal); 
        }
    }
   
 
    evaluation_local = 0;
 
    initcooperativestoppingcriteria_( exp1 );
    index = extract_best_index(populLocal, tam, D);
    best = populLocal[index* (D+1)+D];
    
    //printf("\n**** (%d)  - ", idp);
    //for (k=0;k<10;k++) {
    //    printf("%lf ", populLocal[k*(D+1)+D]);
    //}
    //printf("\n");
    
    //if (idp == 0) printf("%d -- init best %lf\n", idp, best);

    if (exp1[0].test.ouput == 1) {
        currenttime = calctimempi_(exp1,&starttime);
    }
    initprintfile_((void *) exp1, (void*) output, &best, &par, &idp, &currenttime, &evaluation_local); 

    
    for (k = 0; ; k++) {
    	/* Allocating memory for a trial vector U	*/
    	U = (double *) malloc((D + 1)* sizeof (double));
    	if (U == NULL) perror("malloc");

    	iptr = (double *) malloc((D + 1)* sizeof (double));
    	if (iptr == NULL) perror("malloc");

        for (i = 0; i < tam; i++) {
                U = mutation_operation((void *) exp1, populLocal, U, D, tam, i, index, idp, _CR, _F);
                callfitnessfunction_(fitnessfunction, (void *) exp1,  U,  &D, Xl, Xm);
                evaluation_local=evaluation_local+1;
                if (U[D] <= populLocal[i * (D + 1) + D]) {
                    for (z = 0; z < (D + 1); z++) {
                        iptr[z] = U[z];
                        U[z] = nextLocal[i * (D + 1) + z];
                        nextLocal[i * (D + 1) + z] = iptr[z];
                    }

                } else {
                    for (z = 0; z < (D + 1); z++) {
                        nextLocal[ i * (D + 1) + z] = populLocal[ i * (D + 1) + z ];
                    }
                }
        }
    
    	free(U);
    	U = NULL;
    
   	free(iptr);
   	iptr=NULL;
        
        checkcooperativemigrationcriteria_(exp1); 
        migrationcooperativesent_(exp1, nextLocal, &stop, &D, Xl, Xm, &rest);
        migrationcooperativereception_( exp1, fitnessfunction, nextLocal, &stop, &D, Xl,Xm, &rest);


        ptrLocal = populLocal;
        populLocal = nextLocal;
        nextLocal = ptrLocal;

        index = extract_best_index(populLocal, tam, D);
        best = populLocal[index * (D + 1) + D];

        if (stop < 1) {
            if (best < bestOLD) {
                bestOLD = best;
                valor = 0;
                setexpexecutionstuckcount(exp1, &valor);
            } else {
                valor = getexpexecutionstuckcount(exp1) + 1;
                setexpexecutionstuckcount(exp1, &valor);
            }
        
        }
       
        // printf("%d- localsolverinterface_ - %d \n",idp,k); 
        localsolverinterface_((void *) exp1, (void *) local_s, (void *) output, fitnessfunction, populLocal, &tam, Ptos, &stop, &index, &best, &(exp1[0].execution.idp), &NPROC, &evaluation_local );
	
        if (idp == 0 )
             verboseiteration_( (void *) exp1, k,  starttime,  ftarget,  best,  evaluation_local, idp);
      
        asynchronousstoppingcriteriawithrestart_(exp1, &stop, &evaluation_local, &best, populLocal, &D); 
        if (stop < 1) {
                for (l = 0; l < NPROC; l++) {
                    flag = 0;
                    flag = cooperativempitest_(exp1, &l);
                
                    if (flag == 1) {
                        stop = 1;
                        break;
                    }
                } 
        } 
 
        if (exp1[0].test.ouput == 1) {
            currenttime = calctimempi_(exp1,&starttime);
        }
        printiteration_((void *) exp1,(void *) output, (void *) local_s, &k, &best, &evaluation_local,&currenttime);
        addlocalscounter_((void *) local_s);

        if (stop == 1) {
            printf("\n************%d- END - VTR ACHIEVED - \n",idp);
            break;
        } else if (stop == 2)  {
            printf("\n************%d- END - EVALUATION MAXIMUM ACHIEVED\n",idp);
            break;
        } else if (stop >2){
            break;
        }
        

    }
    
    index = extract_best_index(populLocal, tam, D);
    best = populLocal[index * (D + 1) + D];
            
    MPI_Barrier(MPI_COMM_WORLD);
    
    if (exp1[0].test.ouput == 1) {
        currenttime = calctimempi_(exp1,&starttime);
    }
    printenditeration_((void *) exp1, (void *) output, &best, &currenttime, &evaluation_local);
        
    MPI_Reduce(&k, &global_k, 1, MPI_INT, MPI_SUM, 0,
           MPI_COMM_WORLD);
    if (idp == 0){
 	printf("GLOBAL K = %d\n", global_k);
	printf("GLOBAL NPROC = %lf\n", (double)global_k / (double)NPROC);
    }
    MPI_Barrier(MPI_COMM_WORLD);
    gatherresults_(exp1, populLocal, popul, &D, &starttime, &time_total, &evaluation_local, &eval_total);
   
    

            
    updateresultsandprint_((void *) exp1, (void *) result, (void *) output, (void *) local_s, &time_total, &eval_total, &D, &populLocal[index * (D + 1)], &best, &idp, &NPROC);
    
    destroycooperativestructures_((void *) exp1);
    
    
    if (nextLocal != NULL) {
        free(nextLocal);
        nextLocal = NULL;
    }
    
    if (populLocal != NULL) {
        free(populLocal);
        populLocal=NULL;
    }
    
    if (popul != NULL) {
        free(popul);
        popul=NULL;
    }
    
    if (iptr != NULL) {
        free(iptr);
        iptr=NULL;
    }
    
    if (U != NULL) {
        free(U);
        U=NULL;
    }
    
    if (populVector != NULL) {
        free(populVector);
        populVector=NULL;
    }
    
    if (Xl != NULL) {
        free(Xl);
        Xl=NULL;
    }
    
    if (Xm != NULL) {
        free(Xm);
        Xm=NULL;
    }
    
    if (Ptos != NULL) {
        free(Ptos);
        Ptos=NULL;
    }


    return 1;
}

int parallel_synchronous_DE(void *exp1_, void *local_s_, void  *output_, void*(*fitnessfunction)(double*, void *), void *result_, long maxfunevals, double ftarget, int idp) {
    int i, j, z, k, l, NPROC, tam, flag, sizeTTT;
    int D, NP, index, cont, contador ;
    long evaluation_local, eval_total;
    double currenttime, starttime, time_total;
    int valor;
    double _CR, _F, best, bestOLD, *Xl, *Xm, *nextLocal, *populLocal, *ptrLocal,
            *popul, *iptr, *U, *populVector, *Ptos;

    double    *populonly;
    
    int stop, par;
    int ccop, optioncoop;


    
    experiment_total *exp1;
    local_solver *local_s;
    output_struct  *output;
    result_solver *result;
    exp1 = (experiment_total *) exp1_;
    local_s = (local_solver *) local_s_;
    output = (output_struct *) output_;
    result = (result_solver *) result_;
    par = 1;
    
    chargedimension_((void *) exp1, &D);
    charge_DE_parameters((void *) exp1, D, &NP, &_CR, &_F); 
    initlocalsolvervars_((void *) local_s);
    initoutputvars_((void *) output);
    Xl = (double *) malloc(D* sizeof (double));
    Xm = (double *) malloc(D* sizeof (double));
    
    initializebenchmarks_((void *) exp1, Xl, Xm, &D);
    setnproc_((void *) exp1, &NPROC);
    ccop = iscoop_(exp1);
    chargecooperativeparameters_( (void *) exp1, &NP, &tam, &idp, &ftarget, &maxfunevals,&ccop);  
    createcooperativetopology_( (void *) exp1);

    
    bestOLD = DBL_MAX;
    best = DBL_MAX;
    index = 0;
    stop = 0;
    contador = 0;
    
    INITRAND_PAR_BBOB(idp);

    
    starttime = MPI_Wtime();
    
    if (NP == tam) popul = (double *) malloc(NP * NPROC * (D + 1)* sizeof (double));
    else  popul = (double *) malloc(NP * (D + 1)* sizeof (double));
    if (popul == NULL) perror("malloc");

    populonly = (double *) malloc(tam * D * sizeof (double));
    if (populonly == NULL) perror("malloc");

    nextLocal = (double *) malloc((tam)*(D + 1)* sizeof (double));
    if (nextLocal == NULL) perror("malloc");

    populLocal = (double *) malloc((tam)*(D + 1)* sizeof (double));
    if (populLocal == NULL) perror("malloc");
    
    Ptos = (double *) malloc(tam * (D + 1)* sizeof (double));
    if (Ptos == NULL) perror("malloc");

    for (i = 0; i < tam; i++) {
        for (j = 0; j < (D + 1); j++) Ptos[i * (D + 1) + j] = DBL_MAX;
    }

    iptr = (double *) malloc((D + 1)* sizeof (double));
    if (iptr == NULL) perror("malloc");

    populVector = (double *) malloc((D + 1)* sizeof (double));
    if (populVector == NULL) perror("malloc");

    
    
    if ((*exp1).test.init_point == 1) {
      
        populonly = (double *) malloc(NP * D * sizeof (double));
        if (populonly == NULL) perror("malloc");
        if (idp == 0) {
            U = (double *) malloc((D + 1)* sizeof (double));
            openhdf5solutions_(exp1, populonly, &D, &NP);
	    cont = 0;
            for (i = 0; i < NP; i++) {
                    memmove(U, &(populonly[cont * D]), D * sizeof (double));
                    if (exp1[0].test._log == 1) converttolog_(U, &D);
                    callfitnessfunction_(fitnessfunction, (void *) exp1, U, &D, Xl, Xm);
                    if (NP == tam) memmove(&(populLocal[i * (D + 1)]), U, (D + 1) * sizeof (double));
                    if (NP > tam) memmove(&(popul[i * (D + 1)]), U, (D + 1) * sizeof (double));
                    cont++;
            }
            free(U);
            U=NULL;
        }
        
        if (NP == tam) {
            sizeTTT = NP * (D+1);
            optioncoop = 0;
            cooperativebcastelement_( (void *) exp1, populLocal, &sizeTTT, &optioncoop ); 
        }
        else if (NP > tam) cooperativedist_( (void *) exp1, popul,&D, populLocal); 

        free(populonly);
        populonly = NULL; 
    
    }
    else {
        if (NP == tam) {
            if (idp == 0) generatepopulation_((void *) exp1, populLocal, tam, D, fitnessfunction,Xl, Xm, &evaluation_local);
            sizeTTT = NP * (D+1);
            optioncoop = 0;
            cooperativebcastelement_((void *) exp1, populLocal, &sizeTTT, &optioncoop ); 
        }
        else if (NP > tam) {
            if (idp == 0) generatepopulation_((void *) exp1, popul, NP, D, fitnessfunction,Xl, Xm, &evaluation_local);
            cooperativedist_(exp1, popul,&D, populLocal); 
        }
    }
    
    
    /* Allocating memory for a trial vector U	*/
    U = (double *) malloc((D + 1)* sizeof (double));
    if (U == NULL) perror("malloc");
    
    evaluation_local = 0;


    initcooperativestoppingcriteria_( exp1 );
    
    index = extract_best_index(populLocal, tam, D);
    best = populLocal[index* (D+1)+D];
    
    if (exp1[0].test.ouput == 1) {
        currenttime = calctimempi_(exp1,&starttime);
    }
    
    initprintfile_((void *) exp1, (void*) output, &best, &par, &idp, &currenttime, &evaluation_local);    
    
    for (k = 0;; k++) {
        
        
        for (i = 0; i < tam; i++) {

            if (stop < 1) {
                U = mutation_operation((void *) exp1, populLocal, U, D, tam, i, index, idp, _CR, _F);
                callfitnessfunction_(fitnessfunction, (void *) exp1,  U,  &D, Xl, Xm);
                evaluation_local++;
                if (U[D] <= populLocal[i * (D + 1) + D]) {
                    for (z = 0; z < (D + 1); z++) {
                        iptr[z] = U[z];
                        U[z] = nextLocal[i * (D + 1) + z];
                        nextLocal[i * (D + 1) + z] = iptr[z];
                    }

                } else {
                    for (z = 0; z < (D + 1); z++) {
                        nextLocal[ i * (D + 1) + z] = populLocal[ i * (D + 1) + z ];
                    }
                }
            } else break;
        }

        ptrLocal = populLocal;
        populLocal = nextLocal;
        nextLocal = ptrLocal;

        checkcooperativemigrationcriteria_(exp1); 

        
        index = extract_best_index(populLocal, tam, D);
        best = populLocal[index * (D + 1) + D];
        
        if (best < bestOLD) {
                bestOLD = best;
                valor = 0;
                setexpexecutionstuckcount(exp1, &valor);
        } else {
                valor = getexpexecutionstuckcount(exp1) + 1;
                setexpexecutionstuckcount(exp1, &valor);
        }
        
        
        localsolverinterface_((void *) exp1, (void *) local_s, (void *) output, fitnessfunction, populLocal, &tam, Ptos, &stop, &index, &best, &(exp1[0].execution.idp), &NPROC, &evaluation_local);
        
        if (idp == 0 )
             verboseiteration_( (void *) exp1, k,  starttime,  ftarget,  best,  evaluation_local, idp);
        
        if (exp1[0].test.ouput == 1) {
            currenttime = calctimempi_(exp1,&starttime);
        }
        
        printiteration_((void *) exp1,(void *) output, (void *) local_s, &k, &best, &evaluation_local,  &currenttime);      
        addlocalscounter_((void *) local_s);
        
        
        if ((exp1[0].execution.enterMigrat == 1) || NPROC == 1) {

            migrationsynchcooperativesent_(exp1,&D,populLocal,&best,&bestOLD, Xl, Xm);

            synchronousstoppingcriteriawithrestart_(exp1,&stop,&evaluation_local,&best, populLocal, &D); 

            if (stop < 1) {
                for (l = 0; l < NPROC; l++) {
                    flag = 0;
                    flag = cooperativempitest_(exp1, &l);
                    if (flag == 1) {
                        stop = 1;
                        break;
                    }
                }
            }
            
            setcountstopsvar_(exp1, &stop, &contador);
            if (contador == NPROC) {
                break;
            }
            else contador = 0;

        }
        
    }
    
    index = extract_best_index(populLocal, tam, D);
    best = populLocal[index * (D + 1) + D];
    
    MPI_Barrier(MPI_COMM_WORLD);
    
    if (exp1[0].test.ouput == 1) {
        currenttime = calctimempi_(exp1,&starttime);
    }
    
    printenditeration_((void *) exp1, (void *) output, &best, &currenttime, &evaluation_local);
            
    gatherresults_(exp1, populLocal, popul, &D, &starttime, &time_total, &evaluation_local, &eval_total);

    
    updateresultsandprint_((void *) exp1, (void *) result, (void *) output, (void *) local_s, &time_total, &eval_total, &D, &populLocal[index * (D + 1)], &best, &idp, &NPROC);

    
    if (nextLocal != NULL) {
        free(nextLocal);
        nextLocal=NULL;
    }
    
    if (populLocal != NULL) {
        free(populLocal);
        populLocal=NULL;
    }
    
    if (popul != NULL) {
        free(popul);
        popul=NULL;
    }
    
    if (iptr != NULL) {
        free(iptr);
        iptr=NULL;
    }
    
    if (U != NULL) {
        free(U);
        U=NULL;
    }
    
    if (populVector != NULL) {
        free(populVector);
        populVector=NULL;
    }
    
    if (Xl != NULL) {
        free(Xl);
        Xl=NULL;
    }
    
    if (Xm != NULL) {
        free(Xm);
        Xm=NULL;
    }
    
    if (Ptos != NULL) {
        free(Ptos);
        Ptos=NULL;
    }
    
  
    return 1;
}

#endif
