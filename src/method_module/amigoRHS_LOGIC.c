#include <amigoRHS.h>
#include <math.h>
#include <AMIGO_model.h>
//#include <amigoJAC.h>
//#include <amigoSensRHS.h>

//#define inhibitor(i)



/* Right hand side of the system (f(t,x,p))*/
int amigoRHS_LOGIC(realtype t, N_Vector y, N_Vector ydot, void *data){
    AMIGO_model* amigo_model=(AMIGO_model*)data;
    
    
    int n_states, n_inputs;
    
    double inhibitor,x,k,n,w,tau;
    int i,j,m,n_minterms;
    int ind,ind2;
    int counter=0;
    int input;
    int dec;
    int countBin=0;
    int vecBin[300];
    double resprod;
    double ressum;
    double scale;
    
    
    double fHill[30];
    
    
    n_states=amigo_model->n_states;
    
    for(i = 0; i < n_states; i++){
        //
        n_inputs=(int)amigo_model->pars[counter++];
        //mexPrintf("n_inputs=%d\n", n_inputs);
        if(n_inputs>0){
            
            for(j = 0; j < n_inputs; j++){
                
                input=(int)amigo_model->pars[counter++];
                k=amigo_model->pars[counter++];
                n=amigo_model->pars[counter++];
                scale=amigo_model->pars[counter++];
                
                if(input==-1){
                    fHill[j]=1;
                }else if(input==-2){
                    fHill[j]=0;
                }else{
                    x=Ith(y,input);
                    fHill[j]=(pow(scale*x,n)/(pow(scale*x,n)+pow(k,n)))*(1+pow(k,n));
                }
                
            }
            
            ressum=0;
            n_minterms=(int)pow(2,n_inputs);
            
            for(ind = 0; ind < n_minterms; ind++){
                //mexPrintf("n_mini=%d j=%d\n",n_minterms,ind);
                dec=ind;
                resprod=1;
                
                for(m=n_inputs-1;m>=0;m--){
                    vecBin[m]=(int)dec%2;
                    dec=(int)dec/2;
                    //mexPrintf("m=%d\n",m);
                }
                
                for(ind2 = 0; ind2 < n_inputs; ind2++){
                    
                    if(vecBin[ind2]){
                        resprod*=fHill[ind2];
                    }else{
                        resprod*=(1-fHill[ind2]);
                    }
                    // mexPrintf("%d ",vecBin[ind2]);
                    
                }
                //mexPrintf("\n");
                //mexPrintf("\nind=%d\n",ind);
                
                w=amigo_model->pars[counter++];
                ///mexPrintf("w=%e\n",w);
                ressum+=resprod*w;
                
            }
            inhibitor=(*amigo_model).controls_v[i][(*amigo_model).index_t_stim]+(t-(*amigo_model).tlast)*(*amigo_model).slope[i][(*amigo_model).index_t_stim];
            tau=amigo_model->pars[counter++];
            Ith(ydot,i)=(ressum-Ith(y,i))*tau*(1-inhibitor);
            //mexPrintf("%Tau=%e\n",tau);
            
            
        }else{
            tau=amigo_model->pars[counter++];
            Ith(ydot,i)=0;
            
        }
        
    }
    

    return(0);
    
}
void amigoRHS_get_sens_OBS_LOGIC(void* data){
    
}

void amigoRHS_get_OBS_LOGIC(void* data){
    
}

void amigo_Y_at_tcon_LOGIC(void* data, realtype t, N_Vector y){
    AMIGO_model* amigo_model=(AMIGO_model*)data;
    
}

