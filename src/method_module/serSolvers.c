//#define initess
//#define initde

#include <benchmark_functions_SystemBiology.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <limits.h>
#include <bbobStructures.h>
#include <structure_paralleltestbed.h>
#include <common_solver_operations.h>
#include <configuration.h>
#include <output.h>
#include <local_solvers.h>
#include <input_module.h>


int serialDE(void *exp1_, void *local_s_, void  *output_, 
        void*(*fitnessfunction)(double*, void *), void *result_, 
        long maxfunevals, double ftarget) {
    int i, j, z, k, NPROC, D, NP, index, stuckcond_lowVar, stuckcount, stop, par, idp;
    double  currenttime, endtime, starttime, totaltime; 
    long evaluation_local;
    double _CR, _F, minvarcondition;
    double *Xl, *Xm, *next, *popul, *ptr, *iptr, *U, *Ptos;
#ifdef initess
    double *populonly;
#endif
    double best, bestOLD;
    //void *request;
    experiment_total *exp1;
    local_solver *local_s;
    output_struct  *output;
    result_solver *result;
 
    starttime = clock();   
    par = 0;
    idp=0;
    exp1 = (experiment_total *) exp1_;
    local_s = (local_solver *) local_s_;
    output = (output_struct *) output_;
    result = (result_solver *) result_;
    
    index = -1;
    chargedimension_((void *) exp1, &D);
    charge_DE_parameters((void *) exp1, D, &NP, &_CR, &_F); 

    bestOLD = DBL_MAX;
    best = DBL_MAX;
    stuckcount = 0;
    stuckcond_lowVar = (int) 30;
    minvarcondition = 1e-10;
    endtime = 0.0;
    index = 0;
    //request = NULL;
    
    initlocalsolvervars_((void *) local_s);
    initoutputvars_((void *) output);
    
    
    Xl = (double *) malloc( D * sizeof (double));
    Xm = (double *) malloc(D * sizeof (double));
    initializebenchmarks_((void *) exp1, Xl, Xm, &D);
    NPROC = 1;
 
    
    stop = 0;
    INITRAND_BBOB;
    evaluation_local = 0;

    popul = (double *) malloc(NP * (D + 1)* sizeof (double));
    if (popul == NULL) perror("malloc");

    next = (double *) malloc((NP)*(D + 1)* sizeof (double));
    if (next == NULL) perror("malloc");
    
    iptr = (double *) malloc((D + 1)* sizeof (double));
    if (iptr == NULL) perror("malloc");

    Ptos = (double *) malloc(NP * (D + 1)* sizeof (double));
    if (Ptos == NULL) perror("malloc");

#ifdef initess
    populonly = (double *) malloc(NP * D * sizeof (double));
    if (populonly == NULL) perror("malloc");
#endif
    
    
    for (i = 0; i < NP; i++) {
        for (j = 0; j < (D + 1); j++) Ptos[i * (D + 1) + j] = DBL_MAX;
    }
    
    if ((*exp1).test.init_point != 1) {
            generatepopulation_((void *) exp1, popul, NP, D, fitnessfunction,Xl, Xm, &evaluation_local);
    } else {
#ifdef  initde       
            init_point_SystemBiology_Problems_matrix_( (void *) exp1, popul, NP, D, NPROC);
#endif

#ifdef initess    
            openhdf5solutions_(exp1,populonly,&D,&NP);
            U = (double *) malloc((D + 1)* sizeof (double));
            for (i=0;i<NP;i++) {
                memmove(U, &(populonly[i*D]), D*sizeof(double));
                if (exp1[0].test._log == 1)  converttolog_(U, &D); 
                callfitnessfunction_(fitnessfunction, (void *) exp1,  U,  &D, Xl, Xm);
                evaluation_local++;
                memmove(&(popul[i*(D+1)]),U,(D+1)*sizeof(double));
            }
            
            free(U);
            U=NULL;
#endif
    }
    
    U = (double *) malloc((D + 1)* sizeof (double));
    if (U == NULL) perror("malloc");    
    
#ifdef initess     
    free(populonly);
    populonly = NULL;
#endif
    index = extract_best_index(popul, NP, D);
    best = popul[index*(D+1) + D];
    
    currenttime = calctime(exp1,starttime);
    initprintfile_((void *) exp1, (void*) output, &best, &par, &idp, &currenttime, &evaluation_local);       
    
    for (k = 0;; k++) {

        for (i = 0; i < NP; i++) {
            U = mutation_operation((void *) exp1, popul, U, D, NP, i, index, 0, _CR, _F);
            callfitnessfunction_(fitnessfunction, (void *) exp1,  U,  &D, Xl, Xm);
            evaluation_local++;

            if (U[D] <= popul[i * (D + 1) + D]) {
                for (z = 0; z < (D + 1); z++) {
                    iptr[z] = U[z];
                    U[z] = next[i * (D + 1) + z];
                    next[i * (D + 1) + z] = iptr[z];
                }
            } else {
                for (j = 0; j <= D; j++)
                    next[i * (D + 1) + j] = popul[i * (D + 1) + j];
            }
        }

        ptr = popul;
        popul = next;
        next = ptr;

        index = extract_best_index(popul, NP, D);
        best = popul[index * (D + 1) + D];

        if (best < bestOLD) {
            bestOLD = best;
            stuckcount = 0;
        } else {
            stuckcount++;
        }
        
        localsolverinterface_((void *) exp1, (void *) local_s, (void *) output, fitnessfunction, popul, &NP, Ptos, &stop, &index, &best, &idp, &NPROC, &evaluation_local);

        verboseiteration_( (void *) exp1, k,  starttime,  ftarget,  best,  evaluation_local, idp);

        

        if ((best <= ftarget)
                ||
                (evaluation_local >= maxfunevals)
                ||
                (stuckcount > stuckcond_lowVar && (sumvar(popul, NP, D) / D) < minvarcondition)) 
                {
            stop = 1;
        };
        
        currenttime = calctime(exp1,starttime);
        printiteration_((void *) exp1,(void *) output, (void *) local_s, &k, &best, &evaluation_local, &currenttime);  
        addlocalscounter_((void *) local_s);
    
        if (stop == 1) {    
            break;
        }

    }
    
    index = extract_best_index(popul, NP, D);
    best = popul[index * (D + 1) + D];
    
    currenttime = calctime(exp1,starttime);
    printenditeration_((void *) exp1, (void *) output, &best, &currenttime, &evaluation_local);   
            
    endtime = clock();
    totaltime = ((double) (endtime - starttime) / (double) CLOCKS_PER_SEC);

    index = extract_best_index(popul, NP, D);
    

    updateresultsandprint_((void *) exp1, (void *) result, (void *) output, (void *) local_s, &totaltime, &evaluation_local, &D, &popul[index * (D + 1)], &best, &idp, &NPROC);
    

    if (Ptos != NULL) {
        free(Ptos);
        Ptos = NULL;
    }
    if (next != NULL) {
        free(next);
        next=NULL;
    }
    if (popul != NULL) {
        free(popul);
        popul=NULL;
    }
    if (iptr != NULL) {
        free(iptr);
        iptr=NULL;
    }
    if (U != NULL) {
        free(U);
        U=NULL;
    }
    if (Xl != NULL) {
        free(Xl);
        Xl=NULL;
    }
    if (Xm != NULL) {
        free(Xm);
        Xm=NULL;
    }
    


    return 1;
}




