#include <structure_paralleltestbed.h>
#include <serSolvers.h>
#include <configuration.h>
#include <benchmark_functions_BBOB.h>
#include <benchmark_functions_SystemBiology.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <limits.h>
#include <string.h>
#include <time.h>
#include <float.h>
#include <def_errors.h>
#include <sys/time.h>
#include <errno.h>
#include <output.h>
#include <parallelsolverfortran.h>
#include <sersolverfortran.h>

#ifdef OPENMP
#include <omp.h>
#endif

#ifdef MPI2
#include <parallel_islands_Solvers.h>
#endif

void destroyexp(experiment_total *exp) {

    if (exp->test.bench.BestSol != NULL) {
        free(exp->test.bench.BestSol);
        exp->test.bench.BestSol = NULL;
    }
    if (exp->test.bench.max_dom != NULL) {
        free(exp->test.bench.max_dom);
        exp->test.bench.max_dom = NULL;
    }
    if (exp->test.bench.min_dom != NULL) {
        free(exp->test.bench.min_dom);
        exp->test.bench.min_dom = NULL;
    }
    
    if (exp->methodDE != NULL) {
        free(exp->methodDE);
        exp->methodDE = NULL;
    }

    if (exp->methodRandomSearch != NULL) {
        free(exp->methodRandomSearch);
        exp->methodRandomSearch = NULL;
    }

    if (exp->methodScatterSearch != NULL) {
        free(exp->methodScatterSearch);
        exp->methodScatterSearch = NULL;
    }

    if (exp->par_st != NULL) {
        free(exp->par_st);
        exp->par_st = NULL;
    }
    if (exp->param != NULL) {
        free(exp->param);
        exp->param = NULL;
    }

    if (exp->amigo != NULL) {
        free_AMIGO_problem(exp->amigo);
        exp->amigo = NULL;
    }


}

int is_asynchronous(experiment_total exp) {
    if (strcmp(exp.par_st->commu, "asynchronous") == 0) return 1;
    else return 0;
}

int is_parallel(experiment_total exp) {

    if (exp.par_st != NULL) return 1;
    else return 0;
}

int is_noise(experiment_total exp) {
    if (strcmp(exp.test.bench.type, "noiselessBBOB") == 0) return 0;
    else return 1;
}

void init_printf(int NPROC, int NN, const char *namealg, experiment_total exp) {

    printf("Start BBOB WITH :\n");
    printf("        - PROCESORS NUMBER: %d\n", NPROC);
    if (is_noise(exp) == 0)
        printf("        - WITHOUT NOISY FUNCTIONS \n");
    else
        printf("        - WITH NOISY FUNCTIONS \n");

    if (is_parallel(exp)) {
        if (is_asynchronous(exp) == 1) printf("        - ASYNCHRONOUS \n");
        else printf("        - SYNCHRONOUS \n");
    }
    printf("        - ALGORITHM --> %s\n", namealg);
    printf("        - REPETITIONS: %d eache instance (%d)\n", NN, ISIZE_BBOB);

}

int number_benchmark(experiment_total *exp) {
    char const *noiseBBOB;
    char const *noiselessBBOB;
    char const *system;
    char const *test;

    noiseBBOB = "noiseBBOB";
    noiselessBBOB = "noiselessBBOB";
    system = "systemBiology";
    test = "test";

    if (strcmp((*exp).test.bench.type, noiseBBOB) == 0 || strcmp((*exp).test.bench.type, noiselessBBOB) == 0) {
        return 0;
    } else if (strcmp((*exp).test.bench.type, system) == 0) {
        return 1;
    } else if (strcmp((*exp).test.bench.type, test) == 0) {
        return 2;
    } else {
        return -1;
    }

}

void check_fun(experiment_total exp, int current_bench, int noise_funcion) {
    char const *noiseBBOB;
    char const *noiselessBBOB;
    char const *system;
    char const *test;

    noiseBBOB = "noiseBBOB";
    noiselessBBOB = "noiselessBBOB";
    system = "systemBiology";
    test = "test";


    if (strcmp(exp.test.bench.type, noiseBBOB) == 0 || strcmp(exp.test.bench.type, noiselessBBOB) == 0) {
        if (noise_funcion) {
            if (current_bench > 130) perror("ERROR FUNCTION");
            if (current_bench < 101) perror("ERROR FUNCTION");
        } else {
            if (current_bench > 1) perror("ERROR FUNCTION");
            if (current_bench < 24) perror("ERROR FUNCTION");
        }
    } else if (strcmp(exp.test.bench.type, system) == 0) {
        if (current_bench > 3) perror("ERROR FUNCTION");
        if (current_bench < 0) perror("ERROR FUNCTION");
    } else if (strcmp(exp.test.bench.type, test) == 0) {

    }
}

int execute_parallel_solver(experiment_total *exp, output_struct *output, local_solver *local_s, result_solver *result, long maxfunevals, int id, double target) {
    int error, noise_funcion, synchronous, i;

    error = 0;
    char const *noiseBBOB;
    char const *noiselessBBOB;
    char const *system;
    char const *test;
    noiseBBOB = "noiseBBOB";
    noiselessBBOB = "noiselessBBOB";
    system = "systemBiology";
    test = "test";
    noise_funcion = is_noise(*exp);
    if (is_asynchronous(*exp)) synchronous = 0;
    else synchronous = 1;
    experiment_total *exparray;
    int benchmark;

    benchmark = number_benchmark(exp);

    if (benchmark == 1) {
#if OPENMP
#pragma omp parallel
        {
            exp[0].par_st->NPROC_OPENMP = omp_get_max_threads();
        }
        exparray = (experiment_total *) malloc(exp[0].par_st->NPROC_OPENMP * sizeof (experiment_total));
        copylocalexperimentsb_(exp, exparray, exp[0].par_st->NPROC_OPENMP);
        
#endif
    } else {
        exparray = exp;
    }

    if ((*exp).methodDE != NULL) {
#ifdef MPI2
        if (strcmp((*exp).test.bench.type, noiseBBOB) == 0 || strcmp((*exp).test.bench.type, noiselessBBOB) == 0) {
            if (noise_funcion == 1) {
                if (synchronous == 1)
                    error = parallel_synchronous_DE((void *) exparray, (void *) local_s, (void *) output, &fgeneric_noise, (void *) result, maxfunevals,
                        target, id);
                else
                    error = parallel_asynchronous_DE((void *) exparray, (void *) local_s, (void *) output, &fgeneric_noise, (void *) result, maxfunevals,
                        target, id);
            } else {
                if (synchronous == 1)
                    error = parallel_synchronous_DE((void *) exparray, (void *) local_s, (void *) output, &fgeneric_noiseless, (void *) result, maxfunevals,
                        target, id);
                else
                    error = parallel_asynchronous_DE((void *) exparray, (void *) local_s, (void *) output, &fgeneric_noiseless, (void *) result, maxfunevals,
                        target, id);
            }
        } else if (strcmp((*exp).test.bench.type, system) == 0) {
            if (synchronous == 1) {
                error = parallel_synchronous_DE((void *) exparray, (void *) local_s, (void *) output, &evalSB_, (void *) result, maxfunevals,
                        target, id);
            } else {
                error = parallel_asynchronous_DE((void *) exparray, (void *) local_s, (void *) output, &evalSB_, (void *) result, maxfunevals,
                        target, id);
            }
        }
#endif
    } else if ((*exp).methodScatterSearch != NULL) {
        if (strcmp((*exp).test.bench.type, noiseBBOB) == 0 || strcmp((*exp).test.bench.type, noiselessBBOB) == 0) {

        } else if (strcmp((*exp).test.bench.type, system) == 0) {
#ifdef OPENMP
    //        error = __parscattersearch_MOD_parsscattersearch((void *) exparray, (void *) local_s, (void *) output, &evalSB_, (void *) result, &maxfunevals, &target);
#endif
        } else if (strcmp((*exp).test.bench.type, test) == 0) {
#ifdef OPENMP            
    //        error = __parscattersearch_MOD_parsscattersearch((void *) exparray, (void *) local_s, (void *) output, &functiontest2, (void *) result, &maxfunevals, &target);
#endif
        }
    }

    if (benchmark == 1) {
#ifdef OPENMP 
        for (i = 0; i < exp[0].par_st->NPROC_OPENMP; i++) {
            destroyexp(&exparray[i]);
        }


        
        free(exparray);
        exparray = NULL;
        


#endif

    }
    return error;
}

int execute_serial_solver(experiment_total *exp, output_struct *output, local_solver *local_s, result_solver *result, long maxfunevals, double target) {
    int error, noise_funcion;
    char const *noiseBBOB;
    char const *noiselessBBOB;
    char const *system;
    char const *test;

    error = 0;
    noiseBBOB = "noiseBBOB";
    noiselessBBOB = "noiselessBBOB";
    system = "systemBiology";
    test = "test";

    noise_funcion = is_noise(*exp);
    if ((*exp).methodDE != NULL) {
        if (strcmp((*exp).test.bench.type, noiseBBOB) == 0 || strcmp((*exp).test.bench.type, noiselessBBOB) == 0) {
            if (noise_funcion == 1)
                error = serialDE((void *) exp, (void *) local_s, (void *) output, &fgeneric_noise, (void *) result, maxfunevals, target);
            else
                error = serialDE((void *) exp, (void *) local_s, (void *) output, &fgeneric_noiseless, (void *) result, maxfunevals, target);
        } else if (strcmp((*exp).test.bench.type, system) == 0) {
            error = serialDE((void *) exp, (void *) local_s, (void *) output, &evalSB_, (void *) result, maxfunevals, target);
        }
    } else if ((*exp).methodRandomSearch != NULL) {
        if (strcmp((*exp).test.bench.type, noiseBBOB) == 0 || strcmp((*exp).test.bench.type, noiselessBBOB) == 0) {

        } else if (strcmp((*exp).test.bench.type, system) == 0) {

            //error = __serialrandomsearch_MOD_srandomsearch((void *) exp, (void *) local_s, (void *) output, &evalSB_, (void *) result, &maxfunevals, &target);
        }

    } else if ((*exp).methodScatterSearch != NULL) {
        if (strcmp((*exp).test.bench.type, noiseBBOB) == 0 || strcmp((*exp).test.bench.type, noiselessBBOB) == 0) {

        } else if (strcmp((*exp).test.bench.type, system) == 0) {
       //     error = __scattersearch_MOD_sscattersearch((void *) exp, (void *) local_s, (void *) output, &evalSB_, (void *) result, &maxfunevals, &target);
        } else if (strcmp((*exp).test.bench.type, test) == 0) {
       //     error = __scattersearch_MOD_sscattersearch((void *) exp, (void *) local_s, (void *) output, &functiontest2, (void *) result, &maxfunevals, &target);
        }
    }
    return error;
}



void initialize_benchmark(experiment_total *exp, const char *namealg, int id, int benchmark, double *target, double *ftarget) {
    if (benchmark == 0) {
        initializeBBOB(exp, namealg, id);
        updateFunctionsBBOB(exp, exp->test.bench.current_bench, 1);
        *target = fgeneric_ftarget_tol((*exp).test.tolerance);
        *ftarget = fgeneric_ftarget_tol(0.0);
        printf("target %lf - ftarget - %lf\n", *target, *ftarget);
    }
    if (benchmark == 1) {
        load_benchmark_SystemBiology(exp);
        *target = (*exp).test.tolerance;
        *ftarget = (*exp).test.tolerance;
    }
    if (benchmark == 2) {
        load_benchmark_test(exp, target, ftarget);
    }
}

const char* getname(experiment_total *exp) {
    const char *retchar;
    if (exp->methodDE != NULL) retchar = "Differential Evolution";
    else if (exp->methodRandomSearch != NULL) retchar = "RandomSearch";
    else if (exp->methodScatterSearch != NULL) retchar = "ScatterSearch";


    return retchar;

}

int execute_Solver(experiment_total *exp, int id) {
    result_solver result;
    local_solver *local_s;
    output_struct *output;

    int contaruns;
    int NN, NPROC, error;
    double valueAVG, BestValueLIST, sdValue, timeAVG, sdTime, target, ftarget, *valuesarray, *timearray;
    const char *namealg;
    long maxfunevals;
    double numberEvalAVG;
    double restartsAVG, porcenthits;
    int isparallel, noise_funcion, break_int, break_int2, hit, j, independent_restarts;
    int contador, contadorX, benchmark, dim;


    NN = ((*exp).test.repetitions);
    benchmark = number_benchmark(exp);
    dim = (*exp).test.bench.dim;
    printf("DIMENSION - %d\n", dim);
    result.totaltime = 0.0;
    result.paralleltime = 0.0;
    result.localsolvertime = 0.0;

    if (is_parallel(*exp)) isparallel = 1;
    else isparallel = 0;
    if (is_noise(*exp) == 0) noise_funcion = 0;
    else noise_funcion = 1;

    if (is_parallel(*exp)) {
        NPROC = (*exp).par_st->NPROC;
    } else NPROC = 1;

    namealg = getname(exp);
    independent_restarts = 0;

    check_fun(*exp, (*exp).test.bench.current_bench, noise_funcion);

    if (id == 0) init_printf(NPROC, NN, namealg, *exp);

    if (isparallel == 0 && NPROC > 1) {
        perror(error7);
        exit(7);
    }

    initialize_benchmark(exp, namealg, id, benchmark, &target, &ftarget);


    contadorX = 0;
    contaruns = 0;

    BestValueLIST = DBL_MAX;
    timeAVG = 0.0;
    valueAVG = 0.0;
    numberEvalAVG = 0.0;
    restartsAVG = 0.0;
    hit = 0;
    valuesarray = (double *) malloc(NN * sizeof (double));
    timearray = (double *) malloc(NN * sizeof (double));
    contadorX = 0;

    for (j = 0; j < NN; j++) {

        // INIT PATH
        if (j == 0) (*exp).execution.initpath = 1;
        else (*exp).execution.initpath = 0;

        // EXECUTION ID
        (*exp).execution.rep = j + NPROC + NN + 10;

        maxfunevals = (long) (*exp).test.max_eval;
        independent_restarts = -1;

        break_int = 0;
        result.eval_value = 0;
        result.time_value = 0.0;
        result.best_value = DBL_MAX;



        while ((result.eval_value <= maxfunevals)) {
            local_s = (local_solver *) malloc(sizeof (local_solver));
            output = (output_struct *) malloc(sizeof (output_struct));
#ifdef MPI2
            MPI_Barrier(MPI_COMM_WORLD);
#endif

            if (id == 0) {
                if (++independent_restarts > 0) {
                    restartsAVG++;
                    printf("restart\n");
                }
            }
            printf("CHEGANDO\n");
            if (isparallel == 1) {
                error = execute_parallel_solver(exp, output, local_s, &result, (maxfunevals - result.eval_value), id, target);
            } else {
                error = execute_serial_solver(exp, output, local_s, &result, (maxfunevals - result.eval_value), target);
            }


            if (error == 0) {
                perror(error8);
                exit(8);
            }

            if (id == 0) {
                printf("result.best_value  %.20lf < target %.20lf  -- evals (%ld) --- time (%lf) --- VTR -> %lf\n",
                        result.best_value,
                        target,
                        result.eval_value,
                        result.time_value,
                        ftarget);

                if (result.best_value <= target) {
                    break_int = 1;
                } else {
                    break_int = 0;
                }

                if (exp[0].test.maxtime != -1) {
                    if (result.time_value >= exp[0].test.maxtime) {
                        printf("TIME OVER - %lf\n", exp[0].test.maxtime);
                        break_int2 = 1;
                    } else {
                        break_int2 = 0;
                    }
                }

            }

#ifdef MPI2
            MPI_Bcast(&break_int, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
            MPI_Bcast(&break_int2, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
#endif 
            if ((break_int == 1) || (break_int2 == 1)) {
                break;
            }

            if (local_s != NULL) {
                free(local_s);
                local_s = NULL;
            }

            if (output != NULL) {
                free(output);
                output = NULL;
            }

        }






        if (id == 0) {
            if (break_int == 1) {
                hit++;
            }
            if (result.best_value  <= BestValueLIST) {
                BestValueLIST = result.best_value ;
            }
            timeAVG = timeAVG + (double) result.time_value;

            valueAVG = valueAVG + result.best_value;
            numberEvalAVG = numberEvalAVG + (double) result.eval_value;
            valuesarray[contadorX] = fabsl(result.best_value);
            timearray[contadorX] = (double) result.time_value;
            contadorX++;
            contaruns++;
        }

//        if (benchmark == 0)
//            destroyBBOB(exp);
//        if (benchmark == 1)
//            destroySystemBiology(exp);

    }





    if (benchmark == 0)
            destroyBBOB(exp);
    if (benchmark == 1)
            destroySystemBiology(exp);

    if (id == 0) {
        timeAVG = (double) timeAVG / (double) contaruns;
        valueAVG = (double) valueAVG / (double) contaruns;
        numberEvalAVG = numberEvalAVG / (double) contaruns;
        porcenthits = ((double) hit / (double) contaruns)*100.0;

        restartsAVG = restartsAVG / (double) contaruns;
        sdValue = calcSD(valueAVG, valuesarray, NN);
        sdTime = calcSD(timeAVG, timearray, NN);
        printf("%lf - %lf\n", sdValue, sdTime);
        contador++;
        printf("\nTABLE - MAXIMUM EVALS %.2lf  - STOP IN => f(x) <= OPTIMAL+%.2lf\n", maxfunevals, ftarget);
        printf("-----------------------------------------------------------------------------------------------------------------------------------------------------------------\n");
        printf("Name\tDIM\tHITS\t\tbest(fbest-ftarget)\tavg(fbest-ftarget)");
        printf("\t#evals\ttime\t\trestarts\tsd(best-ftarget)\tsd(time)\n");
        printf("f-%d\t%d\t%lf%\t\t%.20lf\t%.20lf\t%lf\t%.4lf\t\t%lf\t%lf\t%lf\n",
                exp[0].test.bench.current_bench,
                dim,
                (double) porcenthits,
                BestValueLIST,
                valueAVG,
                numberEvalAVG,
                timeAVG,
                restartsAVG,
                sdValue,
                sdTime);
        printf("\n\nTOTAL EXECUTION TIME ====> %lf\n", result.totaltime);
        printf("\t \tIN PARALLEL REGION EXECUTION TIME ====> %lf\n", result.paralleltime);
        printf("\t \tIN LOCAL SOLVER TIME ====> %lf\n", result.localsolvertime);
        printf("RUNS COMPLETED======> %d \n", contaruns);

    }

    if (valuesarray != NULL) {
        free(valuesarray);
        valuesarray = NULL;
    }

    if (timearray != NULL) {
        free(timearray);
        timearray = NULL;
    }

#ifdef MPI2 
    MPI_Barrier(MPI_COMM_WORLD);
#endif            


    //}



    return 1;
}







// plantilla para executar problemas de system biology
// meter options polo XML
// 1- CIRCADIAN
// 2- MENDES
// 3- NFKB

int execute_methodSB(experiment_total *exp, int id) {
    result_solver result;
    local_solver *local_s;
    output_struct *output;

    int contaruns;
    int NN, SIZERESULT, NPROC, error;
    double valueAVG, BestValueLIST, sdValue, timeAVG, sdTime, target, ftarget, *valuesarray, *timearray;
    long maxfunevals;
    double numberEvalAVG;
    double porcenthits;
    int isparallel, hit, j;
    int ifun, contador, contadorX;


    NN = ((*exp).test.repetitions);
    result.totaltime = 0.0;
    result.paralleltime = 0.0;
    result.localsolvertime = 0.0;

    if (is_parallel(*exp)) isparallel = 1;
    else isparallel = 0;

    if (is_parallel(*exp)) {
        NPROC = (*exp).par_st->NPROC;
    } else NPROC = 1;

    if (isparallel == 0 && NPROC > 1) {
        perror(error7);
        exit(7);
    }


    contaruns = 0;
    BestValueLIST = DBL_MAX;
    timeAVG = 0.0;
    valueAVG = 0.0;
    numberEvalAVG = 0.0;
    hit = 0;
    valuesarray = (double *) malloc(NN * sizeof (double));
    timearray = (double *) malloc(NN * sizeof (double));
    contadorX = 0;

    for (j = 0; j < NN; j++) {

        if (j == 0) (*exp).execution.initpath = 1;
        else (*exp).execution.initpath = 0;

        (*exp).execution.rep = j + NPROC + NN + 10;

#ifdef MPI2
        MPI_Barrier(MPI_COMM_WORLD);
#endif        
        load_benchmark_SystemBiology(exp);
        target = (*exp).test.tolerance;
        ftarget = (*exp).test.tolerance;

        maxfunevals = (long) (*exp).test.max_eval;

        result.eval_value = 0;
        result.time_value = 0.0;
        result.best_value = DBL_MAX;
        local_s = (local_solver *) malloc(sizeof (local_solver));
        output = (output_struct *) malloc(sizeof (output_struct));


#ifdef MPI2
        MPI_Barrier(MPI_COMM_WORLD);
#endif

        if (isparallel == 1) {
            error = execute_parallel_solver(exp, output, local_s, &result, maxfunevals, id, target);
        } else {
            error = execute_serial_solver(exp, output, local_s, &result, maxfunevals, target);
        }


        if (error == 0) {
            perror(error8);
            exit(8);
        }

        if (id == 0)
            printf("result.best_value  %.20lf < target %.20lf  -- evals (%ld) --- time (%lf) --- VTR -> %lf\n", result.best_value, target, result.eval_value, result.time_value, exp[0].test.tolerance);

        if (local_s != NULL) {
            free(local_s);
            local_s = NULL;
        }

        if (output != NULL) {
            free(output);
            output = NULL;
        }



        if (id == 0) {
            hit++;
            if (fabsl(result.best_value - ftarget) <= BestValueLIST) {
                BestValueLIST = fabsl(result.best_value - ftarget);
            }
            timeAVG = timeAVG + (double) result.time_value;
            valueAVG = valueAVG + fabsl(result.best_value - ftarget);
            numberEvalAVG = numberEvalAVG + (double) result.eval_value;
            valuesarray[contadorX] = fabsl(result.best_value - ftarget);
            timearray[contadorX] = (double) result.time_value;
            contadorX++;
            contaruns++;
        }

        destroySystemBiology(exp);

    }



#ifdef MPI2
    MPI_Barrier(MPI_COMM_WORLD);
#endif
    if (id == 0) {
        timeAVG = (double) timeAVG / (double) contaruns;
        valueAVG = (double) valueAVG / (double) contaruns;
        numberEvalAVG = numberEvalAVG / (double) contaruns;
        porcenthits = ((double) hit / (double) contaruns)*100.0;

        sdValue = calcSD(valueAVG, valuesarray, NN);
        sdTime = calcSD(timeAVG, timearray, NN);
        contador++;
        printf("\n\nTABLE - MAXIMUM EVALS %.2lf  - STOP IN => f(x) <= OPTIMAL+%.2lf\n", (*exp).test.max_eval, (*exp).test.tolerance);
        printf("-----------------------------------------------------------------------------------------------------------------------------------------------------------------\n");
        printf("Name\tDIM\tHITS\t\tbest(fbest-ftarget)\tavg(fbest-ftarget)");
        printf("\t#evals\ttime\tsd(best-ftarget)\tsd(time)\n");
        printf("f-%d\t%d\t%.1f%\t\t%.20lf\t%.20lf\t%.lf\t%.4lf\t%.20lf\t%.20lf\n",
                ifun,
                (*exp).test.bench.dim,
                (double) porcenthits,
                BestValueLIST,
                valueAVG,
                numberEvalAVG,
                timeAVG,
                sdValue,
                sdTime);
        printf("\n\nTOTAL EXECUTION TIME ====> %lf\n", result.totaltime);
        printf("\t \tIN PARALLEL REGION EXECUTION TIME ====> %lf\n", result.paralleltime);
        printf("\t \tIN LOCAL SOLVER TIME ====> %lf\n", result.localsolvertime);
        printf("RUNS COMPLETED======> %d \n", contaruns);

    }

#ifdef MPI2
    MPI_Barrier(MPI_COMM_WORLD);
#endif

    if (valuesarray != NULL) {
        free(valuesarray);
        valuesarray = NULL;
    }

    if (timearray != NULL) {
        free(timearray);
        timearray = NULL;
    }

#ifdef MPI2 
    MPI_Barrier(MPI_COMM_WORLD);
#endif            






    return 1;
}








