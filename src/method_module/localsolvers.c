

#include <stdio.h>
#include <stdlib.h>
#include <structure_paralleltestbed.h>
#include <limits.h>
#include <AMIGO_problem.h>
#include <common_solver_operations.h>
#include <output.h>
#include <benchmark_functions_SystemBiology.h>
#include <string.h>

    

void localsolverscattersearchinterface_(void * exp1_, void *(*fitnessfunction)(double*, void*),
         int *NPROC, int *stop, double *xbest, double *fval, int *neval) {

    experiment_total *exp1;
    output_function * outf;
    double *populVector, *U;
    int i, D;
    void *request_recep;

    exp1 = (experiment_total *) exp1_;
    outf = (output_function *) malloc(sizeof (output_function) );

    D = (*exp1).test.bench.dim;
    populVector = (double *) malloc((D+1)* sizeof (double));
    U = (double *) malloc((D+1) * sizeof (double));

    for (i = 0; i < D; i++) {
        populVector[i] = xbest[i];
        U[i] = xbest[i];
    }

    #ifdef MPI2 
            request_recep = (void *) exp1[0].execution.request_recept;
    #else
            request_recep = NULL;
    #endif

    DE_evaluation_NL2SOL(U, exp1, populVector, stop, request_recep, NPROC, &D, neval);
    
    outf = (output_function *) fitnessfunction(populVector, exp1);
    *fval = outf->value;
    
    *neval = *neval + 1;
    
    for (i = 0; i < D; i++) {
        xbest[i] = populVector[i];
    }
    
    
    free(populVector);
    populVector = NULL;
    
    
    
    free(U);
    U=NULL;
    
    free(outf);
    outf = NULL;
    

}


void call_to_local_solver_N2SOL(experiment_total *exp1, local_solver *local_s, output_struct *output,
        double *popul, double *Ptos, void*(*fitnessfunction)(double*, void*),
        int NP, int D, int index_best, int par, int *stop, void *array_stop, int *NPROC, int *idp, int *neval) {

    output_function *outf;
    double *populVector, *U,  *U2;
    double best;
    int i, j;
    
    best = popul[index_best * (D + 1) + D];

    if ((*local_s).counter == (*exp1).methodDE->ls_counter) {
        
        outf = (output_function *) malloc(sizeof (output_function) );
        populVector = (double *) malloc( (D + 1) * sizeof (double));
        U = (double *) malloc((D + 1) * sizeof (double));
        U2 = (double *) malloc((D + 1) * sizeof (double));
        (*local_s).counter = 0;
        reorder_best(popul, NP, D);


        if ((*exp1).methodDE->cache == 1) {
            index_best = -1;
            for (i = 0; i < NP; i++) {
                for (j = 0; j < NP; j++) {
                    if (Ptos [j * (D + 1) + D] != DBL_MAX) {
                        if (calc_euclidean_distance(&popul[i * (D + 1)], &Ptos [j * (D + 1)], D,
                                (*exp1).test.bench.max_dom, (*exp1).test.bench.min_dom) <= (*exp1).methodDE->ls_threshold) {
                            break;
                        }
                    }
                }
                if (j == NP) {
                    index_best = i;
                    break;
                }
            }
            if (index_best == -1) {
                index_best = 0;
            }
        } else {
            index_best = extract_best_index(popul, NP, D);
        }


        memmove(U, &popul[index_best * (D + 1)], (D + 1) * sizeof (double));
        memmove(U2, &popul[index_best * (D + 1)], (D + 1) * sizeof (double));

        if ((*exp1).test._log == 1) converttonormal_(U, &D);

        outf = (output_function *) fitnessfunction(U, exp1);
        U[D] = outf->value;
        U2[D] = outf->value;

        insert_matrix_tabu_list(U2, Ptos, D, NP, (*local_s).total_local);


        printverboselocaloutput_(exp1, &D, U, &U[D], output, idp);

        
//	printf("PROBLEMA %d- index_best %d --> ", *idp,index_best);
//	for (i=0;i<NP;i++) printf(" %lf ", popul[i*(D+1)+D]);
//	printf("\n");		

        DE_evaluation_NL2SOL(U, exp1, populVector, stop, array_stop, NPROC, &D, neval);
        if (*stop < 1) {
            outf = (output_function *) fitnessfunction(populVector, exp1);
            populVector[D] = outf->value;
        } else populVector[D] = U[D];


        if ((*exp1).test._log == 1) converttolog_(populVector, &D);
        
        printverboselocaloutput2_(exp1, output, &populVector[D], idp);

        if (populVector[D] < best) {
            (*local_s).state_ls = 1;
            (*local_s).total_local = (*local_s).total_local + 1;
            (*local_s).sucess_local = (*local_s).sucess_local + 1;
            (*local_s).num_ls++;
            (*local_s).sucess_interval++;
            if (par == 1) print_verbose_local_success(*exp1, 1);
            else print_verbose_local_success(*exp1, 1);
            replaceWorst(popul, NP, D, populVector, 1);
            index_best = extract_best_index(popul, NP, D);

        } else {
            (*local_s).state_ls = 2;
            (*local_s).num_ls++;
            (*local_s).total_local++;
            if (par == 1) print_verbose_local_success(*exp1, 0);
            else print_verbose_local_success(*exp1, 0);
        }


        if (populVector != NULL) {
            free(populVector);
            populVector = NULL;
        }
        
        if (U != NULL) {
            free(U);
            U = NULL;
        }
        
        if (U2 != NULL) {
            free(U2);
            U2 = NULL;
        }
        
        if (outf != NULL) {
            free(outf);
            outf = NULL;
        }
    }

    
}

void localsolverinterface_(void * exp1_, void* localsolver, void *output_, void *(*fitnessfunction)(double*, void*),
        double *popul, int *NP, double *Ptos, int *stop, int *index, double *best, int *idp, int *NPROC, int *eval) {
    int D, par;
    void *request_recep;


    experiment_total *exp1;
    local_solver *local_s;
    output_struct * output;

    exp1 = (experiment_total *) exp1_;
    local_s = (local_solver *) localsolver;
    output = (output_struct *) output_;
    if (*NPROC > 1) par = 1;
    else par = 0;
    D = (*exp1).test.bench.dim;



    if (exp1[0].test.local_search == 1 && *stop < 1) {
        #ifdef MPI2 
            request_recep = (void *) exp1[0].execution.request_recept;
        #else
            request_recep = NULL;
        #endif
        call_to_local_solver_N2SOL(exp1, local_s, output, popul, Ptos, fitnessfunction, *NP, D, *index, par, stop, request_recep, NPROC, idp, eval);

        *index = extract_best_index(popul, *NP, D);
        *best = popul[*index * (D + 1) + D];

    }

}


void initlocalsolvervars_(void* localsolver){
    local_solver *local_s;
    local_s = (local_solver *) localsolver;
    local_s->counter = 0;
    local_s->total_local = 0.0;
    local_s->sucess_local = 0.0;
    local_s->state_ls = 0;
    local_s->sucess_interval = 0;
    local_s->num_ls = 0;
}




void addlocalscounter_(void* local_s_){
    local_solver *local_s;
    
    local_s = (local_solver *) local_s_;
    local_s->counter++;
}



void setentervalue_( void *local_s_, double *fval){
    local_solver *local_s;
    
    local_s = (local_solver *) local_s_;    
    
    (*local_s).enter_value = *fval;
}

