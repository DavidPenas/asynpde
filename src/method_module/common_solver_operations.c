#include <structure_paralleltestbed.h>
#include <configuration.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <float.h>
#include <def_errors.h>
#include <configuration.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <time.h>  

double calcSD(double mean, double *population, int sizePOPUL) {
    int i;
    double total;

    total = 0.0;
//    printf("mean %lf\n", mean);
    for (i = 0; i < sizePOPUL; i++) {
//        printf("popul[%d]= %lf\n", i, population[i] );
        total = total + pow(fabs(population[i] - mean), 2);
    }
    total = total / (((double) sizePOPUL) - 1.0);
    total = sqrt(total);
//    printf("TOTAL %lf\n", total);
    return total;
}

double calctime(void *exp, double starttime){
    double current;
    experiment_total *exp1;

    current = 0.0;
    exp1 = (experiment_total *) exp;
    
    if (exp1->test.ouput == 1) {    
        current = (double) clock();
    }
    
    return ((double) (current - starttime) / (double) CLOCKS_PER_SEC);
}

double returnseed_(void *exp1_) {
    experiment_total *exp1;
    exp1 = (experiment_total *) exp1_;  
    
    return exp1[0].seed;
}

void initrngrandomserial_(void *exp1_) {
    experiment_total *exp1;
    exp1 = (experiment_total *) exp1_;
    
    exp1[0].seed  = time(NULL);
    exp1[0].random = gsl_rng_alloc (gsl_rng_mt19937);
    gsl_rng_set (exp1[0].random , exp1[0].seed);
}

double getrngrandomserial_(void *exp1_) {
    experiment_total *exp1;
    double res;
    
    exp1 = (experiment_total *) exp1_;
    
    res = gsl_rng_uniform(exp1[0].random);
    return res;
}

int allocatematrixdouble_(double *vector, int *fil, int *col) {
    
    vector = (double *)malloc((*fil)*(*col)*sizeof(double));
    
    return 1;
}

int chargedimension_(void *exp1_, int *D) {
    experiment_total *exp1;
    exp1 = (experiment_total *) exp1_;
            
    *D = exp1[0].test.bench.dim;
    
    return 1;
}

int chargedimensionopenmp_(void *exp1_, int *D) {
    experiment_total *exp1;
    exp1 = (experiment_total *) exp1_;
    
    *D = exp1[0].test.bench.dim;
    
    return 1;
}

void charge_DE_parameters(void *exp1_, int D, int *NP, double *_CR, double *_F) {
    experiment_total *exp1;
    exp1 = (experiment_total *) exp1_;
    
    *NP =  exp1[0].methodDE->_NP;
    printf("NP = %d\n", *NP);
    *_CR =  exp1[0].methodDE->_CR;
    *_F  =  exp1[0].methodDE->_F;    
}


void DE_correction_bounds(double *X, int D, double* Xl, double* Xu) {
    register int i;

    for (i = 0; i < D; i++) {
    	if (Xl[i] == Xu[i]) {
            X[i] = Xl[i];
        } else {
            while (X[i] < Xl[i] || X[i] > Xu[i]) {
                if (X[i] < Xl[i]) X[i] = 2.0 * Xl[i] - X[i];
                if (X[i] > Xu[i]) X[i] = 2.0 * Xu[i] - X[i];
            }
        }
    }
 
}


void DE_correction_bounds2(double *X, int D, double* Xl, double* Xu) {
    register int i;

    for (i = 0; i < D; i++) {
    	if (Xl[i] == Xu[i]) {
            X[i] = Xl[i];
        } else {
            if (X[i] < Xl[i]) {
                X[i] = Xl[i];
            }
            else if (X[i] > Xu[i]) {
                X[i] = Xu[i];
            }
        }
    }
 
}

void converttonormal_(double *vector, int *D) {
    int i;
    for (i = 0; i < *D; i++) {
        if (vector[i] == EPSILON_LOG) vector[i] = 0.0;
        else     vector[i] = exp(vector[i]);
    }
}

void converttonormal2_(double *vector, int *D, int *index) {
    int i;
    for (i = 0; i < *D; i++) {
        if (i == (index[i]-1) ) {
                if (vector[i] == EPSILON_LOG) vector[i] = 0.0;
                else     vector[i] = exp(vector[i]);
        }
    }
}


void converttolog_(double *vector, int *D) {
    int i;

    for (i = 0; i < *D; i++) {
        if (vector[i] == 0) vector[i] = log(EPSILON_LOG);
        else vector[i] = log(vector[i]);
    }
}

void converttolog2_(double *vector, int *D, int *index) {
    int i;

    for (i = 0; i < *D; i++) {
        if (i == (index[i]-1) ) {
                if (vector[i] == 0) vector[i] = log(EPSILON_LOG);
                else vector[i] = log(vector[i]);
        }
    }
}


double callfitnessfunction_(void *(*fitnessfunction)(double*, void *), void *exp1_, double *U, int *D, double *Xl, double *Xm) {

    experiment_total *exp1;
    output_function *res;
    res = (output_function *) malloc(sizeof(output_function));
    exp1 = (experiment_total *) exp1_;

    
    
    DE_correction_bounds(U, *D, Xl, Xm);

    if (exp1[0].test._log == 1)
        converttonormal_(U, D);

    
    res = (output_function *) fitnessfunction(U,  &(exp1[0]) );
    U[*D] = res->value;
    
    if (exp1[0].test._log == 1)
        converttolog_(U, D);

    free(res);
    res = NULL;
    
    return U[*D]; 
}



double callfitnessfunctionfortran_(void *(*fitnessfunction)(double*, void *), void *exp1_, double *U, int *D, double *Xl, double *Xm,double*nlc) {
    
    experiment_total *exp1;
    int i;
    output_function *res;
    double value;
    
    res = NULL;    
    res = (output_function *) malloc(sizeof(output_function));
    
    exp1 = (experiment_total *) exp1_;
    DE_correction_bounds2(U, *D, Xl, Xm);
    res = (output_function *) fitnessfunction(U, & (exp1[0]) );
    
    if (res->g != NULL ) {
        for (i=0;i<exp1[0].methodScatterSearch->ineq;i++) {
                nlc[i] = res->g[i];
        }
    }
            

    value = res->value;
    free(res);
    res=NULL;
    
    return value; 
}


double callfitnessfunctionfortranopenmp_(void *(*fitnessfunction)(double*, void *), void *exp1_, double *U, int *D, double *Xl, double *Xm,double*nlc, int *idp) {
    
    experiment_total *exp1;
    int i;
    output_function *res;
    double value;
    
    
    exp1 = (experiment_total *) exp1_;
    
    DE_correction_bounds2(U, *D, Xl, Xm);
      
    res = (output_function *) fitnessfunction(U, &(exp1[*idp]));
    
    if (res->g != NULL ) {
        for (i=0;i<exp1[0].methodScatterSearch->ineq;i++) {
                nlc[i] = res->g[i];
        }
    }

    value=res->value;
            
    free(res);
    res = NULL;
    
    return value; 
}


double callfitnessfunctionopenmp_(void *(*fitnessfunction)(double*, void *), void *exp1_, double *U, int *D, double *Xl, double *Xm, int *idp) {
    output_function *res;
    
    res = (output_function *) malloc(sizeof(output_function) );
    experiment_total *exp1;
    exp1 = (experiment_total *) exp1_;
    
    
    DE_correction_bounds(U, *D, Xl, Xm);
    if (exp1[*idp].test._log == 1)
        converttonormal_(U, D);
    res = (output_function *) fitnessfunction(U, &(exp1[*idp]));
    U[*D] = res->value;
    
    if (exp1[*idp].test._log == 1)
        converttolog_(U, D);
    
    free(res);
    res = NULL;
    
    return U[*D]; 
}

void generatepopulation_(void* exp1_, double *popul, int NP, int D, void *(*fitnessfunction)(double*, void *), double *Xl, double *Xm, long *evaluation_local) {
    
    output_function *res;
    res = (output_function *) malloc(sizeof(output_function) );
    int i,j;
    double *populVector;
    double random;
    experiment_total *exp1;
    
    exp1 = (experiment_total *) exp1_;
    
    populVector = (double *) malloc((D + 1) * sizeof (double));
    if (populVector == NULL) perror("malloc");
    
    for (i = 0; i < NP; i++) {
        for (j = 0; j < D; j++) {
            random = (double) URAND_BBOB;
            popul[i * (D + 1) + j] = Xl[j] + (Xm[j] - Xl[j]) * random;
            populVector[j] = popul[i * (D + 1) + j];
        }


        DE_correction_bounds(populVector, D, Xl, Xm);
        for (j = 0; j < D; j++) popul[i * (D + 1) + j] = populVector[j];

        if (exp1[0].test._log == 1)
            converttonormal_(populVector, &D);
                
        res = (output_function *) fitnessfunction(populVector, &(exp1[0]));
        *evaluation_local = *evaluation_local + 1;
        
        popul[i * (D + 1) + D] = res->value ;     
        free(res);
        res = NULL;
    
    }
    
    free(populVector);
    populVector = NULL;
}


void coord_transformation(double *OLD, double *New, int D, double *UB, double *LB) {
    int i;

    for (i = 0; i < D; i++) {
        if (OLD[i] == LB[i]) New[i] = 0.0;
        else if ((fabs(LB[i]) + fabs(UB[i])) == 0) New[i] = 0.0;
        else if (LB[i] == UB[i]) New[i] = 0.0;
        else New[i] = (OLD[i] - LB[i]) / (fabs(LB[i]) + fabs(UB[i]));
    }


}



void read_matrix_init_points(char const *path, double *popul, int NP, int D, int cooperative, int NPROC) {
    FILE *fp;
    int i,j, NP_old;
    double valor;

 
    if (cooperative != 1) {
        fp = fopen(path, "r");
        if (fp == NULL) {
            printf("ERROR LOAD MATRIX\n");
            exit(0);
        }

        for (i = 0; i < NP; i++) {
            for (j = 0; j < (D + 1); j++) {
                fscanf(fp, "%lf", &valor);
                popul[i * (D + 1) + j] = valor;
            }
        }
    } else {
        NP_old = (int) NP / NPROC;
        
        fp = fopen(path, "r");
        if (fp == NULL) {
            printf("ERROR LOAD MATRIX\n");
            exit(0);
        }

        for (i = 0; i < NP_old; i++) {
            for (j = 0; j < (D + 1); j++) {
                fscanf(fp, "%lf", &valor);
                popul [i * (D + 1) + j] = valor;
            }
        }
        
        int contador;
        contador = 0;
        for (i = NP_old; i < NP; i++) {
                for (j = 0; j < (D + 1); j++) {
                    popul[i*(D+1)+j] = popul[contador*(D+1)+j];
                }
                if (contador < NP_old) contador++; else contador=0;
        }
        
        
        
    }
    
}

int extract_worst_index(double *populLocal, int tam, int D) {
    double max_value;
    int i, index;

    // MIRAMOS O MELLOR
    max_value = populLocal[0 * (D + 1) + D];
    index = 0;
    for (i = 0; i < tam; i++) {
        if (populLocal[i * (D + 1) + D] > max_value) {
            max_value = populLocal[i * (D + 1) + D];
            index = i;
        }
    }


    return index;
}

void insert_matrix_tabu_list(double *U, double *matrix, int D, int NP, int contador) {
    int i, index;

    if (contador >= NP) {
        index = extract_worst_index(matrix, NP, D);
        for (i = 0; i < (D + 1); i++)
            matrix[index * (D + 1) + i] = U[i];
    } else {
        for (i = 0; i < (D + 1); i++)
            matrix[contador * (D + 1) + i] = U[i];
    }
}



double calc_euclidean_distance(double *U1, double *U2, int D, double *UB, double *LB) {
    int i;
    double sum;
    double *U1_NEW, *U2_NEW;
    double *tempU1, *tempU2;
    

    tempU1 = (double *) malloc(D* sizeof (double));
    tempU2 = (double *) malloc(D* sizeof (double));
    
    memmove(tempU1,U1,D*sizeof(double));
    memmove(tempU2,U2,D*sizeof(double));
    
    U1_NEW = (double *) malloc(D* sizeof (double));
    U2_NEW = (double *) malloc(D* sizeof (double));

    coord_transformation(tempU1, U1_NEW, D, UB, LB);
    coord_transformation(tempU2, U2_NEW, D, UB, LB);
    sum = 0.0;


    
    for (i = 0; i < D; i++) {
        sum = sum + pow(U1_NEW[i] - U2_NEW[i], 2.0);      
    }
    
    if (tempU1!=NULL) {
        free(tempU1);
        tempU1 = NULL;
    }
    
    if (tempU2!=NULL) {
        free(tempU2);
        tempU2 = NULL;
    }
    
    if (U1_NEW != NULL) {
        free(U1_NEW);
        U1_NEW = NULL;
    }
    
    if (U2_NEW != NULL) {
        free(U2_NEW);
        U2_NEW = NULL; 
    }
    
    return sqrt(fabs(sum));
}

int initializebenchmarks_(void *exp1_, double *Xl, double *Xm, int *D) {
    int j;
    char const *noiseBBOB;
    char const *noiselessBBOB;
    char const *system;

    noiseBBOB = "noiseBBOB";
    noiselessBBOB = "noiselessBBOB";
    system = "systemBiology";
    
    experiment_total *exp1;
    exp1 = (experiment_total *) exp1_;
    
    if ((strcmp(exp1[0].test.bench.type, noiseBBOB) == 0) || (strcmp(exp1[0].test.bench.type, noiselessBBOB) == 0)) {
        for (j = 0; j < *D; j++) {
            Xl[j] = (double) exp1[0].test.bench.min_dom[0];
            Xm[j] = (double) exp1[0].test.bench.max_dom[0];
        }
    } else if (strcmp(exp1[0].test.bench.type, system) == 0) {
        if ( exp1[0].test._log == 0 ) {
            for (j = 0; j < *D; j++) {
                Xl[j] = (double) exp1[0].test.bench.min_dom[j];
                Xm[j] = (double) exp1[0].test.bench.max_dom[j];
            }
        } else {
            for (j = 0; j < *D; j++) {
                Xl[j] = (double) exp1[0].test.bench.log_min_dom[j];
                Xm[j] = (double) exp1[0].test.bench.log_max_dom[j];
            }            
        }
    }
    return 1;
}


int initializebenchmarksopenmp_(void *exp1_, double *Xl, double *Xm, int *D, int *idp) {
    int j;
    char const *noiseBBOB;
    char const *noiselessBBOB;
    char const *system;

    noiseBBOB = "noiseBBOB";
    noiselessBBOB = "noiselessBBOB";
    system = "systemBiology";
    
    experiment_total *exp1;
    exp1 = (experiment_total *) exp1_;
    
    if ((strcmp(exp1[*idp].test.bench.type, noiseBBOB) == 0) || (strcmp(exp1[*idp].test.bench.type, noiselessBBOB) == 0)) {
        for (j = 0; j < *D; j++) {
            Xl[j] = (double) exp1[*idp].test.bench.min_dom[0];
            Xm[j] = (double) exp1[*idp].test.bench.max_dom[0];
        }
    } else if (strcmp(exp1[*idp].test.bench.type, system) == 0) {
        for (j = 0; j < *D; j++) {
            Xl[j] = (double) exp1[*idp].test.bench.min_dom[j];
            Xm[j] = (double) exp1[*idp].test.bench.max_dom[j];
        }
    }
    return 1;
}

/* SUM VAR */
double sumvar(double *Matriz, int sizeMatriz, int sizeVector) {
    int i, j;
    double *vector1, *vector_matrices;
    double avg, suma, varian;

    vector1 = (double *) malloc(sizeMatriz* sizeof (double));
    vector_matrices = (double *) malloc(sizeVector* sizeof (double));
    // VARIANZAS
    for (i = 0; i < sizeVector; i++) {
        suma = 0.0;
        for (j = 0; j < sizeMatriz; j++) {
            vector1[j] = Matriz[j * (sizeVector + 1) + i];
            suma = suma + vector1[j];
        }
        avg = suma / (double) sizeMatriz;
        suma = 0.0;
        for (j = 0; j < sizeMatriz; j++) {
            suma = suma + powl((vector1[j] - avg), 2);
        }
        varian = suma / (double) sizeMatriz;
        vector_matrices[i] = varian;
    }

    // SUMA
    suma = 0.0;
    for (j = 0; j < sizeVector; j++) {
        suma = suma + vector_matrices[j];
    }


    if (vector1 != NULL) {
        free(vector1);
        vector1 = NULL; 
    }
    
    if (vector_matrices != NULL) {
        free(vector_matrices);
        vector_matrices = NULL;
    }
    
    return fabsl(suma);
}

/* ALLOCATE QUICKSHORT */
int allocate_QuickShort(double *v, int b, int NP, int D) {
    int i; 
            //z;
    int pivote;
    double *valor_pivote;
    double *temp;

    valor_pivote = (double *) malloc((D + 1)* sizeof (double));
    temp = (double *) malloc((D + 1)* sizeof (double));

    pivote = b;
    
    memmove(valor_pivote, &v[pivote * (D + 1)], (D+1)*sizeof(double));

    for (i = b + 1; i <= NP; i++) {
        if (v[i * (D + 1) + D] < valor_pivote[D]) {
            pivote++;
            memmove(temp,&v[i * (D + 1)],(D+1)*sizeof(double));
            memmove(&v[i * (D + 1)],&v[pivote * (D + 1)],(D+1)*sizeof(double));
            memmove(&v[pivote * (D + 1)],temp,(D+1)*sizeof(double));
        }
    }

    memmove(temp,&v[b * (D + 1)], (D+1)*sizeof(double));
    memmove(&v[b * (D + 1)],&v[pivote * (D + 1)], (D+1)*sizeof(double));
    memmove(&v[pivote * (D + 1)],temp, (D+1)*sizeof(double));

    if (valor_pivote != NULL) {
        free(valor_pivote);
        valor_pivote=NULL;
    }
    
    if (temp != NULL) {
        free(temp);
        temp=NULL;
    }

    return pivote;
}

/* REORDER QUICK SHORT */
double* reorderVector_QuickShort(double* v, int b, int NP, int D) {
    int pivote;


    if (b < NP) {
        pivote = allocate_QuickShort(v, b, NP, D);
        reorderVector_QuickShort(v, b, pivote - 1, D);
        reorderVector_QuickShort(v, pivote + 1, NP, D);
    }

    return v;
}

/* REPLACE_WORST */
void replaceWorst(double *v, int NP, int D, double *best, int sizeBest) {
    int i;
    int z = 0;


    v = reorderVector_QuickShort(v, 0, NP - 1, D);

    
    
    for (i = (NP - sizeBest); i < NP; i++) {
        memmove(&v[i * (D + 1)], &best[z * (D + 1)], (D+1) * sizeof(double) );
        z++;
    }


}


void replaceWorstRecp(double *v, int NP, int D, double *best, int sizeBest, double *LB, double *UB, int rest) {
    int i,j,z,k;
    int *index_state;
    double *U1, *U2;

    index_state = (int *) malloc(sizeBest*sizeof(int));
    U1 = (double *) malloc((D+1)*sizeof(double));
    U2 = (double *) malloc((D+1)*sizeof(double));
            
    v = reorderVector_QuickShort(v, 0, NP - 1, D);
    
    for (i=0; i<sizeBest; i++) {
        memmove(U1,&best[i * (D + 1)],(D+1-rest)*sizeof(double));
        for (j=0; j<NP; j++) {
            memmove(U2,&v[j * (D + 1)],(D+1-rest)*sizeof(double));
            
            if ( calc_euclidean_distance(U1, U2, D, UB, LB) <= 1e-1 ) {
                index_state[i] = 0;
                break;
            } 
        }
        if (j==NP) {
            index_state[i] = 1;
        }
    }

    //printf("\nINDEX : ");
    //    for (i=0;i<sizeBest;i++)  printf(" %d ", index_state[i]);
    //        printf("\n");
                
    //               printf("\nBEST : ");
    //                   for (i=0;i<sizeBest;i++)  printf(" %lf ", best[i * (D + 1) + D]);
    //                       printf("\n");
            
    z=0;
    k=NP-1;
   
    //printf("OLD : "); 
    for (i = (NP - sizeBest); i < NP; i++) {
        if (index_state[z] == 1 ) {
       //     printf(" %lf ", v[k * (D + 1)+D]);
            memmove(&v[k * (D + 1)], &best[z * (D + 1)], (D+1) * sizeof(double) );
            k--;
        }
        z++;
    }
    //printf("\n");

       
   // printf("POPUL : ");
   //     for (i=0;i<NP;i++)  printf(" %lf ", v[i * (D + 1)+D]);
   //         printf("\n"); 
    free(index_state);
    index_state = NULL;
    free(U1);
    U1=NULL;
    free(U2);
    U2=NULL;
}

/* REPLACE RANDOM */
void replaceRandom(double *v, int NP, int D, double *best, int sizeBest) {
    int i, j, punto, valido;
    int *vector_puntos;

   // v = reorderVector_QuickShort(v, 0, NP - 1, D);
    vector_puntos = (int *) malloc(sizeBest* sizeof (int));
    for (i = 0; i < sizeBest; i++) {
        vector_puntos[i] = -1;
    }
    
    for (i = 0; i < sizeBest; i++) {
        valido = 0;
        while (valido == 0) {
            punto = (int) (NP * URAND_BBOB);
            for (j = 0; j < sizeBest; j++) {
                if (punto == vector_puntos[j]) break;
            }
            if  ((j == sizeBest)){//&&(punto!=0))  {
                valido = 1;
            }
        }

        vector_puntos[i] = punto;
        for (j = 0; j < (D + 1); j++) {
            v[punto * (D + 1) + j] = best[i * (D + 1) + j];

        }
    }

    if (vector_puntos != NULL) {
        free(vector_puntos);
        vector_puntos=NULL;
    }
}

/* REPLACE BEST */
void returnBest(double *v, int N, int D, double *best, int sizeBest) {
    int i,  cont = 0;
    double *vect;

    vect = (double *) malloc(N * (D + 1) * sizeof (double));

    vect = reorderVector_QuickShort(v, 0, N - 1, D);

   for (i = 0; i < N; i++) {       
       memmove(&v[i * (D + 1)],&vect[i * (D + 1)],(D+1)*sizeof(double));
   }
    
    cont = 0;
    for (i = 0; i < (N - 1); i++) {
        if (cont < sizeBest) {
            memmove(&best[cont * (D + 1)], &vect[i * (D + 1)], (D+1)*sizeof(double));
            cont++;
        } else break;
    }


   // free(vect);
   // vect = NULL;
}

/* REPLACE RANDOM */
void returnRandom(double *v, int NP, int D, double *best, int sizeBest) {
    int i, punto;

    for (i = 0; i < sizeBest; i++) {
        punto = (int) (NP * URAND_BBOB);
        memmove(&best[i * (D + 1)], &v[punto * (D + 1)], (D+1)*sizeof(double));
    }


}



/* REPLACE BEST 
void returnBestTabuList(experiment_total exp1, double *v, int N, int D, double *best, int sizeBest, double *UB, double *LB, int rest) {
    int i, j, cont = 0;
    double *vect, *U1, *U2;

    vect = (double *) malloc(N * (D + 1) * sizeof (double));
    U1 = (double *) malloc((D+1)*sizeof(double));
    U2 = (double *) malloc((D+1)*sizeof(double));
    
    
    vect = reorderVector_QuickShort(v, 0, N - 1, D);

    for (i = 0; i < N; i++) {       
       memmove(&v[i * (D + 1)],&vect[i * (D + 1)],(D+1)*sizeof(double));
    }
    
    cont = 0;
    for (i = 0; i < (N - 1); i++) {
        if (cont < sizeBest) {
            memmove(U1,&vect[i * (D + 1-rest)],(D+1)*sizeof(double));
            for (j=0; j<exp1.execution.tabusend.size; j++) {
                memmove(U2,&exp1.execution.tabusend.array[j * (D + 1-rest)],(D+1)*sizeof(double));
                if ( calc_euclidean_distance(U1, U2, D, UB, LB) <= 1e-1 ) {
                    break;
                }                 
            }
            if (j == cont) {
                memmove(&best[cont * (D + 1)], &vect[i * (D + 1)], (D+1)*sizeof(double));
                cont++;
            }
        } else break;
    }

    
    if (cont < sizeBest) {
        memmove(U1, best, (D+1)*sizeof(double)); 
        for (i=cont;i<sizeBest;i++) {
            memmove(&best[i * (D + 1)], U1, (D+1)*sizeof(double));
        }
    } else {
        for (i=0;i<sizeBest;i++) {
            insert_matrix_tabu_list(  
                    &best[i*(D+1)], 
                    exp1.execution.tabusend.array,
                    D,exp1.execution.tabusend.size,
                    exp1.execution.tabusend.counter);
        }
                
    }

    
    
        cont = 0;
    for (i = 0; i < (N - 1); i++) {
        if (cont < sizeBest) {
            memmove(&best[cont * (D + 1)], &vect[i * (D + 1)], (D+1)*sizeof(double));
            cont++;
        } else break;
    }
        
        
    free(U1);
    U1 = NULL;
    
    free(U2);
    U2 = NULL;
}*/


void returnIndv(experiment_total exp1, double *v, int N, int D, double *best, int sizeBest, double *UB, double *LB, int rest) {
    const char *bestword;
    const char *random;

    bestword = "Best";
    random = "Random";
    
    
    if (strcmp(exp1.par_st->SelectionPolicy, bestword) == 0) {
	returnBest(v, N, D, best, sizeBest);
        //returnBestTabuList(exp1,v, N, D, best, sizeBest,UB,LB, rest);
    } else if (strcmp(exp1.par_st->SelectionPolicy, random) == 0) {
        returnRandom(v, N, D, best, sizeBest);
    } else {
        perror(error1);
        exit(2);
    }


}

void replaceIndv(experiment_total exp1, double *v, int N, int D, double *worst, int sizeBest, double *Xl, double *Xm, int rest) {
    const char *worstw;
    const char *random;

    worstw = "Worst";
    random = "Random";

    if (strcmp(exp1.par_st->ReplacePolicy, worstw) == 0) {
        replaceWorst(v, N, D, worst, sizeBest);
        //replaceWorstRecp(v, N, D, worst, sizeBest,Xl,Xm, rest);
    } else if (strcmp(exp1.par_st->ReplacePolicy, random) == 0) {
        replaceRandom(v, N, D, worst, sizeBest);
    } else {
        perror(error2);
        exit(2);
    }
}

/* SELECT MUTATION OPERATION*/
double* mutation_operation(void * exp_, double *populLocal, double *U, int D, int NP, int i, int index, int idp, double _CR, double _F) {
    int r1,r2,r3,r4,r5,jrand,j;
    const char *rand1, *rand2, *best1, *best2, *currentbest, *currentrand;
    experiment_total *exp;
    
    exp = (experiment_total *) exp_;
    rand1 = "DE/rand/1";
    rand2 = "DE/rand/2";
    best1 = "DE/best/1";
    best2 = "DE/best/2";
    currentbest = "DE/current-to-best/1";
    currentrand = "DE/current-to-rand/1";
    
    if (strcmp(exp[0].methodDE->mutation_strategy, rand1)==0) {
        
                do {
                    r1 = (int) ((NP) * URAND_BBOB);
                } while (r1 == i);

                do {
                    r2 = (int) ((NP) * URAND_BBOB);
                } while (r2 == i || r2 == r1);

                do {
                    r3 = (int) ((NP) * URAND_BBOB);
                } while ((r3 == i || r3 == r1 || r3 == r2));

                jrand = (int) (D * URAND_BBOB);

                for (j = 0; j < D; j++) {

                    if ((URAND_BBOB < _CR || j == jrand)) {
                        U[j] = populLocal[r3 * (D + 1) + j] + _F * (populLocal[r1 * (D + 1) + j] - populLocal[r2 * (D + 1) + j]);

                    } else {
                        U[j] = populLocal[i * (D + 1) + j];
                    }

                }   
             
    }
    else if(strcmp(exp[0].methodDE->mutation_strategy, best1)==0){
                do {
                    r1 = (int) ((NP) * URAND_BBOB);
                } while (r1 == i);

                do {
                    r2 = (int) ((NP) * URAND_BBOB);
                } while (r2 == i || r2 == r1);


                jrand = (int) (D * URAND_BBOB);

                for (j = 0; j < D; j++) {
                    if ((URAND_BBOB < _CR || j == jrand)) {
                        U[j] = populLocal[index * (D + 1) + j] + _F * (populLocal[r1 * (D + 1) + j] - populLocal[r2 * (D + 1) + j]);
                    } else {
                        U[j] = populLocal[i * (D + 1) + j];
                    }
                }    
                
                
                
    } 
    else if (strcmp(exp[0].methodDE->mutation_strategy, rand2)==0) {
        if (NP > 5) {
                do {
                    r1 = (int) ((NP) * URAND_BBOB);
                } while (r1 == i);

                do {
                    r2 = (int) ((NP) * URAND_BBOB);
                } while (r2 == i || r2 == r1);

                do {
                    r3 = (int) ((NP) * URAND_BBOB);
                } while ((r3 == i || r3 == r1 || r3 == r2));

                do {
                    r4 = (int) ((NP) * URAND_BBOB);
                } while ((r4 == i || r4 == r1 || r4 == r2 || r4 == r3));

                do {
                    r5 = (int) ((NP) * URAND_BBOB);
                } while ((r5 == i || r5 == r1 || r5 == r2 || r5 == r3 || r5 == r4));
         } else {
                r1 = (int) ((NP) * URAND_BBOB);

                do {
                    r2 = (int) ((NP) * URAND_BBOB);
                } while ( r2 == r1);

                do {
                    r3 = (int) ((NP) * URAND_BBOB);
                } while (( r3 == r1 || r3 == r2));

                do {
                    r4 = (int) ((NP) * URAND_BBOB);
                } while (( r4 == r1 || r4 == r2 || r4 == r3));

                do {
                    r5 = (int) ((NP) * URAND_BBOB);
                } while (( r5 == r1 || r5 == r2 || r5 == r3 || r5 == r4));
         }
                jrand = (int) (D * URAND_BBOB);

                for (j = 0; j < D; j++) {

                    if ((URAND_BBOB < _CR || j == jrand)) {
                        U[j] = populLocal[r3 * (D + 1) + j] + 
                                _F * (populLocal[r1 * (D + 1) + j] - populLocal[r2 * (D + 1) + j]) + 
                                _F * (populLocal[r4 * (D + 1) + j] - populLocal[r5 * (D + 1) + j]) 
                                ;

                    } else {
                        U[j] = populLocal[i * (D + 1) + j];
                    }

                }   
             
    }  
    else if (strcmp(exp[0].methodDE->mutation_strategy, best2)==0) {
                do {
                    r1 = (int) ((NP) * URAND_BBOB);
                } while (r1 == i);

                do {
                    r2 = (int) ((NP) * URAND_BBOB);
                } while (r2 == i || r2 == r1);


                do {
                    r4 = (int) ((NP) * URAND_BBOB);
                } while ((r4 == i || r4 == r1 || r4 == r2 ));

                do {
                    r5 = (int) ((NP) * URAND_BBOB);
                } while ((r5 == i || r5 == r1 || r5 == r2 || r5 == r4));
                
                jrand = (int) (D * URAND_BBOB);

                for (j = 0; j < D; j++) {

                    if ((URAND_BBOB < _CR || j == jrand)) {
                        U[j] = populLocal[index * (D + 1) + j] + 
                                _F * (populLocal[r1 * (D + 1) + j] - populLocal[r2 * (D + 1) + j]) + 
                                _F * (populLocal[r4 * (D + 1) + j] - populLocal[r5 * (D + 1) + j]) 
                                ;

                    } else {
                        U[j] = populLocal[i * (D + 1) + j];
                    }

                }   
             
    }  
    else if (strcmp(exp[0].methodDE->mutation_strategy, currentbest)==0) {
                do {
                    r1 = (int) ((NP) * URAND_BBOB);
                } while (r1 == i);

                do {
                    r2 = (int) ((NP) * URAND_BBOB);
                } while (r2 == i || r2 == r1);

                
                jrand = (int) (D * URAND_BBOB);

                for (j = 0; j < D; j++) {

                    if ((URAND_BBOB < _CR || j == jrand)) {
                        U[j] = populLocal[i * (D + 1) + j] + 
                                _F * (populLocal[index * (D + 1) + j] - populLocal[i * (D + 1) + j]) +                                
                                _F * (populLocal[r1 * (D + 1) + j] - populLocal[r2 * (D + 1) + j])  
                                ;

                    } else {
                        U[j] = populLocal[i * (D + 1) + j];
                    }

                }   
             
    }  
    else if (strcmp(exp[0].methodDE->mutation_strategy, currentrand)==0) {
                do {
                    r1 = (int) ((NP) * URAND_BBOB);
                } while (r1 == i);

                do {
                    r2 = (int) ((NP) * URAND_BBOB);
                } while (r2 == i || r2 == r1);
                
                do {
                    r3 = (int) ((NP) * URAND_BBOB);
                } while ((r3 == i || r3 == r1 || r3 == r2));
                
                jrand = (int) (D * URAND_BBOB);

                for (j = 0; j < D; j++) {

                    if ((URAND_BBOB < _CR || j == jrand)) {
                        U[j] = populLocal[i * (D + 1) + j] + 
                                _F * (populLocal[r3 * (D + 1) + j] - populLocal[i * (D + 1) + j]) +                                
                                _F * (populLocal[r1 * (D + 1) + j] - populLocal[r2 * (D + 1) + j])  
                                ;

                    } else {
                        U[j] = populLocal[i * (D + 1) + j];
                    }

                }   
             
    }       
    else {
        perror(error3); 
        exit(3);
    }
    
    return U;
}

int extract_best_index(double *populLocal, int tam, int D) {
    double min_value;
    int i, index;

    // MIRAMOS O MELLOR
    min_value = populLocal[0 * (D + 1) + D];
    index = 0;
    for (i = 0; i < tam; i++) {
        if (populLocal[i * (D + 1) + D] < min_value) {
            min_value = populLocal[i * (D + 1) + D];
            index = i;
        }
    }


    return index;
}



void reorder_best(double *v, int N, int D) {
    int i, j;
    double *vect;

    vect = (double *) malloc(N * (D + 1) * sizeof (double));

    vect = reorderVector_QuickShort(v, 0, N - 1, D);

    for (i = 0; i < N; i++) {
        for (j = 0; j < (D + 1); j++) {
            v[i * (D + 1) + j] = vect[i * (D + 1) + j];
        }
    }
}

int chargeuseroptions_(void *exp_, double *maxtime, int *weight, double *tolc, double *prob_bound,
        int *nstuck_solution, int *strategy, int *inter_save, int *iterprint, int *plot, int *log_var, int *init_point) {
    experiment_total *exp;
    exp = (experiment_total *) exp_;
    
    if (exp[0].methodScatterSearch != NULL) {
        if (exp[0].methodScatterSearch->uoptions != NULL) {
            
            *maxtime=exp[0].test.maxtime;
            *weight = exp[0].methodScatterSearch->uoptions->weight;
            *tolc = exp[0].methodScatterSearch->uoptions->tolc;
            *prob_bound = exp[0].methodScatterSearch->uoptions->prob_bound;
            *nstuck_solution = exp[0].methodScatterSearch->uoptions->nstuck_solution;
            *strategy = exp[0].methodScatterSearch->uoptions->strategy;
            *inter_save = exp[0].methodScatterSearch->uoptions->inter_save;
            *iterprint = exp[0].test.verbose;
            *plot = exp[0].test.ouput;
            if (exp[0].test._log == 1) {
                *log_var = 1;
            } else *log_var = 0;
            *init_point=exp[0].test.init_point;
            return 1;
        } else return 0;
    } else return 0;
    return 1;
}

int chargeglobaloptions_( void *exp_, int *dim_ref, int *ndiverse, int *initiate, int *combination, int *regenerate, 
        char* delete, int *intens, double * tolf, int *diverse_criteria, double * tolx, int *n_stuck) {
    experiment_method_ScatterSearch *method;
    experiment_total *exp;
    const char *NULLchar;
    
    NULLchar = "\0";
    
    exp = (experiment_total *) exp_;
    method = exp[0].methodScatterSearch;
    if (method != NULL) {   
        if (method->goptions != NULL) {
            *dim_ref = method->goptions->dim_ref;
            *ndiverse = method->goptions->ndiverse;
            *initiate = method->goptions->initiate;
            *combination = method->goptions->combination;
            *regenerate = method->goptions->regenerate;
           
            if (method->goptions->delete == NULL ) {
                method->goptions->delete = strtok(method->goptions->delete, NULLchar);
                sprintf(delete,"%s", (const char *) method->goptions->delete);
            }
            *intens = method->goptions->intens; 
            *tolf = method->goptions->tolf;
            *diverse_criteria = method->goptions->diverse_criteria;
            *tolx = method->goptions->tolx;
            *n_stuck = method->goptions->n_stuck;
            return 1;
        } else return 0;
    } else return 0;
       
    return 1;     
}

int chargelocaloptions_( void *exp_, int *tol, int *iterprint, int *n1, int *n2, float * balance, 
        char * finish, int *bestx, int *merit_filter, int *distance_filter, float *thfactor, 
        float *maxdistfactor, int *wait_maxdist_limit, int *wait_th_limit, char *solver, double *threshold_local) {
    experiment_method_ScatterSearch *method;
    experiment_total *exp;
    const char *NULLchar;
    NULLchar = "\0";
    
    exp = (experiment_total *) exp_;

    if (exp[0].test.local_search == 1) {
        method = exp[0].methodScatterSearch;
        if (method != NULL) {

            if (method->loptions != NULL) {

                * tol = method->loptions->tol;
                * iterprint = method->loptions->iterprint;
                * n1 = method->loptions->n1;
                * n2 = method->loptions->n2;
                * balance = method->loptions->balance;
                if (*n1 > -1) {
                    method->loptions->solver = strtok(method->loptions->solver, NULLchar);
                    if (method->loptions->solver != NULL) {
                        sprintf(solver, "%s", method->loptions->solver);
                    }
                    method->loptions->finish = strtok(method->loptions->finish, NULLchar);
                    if (method->loptions->finish != NULL) {
                        sprintf(finish, "%s", method->loptions->finish);
                    } else if (method->loptions->finish == NULL) {
                        finish = "";
                    }
                    *bestx = method->loptions->bestx;
                    *merit_filter = method->loptions->merit_filter;
                    *distance_filter = method->loptions->distance_filter;
                    *thfactor = method->loptions->thfactor;
                    *maxdistfactor = method->loptions->maxdistfactor;
                    *wait_maxdist_limit = method->loptions->wait_maxdist_limit;
                    *wait_th_limit = method->loptions->wait_th_limit;
                    *threshold_local = method->ls_threshold;
                    return 1;
                } else {
                    return 0;
                }
            } else return 0;
        } else return 0;
    } else return 0;
}


int chargeproblemargs_(void *exp_, int *ineq, int *int_var, int *bin_var) {
    experiment_total *exp1;
    exp1 = (experiment_total *) exp_;
    *ineq = exp1[0].methodScatterSearch->ineq;
    *int_var = exp1[0].methodScatterSearch->int_var;
    *bin_var = exp1[0].methodScatterSearch->bin_var;  
    
    return 1;
}

int chargeboundsnconst_(void *exp_, double *XU, double *XL, int *nvar , double *CU, double *CL, int *ineq) {
    experiment_total *exp1;
    int i;
    exp1 = (experiment_total *) exp_;
    
    for (i=0;i<*nvar;i++) {
                XU[i] = exp1[0].test.bench.max_dom[i];
                XL[i] = exp1[0].test.bench.min_dom[i];
    }

    if (exp1[0].test.bench.CU != NULL && (*ineq > 0)) {
        for (i = 0; i<*ineq; i++) {
            CU[i] = exp1[0].test.bench.CU[i];
        }
    }
    
    if (exp1[0].test.bench.CL != NULL && (*ineq> 0)) {
        for (i = 0; i<*ineq; i++) {
            CL[i] = exp1[0].test.bench.CL[i];
        }
    }

    return 1;
}


int chargebounds_(void *exp_, double *XU, double *XL, int *nvar) {
    experiment_total *exp1;
    int i;
    exp1 = (experiment_total *) exp_;
    
    
    for (i=0;i<*nvar;i++) {
                XU[i] = exp1[0].test.bench.max_dom[i];
                XL[i] = exp1[0].test.bench.min_dom[i];
    }
    
    return 1;
}



int getbench_(void *exp_) {
    experiment_total *exp1;
    exp1 = (experiment_total *) exp_;
    
    
    return exp1->test.bench.current_bench;
    
}


void setinfinity_(double *valor){
    *valor = INFINITY;
}

void setnan_(double *valor){
    *valor = NAN;
}


double gettotaltime_( result_solver *timestruct) {
    return timestruct->totaltime;
}


int ishete_(void *exp_) {
    experiment_total *exp1;
    exp1 = (experiment_total *) exp_;
    char *homo;
    char *hete;
    
    homo = "homo";
    hete = "hete";
    
            
    if (strcmp(exp1->par_st->islandstrategy,homo)==0) {
        return 0;
    }
    else if (strcmp(exp1->par_st->islandstrategy,hete)==0 ){
        return 1;
    } else return 0;
    
}



int iscoop_(void *exp_) {
    experiment_total *exp1;
    exp1 = (experiment_total *) exp_;
    char *coop;
    char *island;
    
    
    island="island";
    coop="cooperative";
            
    if (strcmp(exp1->par_st->type,island)==0) {
        return 0;
    }
    else if (strcmp(exp1->par_st->type,coop)==0 ){
        return 1;
    } else return 1;
    
}

double getparalleltime_( result_solver *timestruct) {
    return timestruct->paralleltime;
}

double getlocalsolvertime_( result_solver *timestruct) {
    return timestruct->localsolvertime;
    
}



void settotaltime_(result_solver *timestruct, double *time){
    timestruct->totaltime = timestruct->totaltime + *time;
}

void setparalleltime_(result_solver *timestruct, double *time){
    timestruct->paralleltime = timestruct->paralleltime + *time;
}

void setlocalsolvertime_(result_solver *timestruct, double *time){
    timestruct->localsolvertime = timestruct->localsolvertime + *time;
}


void setiteration_(void *exp_, int *ite) {
    experiment_total *exp1;
    exp1 = (experiment_total *) exp_;
    
    exp1->execution.iteration = *ite;
    
}


void setnumit_(void *exp_, int *ite) {
    experiment_total *exp1;
    exp1 = (experiment_total *) exp_;
    
    exp1->execution.num_it = *ite;
    
}



int getidp_(void *exp_) {
    experiment_total *exp1;
    exp1 = (experiment_total *) exp_;
    
    
    return exp1->execution.idp;
}

