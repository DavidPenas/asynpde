#include <AMIGO_pe.h>
#include <time.h>
#include "simulate_amigo_model.h"
#include <string.h>
#ifdef MPI2
#include <mpi.h>
#endif
#ifdef OPENMP
#include <omp.h>
#endif

    int NL2SOL_AMIGO_pe(AMIGO_problem* amigo_problem, int gradient, int *stop, void *array_stop, int *NPROC, int *nevals) {

    int i, counter;
    int *ui;
    int liv, lty, lv, n, p, one, printLevel;
    int* IV;
    double *TY, *B, *X, *V;
    int evalaux;

    one = 1;
    lty = 10;
    p = amigo_problem->nx;
    n = amigo_problem->n_data;
    printLevel = 1;
    //LIV GIVES THE LENGTH OF IV.  IT MUST BE AT LEAST 82 + 4*P.
    liv = 4 * p + 82;
    

    IV = (int*) malloc(liv * sizeof (int));
    for (i=0;i<liv;i++) IV[i]=0;
    
    TY = (double*) malloc(2 * sizeof (double));
    for (i=0;i<2;i++) TY[i]=0.0;
    
    //LV GIVES THE LENGTH OF V.  THE MINIMUM VALUE
    //FOR LV IS LV0 = 105 + P*(N + 2*P + 21) + 2*N
    lv = 105 + p * (n + 2 * p + 21) + 2 * n;

    V = (double*) malloc(lv * sizeof (double));
    for (i=0;i<lv;i++) V[i]=0.0;
    
    X = (double*) malloc(p * sizeof (double));
    for (i=0;i<p;i++) X[i]=0.0;
    
    ui = (int *) malloc(2 * sizeof (int));
    for (i=0;i<2;i++) ui[i]=0.0;
    
    //Set Options
    //DFAULT(iv,v);								//set defaults (OLD v2.2)

    //DIVSET(ALG, IV, LIV, LV, V) 
    divset_(&one, IV, &liv, &lv, V);


    IV[13] = IV[14] = 0; //no covar
    IV[16] = amigo_problem->local_max_evals; //limit on fevals + gevals
    IV[17] = amigo_problem->local_max_iter; //max iter
    IV[18] = 1; //no iteration printing
    IV[19] = 1; //no default printing
    IV[20] = 1; //no output unit printing
    IV[21] = 1; //no x printing
    IV[22] = 1; //no summary printing
    IV[23] = 1;
    //v[30] = fatol;   
    //v[31] = frtol;
    //V[31] = 1e-9f;
    //V[32] = 1e-9f;

    //MEX Options 

    //The circadian problem fails if you don't specify this
    V[33] = 1e-9f;

    V[41] = 0.0046; // V(DLTFDC) Default = MACHEP^1/3. error tolerance in CVODES 1e-7, 1e-7^-3=0.0046
    V[42] = 3.1623e-4; // V(DLTFDJ) Default = MACHEP^1/2  error tolerance in CVODES 1e-7  1e-7^-2=3.1623e-4

    ui[1] = printLevel;
    memmove(X,amigo_problem->x0,sizeof(double)*p);

    B = (double*) malloc(2 * p * sizeof (double));

    counter = 0;
    
    
    
    for (i = 0; i < p; ++i) {

        B[counter++] = amigo_problem->LB[i];
        B[counter++] = amigo_problem->UB[i];
    }
    

    evalaux = amigo_problem->nevals;

    if (gradient) {
        dn2gb_(&n, &p, X, B, IV, &liv, &lv, V, ui, TY, (void *) amigo_problem, stop, (void *) array_stop, NPROC);
    } else {
        dn2fb_(&n, &p, X, B, IV, &liv, &lv, V, ui, TY, (void *) amigo_problem, stop, (void *) array_stop, NPROC);
    }
    //Final Rnorm
    amigo_problem->local_fbest = V[9]*2; //nl2sol uses 1/2 sum(resid^2)
    //Save Status & Iterations
    amigo_problem->local_flag = IV[0];
    //printf("local_flag %d\n", IV[0]);
    amigo_problem->local_niter = IV[30];

    memmove(amigo_problem->xbest,X,p*sizeof(double));
    *nevals = *nevals + (amigo_problem->nevals - evalaux) ; 

    set_AMIGO_problem_pars(X, amigo_problem);
    eval_AMIGO_problem_LSQ(amigo_problem);

    free(B);
    free(X);
    free(IV);
    free(V);
    free(ui);
    free(TY);
    B = NULL;
    X = NULL;
    IV = NULL;
    V = NULL;
    ui = NULL;
    TY = NULL;

    return 0;
}

int calcr_(int *n, int *p, double *x, int *nf, double *r__,
        int *lty, double *ty, void*data, int *stop, void *array_stop, int *NPROC) {

    int i, j, k, n_exps, n_times, n_obs, flag;
    int counter = 0;
    double sum = 0;

    AMIGO_problem* amigo_problem = (AMIGO_problem*) data;
    amigo_problem->nevals++;
    amigo_problem->local_nfeval++;
    set_AMIGO_problem_pars(x, amigo_problem);
    n_exps = amigo_problem->n_models;
#ifdef MPI2
    MPI_Request *request_recep = (MPI_Request *) array_stop;
    int flag2, l;
#endif
    for (i = 0; i < n_exps; ++i) {
#ifdef MPI2

        if (*stop < 1) {
            for (l = 0; l < *NPROC; l++) {
                MPI_Test(&request_recep[l], &flag2, MPI_STATUS_IGNORE);

                if (flag2 == 1) {
                    *stop = 1;
                    break;
                }
            }
        }
        

#endif
        if (*stop >= 1) {
            break;
        }

        //0 For no sensitivity
        flag = simulate_AMIGO_model_observables(amigo_problem->amigo_models[i], 0);

        n_obs = amigo_problem->amigo_models[i]->n_observables;

        if (!flag) {

            handle_AMIGO_problem_stat_fails(i, amigo_problem);
            for (i = 0; i < n_exps; ++i) {
                for (j = 0; j < n_obs; ++j) {
                    n_times = amigo_problem->amigo_models[i]->n_times;
                    for (k = 0; k < n_times; ++k) {
                        r__[counter++] = DBL_MAX;
                    }
                }
            }
            if (amigo_problem->verbose) {
#ifdef MATLAB
                mexPrintf("Eval-%d Simulation failed\n", amigo_problem->nevals);
#else
                printf("Eval-%d Simulation failed\n", amigo_problem->nevals);
#endif
            }

            return (0);
        }

        for (j = 0; j < n_obs; ++j) {
            n_times = amigo_problem->amigo_models[i]->n_times;
            for (k = 0; k < n_times; ++k) {
                if (!isnan(amigo_problem->amigo_models[i]->exp_data[j][k]) &&
                        !isnan(amigo_problem->amigo_models[i]->Q[j][k]) &&
                        amigo_problem->amigo_models[i]->Q[j][k] != 0) {
                    r__[counter++] =
                            (amigo_problem->amigo_models[i]->obs_results[j][k] -
                            amigo_problem->amigo_models[i]->exp_data[j][k]) *
                            amigo_problem->amigo_models[i]->Q[j][k];
                    sum += pow(r__[counter - 1], 2);
                } else {
                    r__[counter++] = 0;
                }
            }
        }
    }

    if (amigo_problem->verbose) {
#ifdef MATLAB
        mexPrintf("Eval %d f=%f\n", amigo_problem->nevals, sum);
#else
        printf("Eval %d f=%f\n", amigo_problem->nevals, sum);
#endif
    }

    return (0);
}

int calcj_(int *n, int* p, double *x, int *nf, double *dr__, int *lty, double *ty, void*data, int *stop, void *array_stop, int *NPROC) {

    int i, j, k, m, n_exps, n_times, n_obs;
    int counter = 0;
    int n_ics = 0, count_ic = 0, flag;
    double t1,t2,t3,t4;

    AMIGO_problem* amigo_problem = (AMIGO_problem*) data;
    amigo_problem->nevals++;
    amigo_problem->local_nfeval++;
    set_AMIGO_problem_pars(x, amigo_problem);
    n_exps = amigo_problem->n_models;
#ifdef MPI2
    MPI_Request *request_recep = (MPI_Request *) array_stop;
    int flag2, l;
#endif
    t3 = clock();
    for (i = 0; i < n_exps; ++i) {
#ifdef MPI2

        if (*stop < 1) {
            for (l = 0; l < *NPROC; l++) {
                MPI_Test(&request_recep[l], &flag2, MPI_STATUS_IGNORE);

                if (flag2 == 1) {
                    *stop = 1;
                    break;
                }
            }
        }
#endif
        
        if (*stop >= 1) {
            break;
        }
        
#ifdef MKL
        flag = get_AMIGO_model_sens(amigo_problem->amigo_models[i], amigo_problem->cvodes_gradient, amigo_problem->mkl_gradient);
#else
        flag = get_AMIGO_model_sens(amigo_problem->amigo_models[i], 1, 0);
#endif

        if (!flag) {

            handle_AMIGO_problem_stat_fails(i, amigo_problem);
            counter = 0;
            
            for (m = 0; m < *p; ++m) {

                for (i = 0; i < n_exps; ++i) {
                    n_obs = amigo_problem->amigo_models[i]->n_observables;

                    for (j = 0; j < n_obs; ++j) {
                        n_times = amigo_problem->amigo_models[i]->n_times;

                        for (k = 0; k < n_times; ++k) {
                            dr__[counter++] = 0;
                        }
                    }

                }
            }
            

            if (amigo_problem->verbose) {
#ifdef MATLAB
                (int) mexPrintf("Gradient failed\n");
#else
                printf("Gradient failed\n");
#endif

            }

            return (0);
        }
    }


    //t1 =clock();
    for (m = 0; m < *p; ++m) {
        n_ics = amigo_problem->n_pars;
        if (*stop >= 1) {
            break;
        }
        
        for (i = 0; i < n_exps; ++i) {

            n_obs = amigo_problem->amigo_models[i]->n_observables;

            if (m < amigo_problem->n_pars) {

                for (j = 0; j < n_obs; ++j) {

                    n_times = amigo_problem->amigo_models[i]->n_times;

                    for (k = 0; k < n_times; ++k) {

                        if (!isnan(amigo_problem->amigo_models[i]->exp_data[j][k]) &&
                                !isnan(amigo_problem->amigo_models[i]->Q[j][k]) &&
                                amigo_problem->amigo_models[i]->Q[j][k] != 0) {

                            dr__[counter++] =
                                    amigo_problem->amigo_models[i]->sens_obs[j][m][k]*
                                    (amigo_problem->amigo_models[i]->Q[j][k]);
                        } else {
                            dr__[counter++] = 0;

                        }
                    }

                }

            } else if (m >= amigo_problem->n_pars && m >= n_ics && m < n_ics + amigo_problem->amigo_models[i]->n_opt_ics) {

                for (j = 0; j < n_obs; ++j) {

                    n_times = amigo_problem->amigo_models[i]->n_times;

                    for (k = 0; k < n_times; ++k) {
                        if (!isnan(amigo_problem->amigo_models[i]->exp_data[j][k]) &&
                                !isnan(amigo_problem->amigo_models[i]->Q[j][k]) &&
                                amigo_problem->amigo_models[i]->Q[j][k] != 0) {

                            dr__[counter++] =
                                    (amigo_problem->amigo_models[i]->sens_obs[j][m - n_ics + amigo_problem->n_pars][k])*
                                    (amigo_problem->amigo_models[i]->Q[j][k]);
                        } else {
                            dr__[counter++] = 0;
                        }

                    }
                }
                count_ic++;

            } else {
                for (j = 0; j < n_obs; ++j) {

                    n_times = amigo_problem->amigo_models[i]->n_times;

                    for (k = 0; k < n_times; ++k) {
                        dr__[counter++] = 0;
                    }
                }
            }
            n_ics += amigo_problem->amigo_models[i]->n_opt_ics;
        }
    }
    // t2 = clock();

    //  printf("\t\t time 2loop %.100lf\n", (double) (t2 - t1) / (double) CLOCKS_PER_SEC);
    return 0;
}



int calcj_OLD(int *n, int* p, double *x, int *nf, double *dr__, int *lty, double *ty, void*data, int *stop, void *array_stop, int *NPROC) {

    int i, j, k, m, n_exps, n_times, n_obs;
    int counter, out;
    int n_ics = 0, count_ic = 0, flag;
    double t1,t2,t3,t4;

    AMIGO_problem* amigo_problem = (AMIGO_problem*) data;
    amigo_problem->nevals++;
    amigo_problem->local_nfeval++;
    set_AMIGO_problem_pars(x, amigo_problem);
    n_exps = amigo_problem->n_models;
    counter = 0;
    
#ifdef MPI2
    MPI_Request *request_recep = (MPI_Request *) array_stop;
    int flag2, l;
#endif
    t3 = clock();
    
    #ifdef MPI2

        if (*stop < 1) {
            for (l = 0; l < *NPROC; l++) {
                MPI_Test(&request_recep[l], &flag2, MPI_STATUS_IGNORE);

                if (flag2 == 1) {
                    *stop = 1;
                    break;
                }
            }
        }
#endif
        
        if (*stop >= 1) out=1; else out=0;
    
#pragma omp parallel  default(shared)
{
    
#pragma omp for schedule(dynamic,1) private(i,m,n_obs,n_times,k, flag, counter) 
    for (i = 0; i < n_exps; ++i) {
        
#ifdef MKL
        flag = get_AMIGO_model_sens(amigo_problem->amigo_models[i], amigo_problem->cvodes_gradient, amigo_problem->mkl_gradient);
#else
        
        flag = get_AMIGO_model_sens(amigo_problem->amigo_models[i], 1, 0);
#endif

        if (!flag) {
#pragma omp critical (handle)
            handle_AMIGO_problem_stat_fails(i, amigo_problem);
            counter = 0;
#pragma omp critical (looperror)            
            for (m = 0; m < *p; ++m) {

                for (i = 0; i < n_exps; ++i) {
                    n_obs = amigo_problem->amigo_models[i]->n_observables;

                    for (j = 0; j < n_obs; ++j) {
                        n_times = amigo_problem->amigo_models[i]->n_times;

                        for (k = 0; k < n_times; ++k) {
                            dr__[counter++] = 0;
                        }
                    }

                }
            }

            if (amigo_problem->verbose) {
#ifdef MATLAB
                (int) mexPrintf("Gradient failed\n");
#else
                printf("Gradient failed\n");
#endif

            }
        }
    }
}
     t4 =clock();
     printf("\t\t exp %d loop %.30lf\n", n_exps, (double) (t4 - t3) / (double) CLOCKS_PER_SEC);


    //t1 =clock();
    for (m = 0; m < *p; ++m) {
        n_ics = amigo_problem->n_pars;
        if (*stop >= 1) {
            break;
        }
        
        for (i = 0; i < n_exps; ++i) {

            n_obs = amigo_problem->amigo_models[i]->n_observables;

            if (m < amigo_problem->n_pars) {

                for (j = 0; j < n_obs; ++j) {

                    n_times = amigo_problem->amigo_models[i]->n_times;

                    for (k = 0; k < n_times; ++k) {

                        if (!isnan(amigo_problem->amigo_models[i]->exp_data[j][k]) &&
                                !isnan(amigo_problem->amigo_models[i]->Q[j][k]) &&
                                amigo_problem->amigo_models[i]->Q[j][k] != 0) {

                            dr__[counter++] =
                                    amigo_problem->amigo_models[i]->sens_obs[j][m][k]*
                                    (amigo_problem->amigo_models[i]->Q[j][k]);
                        } else {
                            dr__[counter++] = 0;

                        }
                    }

                }

            } else if (m >= amigo_problem->n_pars && m >= n_ics && m < n_ics + amigo_problem->amigo_models[i]->n_opt_ics) {

                for (j = 0; j < n_obs; ++j) {

                    n_times = amigo_problem->amigo_models[i]->n_times;

                    for (k = 0; k < n_times; ++k) {
                        if (!isnan(amigo_problem->amigo_models[i]->exp_data[j][k]) &&
                                !isnan(amigo_problem->amigo_models[i]->Q[j][k]) &&
                                amigo_problem->amigo_models[i]->Q[j][k] != 0) {

                            dr__[counter++] =
                                    (amigo_problem->amigo_models[i]->sens_obs[j][m - n_ics + amigo_problem->n_pars][k])*
                                    (amigo_problem->amigo_models[i]->Q[j][k]);
                        } else {
                            dr__[counter++] = 0;
                        }

                    }
                }
                count_ic++;

            } else {
                for (j = 0; j < n_obs; ++j) {

                    n_times = amigo_problem->amigo_models[i]->n_times;

                    for (k = 0; k < n_times; ++k) {
                        dr__[counter++] = 0;
                    }
                }
            }
            n_ics += amigo_problem->amigo_models[i]->n_opt_ics;
        }
    }
    // t2 = clock();

    //  printf("\t\t time 2loop %.100lf\n", (double) (t2 - t1) / (double) CLOCKS_PER_SEC);
    return 0;
}

/*
double getStatus(int stat)
{
switch((int)stat)
{     
case 3:         //stopped by xtol
case 4:         //stopped by ftol
case 5:         //stopped by ftol and xtol
case 6:         //stopped by abs ftol
return 1;
break;
case 9:         //feval max
case 10:        //itmax
return 0;
break;
case 8:         //tol too small
return -1;
case 13:        //bad initial f(x)
case 14:        //bad parameters
case 15:        //bad g(x)
case 16:        //n or p out of range
case 17:        //restart attempted (?)
case 18:        //iv out of range
case 19:        //v out of range
case 50:        //iv[0] out of range
case 87:        //v problem
return -2;
case 11:
return -5;  //user exit
default:
return -3;        
}
}*/
